#include "pb_stepcomputerastermetrics.h"

#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/abstract/ct_abstractimage2d.h"
#include "ct_itemdrawable/abstract/ct_abstractareashape2d.h"
#include "ct_itemdrawable/ct_attributeslist.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "ct_abstractstepplugin.h"

#include "ctlibmetrics/ct_metric/abstract/ct_abstractmetric_raster.h"

#include "ct_view/elements/ctg_configurableelementsselector.h"

#include <QDebug>

// Alias for indexing models
#define DEFin_res "res"
#define DEFin_grp "grp"
#define DEFin_raster "raster"
#define DEFin_areaShape "shapeArea"


// Constructor : initialization of parameters
PB_StepComputeRasterMetrics::PB_StepComputeRasterMetrics(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

PB_StepComputeRasterMetrics::~PB_StepComputeRasterMetrics()
{
    qDeleteAll(_selectedMetrics);
    _selectedMetrics.clear();
}

void PB_StepComputeRasterMetrics::init()
{
    CT_AbstractStep::init();

    _availableMetrics.clear();

    PluginManagerInterface *pm = PS_CONTEXT->pluginManager();

    int s = pm->countPluginLoaded();

    for(int i=0; i<s; ++i)
    {
        CT_AbstractStepPlugin *p = pm->getPlugin(i);
        QList<CT_AbstractMetric*> metrics = p->getMetricsAvailable();

        QListIterator<CT_AbstractMetric*> it(metrics);
        while (it.hasNext())
        {
            CT_AbstractMetric* metric = it.next();

            CT_AbstractMetric_Raster* pointMetric = dynamic_cast<CT_AbstractMetric_Raster*>(metric);

            if (pointMetric != NULL)
                _availableMetrics.append(pointMetric);
        }
    }
}

// Step description (tooltip of contextual menu)
QString PB_StepComputeRasterMetrics::getStepDescription() const
{
    return tr("2- Métriques de rasters");
}

// Step detailled description
QString PB_StepComputeRasterMetrics::getStepDetailledDescription() const
{
    QString ret;

    QListIterator<CT_AbstractConfigurableElement*> it(_availableMetrics);

    while(it.hasNext()) {
        CT_AbstractConfigurableElement *ce = it.next();

        ret += tr("<b>%1</b><br/><br/><i>%2</i>").arg(ce->getShortDisplayableName()).arg(ce->getShortDescription());

        if(it.hasNext())
            ret += "<br/><br/>";
    }

    if(!ret.isEmpty())
        return ret;

    return tr("No detailled description for this step");
}

// Step URL
QString PB_StepComputeRasterMetrics::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

void PB_StepComputeRasterMetrics::savePostSettings(SettingsWriterInterface &writer)
{
    QListIterator<CT_AbstractConfigurableElement *> it(_selectedMetrics);

    while(it.hasNext()) {
        CT_AbstractConfigurableElement* metric = it.next();
        writer.addParameter(this, "Metric", metric->getUniqueName());
        metric->saveSettings(writer);
    }

    SuperClass::savePostSettings(writer);
}

bool PB_StepComputeRasterMetrics::restorePostSettings(SettingsReaderInterface &reader)
{
    qDeleteAll(_selectedMetrics);
    _selectedMetrics.clear();

    QVariant value;

    const int nMetrics = reader.parameterCount(this, "Metric");

    for(int i=0; i<nMetrics; ++i) {

        if(reader.parameter(this, "Metric", value)) {
            const QString metricUniqueName = value.toString();

            CT_AbstractConfigurableElement *metricFound = findAvailableMetricByUniqueName(metricUniqueName);

            if(metricFound == NULL)
                return false;

            metricFound = metricFound->copy();

            if(metricFound->restoreSettings(reader))
                _selectedMetrics.append(metricFound);
            else {
                delete metricFound;
                return false;
            }
        }
    }

    return SuperClass::restorePostSettings(reader);
}

bool PB_StepComputeRasterMetrics::setAllSettings(const SettingsNodeGroup *settings)
{
    qDeleteAll(_selectedMetrics);
    _selectedMetrics.clear();

    bool ok = CT_AbstractStep::setAllSettings(settings);

    if(ok) {
        SettingsNodeGroup* group = settings->firstGroupByTagName("PB_StepComputeRasterMetrics");

        if(group == NULL)
            return false;

        QList<SettingsNodeGroup*> groups = group->groupsByTagName("Metric");

        QListIterator<SettingsNodeGroup*> it(groups);

        while(it.hasNext()) {
            SettingsNodeGroup *groupM = it.next();

            SettingsNodeValue* value = groupM->firstValueByTagName("className");

            if(value == NULL)
                return false;

            QString metricUniqueName = value->value().toString();

            CT_AbstractConfigurableElement *metricFound = NULL;

            QListIterator<CT_AbstractConfigurableElement*> itM(_availableMetrics);

            while(itM.hasNext() && (metricFound == NULL)) {
                CT_AbstractConfigurableElement *metric = itM.next();

                if(metric->getUniqueName() == metricUniqueName)
                    metricFound = metric;
            }

            if(metricFound == NULL)
                return false;

            CT_AbstractConfigurableElement *metricCpy = metricFound->copy();

            SettingsNodeGroup *groupS = NULL;

            if(!groupM->groups().isEmpty())
                groupS = groupM->groups().first();

            if(!metricCpy->setAllSettings(groupS)) {
                delete metricCpy;
                return false;
            }

            _selectedMetrics.append(metricCpy);
        }
    }

    return ok;
}

SettingsNodeGroup *PB_StepComputeRasterMetrics::getAllSettings() const
{
    SettingsNodeGroup *root = CT_AbstractStep::getAllSettings();

    SettingsNodeGroup *group = new SettingsNodeGroup("PB_StepComputeRasterMetrics");

    QListIterator<CT_AbstractConfigurableElement *> it(_selectedMetrics);

    while(it.hasNext()) {
        SettingsNodeGroup *groupM = new SettingsNodeGroup("Metric");

        CT_AbstractConfigurableElement *metric = it.next();

        groupM->addValue(new SettingsNodeValue("className", metric->getUniqueName()));

        SettingsNodeGroup *childM = metric->getAllSettings();

        if(childM != NULL)
            groupM->addGroup(childM);

        group->addGroup(groupM);
    }

    root->addGroup(group);

    return root;
}


// Step copy method
CT_VirtualAbstractStep* PB_StepComputeRasterMetrics::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new PB_StepComputeRasterMetrics(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void PB_StepComputeRasterMetrics::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_res = createNewInResultModelForCopy(DEFin_res, tr("Points"));
    resIn_res->setZeroOrMoreRootGroup();
    resIn_res->addGroupModel("", DEFin_grp, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    resIn_res->addItemModel(DEFin_grp, DEFin_raster, CT_AbstractImage2D::staticGetType(), tr("Raster"));
    resIn_res->addItemModel(DEFin_grp, DEFin_areaShape, CT_AbstractAreaShape2D::staticGetType(), tr("Emprise de la placette"), "", CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);
}

bool PB_StepComputeRasterMetrics::postConfigure()
{    
    CTG_ConfigurableElementsSelector cd(NULL, !getStepChildList().isEmpty());
    cd.setWindowTitle("Métriques séléctionnées");
    cd.setElementsAvailable(_availableMetrics);
    cd.setElementsSelected(&_selectedMetrics);

    if(cd.exec() == QDialog::Accepted)
    {
        setSettingsModified(true);
        return true;
    }

    return false;
}


// Creation and affiliation of OUT models
void PB_StepComputeRasterMetrics::createOutResultModelListProtected()
{       
    CT_OutResultModelGroupToCopyPossibilities *resCpy_res = createNewOutResultModelToCopy(DEFin_res);

    if(resCpy_res != NULL) {
        resCpy_res->addItemModel(DEFin_grp, _outMetrics_ModelName, new CT_AttributesList(), "Metrics");

        QListIterator<CT_AbstractConfigurableElement *> it(_selectedMetrics);
        while (it.hasNext())
        {
            CT_AbstractMetric_Raster* metric = (CT_AbstractMetric_Raster*) it.next();
            metric->postConfigure();
            metric->initAttributesModels(resCpy_res, _outMetrics_ModelName);
        }
    }
}

void PB_StepComputeRasterMetrics::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* outRes = outResultList.at(0);

    // COPIED results browsing
    CT_ResultGroupIterator itCpy_grp(outRes, this, DEFin_grp);
    while (itCpy_grp.hasNext() && !isStopped())
    {
        CT_StandardItemGroup* grp = (CT_StandardItemGroup*) itCpy_grp.next();
        
        const CT_AbstractImage2D* raster = (CT_AbstractImage2D*)grp->firstItemByINModelName(this, DEFin_raster);
        const CT_AbstractAreaShape2D* plotArea = (CT_AbstractAreaShape2D*)grp->firstItemByINModelName(this, DEFin_areaShape);

        if (raster != NULL)
        {
            CT_AttributesList* outAttributes = new CT_AttributesList(_outMetrics_ModelName.completeName(), outRes);
            grp->addItemDrawable(outAttributes);

            QListIterator<CT_AbstractConfigurableElement *> it(_selectedMetrics);
            while (it.hasNext())
            {
                CT_AbstractMetric_Raster* metric = (CT_AbstractMetric_Raster*) it.next();

                if (metric != NULL)
                {
                    const CT_AreaShape2DData* areaData = NULL;
                    if (plotArea != NULL) {areaData = &plotArea->getAreaData();}

                    if (metric->initDatas(raster, areaData))
                    {
                        metric->computeMetric(outAttributes);
                    }
                }
            }
        }
    }
}

CT_AbstractConfigurableElement *PB_StepComputeRasterMetrics::findAvailableMetricByUniqueName(const QString &uniqueName) const
{
    CT_AbstractConfigurableElement* metricFound = NULL;

    QListIterator<CT_AbstractConfigurableElement*> itM(_availableMetrics);

    while(itM.hasNext() && (metricFound == NULL)) {
        CT_AbstractConfigurableElement *metric = itM.next();

        if(metric->getUniqueName() == uniqueName)
            metricFound = metric;
    }

    return metricFound;
}
