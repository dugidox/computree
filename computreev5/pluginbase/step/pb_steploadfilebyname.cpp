#include "pb_steploadfilebyname.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_reader/ct_standardreaderseparator.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_model/tools/ct_modelsearchhelper.h"
#include "ct_abstractstepplugin.h"


#include "ct_global/ct_context.h"
#include "ct_itemdrawable/ct_readeritem.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_view/tools/ct_configurablewidgettodialog.h"

#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QFileInfo>

// Alias for indexing models
#define DEF_inResult "inResult"
#define DEF_inGroup  "inGroup"
#define DEF_inItem   "inItem"
#define DEF_inAttName   "inAtt"


// Constructor : initialization of parameters
PB_StepLoadFileByName::PB_StepLoadFileByName(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _readersListValue = "";
    initListOfAvailableReaders();
}

PB_StepLoadFileByName::~PB_StepLoadFileByName()
{
    clear();
}

// Step description (tooltip of contextual menu)
QString PB_StepLoadFileByName::getStepDescription() const
{
    return tr("3- Create file reader from name");
}

// Step detailled description
QString PB_StepLoadFileByName::getStepDetailledDescription() const
{
    return tr("Create a file reader, using a base name obtained from checked field.\n You have to select a sample file, to allow reader configuration. ");
}

// Step URL
QString PB_StepLoadFileByName::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* PB_StepLoadFileByName::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new PB_StepLoadFileByName(dataInit);
}


//////////////////// PROTECTED METHODS //////////////////

void PB_StepLoadFileByName::createPreConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPreConfigurationDialog();

    QStringList list_readersList;

    QListIterator<CT_AbstractReader*> it(_readersInstancesList);

    while (it.hasNext())
        list_readersList.append(it.next()->GetReaderClassName());

    if (list_readersList.isEmpty())
        list_readersList.append(tr("ERREUR : aucun reader disponible"));

    configDialog->addStringChoice(tr("Choose file type"), "", list_readersList, _readersListValue);
}

//void PB_StepLoadFileByName::createPostConfigurationDialog()
//{
//    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

//    configDialog->addFileChoice(tr("Source directory"), CT_FileChoiceButton::OneExistingFolder, "", _directory);
//}

bool PB_StepLoadFileByName::postConfigure()
{
    QString fileFilter = getFormat(_readersListValue);

    QStringList fileList;

    CT_GenericConfigurableWidget configDialog;
    configDialog.addFileChoice(tr("Choisir un fichier exemple"), CT_FileChoiceButton::OneOrMoreExistingFiles, fileFilter, fileList);
    configDialog.addTitle(tr("Le fichier choisi doit :"));
    configDialog.addText("", tr("- Etre dans le répertoire des fichiers à charger"), "");
    configDialog.addText("", tr("- Avoir le même format que les fichiers à charger"), "");
    configDialog.addText("", tr("- Avoir la même structure / version que les fichiers à charger"), "");


    if(CT_ConfigurableWidgetToDialog::exec(&configDialog) == QDialog::Accepted) {

        if(fileList.isEmpty())
            return false;

        CT_AbstractReader *reader = getReader(_readersListValue);

        if((reader != NULL) && reader->setFilePath(fileList.first())) {
            reader->setFilePathCanBeModified(false);
            bool ok = reader->configure();
            reader->setFilePathCanBeModified(true);

            if(ok) {
                _directory.clear();
                _directory.append(QFileInfo(fileList.first()).path());
                setSettingsModified(true);
            }

            return ok;
        }
    }

    return false;
}

void PB_StepLoadFileByName::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_inResult, tr("Résultat"));
    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_inGroup, CT_AbstractItemGroup::staticGetType(), tr("Groupe"), "", CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    resultModel->addItemModel(DEF_inGroup, DEF_inItem, CT_AbstractSingularItemDrawable::staticGetType(), tr("Item"));
    resultModel->addItemAttributeModel(DEF_inItem, DEF_inAttName, QList<QString>() << CT_AbstractCategory::DATA_FILE_NAME << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::ANY, tr("Nom"));
}

// Creation and affiliation of OUT models
void PB_StepLoadFileByName::createOutResultModelListProtected()
{
    // get the reader selected
    CT_AbstractReader *reader = getReader(_readersListValue);

    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_inResult);

    if (resultModel != NULL)
        if (reader != NULL && _directory.size() > 0)
        {
            // get the header
            CT_FileHeader *rHeader = reader->createHeaderPrototype();

            if(rHeader != NULL)
            {
                // copy the reader (copyFull = with configuration and models)
                CT_AbstractReader* readerCpy = reader->copyFull();

                resultModel->addItemModel(DEF_inGroup, _outReaderModelName, new CT_ReaderItem(NULL, NULL, readerCpy), tr("Reader"));
                resultModel->addItemModel(DEF_inGroup, _outHeaderModelName, rHeader, tr("Header"));
            }
        }
}


void PB_StepLoadFileByName::compute()
{
    CT_AbstractReader *reader = getReader(_readersListValue);
    if(reader == NULL) {return;}

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resultOut = outResultList.at(0);

    CT_ResultGroupIterator it(resultOut, this, DEF_inGroup);
    while(!isStopped() && it.hasNext())
    {
        CT_StandardItemGroup *group = (CT_StandardItemGroup*) it.next();

        if (group != NULL)
        {
            CT_AbstractSingularItemDrawable* item = (CT_AbstractSingularItemDrawable*) group->firstItemByINModelName(this, DEF_inItem);
            if (item != NULL)
            {
                CT_AbstractItemAttribute* att = item->firstItemAttributeByINModelName(resultOut, this, DEF_inAttName);
                if (att !=  NULL)
                {
                    CT_AbstractReader* readerCpy = reader->copyFull();
                    const QList<FileFormat> &formats = readerCpy->readableFormats();

                    QString filename = QFileInfo(att->toString(item, NULL)).baseName();

                    QString filepath = QString("%1/%2").arg(_directory.first()).arg(filename);
                    if (formats.size() > 0)
                    {
                        filepath = QString("%1.%2").arg(filepath).arg(formats.first().suffixes().first());
                    }

                    PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Chargement du fichier %1").arg(filepath));

                    // set the new filepath and check if it is valid
                    if (readerCpy->setFilePath(filepath))
                    {
                        // create models of this reader
                        if(readerCpy->outItemDrawableModels().isEmpty() && reader->outGroupsModel().isEmpty())
                        {
                            readerCpy->createOutItemDrawableModelList();
                        }

                        CT_OutAbstractItemModel* headerModel = (CT_OutAbstractItemModel*)PS_MODELS->searchModelForCreation(_outHeaderModelName.completeName(), resultOut);

                        CT_FileHeader *header = readerCpy->readHeader();

                        header->changeResult(resultOut);
                        header->setModel(headerModel);

                        // add the header
                        group->addItemDrawable(header);

                        // add the reader
                        group->addItemDrawable(new CT_ReaderItem(_outReaderModelName.completeName(), resultOut, readerCpy));
                    } else
                    {
                        PS_LOG->addMessage(LogInterface::warning, LogInterface::step, tr("Fichier %1 inexistant ou non valide").arg(filepath));
                        delete readerCpy;
                    }
                }
            }
        }
    }
}

SettingsNodeGroup *PB_StepLoadFileByName::getAllSettings() const
{
    SettingsNodeGroup *root = CT_AbstractStep::getAllSettings();
    SettingsNodeGroup *group = new SettingsNodeGroup("PB_StepLoadFileByName");
    group->addValue(new SettingsNodeValue("Version", "1"));

    SettingsNodeGroup *settings = new SettingsNodeGroup("Settings");
    settings->addValue(new SettingsNodeValue("ReaderSelected", _readersListValue));

    QString fileFolder = "";
    if (_directory.size() > 0)
    {
        fileFolder = _directory.first();
    }
    settings->addValue(new SettingsNodeValue("path", fileFolder));


    SettingsNodeGroup *readerSettings = new SettingsNodeGroup("ReaderSettings");
    settings->addGroup(readerSettings);

    CT_AbstractReader *reader = getReader(_readersListValue);

    if(reader != NULL)
        readerSettings->addGroup(reader->getAllSettings());

    group->addGroup(settings);
    root->addGroup(group);

    return root;
}

bool PB_StepLoadFileByName::setAllSettings(const SettingsNodeGroup *settings)
{
    bool ok = CT_AbstractStep::setAllSettings(settings);

    if(ok)
    {
        QList<SettingsNodeGroup*> groups = settings->groupsByTagName("PB_StepLoadFileByName");
        if(groups.isEmpty()) {return false;}

        groups = groups.first()->groupsByTagName("Settings");
        if(groups.isEmpty()) {return false;}

        SettingsNodeGroup *settings2 = groups.first();

        QList<SettingsNodeValue*> values = settings2->valuesByTagName("ReaderSelected");
        if(values.isEmpty()) {return false;}

        _readersListValue = values.first()->value().toString();

        _directory.clear();

        values = settings2->valuesByTagName("path");
        if(values.isEmpty()) {return false;}

        _directory.append(values.first()->value().toString());


        groups = settings2->groupsByTagName("ReaderSettings");
        if(groups.isEmpty()) {return false;}

        groups = groups.first()->groups();

        CT_AbstractReader *reader = getReader(_readersListValue);

        if(reader != NULL) {
            if(groups.isEmpty()) {return false;}

            ok = reader->setAllSettings(groups.first());
        }

        //        if(!groups.isEmpty())
        //            return false;

        return true;
    }

    return ok;
}

void PB_StepLoadFileByName::initListOfAvailableReaders()
{
    clear();

    // get the plugin manager
    PluginManagerInterface *pm = PS_CONTEXT->pluginManager();
    int s = pm->countPluginLoaded();

    // for each plugin
    for(int i = 0; i < s; ++i)
    {
        CT_AbstractStepPlugin *p = pm->getPlugin(i);

        // get readers
        QList<CT_StandardReaderSeparator*> rsl = p->getReadersAvailable();
        QListIterator<CT_StandardReaderSeparator*> itR(rsl);

        while(itR.hasNext())
        {
            CT_StandardReaderSeparator *rs = itR.next();
            QListIterator<CT_AbstractReader*> itE(rs->readers());

            while(itE.hasNext())
            {
                CT_AbstractReader *reader = itE.next();

                // copy the reader
                CT_AbstractReader *readerCpy = reader->copy();
                readerCpy->init(false);

                // and add it to the list
                _readersInstancesList.append(readerCpy);
            }
        }
    }
}

void PB_StepLoadFileByName::clear()
{
    qDeleteAll(_readersInstancesList.begin(), _readersInstancesList.end());
    _readersInstancesList.clear();
}

QString PB_StepLoadFileByName::getFormat(QString readerClassName) const
{
    CT_AbstractReader *reader = getReader(readerClassName);

    if(reader != NULL) {
        FileFormat fileFormat = reader->readableFormats().first();

        QString formatText = fileFormat.description();
        formatText.append(" (");
        const QList<QString> &suffixes = fileFormat.suffixes();
        for (int i = 0 ; i < suffixes.size() ; i++)
        {
            if (i > 0) {formatText.append(" ");}
            formatText.append("*.");
            formatText.append(suffixes.at(i));
        }
        formatText.append(")");

        return formatText;
    }

    return tr("Aucun reader disponible (*.error)");
}

CT_AbstractReader *PB_StepLoadFileByName::getReader(QString readerClassName) const
{
    QListIterator<CT_AbstractReader*> it(_readersInstancesList);

    while(it.hasNext()) {
        CT_AbstractReader *reader = it.next();

        if(reader->GetReaderClassName() == readerClassName) {
            return reader;
        }
    }

    return NULL;
}

