#include "ct_inresultmodelnotneedinputresult.h"

#include "interfaces.h"

CT_InResultModelNotNeedInputResult::CT_InResultModelNotNeedInputResult() : CT_InAbstractResultModel("CT_InResultModelNotNeedInputResult",
                                                                                                    tr("Not input result"),
                                                                                                    "No input result",
                                                                                                    false)
{
    setMinimumNumberOfPossibilityThatMustBeSelectedForOneTurn(0);
}

QString CT_InResultModelNotNeedInputResult::modelTypeDisplayable() const
{
    return QString("CT_InResultModelNotNeedInputResult");
}

void CT_InResultModelNotNeedInputResult::saveSettings(SettingsWriterInterface &writer) const
{
    SuperClass::saveSettings(writer);

    writer.addParameter(this, "Type", "CT_InResultModelNotNeedInputResult");
}

bool CT_InResultModelNotNeedInputResult::restoreSettings(SettingsReaderInterface &reader)
{
    QVariant value;

    if(!reader.parameter(this, "Type", value) || (value.toString() != "CT_InResultModelNotNeedInputResult"))
        return false;

    return SuperClass::restoreSettings(reader);
}

CT_InAbstractModel* CT_InResultModelNotNeedInputResult::copy(bool withPossibilities) const
{
    Q_UNUSED(withPossibilities)

    CT_InResultModelNotNeedInputResult *cpy = new CT_InResultModelNotNeedInputResult();
    cpy->setStep(step());
    cpy->setOriginalModel(this);

    return cpy;
}

QList<SettingsNodeGroup*> CT_InResultModelNotNeedInputResult::getAllValues() const
{
    QList<SettingsNodeGroup*> retList = CT_InAbstractResultModel::getAllValues();

    SettingsNodeGroup *root = new SettingsNodeGroup("CT_InResultModelNotNeedInputResult_Values");
    root->addValue(new SettingsNodeValue("Version", 1));
    root->addValue(new SettingsNodeValue("Type", "CT_InResultModelNotNeedInputResult"));
    retList.append(root);

    return retList;
}

bool CT_InResultModelNotNeedInputResult::setAllValues(const QList<SettingsNodeGroup*> &list)
{
    SettingsNodeGroup *root = NULL;

    QListIterator<SettingsNodeGroup*> itS(list);

    while(itS.hasNext()
          && (root == NULL))
    {
        SettingsNodeGroup *gg = itS.next();

        if(gg->name() == "CT_InResultModelNotNeedInputResult_Values")
            root = gg;
    }

    if(root == NULL)
        return false;

    return CT_InAbstractResultModel::setAllValues(list);
}
