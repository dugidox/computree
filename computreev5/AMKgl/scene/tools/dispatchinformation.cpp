#include "dispatchinformation.h"

#include <limits>

const DispatchInformation::BEGIN_TYPE DispatchInformation::INVALID_BEGIN = std::numeric_limits<DispatchInformation::BEGIN_TYPE>::max();
