/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "ifp_stepComputeMeshVolume.h"

#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include <ct_itemdrawable/ct_fileheader.h>
#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_itemattributelist.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#define DEF_SearchInResult "rin"
#define DEF_SearchInGroup   "gin"
#define DEF_SearchInMesh   "scin"

// Alias for indexing out models
#define DEF_resultOut_resOut "resOut"
#define DEF_groupOut_grpOut "grpOut"
#define DEF_itemOut_demSurf "demSurf"

// On recupere nuage de points et nuages d'index
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"

#include "ct_iterator/ct_pointiterator.h"
#include "ct_accessor/ct_pointaccessor.h"
#include "ct_itemdrawable/ct_meshmodel.h"

#include "itemdrawable/itemvolume.h"
#include "utils/volumedrawerhelper.h"

#include <limits>
#include <algorithm>
#include <ctime>
#include<set>

#include <pcl/point_types.h>
#include <pcl/common/common.h>

#include <QDir>
#include <QLocale>
#include <QString>
#include <QTextStream>

// Constructor : initialization of parameters
IFP_stepComputeMeshVolume::IFP_stepComputeMeshVolume(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _res = 1.0;
    _divideMesh=false;
}

// Step description (tooltip of contextual menu)
QString IFP_stepComputeMeshVolume::getStepDescription() const
{
    return tr("Compute the volume of a closed mesh");
}

QString IFP_stepComputeMeshVolume::getStepDetailledDescription() const
{
    return tr("This steps compute the volume of a closed triangulated mesh. To do so, it considers each tetrahedron formed by the triangles of the mesh and the centroid of the mesh, then it computes their volume, and finally it sums those signed volumes."
              "The volume can also be computed by slice. In this mode, the mesh is divided along the Oz axis in closed sub-meshes. The volume is then computed for each sub-meshes");
}

QStringList IFP_stepComputeMeshVolume::getStepRISCitations() const
{
    return QStringList() << tr("TY  - JOUR\n"
                               "TI  - Efficient feature extraction for 2D/3D objects in mesh representation\n"
                               "T2  - Proceedings of International Conference on Image Processing\n"
                               "A1  - Zhang, Cha\n"
                               "A2  - Chen, Tsuhan\n"
                               "PY  - 2001\n");
}

// Step copy method
CT_VirtualAbstractStep* IFP_stepComputeMeshVolume::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new IFP_stepComputeMeshVolume(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

void IFP_stepComputeMeshVolume::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("Scène"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInMesh, CT_MeshModel::staticGetType(), tr("Mesh"));

    CT_InResultModelGroup* resModelName = createNewInResultModel(DEF_IN_RESULT2, tr("Input for name"), "", true);
    resModelName->setZeroOrMoreRootGroup();
    resModelName->addGroupModel("",
                                DEF_IN_GRP_CLUSTER3,
                                CT_AbstractItemGroup::staticGetType(),
                                tr("Name Group"),
                                "",
                                CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    resModelName->addItemModel(DEF_IN_GRP_CLUSTER3, DEF_IN_NAME, CT_FileHeader::staticGetType(), tr("Name"));
}

// Création et affiliation des modèles OUT
void IFP_stepComputeMeshVolume::createOutResultModelListProtected(){

    CT_OutResultModelGroupToCopyPossibilities *resultModelMesh = createNewOutResultModelToCopy(DEF_SearchInResult);
    if (resultModelMesh != NULL)
    {
        resultModelMesh->addItemModel(DEF_SearchInGroup, _outVolumeItem_ModelName, new CT_ItemAttributeList(), tr("Volume"));
        resultModelMesh->addItemAttributeModel(_outVolumeItem_ModelName, _outVolumeAttribute_ModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_NUMBER), tr("Volume"));
    }

    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resOut, tr("Surfaces extraites"));

    resultModel->setRootGroup(DEF_groupOut_grpOut);
    resultModel->addItemModel(DEF_groupOut_grpOut, DEF_itemOut_demSurf, new itemVolume(), tr("Volume helper"));
}

// Semi-automatic creation of step parameters DialogBox
void IFP_stepComputeMeshVolume::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
    configDialog->addBool(tr("Divide the mesh along the Oz axis? (compute the volume per division)"), "", "", _divideMesh);
    configDialog->addDouble("Size of the divisions", "m", 0.001, 100, 3, _res, 0);    
    configDialog->addFileChoice(tr("Select an export folder"), CT_FileChoiceButton::OneExistingFolder, "", m_filePath);
}

void IFP_stepComputeMeshVolume::compute()
{
    if (m_filePath.empty() || m_filePath.first() == "") {
        QString errorMsg = "IFP_stepComputeMeshVolume please give a path to export the result. QSM export skipped.";
        PS_LOG->addMessage(LogInterface::info, LogInterface::step, errorMsg);
        return;
      }
      QString path = m_filePath.first();
      QLocale::setDefault(QLocale(QLocale::English, QLocale::UnitedStates));
      QDir dir(path);
      if (!dir.exists()) {
          dir.mkpath(".");
      }

      CT_ResultGroup* outResultName = getInputResults().at(1);
      CT_ResultGroupIterator outResItName(outResultName, this, DEF_IN_GRP_CLUSTER3);
      std::vector<QString> names;
      while (!isStopped() && outResItName.hasNext()) {
        CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResItName.next();
        CT_FileHeader* header = (CT_FileHeader*)group->firstItemByINModelName(this, DEF_IN_NAME);
        QString str = header->getFileName();
        QString cropedFilename = str.split(".", QString::SkipEmptyParts).at(0);
        names.push_back(cropedFilename);
      }
      std::uint32_t indexName = 0u;




    CT_ResultGroup *outResult = getOutResultList().first();

    CT_ResultGroupIterator itr(outResult, this, DEF_SearchInGroup);
    while (itr.hasNext())
    {
        CT_StandardItemGroup* group = (CT_StandardItemGroup*) itr.next();

        double volumeTot = 0;

        CT_MeshModel *model = (CT_MeshModel*)group->firstItemByINModelName(this, DEF_SearchInMesh);
        if (model != NULL)
        {
            CT_Mesh *mesh = model->mesh();

            pcl::PointCloud<pcl::PointXYZ>  ptsCloud;
            std::vector<std::vector<int>> tris;
            volumeDrawerHelper* vdh = NULL;

            if(mesh != NULL)
            {
                CT_PointIterator itP(mesh->abstractVert());
                CT_FaceIterator itF(mesh->abstractFace());
                QHash<size_t, size_t> hashTablePoint;
                size_t i = 0;

                std::cout<<"Mesh: "<<itP.size()<<" points, "<<itF.size()<<" faces"<<std::endl;

                while(itP.hasNext())
                {
                    const CT_Point &point = itP.next().currentPoint();
                    hashTablePoint.insert(itP.cIndex(), i);

                    pcl::PointXYZ pt;
                    pt.x = (double)point(0);
                    pt.y = (double)point(1);
                    pt.z = (double)point(2);
                    ptsCloud.push_back(pt);

                    ++i;
                }

                int j=0;

                while(itF.hasNext())
                {
                    const CT_Face &face = itF.next().cT();

                    std::vector<int> tri;
                    tri.push_back(hashTablePoint.value(face.iPointAt(0)));
                    tri.push_back(hashTablePoint.value(face.iPointAt(1)));
                    tri.push_back(hashTablePoint.value(face.iPointAt(2)));

                    tris.push_back(tri);

                    j++;
                }

                if(!_divideMesh){
                    volumeTot = getVolume(tris,ptsCloud);
                    PS_LOG->addMessage(LogInterface::info, LogInterface::action, QString("Volume: ")+QString::number(volumeTot)+QString(" m^3"));
                } else {

                    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_pts_ptr (new pcl::PointCloud<pcl::PointXYZ>(ptsCloud));
                    pcl::PointXYZ BBmin,BBmax;
                    pcl::getMinMax3D (*cloud_pts_ptr, BBmin, BBmax);

                    int numberDiv = (int)((BBmax.z-BBmin.z)/_res);

                    std::vector<std::vector<int>> trisBelow,trisAbove,trisIntersect;
                    std::vector<std::vector<int>> lineTop;
                    std::vector<std::vector<std::vector<int>>> connexLines;
                    std::vector<std::vector<std::vector<int>>> connexLinesForPlot;
                    std::vector<std::vector<std::vector<int>>> connexCompsForPlot;

                    QString sliceInfo = QString("Slice Id | Slice top z (m) | Volume (m^3)");
                    PS_LOG->addMessage(LogInterface::info, LogInterface::action,sliceInfo);
                    PS_LOG->addMessage(LogInterface::info, LogInterface::action,QString("---------------------------------------------------------"));

                    for(int k=0;k<numberDiv;k++){

                        double z0 = (k+1)*_res+BBmin.z;
                        std::cout<<"Division at : "<<z0<<"  -----------------------------------"<<std::endl;

                        for(int i=0;i<tris.size();i++)
                        {
                            std::vector<int> tri = tris.at(i);

                            pcl::PointXYZ p0 = ptsCloud.at(tri.at(0));
                            pcl::PointXYZ p1 = ptsCloud.at(tri.at(1));
                            pcl::PointXYZ p2 = ptsCloud.at(tri.at(2));

                            if(isFaceIntersectedByPlane(p0,p1,p2,z0)){
                                trisIntersect.push_back(tri);
                                fillLineTop(ptsCloud,tri,z0,lineTop);
                            }
                            if(isFaceBelowPlane(p0,p1,p2,z0)){
                                trisBelow.push_back(tri);
                            }
                            if(isFaceAbovePlane(p0,p1,p2,z0)){
                                trisAbove.push_back(tri);
                            }
                        }

                        connexLines = findConnexLines(lineTop);

                        for(int l=0;l<connexLines.size();l++)
                        {
                            std::vector<std::vector<int>> connexLine = connexLines.at(l);

                            pcl::PointXYZ centerLine;
                            centerLine.x=0;
                            centerLine.y=0;
                            centerLine.z=0;
                            for(int j=0;j<connexLine.size();j++)
                            {
                                pcl::PointXYZ p0 = ptsCloud.at(connexLine.at(j).at(0));
                                pcl::PointXYZ p1 = ptsCloud.at(connexLine.at(j).at(1));

                                centerLine.x+=(p0.x+p1.x)/(2*connexLine.size());
                                centerLine.y+=(p0.y+p1.y)/(2*connexLine.size());
                                centerLine.z+=(p0.z+p1.z)/(2*connexLine.size());

                                std::vector<int> triCW;
                                triCW.push_back(connexLine.at(j).at(1));
                                triCW.push_back(connexLine.at(j).at(0));
                                triCW.push_back(ptsCloud.size());

                                std::vector<int> triCCW;
                                triCCW.push_back(connexLine.at(j).at(0));
                                triCCW.push_back(connexLine.at(j).at(1));
                                triCCW.push_back(ptsCloud.size());

                                trisAbove.push_back(triCCW);
                                trisBelow.push_back(triCW);
                            }
                            ptsCloud.push_back(centerLine);
                        }

                        trisBelow.insert( trisBelow.end(), trisIntersect.begin(), trisIntersect.end() );

                        clock_t begin = clock();
                        std::vector<std::vector<std::vector<int>>> elements = findConnexElements(trisBelow);
                        clock_t end = clock();double s = double(end - begin) / CLOCKS_PER_SEC;

                        std::cout<<"   "<<elements.size()<<" elements ("<<s<<" s)"<<std::endl;

                        double volElement=0.;
                        for(int l=0;l<elements.size();l++)
                        {
                            double vol = getVolume(elements.at(l),ptsCloud);
                            std::cout<<"   Volume ("<<l<<"): "<<vol<<std::endl;
                            volumeTot+=vol;
                            volElement+=vol;
                        }

                        PS_LOG->addMessage(LogInterface::info, LogInterface::action,QString::number(k)+QString("\t")+QString::number(z0)+QString("\t")+QString::number(volElement));

                        tris.clear();
                        tris.insert( tris.end(), trisAbove.begin(), trisAbove.end() );

                        if(k<numberDiv-1)trisAbove.clear();
                        trisBelow.clear();
                        trisIntersect.clear();
                        lineTop.clear();

                        connexLinesForPlot.insert( connexLinesForPlot.end(), connexLines.begin(), connexLines.end() );
                        connexLines.clear();

                        connexCompsForPlot.insert(connexCompsForPlot.begin(),elements.begin(),elements.end());
                    }

                    double vol = getVolume(trisAbove,ptsCloud);
                    std::cout<<"   Volume : "<<vol<<std::endl;
                    volumeTot+=vol;

                    PS_LOG->addMessage(LogInterface::info, LogInterface::action,QString("---------------------------------------------------------"));
                    PS_LOG->addMessage(LogInterface::info, LogInterface::action, QString("Volume: ")+QString::number(volumeTot)+QString(" m^3"));

                    std::cout<<" ----------------------------------------------- "<<std::endl;
                    std::cout<<"   Volume total: "<<volumeTot<<std::endl;

                    std::cout<<"   connexCompsForPlot: "<<connexCompsForPlot.size()<<std::endl;

                    QString name =names[indexName];
                    ++indexName;
                    name.append(".csv");
                    QString filepath = path;
                    filepath.append(QDir::separator());
                    filepath.append(name);
                    QFile file(filepath);
                    if (file.open(QIODevice::WriteOnly)) {
                        QTextStream outStream(&file);
                           outStream << "MeshVolume\n";
                           outStream << volumeTot;
                           outStream << "\n";
                           file.close();
                    }

                    vdh = new volumeDrawerHelper(ptsCloud,trisIntersect,trisBelow,trisAbove,connexLinesForPlot,connexCompsForPlot);

                }

                if (vdh != NULL)
                {
                    CT_ResultGroup *outResult = getOutResultList().at(1);
                    CT_StandardItemGroup* groupOut_grpOut = new CT_StandardItemGroup(DEF_groupOut_grpOut, outResult);
                    itemVolume* itemOut_demSurf = new itemVolume(DEF_itemOut_demSurf, outResult,vdh);
                    groupOut_grpOut->addItemDrawable(itemOut_demSurf);
                    outResult->addGroup(groupOut_grpOut);
                }

            }
        }


        CT_ItemAttributeList* attList = new CT_ItemAttributeList(_outVolumeItem_ModelName.completeName(), outResult);
        attList->addItemAttribute(new CT_StdItemAttributeT<double>(_outVolumeAttribute_ModelName.completeName(), CT_AbstractCategory::DATA_NUMBER, outResult, volumeTot));
        group->addItemDrawable(attList);
    }
}

std::vector<std::vector<std::vector<int> > > IFP_stepComputeMeshVolume::findConnexElements(std::vector<std::vector<int>> tris)
{
    std::vector<std::vector<std::vector<int>>> connexes;

    int lineSize = tris.size();

    int counterElements=0;

    while(counterElements<lineSize){

        std::vector<std::vector<int>> connex;
        connex.push_back(tris.at(0));
        tris.erase(tris.begin());

        bool compIsClose=false;

        std::vector<std::vector<int>> connexNew;
        connexNew.push_back(tris.at(0));

        while(!compIsClose)
        {
            compIsClose = findConnectedTris(tris,connexNew);
            connex.insert(connex.end(), connexNew.begin(), connexNew.end());
        }
        connexes.push_back(connex);
        counterElements = counterElements + connex.size();
    }

    return connexes;
}

bool IFP_stepComputeMeshVolume::findConnectedTris(std::vector<std::vector<int>>& inputTris, std::vector<std::vector<int>>& connexTris)
{
    bool isSortingOver=false;

    std::vector<int> nextId;

    for(int i=0;i<connexTris.size();i++)
    {
        int ic0 = connexTris.at(i).at(0);
        int ic1 = connexTris.at(i).at(1);
        int ic2 = connexTris.at(i).at(2);

        for(int j=0;j<inputTris.size();j++)
        {
            int i0 = inputTris.at(j).at(0);
            int i1 = inputTris.at(j).at(1);
            int i2 = inputTris.at(j).at(2);

            if(ic0==i0 || ic0==i1 || ic0==i2 || ic1==i0 || ic1==i1 || ic1==i2 || ic2==i0 || ic2==i1||ic2==i2)
            {
                nextId.push_back(j);
            }
        }
    }

    connexTris.clear();

    std::sort( nextId.begin(), nextId.end() );
    nextId.erase( std::unique( nextId.begin(), nextId.end() ), nextId.end() );

    for (std::vector<int>::reverse_iterator it=nextId.rbegin(); it!=nextId.rend(); ++it)
    {
        int ixRemove = *it;
        std::vector<int> t = inputTris.at(ixRemove);
        connexTris.push_back(t);
        inputTris.erase (inputTris.begin() + ixRemove);
    }

    if(nextId.size()==0)isSortingOver=true;

    return isSortingOver;
}

std::vector<std::vector<std::vector<int> > > IFP_stepComputeMeshVolume::findConnexLines(std::vector<std::vector<int>> lineTop)
{
    std::vector<std::vector<std::vector<int>>> connexes;

    int lineSize = lineTop.size();

    int counterElements=0;

    while(counterElements<lineSize){

        std::vector<std::vector<int>> connex;
        connex.push_back(lineTop.at(0));
        lineTop.erase(lineTop.begin());

        bool compIsClose=false;

        std::vector<std::vector<int>> connexNew;
        connexNew.push_back(lineTop.at(0));

        while(!compIsClose)
        {
            compIsClose = findConnectedSegments(lineTop,connexNew);
            connex.insert(connex.end(), connexNew.begin(), connexNew.end());
        }
        connexes.push_back(connex);
        counterElements = counterElements + connex.size();
    }

    return connexes;
}

bool IFP_stepComputeMeshVolume::findConnectedSegments(std::vector<std::vector<int>>& lineTop, std::vector<std::vector<int>>& connectedSegments)
{
    bool isSortingOver=false;

    std::vector<int> nextId;

    for(int i=0;i<connectedSegments.size();i++)
    {
        int ic0 = connectedSegments.at(i).at(0);
        int ic1 = connectedSegments.at(i).at(1);

        for(int j=0;j<lineTop.size();j++)
        {
            int i0 = lineTop.at(j).at(0);
            int i1 = lineTop.at(j).at(1);

            if(ic0==i0 || ic0==i1 || ic1==i0 || ic1==i1 )
            {
                nextId.push_back(j);
            }
        }
    }

    connectedSegments.clear();

    std::sort( nextId.begin(), nextId.end() );
    nextId.erase( std::unique( nextId.begin(), nextId.end() ), nextId.end() );

    for (std::vector<int>::reverse_iterator it=nextId.rbegin(); it!=nextId.rend(); ++it)
    {
        int ixRemove = *it;
        std::vector<int> t = lineTop.at(ixRemove);
        connectedSegments.push_back(t);
        lineTop.erase (lineTop.begin() + ixRemove);
    }

    if(nextId.size()==0)isSortingOver=true;

    return isSortingOver;
}

bool IFP_stepComputeMeshVolume::isFaceIntersectedByPlane(pcl::PointXYZ p1, pcl::PointXYZ p2, pcl::PointXYZ p3, double z0)
{
    bool result = false;

    if( p1.z<z0 && p2.z>z0 && p3.z>z0){result=true;}
    if( p1.z>z0 && p2.z<z0 && p3.z>z0){result=true;}
    if( p1.z>z0 && p2.z>z0 && p3.z<z0){result=true;}

    if( p1.z>z0 && p2.z<z0 && p3.z<z0){result=true;}
    if( p1.z<z0 && p2.z>z0 && p3.z<z0){result=true;}
    if( p1.z<z0 && p2.z<z0 && p3.z>z0){result=true;}

    return result;
}

bool IFP_stepComputeMeshVolume::fillLineTop(pcl::PointCloud<pcl::PointXYZ>& ptsCloud, std::vector<int> tri, double z0, std::vector<std::vector<int>>& lineTop)
{
    bool result = false;

    pcl::PointXYZ p1 = ptsCloud.at(tri.at(0));
    pcl::PointXYZ p2 = ptsCloud.at(tri.at(1));
    pcl::PointXYZ p3 = ptsCloud.at(tri.at(2));

    std::vector<int> line;

    if( p1.z<z0 && p2.z>z0 && p3.z>z0){
        result=true;
        line.push_back(tri.at(1));
        line.push_back(tri.at(2));
        lineTop.push_back(line);
    }

    if( p1.z>z0 && p2.z<z0 && p3.z>z0){
        result=true;
        line.push_back(tri.at(2));
        line.push_back(tri.at(0));
        lineTop.push_back(line);
    }

    if( p1.z>z0 && p2.z>z0 && p3.z<z0){
        result=true;
        line.push_back(tri.at(0));
        line.push_back(tri.at(1));
        lineTop.push_back(line);
    }

    return result;
}

bool IFP_stepComputeMeshVolume::isFaceBelowPlane(pcl::PointXYZ p1, pcl::PointXYZ p2, pcl::PointXYZ p3, double z0)
{
    bool result = false;
    if( p1.z<z0 && p2.z<z0 && p3.z<z0){result=true;}
    return result;
}

bool IFP_stepComputeMeshVolume::isFaceAbovePlane(pcl::PointXYZ p1, pcl::PointXYZ p2, pcl::PointXYZ p3, double z0)
{
    bool result = false;
    if( p1.z>z0 && p2.z>z0 && p3.z>z0){result=true;}
    return result;
}

double IFP_stepComputeMeshVolume::getVolume(std::vector<std::vector<int>>& tris, pcl::PointCloud<pcl::PointXYZ>& pts)
{
    double volume=0;

    pcl::PointXYZ center = getCentroid(tris,pts);

    for(int i=0;i<tris.size();i++)
    {
        volume += getVolumeTetrahedra(center,pts.at(tris.at(i).at(0)),pts.at(tris.at(i).at(1)),pts.at(tris.at(i).at(2)));
    }

    return volume;
}

pcl::PointXYZ IFP_stepComputeMeshVolume::getCentroid(std::vector<std::vector<int>>& tris, pcl::PointCloud<pcl::PointXYZ>& pts)
{
    pcl::PointXYZ center;
    for(int i=0;i<tris.size();i++)
    {
        center.x+=(pts.at(tris.at(i).at(0)).x+pts.at(tris.at(i).at(1)).x+pts.at(tris.at(i).at(2)).x)/((double)3*tris.size());
        center.y+=(pts.at(tris.at(i).at(0)).y+pts.at(tris.at(i).at(1)).y+pts.at(tris.at(i).at(2)).y)/((double)3*tris.size());
        center.z+=(pts.at(tris.at(i).at(0)).z+pts.at(tris.at(i).at(1)).z+pts.at(tris.at(i).at(2)).z)/((double)3*tris.size());
    }
    return center;
}

double IFP_stepComputeMeshVolume::getVolumeTetrahedra(pcl::PointXYZ& p0,pcl::PointXYZ& p1,pcl::PointXYZ& p2,pcl::PointXYZ& p3)
{
    Eigen::Vector3f v1 = - p1.getVector3fMap() + p0.getVector3fMap();
    Eigen::Vector3f v2 = - p2.getVector3fMap() + p0.getVector3fMap();
    Eigen::Vector3f v3 = - p3.getVector3fMap() + p0.getVector3fMap();

    return v1.dot(v2.cross(v3)) / 6.0;
}
