/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef POLYGONALIZER_H
#define POLYGONALIZER_H

//#include <pcl/point_types.h>
//include <pcl/point_cloud.h>

#include "isosurface.h"
#include "scalarfield.h"
#include "../core/surfacefrbf.h"

class polygonalizer
{
public:

    polygonalizer(){}
    polygonalizer(surfaceFrbf pS,int pNbDivX, int pNbDivY, int pNbDivZ);

    const isoSurface& getIsoSurface() const {return *iso;}
    const surfaceFrbf& getSurface() const {return surf;}

    float getSizeX(){return sizeCellX;}
    float getSizeY(){return sizeCellY;}
    float getSizeZ(){return sizeCellZ;}

private:

    surfaceFrbf surf;
    isoSurface* iso;

    pcl::PointXYZ min,max;
    int nbDivX, nbDivY, nbDivZ;
    float sizeCellX,sizeCellY,sizeCellZ;

    void fillScalarField(scalarField &field);
};

#endif // POLYGONALIZER_H
