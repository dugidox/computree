<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>IFP_stepComputeMeshVolume</name>
    <message>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="74"/>
        <source>Compute the volume of a closed mesh</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="79"/>
        <source>This steps compute the volume of a closed triangulated mesh. To do so, it considers each tetrahedron formed by the triangles of the mesh and the centroid of the mesh, then it computes their volume, and finally it sums those signed volumes.The volume can also be computed by slice. In this mode, the mesh is divided along the Oz axis in closed sub-meshes. The volume is then computed for each sub-meshes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="85"/>
        <source>TY  - JOUR
TI  - Efficient feature extraction for 2D/3D objects in mesh representation
T2  - Proceedings of International Conference on Image Processing
A1  - Zhang, Cha
A2  - Chen, Tsuhan
PY  - 2001
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="103"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="107"/>
        <source>Mesh</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="116"/>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="117"/>
        <source>Volume</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="120"/>
        <source>Surfaces extraites</source>
        <translation>Extracted surfaces</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="123"/>
        <source>Volume helper</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="130"/>
        <source>Divide the mesh along the Oz axis? (compute the volume per division)</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>IFP_stepComputeMnt</name>
    <message>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="75"/>
        <source>Compute the digital terrain model (IFP-LSIS)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="80"/>
        <source>This step extracts the Digital Terrain Model (DTM) from a point cloud based on local minima coming from the step &lt;em&gt;IFP_stepGetMinPtsPerSurface&lt;/em&gt;. The result is provided as a triangulated irregular network (TIN)&lt;br&gt;The method is made of three main steps:&lt;ul&gt;&lt;li&gt;A quadtree construction allowing a dynamic multi-scale local approximation of the ground surface&lt;/li&gt;&lt;li&gt;A refinement step dedicated to identify and correct approximation errors in the quadtree cells&lt;/li&gt;&lt;li&gt;The computation of the implicit function and its polygonization&lt;/li&gt;&lt;/ul&gt;&lt;br&gt;The quadtree subdivision of space is used to provide the supporting neighborhood for CSRBF and parametric patches.The quadtree is iteratively built according to the quality of ”local reconstruction” with respect to data. Indeed the quadtree structure is driven by the quality of the parametric patches thus providing an optimal scale structure for digital terrain modeling using CSRBF.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="94"/>
        <source>TY  - JOUR
TI  - Digital terrain model reconstruction from terrestrial LiDAR data using compactly supported radial basis
T2  - IEEE Computer Graphics and Applications
A1  - Morel, Jules
A2  - Bac, Alexandra
A3  - Vega, Cedric
PY  - 2017
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="114"/>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="118"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="129"/>
        <source>DTM TIN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="130"/>
        <source>Scene FRBF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="184"/>
        <source>[ Computation of the models ]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="194"/>
        <source>[ Rendering ]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="198"/>
        <source>[ MNT Points Creation ]</source>
        <translation>[ DTM Points Creation ]</translation>
    </message>
</context>
<context>
    <name>IFP_stepGetMinPtsPerSurface</name>
    <message>
        <location filename="../step/ifp_stepGetMinPtsPerSurfaceUnit.cpp" line="60"/>
        <source>Filter the point cloud to keep only the minimum points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepGetMinPtsPerSurfaceUnit.cpp" line="65"/>
        <source>The point cloud is projected into a fine regular 2D (x,y) grid and further are selected the points of minimum elevation in each cell.The resulting set of points serves as input data for the digital terrain model (DTM) algorithm.Generally speaking, the size of the 2D grid should be selected according to the expected DTM accuracy. The smaller the grid size, the higher the point density and the finer the DTM.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepGetMinPtsPerSurfaceUnit.cpp" line="81"/>
        <location filename="../step/ifp_stepGetMinPtsPerSurfaceUnit.cpp" line="85"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepGetMinPtsPerSurfaceUnit.cpp" line="95"/>
        <source>Scène extraite</source>
        <translation>Extracted scene</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepGetMinPtsPerSurfaceUnit.cpp" line="165"/>
        <source>2D grid occupancy rate: %1 %.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepGetMinPtsPerSurfaceUnit.cpp" line="191"/>
        <source>La scène de densité réduite comporte %1 points.</source>
        <translation>The reduced density scene contains %1 points.</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepGetMinPtsPerSurfaceUnit.cpp" line="194"/>
        <source>Aucun point conservé pour cette scène</source>
        <translation>No points retained for this scene</translation>
    </message>
</context>
<context>
    <name>IFP_stepPoisson</name>
    <message>
        <location filename="../step/ifp_stepPoisson.cpp" line="106"/>
        <source>Reconstruction of Poisson surface from an oriented point cloud</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepPoisson.cpp" line="111"/>
        <source>This method casts the surface reconstruction from oriented points as a spatial Poisson problem.This Poisson formulation considers all the points at once, without resorting to heuristic spatial partitioning or blending, and is therefore highly resilient to data noise.This step relies on the implementation of the PCL library Poisson surface reconstruction, which is based on Kazhdan et al. (2013)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepPoisson.cpp" line="118"/>
        <source>TY  - JOUR
TI  - Screened poisson surface reconstruction
T2  - ACM Transactions on Graphics (TOG)
A1  - Kazhdan, Michael
A2  - Hoppe, Hugues
PY  - 2013
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepPoisson.cpp" line="137"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepPoisson.cpp" line="140"/>
        <source>Tree Point cloud</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepPoisson.cpp" line="141"/>
        <source>Normals</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepPoisson.cpp" line="151"/>
        <source>Continuous surface</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepPoisson.cpp" line="165"/>
        <source>Research of half edges ?</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>IFP_stepSmoothQSM</name>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="100"/>
        <source>Transform a QSM into a continuous mesh model</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="105"/>
        <source>This step models the woody part of trees with an implicit surface.The method fits the points and fills surface holes by building upon the provided QSM to model the surface of the tree with local quadratic approximation merged together with radial basis functions used as partition of unity.The result is provided as a polygonal model arising from the polygonization of the resulting implicit function.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="112"/>
        <source>TY  - CONF
TI  - Reconstruction of trees with cylindrical quadrics and radial basis functions
T2  - The 9th Symposium on Mobile Mapping Technology, 2015, Sydney, Australia
A1  - Morel, Jules
A2  - Bac, Alexandra
A3  - Vega, Cedric
PY  - 2015
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="132"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="135"/>
        <source>Tree Point cloud</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="137"/>
        <source>QSM(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="140"/>
        <source>Groupe TTree</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="141"/>
        <source>Groupe segments</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="142"/>
        <source>Segment</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="148"/>
        <source>Surfaces extraites</source>
        <translation>Extracted surfaces</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="151"/>
        <source>QSM cylinders</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="152"/>
        <source>Continuous surface</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="160"/>
        <source>Research of half edges ? (speed up loading if unchecked)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="204"/>
        <source>Processing of each QSM cylinders ...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="213"/>
        <source>Blending of local approximations into a global model ...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="226"/>
        <source>Creation of the final mesh model ...</source>
        <translation></translation>
    </message>
</context>
</TS>
