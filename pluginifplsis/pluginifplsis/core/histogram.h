/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include "constant.h"

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <gsl/gsl_histogram.h>

class histogram
{
public:
    histogram(pcl::PointCloud<pcl::PointXYZ> &ptsCloud, float range, int pWinSizeSmooth);
    void simplefilter(pcl::PointCloud<pcl::PointXYZ> &ptsCloud);

private:

    std::vector<int> hist;
    int nbins;
    float minVal, maxVal,range;
    int winSizeSmooth;

    void smooth(int winSize);
    void print();
};

#endif // HISTOGRAM_H
