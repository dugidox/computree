/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "quadleaf.h"

#include <exception>

#include <pcl/common/common.h>

int quadLeaf::levelMax = 1;

quadLeaf::quadLeaf(quadLeaf* pParent, rectangle pRect, int pLevel, std::bitset<QUAD_MAX_LEVEL> pLocationCode, pcl::PointCloud<pcl::PointXYZ> pPts , int nbrPointsMin, float minSizeLeaf, int pHistoWindowSizeSmooth) : quadrant(pRect),histoWindowSizeSmooth(pHistoWindowSizeSmooth)
{
    quadrant.keepPointIn(pPts,ptsSub);

    if(ptsSub.size()<nbrPointsMin || quadrant.getdeltaX()<minSizeLeaf || quadrant.getdeltaY()<minSizeLeaf)
    {
        throw std::domain_error ("Not enough points");

    }else{

        level = pLevel;

        parent = pParent;
        locationCode = pLocationCode;
        getPosition();
        calcDeltaNeighbors();
        localCell = localCellPatch(ptsSub);
        updateBoundingBox(ptsSub);
    }

    hadBeenRebuild = false;
    hadBeenFilterWithHisto = false;
    toFilterWithNeighbors = false;
    toRebuildWithNeighbors = false;
    isRoot = false;
    notToDivide = false;
}

quadLeaf::~quadLeaf()
{
    //delete localCell;
}

void quadLeaf::calcDeltaNeighbors()
{
    if(parent!=NULL)
    {
        deltaLevelEast = 0;
        deltaLevelNorth = 0;
        deltaLevelWest = 0;
        deltaLevelSouth = 0;

        if(position==SOUTH_WEST)
        {
            if(parent->deltaLevelWest == INT_MAX)
            {
                deltaLevelWest=INT_MAX;
            }else{
                deltaLevelWest=parent->deltaLevelWest-1;
            }
            if(parent->deltaLevelSouth == INT_MAX)
            {
                deltaLevelSouth=INT_MAX;
            }else{
                deltaLevelSouth=parent->deltaLevelSouth-1;
            }
        }
        if(position==SOUTH_EAST)
        {
            if(parent->deltaLevelSouth == INT_MAX)
            {
                deltaLevelSouth=INT_MAX;
            }else{
                deltaLevelSouth=parent->deltaLevelSouth-1;
            }
            if(parent->deltaLevelEast == INT_MAX)
            {
                deltaLevelEast=INT_MAX;
            }else{
                deltaLevelEast=parent->deltaLevelEast-1;
            }

        }
        if(position==NORTH_WEST)
        {
            if(parent->deltaLevelNorth == INT_MAX)
            {
                deltaLevelNorth=INT_MAX;
            }else{
                deltaLevelNorth=parent->deltaLevelNorth-1;
            }
            if(parent->deltaLevelWest == INT_MAX)
            {
                deltaLevelWest=INT_MAX;
            }else{
                deltaLevelWest=parent->deltaLevelWest-1;
            }
        }
        if(position==NORTH_EAST)
        {
            if(parent->deltaLevelEast == INT_MAX)
            {
                deltaLevelEast=INT_MAX;
            }else{
                deltaLevelEast=parent->deltaLevelEast-1;
            }
            if(parent->deltaLevelNorth == INT_MAX)
            {
                deltaLevelNorth=INT_MAX;
            }else{
                deltaLevelNorth=parent->deltaLevelNorth-1;
            }
        }
    }else{
        deltaLevelEast = INT_MAX;
        deltaLevelNorth = INT_MAX;
        deltaLevelWest = INT_MAX;
        deltaLevelSouth = INT_MAX;
    }

}

void quadLeaf::getPosition()
{
    if(!locationCode.test(1) & !locationCode.test(0))
    {
        position=SOUTH_WEST;
    }
    else if(!locationCode.test(1) & locationCode.test(0))
    {
        position=SOUTH_EAST;
    }
    else if(locationCode.test(1) & !locationCode.test(0))
    {
        position=NORTH_WEST;
    }
    else if(locationCode.test(1) & locationCode.test(0))
    {
        position=NORTH_EAST;
    }
}

bool quadLeaf::filterWithHisto(float threshold, float histoRange)
{
    bool operationHasBeenDone = false;
    if(localCell.getError()>threshold)
    {
        histogram h(ptsSub,histoRange,histoWindowSizeSmooth);
        h.simplefilter(ptsSub);

        if(ptsSub.size()>=8){ //TODO : constante
            localCell = localCellPatch(ptsSub,radius,false);
            updateBoundingBox(ptsSub);
            operationHasBeenDone = true;
            hadBeenFilterWithHisto = true;
        }else{
            toFilterWithNeighbors = true;
        }
    }
    return operationHasBeenDone;
}

void quadLeaf::updateBoundingBox(pcl::PointCloud<pcl::PointXYZ> cloud)
{
    pcl::getMinMax3D (cloud, BBMin, BBMax);
}

bool quadLeaf::filterWithNeighbors()
{
    bool operationHasBeenDone = false;

    //if(!hadBeenFilterWithHisto || toRebuildWithNeighbors){

        pcl::PointCloud<pcl::PointXYZ> ptsSubNew;

        float centerZ=0.;
        float newZminBB=FLT_MAX;
        float newZmaxBB=-FLT_MAX;
        for(size_t i=0;i<neighbors.size();i++)
        {
            quadLeaf* neighbor = neighbors.at(i);
            //if(neighbor->getLocalCell().getError()<ERROR_LIMIT_ON_LEAF){
            if(!neighbor->toRebuildWithNeighbors || neighbor->hadBeenRebuild){
                ptsSubNew.insert(ptsSubNew.end(),neighbor->ptsSub.begin(),neighbor->ptsSub.end());

                centerZ += neighbor->getLocalCell().getCentroid()[2];

                if(neighbor->getBoundingBoxMin().z<newZminBB)newZminBB=neighbor->getBoundingBoxMin().z;
                if(neighbor->getBoundingBoxMax().z>newZmaxBB)newZmaxBB=neighbor->getBoundingBoxMax().z;
            }
        }

        if(!ptsSubNew.empty()){

            ptsSub.clear();
            ptsSub = ptsSubNew;

            pcl::PointXYZ center;
            center.x = quadrant.getXMin()+quadrant.getdeltaX()/2.;
            center.y = quadrant.getYMin()+quadrant.getdeltaY()/2.;
            center.z = centerZ/(float)neighbors.size();

            //localCell = localCellPatch(center, ptsSub,radius,false);
            localCell = localCellPatch(ptsSub);

            pcl::PointXYZ newMin;
            newMin.x = quadrant.getXMin();
            newMin.y = quadrant.getYMin();
            newMin.z = newZminBB;

            pcl::PointXYZ newMax;
            newMax.x = quadrant.getXMax();
            newMax.y = quadrant.getYMax();
            newMax.z = newZmaxBB;

            updateBoundingBox(newMin,newMax);

            operationHasBeenDone = true;

            hadBeenRebuild = true;
            toRebuildWithNeighbors = false;

        }else{
            std::cout<<"!!!Warning!!! Impossible to reconstruct with neighbors - code : "<<locationCode<<" level : "<<level<<" err "<<localCell.getError()<<std::endl;
        }

   // }
    return operationHasBeenDone;
}

void quadLeaf::getNeighborsPoint(quadLeaf* neighbor, pcl::PointCloud<pcl::PointXYZ> &point, pcl::PointCloud<pcl::PointXYZ> &normal)
{
    if(!neighbor->hadBeenRebuild)
    {
        point.insert(point.end(),neighbor->ptsSub.begin(),neighbor->ptsSub.end());
    }else{
        for(size_t i=0;i<neighbors.size();i++)
        {
            quadLeaf* neighb = neighbors.at(i);
            getNeighborsPoint(neighb,point,normal);
        }
    }
}

void quadLeaf::fillScalarField(scalarField &field)
{
    pcl::PointXYZ min = field.getBBmin();

    int nbDivX = field.getXNode()-1;
    int nbDivY = field.getYNode()-1;
    int nbDivZ = field.getZNode()-1;

    float sizeCellX = field.getSizeCellX();
    float sizeCellY = field.getSizeCellY();
    float sizeCellZ = field.getSizeCellZ();

    for(int k=0;k<nbDivZ+1;k++)
    {
        for(int j=0;j<nbDivY+1;j++)
        {
            for(int i=0;i<nbDivX+1;i++)
            {
                float x = (float)i*sizeCellX+min.x;
                float y = (float)j*sizeCellY+min.y;
                float z = (float)k*sizeCellZ+min.z;

                Eigen::Vector4f X(x,y,z,1);

                float res = (float)((X.transpose()*getLocalCell().getM1().matrix())*X) + (float)(getLocalCell().getM2().transpose() * X);

                field.setValue(i,j,k,res);
            }
        }
    }
}

bool quadLeaf::isOnBorder()
{
    if(deltaLevelEast==INT_MAX || deltaLevelNorth==INT_MAX || deltaLevelSouth==INT_MAX || deltaLevelWest==INT_MAX)
    {
        return true;
    }else{
        return false;
    }
}

float quadLeaf::distancePointToSurface(pcl::PointXYZ pt)
{
    Eigen::Vector4f X(pt.x,pt.y,pt.z,1);
    return (float)((X.transpose()*getLocalCell().getM1().matrix())*X) + (float)(getLocalCell().getM2().transpose() * X);
}
