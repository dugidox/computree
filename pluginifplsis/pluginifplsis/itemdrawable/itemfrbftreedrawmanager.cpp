/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "itemfrbftreedrawmanager.h"
#include "itemfrbftree.h"

const QString itemFrbfTreeDrawManager::INDEX_CONFIG_TRIANGLES_VISIBLE = itemFrbfTreeDrawManager::staticInitConfigTrianglesVisible();
const QString itemFrbfTreeDrawManager::INDEX_CONFIG_SPHERES_VISIBLE = itemFrbfTreeDrawManager::staticInitConfigSpheresVisible();

itemFrbfTreeDrawManager::itemFrbfTreeDrawManager(QString drawConfigurationName) : CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager(drawConfigurationName.isEmpty() ? "Tree" : drawConfigurationName)
{

}

itemFrbfTreeDrawManager::~itemFrbfTreeDrawManager()
{
}

void itemFrbfTreeDrawManager::draw(GraphicsViewInterface &view, PainterInterface &painter, const CT_AbstractItemDrawable &itemDrawable) const
{
    CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager::draw(view, painter, itemDrawable);

    const itemFrbfTree &item = dynamic_cast<const itemFrbfTree&>(itemDrawable);

    if(getDrawConfiguration()->getVariableValue(INDEX_CONFIG_TRIANGLES_VISIBLE).toBool())
        drawTriangles(view, painter, item);
    if(getDrawConfiguration()->getVariableValue(INDEX_CONFIG_SPHERES_VISIBLE).toBool())
        drawSpheres(view, painter, item);
}

CT_ItemDrawableConfiguration itemFrbfTreeDrawManager::createDrawConfiguration(QString drawConfigurationName) const
{
    CT_ItemDrawableConfiguration item = CT_ItemDrawableConfiguration(drawConfigurationName);

    item.addAllConfigurationOf(CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager::createDrawConfiguration(drawConfigurationName));
    item.addNewConfiguration(itemFrbfTreeDrawManager::staticInitConfigTrianglesVisible(), "Surface", CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(itemFrbfTreeDrawManager::staticInitConfigSpheresVisible(), "Spheres", CT_ItemDrawableConfiguration::Bool, false);

    return item;
}

// PROTECTED //

QString itemFrbfTreeDrawManager::staticInitConfigTrianglesVisible()
{
    return "DEM_TV";
}

QString itemFrbfTreeDrawManager::staticInitConfigSpheresVisible()
{
    return "DEM_SP";
}

void itemFrbfTreeDrawManager::drawTriangles(GraphicsViewInterface &view, PainterInterface &painter, const itemFrbfTree &item) const
{
    painter.setColor(0, 75, 75);

    const isoSurface iso = item.getTree().getLevelSet();

    for(std::size_t i=0; i<iso.getTriangles().size();i++)
    {
        pcl::PointXYZ x0 = iso.getMapPoints().at(iso.getTriangles().at(i).pointIndex[0]);
        pcl::PointXYZ x1 = iso.getMapPoints().at(iso.getTriangles().at(i).pointIndex[1]);
        pcl::PointXYZ x2 = iso.getMapPoints().at(iso.getTriangles().at(i).pointIndex[2]);

        painter.drawLine(x0.x,x0.y,x0.z,x1.x,x1.y,x1.z);
        painter.drawLine(x1.x,x1.y,x1.z,x2.x,x2.y,x2.z);
        painter.drawLine(x2.x,x2.y,x2.z,x0.x,x0.y,x0.z);
    }
}

void itemFrbfTreeDrawManager::drawSpheres(GraphicsViewInterface &view, PainterInterface &painter, const itemFrbfTree &item) const
{
    painter.setColor(0, 75, 75);

    continuousQsmTree t = item.getTree();

    for(std::size_t i=0; i<20;i++)
    {
        painter.setColor(0, 75, 75);
        continuousQsmSegment& s = t.getListSpheres().at(i);
        painter.drawPartOfSphere(s.getCenter().x,s.getCenter().y,s.getCenter().z,s.getRadius(),0,360,0,360);

        painter.setColor(100, 150, 150);
        Eigen::Vector2d a;
        a[0]=s.getCenter().x-1.5*s.getRadius();
        a[1]=s.getCenter().y-1.5*s.getRadius();
        Eigen::Vector2d b;
        b[0]=s.getCenter().x+1.5*s.getRadius();
        b[1]=s.getCenter().y+1.5*s.getRadius();
        painter.drawRectXY(a,b,s.getCenter().z);
    }
}
