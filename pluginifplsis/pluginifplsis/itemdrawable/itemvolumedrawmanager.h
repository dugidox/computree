/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ITEMVOLUMEDRAWMANAGER_H
#define ITEMVOLUMEDRAWMANAGER_H

#include "ct_itemdrawable/tools/drawmanager/ct_standardabstractitemdrawablewithoutpointclouddrawmanager.h"

class itemVolume;

class itemVolumeDrawManager : public CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager
{
public:
    itemVolumeDrawManager(QString drawConfigurationName = "");
    virtual ~itemVolumeDrawManager();

    virtual void draw(GraphicsViewInterface &view, PainterInterface &painter, const CT_AbstractItemDrawable &itemDrawable) const;

protected:

    const static QString INDEX_CONFIG_SPHERES_VISIBLE;
    const static QString INDEX_CONFIG_LINE_VISIBLE;

    static QString staticInitConfigSpheresVisible();
    static QString staticInitConfigLineVisible();

    virtual CT_ItemDrawableConfiguration createDrawConfiguration(QString drawConfigurationName) const;

    void drawSpheres(GraphicsViewInterface &view, PainterInterface &painter, const itemVolume &item) const;
    void drawLine(GraphicsViewInterface &view, PainterInterface &painter, const itemVolume &item) const;
};

#endif // ITEMVOLUMEDRAWMANAGER_H
