/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef HOUGH_H
#define HOUGH_H

#include "continuousQsmSegment.h"

#include "plot/implicitfunction.h"
#include "utils/treetopology.h"


class continuousQsmTree  : public implicitFunction
{
public:
    continuousQsmTree(){}
    continuousQsmTree(pcl::PointCloud<pcl::PointXYZ> &inputPts, treeTopology stModel);

    virtual void fillScalarField(scalarField &field);

    std::vector<int>& getListBranchIndices(){return listBranchIndices;}

    std::vector<continuousQsmSegment>& getListSpheres(){return listSphere;}

private:

    pcl::PointCloud<pcl::PointXYZ> ptsCloud;
    std::vector<continuousQsmSegment> listSphere;

    std::vector<int> listBranchIndices;

    bool modelByBranch = false;
};

#endif // HOUGH_H
