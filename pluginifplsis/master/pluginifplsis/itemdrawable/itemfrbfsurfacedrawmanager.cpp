/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "itemfrbfsurfacedrawmanager.h"
#include "itemfrbfsurface.h"

#include <pcl/common/common.h>

const QString itemFrbfsurfaceDrawManager::INDEX_CONFIG_TRIANGLES_VISIBLE = itemFrbfsurfaceDrawManager::staticInitConfigTrianglesVisible();
const QString itemFrbfsurfaceDrawManager::INDEX_CONFIG_QUADTREE_VISIBLE = itemFrbfsurfaceDrawManager::staticInitConfigQuadTreeVisible();
const QString itemFrbfsurfaceDrawManager::INDEX_CONFIG_SPHERES_VISIBLE = itemFrbfsurfaceDrawManager::staticInitConfigSphereVisible();
const QString itemFrbfsurfaceDrawManager::INDEX_CONFIG_CENTERS_VISIBLE = itemFrbfsurfaceDrawManager::staticInitConfigCenterVisible();
const QString itemFrbfsurfaceDrawManager::INDEX_CONFIG_QUADPTS_VISIBLE = itemFrbfsurfaceDrawManager::staticInitConfigQuadPtsVisible();

itemFrbfsurfaceDrawManager::itemFrbfsurfaceDrawManager(QString drawConfigurationName) : CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager(drawConfigurationName.isEmpty() ? "Quadtree" : drawConfigurationName)
{

}

itemFrbfsurfaceDrawManager::~itemFrbfsurfaceDrawManager()
{
}

void itemFrbfsurfaceDrawManager::draw(GraphicsViewInterface &view, PainterInterface &painter, const CT_AbstractItemDrawable &itemDrawable) const
{
    CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager::draw(view, painter, itemDrawable);

    const itemFrbfsurface &item = dynamic_cast<const itemFrbfsurface&>(itemDrawable);

    if(getDrawConfiguration()->getVariableValue(INDEX_CONFIG_TRIANGLES_VISIBLE).toBool())
        drawTriangles(view, painter, item);

    if(getDrawConfiguration()->getVariableValue(INDEX_CONFIG_QUADTREE_VISIBLE).toBool())
        drawQuadTree(view, painter, item);

    if(getDrawConfiguration()->getVariableValue(INDEX_CONFIG_SPHERES_VISIBLE).toBool())
        drawSpheres(view, painter, item);

    if(getDrawConfiguration()->getVariableValue(INDEX_CONFIG_CENTERS_VISIBLE).toBool())
        drawCenters(view, painter, item);

    if(getDrawConfiguration()->getVariableValue(INDEX_CONFIG_QUADPTS_VISIBLE).toBool())
        drawQuadPts(view, painter, item);
}

CT_ItemDrawableConfiguration itemFrbfsurfaceDrawManager::createDrawConfiguration(QString drawConfigurationName) const
{
    CT_ItemDrawableConfiguration item = CT_ItemDrawableConfiguration(drawConfigurationName);

    item.addAllConfigurationOf(CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager::createDrawConfiguration(drawConfigurationName));
    item.addNewConfiguration(itemFrbfsurfaceDrawManager::staticInitConfigTrianglesVisible(), "Surface", CT_ItemDrawableConfiguration::Bool, true);
    item.addNewConfiguration(itemFrbfsurfaceDrawManager::staticInitConfigQuadTreeVisible(), "Quadtree", CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(itemFrbfsurfaceDrawManager::staticInitConfigQuadPtsVisible(), "Points (after filtering)", CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(itemFrbfsurfaceDrawManager::staticInitConfigSphereVisible(), "RBF supports", CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(itemFrbfsurfaceDrawManager::staticInitConfigCenterVisible(), "Local frames centers", CT_ItemDrawableConfiguration::Bool, false);
    return item;
}

// PROTECTED //

QString itemFrbfsurfaceDrawManager::staticInitConfigTrianglesVisible()
{
    return "DEM_TV";
}

QString itemFrbfsurfaceDrawManager::staticInitConfigQuadTreeVisible()
{
    return "DEM_QV";
}

QString itemFrbfsurfaceDrawManager::staticInitConfigQuadPtsVisible()
{
    return "DEM_QP";
}

QString itemFrbfsurfaceDrawManager::staticInitConfigSphereVisible()
{
    return "DEM_SV";
}

QString itemFrbfsurfaceDrawManager::staticInitConfigCenterVisible()
{
    return "DEM_CV";
}

void itemFrbfsurfaceDrawManager::drawTriangles(GraphicsViewInterface &view, PainterInterface &painter, const itemFrbfsurface &item) const
{
    painter.setColor(0, 75, 75);

    const isoSurface iso = item.getImplFrbf().getLevelSet();

    for(std::size_t i=0; i<iso.getTriangles().size();i++)
    {
        pcl::PointXYZ x0 = iso.getMapPoints().at(iso.getTriangles().at(i).pointIndex[0]);
        pcl::PointXYZ x1 = iso.getMapPoints().at(iso.getTriangles().at(i).pointIndex[1]);
        pcl::PointXYZ x2 = iso.getMapPoints().at(iso.getTriangles().at(i).pointIndex[2]);

        painter.drawLine(x0.x,x0.y,x0.z,x1.x,x1.y,x1.z);
        painter.drawLine(x1.x,x1.y,x1.z,x2.x,x2.y,x2.z);
        painter.drawLine(x2.x,x2.y,x2.z,x0.x,x0.y,x0.z);
    }
}

void itemFrbfsurfaceDrawManager::drawSpheres(GraphicsViewInterface &view, PainterInterface &painter, const itemFrbfsurface &item) const
{
    painter.setColor(71, 79, 71);
    surfaceFrbf surf = item.getImplFrbf();

    for(std::size_t i=0; i<surf.listSphere.size();i++)
    {
        pcl::PointXYZ center =  surf.listSphere.at(i).getCenter();
        float radius = surf.listSphere.at(i).getRadius();
        painter.drawPartOfSphere(center.x,center.y,center.z,radius,0.,2*M_PI,0.,2*M_PI);
    }
}

void itemFrbfsurfaceDrawManager::drawCenters(GraphicsViewInterface &view, PainterInterface &painter, const itemFrbfsurface &item) const
{
    surfaceFrbf surf = item.getImplFrbf();

    for(std::size_t i=0; i<surf.listSphere.size();i++)
    {
        pcl::PointXYZ center =  surf.listSphere.at(i).getCenter();
        painter.setColor(255, 0, 0);
        painter.drawLine(center.x,center.y,center.z,center.x+surf.listSphere.at(i).getLocalRef().getU()[0],center.y+surf.listSphere.at(i).getLocalRef().getU()[1],center.z+surf.listSphere.at(i).getLocalRef().getU()[2]);
        painter.setColor(0, 255, 0);
        painter.drawLine(center.x,center.y,center.z,center.x+surf.listSphere.at(i).getLocalRef().getV()[0],center.y+surf.listSphere.at(i).getLocalRef().getV()[1],center.z+surf.listSphere.at(i).getLocalRef().getV()[2]);
        painter.setColor(0, 0, 255);
        painter.drawLine(center.x,center.y,center.z,center.x+surf.listSphere.at(i).getLocalRef().getW()[0],center.y+surf.listSphere.at(i).getLocalRef().getW()[1],center.z+surf.listSphere.at(i).getLocalRef().getW()[2]);
    }
}

void itemFrbfsurfaceDrawManager::drawQuadTree(GraphicsViewInterface &view, PainterInterface &painter, const itemFrbfsurface &item) const
{
    surfaceFrbf surf = item.getImplFrbf();

    for(std::size_t i=0;i<surf.getTree().getListLeaf().size();i++)
    {             
        quadLeaf leaf = *surf.getTree().getListLeaf().at(i);

        if(leaf.hadBeenFilterWithHisto && !leaf.hadBeenRebuild)
        {
            painter.setColor(220, 220, 20);
        }
        if(leaf.hadBeenRebuild && !leaf.hadBeenFilterWithHisto)
        {
            painter.setColor(150, 20, 20);
        }
        if(!leaf.hadBeenRebuild && !leaf.hadBeenFilterWithHisto){
            painter.setColor(20, 150, 20);
        }

        float zmin = surf.getTree().getMin().z;
        float deltaXY = 0.05;

        pcl::PointXYZ x1;
        x1.x = leaf.getQuadrant().getXMin()+deltaXY;
        x1.y = leaf.getQuadrant().getYMin()+deltaXY;
        x1.z = zmin;

        pcl::PointXYZ x2;
        x2.x = leaf.getQuadrant().getXMax()-deltaXY;
        x2.y = leaf.getQuadrant().getYMin()+deltaXY;
        x2.z = zmin;

        pcl::PointXYZ x3;
        x3.x = leaf.getQuadrant().getXMax()-deltaXY;
        x3.y = leaf.getQuadrant().getYMax()-deltaXY;
        x3.z = zmin;

        pcl::PointXYZ x4;
        x4.x = leaf.getQuadrant().getXMin()+deltaXY;
        x4.y = leaf.getQuadrant().getYMax()-deltaXY;
        x4.z = zmin;       

        painter.drawRectXY(Eigen::Vector2d(x1.x,x1.y),Eigen::Vector2d(x2.x,x2.y),zmin);
        painter.drawRectXY(Eigen::Vector2d(x2.x,x2.y),Eigen::Vector2d(x3.x,x3.y),zmin);
        painter.drawRectXY(Eigen::Vector2d(x3.x,x3.y),Eigen::Vector2d(x4.x,x4.y),zmin);
        painter.drawRectXY(Eigen::Vector2d(x4.x,x4.y),Eigen::Vector2d(x1.x,x1.y),zmin);
    }
}

void itemFrbfsurfaceDrawManager::drawQuadPts(GraphicsViewInterface &view, PainterInterface &painter, const itemFrbfsurface &item) const
{
    painter.setColor(0,255,255);
    quadTree tree = item.getImplFrbf().getTree();
    for(std::size_t i=0;i<tree.getPts().size();i++)
    {
        painter.drawPoint(tree.getPts().at(i).x,tree.getPts().at(i).y,tree.getPts().at(i).z);
    }
}
