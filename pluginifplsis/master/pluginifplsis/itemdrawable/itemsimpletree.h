/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ITEMSIMPLETREE_H
#define ITEMSIMPLETREE_H

#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithoutpointcloud.h"
#include "itemsimpletreedrawmanager.h"

#include "utils/treetopology.h"

class itemSimpleTree : public CT_AbstractItemDrawableWithoutPointCloud
{
    Q_OBJECT

public:

    itemSimpleTree();
    itemSimpleTree(const CT_OutAbstractSingularItemModel *model, const CT_AbstractResult *result, treeTopology *pField);
    itemSimpleTree(const QString &modelName, const CT_AbstractResult *result, treeTopology *pField);

    ~itemSimpleTree();

    /**
      * ATTENTION : ne pas oublier de redéfinir ces deux méthodes si vous hérité de cette classe.
      */
    virtual QString getType() const;
    static QString staticGetType();

    const treeTopology& getModel() const;

    virtual itemSimpleTree* copy(const CT_OutAbstractItemModel *model, const CT_AbstractResult *result, CT_ResultCopyModeList copyModeList);
private:

    const static itemSimpleTreeDrawManager  simple_tree_DRAW_MANAGER;

protected:

    treeTopology stModel;
};

#endif // ITEMSIMPLETREE_H
