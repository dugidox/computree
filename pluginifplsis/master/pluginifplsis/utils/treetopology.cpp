/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "treetopology.h"

#include <math.h>

#include <pcl/point_types.h>

void treeTopology::refineCylinders()
{
    bool isFirstCylinderOfbranch=false;

    for(int i=0;i<listSegment.size();i++)
    {
        cylinder c;
        treeSegment s = listSegment.at(i);

        if(s.isAProperties("XX1")){
            c.x1 = s.getProperties("XX1")/100.;
            c.y1 = s.getProperties("YY1")/100.;
            c.z1 = s.getProperties("ZZ1")/100.;

            c.x2 = s.getProperties("XX2")/100.;
            c.y2 = s.getProperties("YY2")/100.;
            c.z2 = s.getProperties("ZZ2")/100.;

            double length=0.;
            //quickfix simpletree error
            if(s.isAProperties("Lenght")){
                length = s.getProperties("Lenght");
            }else{
                length = s.getProperties("Length");
            }

            c.radius = sqrt(s.getProperties("Volume")/(length*M_PI))/100.;

            pcl::PointXYZ direction;
            direction.x= c.x2 - c.x1;
            direction.y= c.y2 - c.y1;
            direction.z= c.z2 - c.z1;
            double h = direction.getVector3fMap().norm();
            int div = (int)(h/(2*c.radius))+1;

            std::vector<cylinder> listtmp;
            for(int d=0;d<div;d++)
            {
                cylinder ctmp;
                ctmp.x1 = c.x1 + d*(1./(double)div) * direction.x;
                ctmp.y1 = c.y1 + d*(1./(double)div) * direction.y;
                ctmp.z1 = c.z1 + d*(1./(double)div) * direction.z;

                ctmp.x2 = c.x1 + (d+1)*(1./(double)div) * direction.x;
                ctmp.y2 = c.y1 + (d+1)*(1./(double)div) * direction.y;
                ctmp.z2 = c.z1 + (d+1)*(1./(double)div) * direction.z;

                ctmp.radius = c.radius;

                ctmp.branchId = c.branchId;

                if(isFirstCylinderOfbranch){
                    ctmp.isFirst=true;
                    isFirstCylinderOfbranch=false;
                }else{
                    ctmp.isFirst=false;
                }

                listtmp.push_back(ctmp);

                numberOfCylinders++;
            }

            listCylinders.insert( listCylinders.end(), listtmp.begin(), listtmp.end());
        }else{
            isFirstCylinderOfbranch=true;
        }
    }
}

void treeTopology::prepareForexport()
{
    int resCircle=20;
    int counterPts=0;

    for(int i=0;i<listCylinders.size();i++)
    {
        cylinder& c = listCylinders.at(i);

        pcl::PointXYZ dir;
        dir.x= c.x2 - c.x1;
        dir.y= c.y2 - c.y1;
        dir.z= c.z2 - c.z1;

        pcl::PointXYZ dirPerp;
        dirPerp.x=0.;
        dirPerp.y=-dir.z/dir.y;
        dirPerp.z=1.;

        double dirPerpNorm = dirPerp.getVector3fMap().norm();
        dirPerp.y=dirPerp.y/dirPerpNorm;
        dirPerp.z=dirPerp.z/dirPerpNorm;

        Eigen::Vector3f z = dir.getVector3fMap().cross(dirPerp.getVector3fMap());
        pcl::PointXYZ dirPerp2;
        dirPerp2.x = z[0]/z.norm();
        dirPerp2.y = z[1]/z.norm();
        dirPerp2.z = z[2]/z.norm();

        for(int j=0;j<resCircle;j++)
        {
            double angle = ((double)j/(double)resCircle)*2.*M_PI;
            pcl::PointXYZ c1;
            c1.x=c.x1+c.radius*(cos(angle)*dirPerp2.x+sin(angle)*dirPerp.x);
            c1.y=c.y1+c.radius*(cos(angle)*dirPerp2.y+sin(angle)*dirPerp.y);
            c1.z=c.z1+c.radius*(cos(angle)*dirPerp2.z+sin(angle)*dirPerp.z);

            pcl::PointXYZ c2;
            c2.x=c.x2+c.radius*(cos(angle)*dirPerp2.x+sin(angle)*dirPerp.x);
            c2.y=c.y2+c.radius*(cos(angle)*dirPerp2.y+sin(angle)*dirPerp.y);
            c2.z=c.z2+c.radius*(cos(angle)*dirPerp2.z+sin(angle)*dirPerp.z);

            mapPoints.insert(std::make_pair(counterPts,c1));
            mapPoints.insert(std::make_pair(counterPts+1,c2));
            counterPts = counterPts + 2;
        }

        for(int j=0;j<resCircle;j++)
        {
            if(j<resCircle-1){
                int ix = counterPts-2*resCircle+2*j;
                triangle t1;
                t1.pointIndex[0]=ix;
                t1.pointIndex[1]=ix+1;
                t1.pointIndex[2]=ix+2;
                triangle t2;
                t2.pointIndex[0]=ix+1;
                t2.pointIndex[1]=ix+2;
                t2.pointIndex[2]=ix+3;

                listTriangles.push_back(t1);
                listTriangles.push_back(t2);
            }else{
                int ix = counterPts-2*resCircle+2*j;
                triangle t1;
                t1.pointIndex[0]=ix;
                t1.pointIndex[1]=ix+1;
                t1.pointIndex[2]=counterPts-2*resCircle;
                triangle t2;
                t2.pointIndex[0]=ix+1;
                t2.pointIndex[1]=counterPts-2*resCircle;
                t2.pointIndex[2]=counterPts-2*resCircle+1;

                listTriangles.push_back(t1);
                listTriangles.push_back(t2);
            }
        }
    }
}
