<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>IFP_stepComputeMeshVolume</name>
    <message>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="74"/>
        <source>Compute the volume of a closed mesh</source>
        <translation>Calcul du volume d&apos;un maillage fermé</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="79"/>
        <source>This steps compute the volume of a closed triangulated mesh. To do so, it considers each tetrahedron formed by the triangles of the mesh and the centroid of the mesh, then it computes their volume, and finally it sums those signed volumes.The volume can also be computed by slice. In this mode, the mesh is divided along the Oz axis in closed sub-meshes. The volume is then computed for each sub-meshes</source>
        <translation>Cette étape calcule le volume d&apos;un maillage triangulé fermé. Pour ce faire, il considère chaque tétraèdre formé par les triangles du maillage et le centroïde du maillage, puis il calcule leur volume, et enfin il somme ces volumes signés. Le volume peut également être calculé par tranche. Dans ce mode, le maillage est divisé le long de l&apos;axe Oz dans des sous-maillages fermés. Le volume est ensuite calculé pour chaque sous-maillage</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="85"/>
        <source>TY  - JOUR
TI  - Efficient feature extraction for 2D/3D objects in mesh representation
T2  - Proceedings of International Conference on Image Processing
A1  - Zhang, Cha
A2  - Chen, Tsuhan
PY  - 2001
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="103"/>
        <source>Scène</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="107"/>
        <source>Mesh</source>
        <translation>Maillage</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="116"/>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="117"/>
        <source>Volume</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="120"/>
        <source>Surfaces extraites</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="123"/>
        <source>Volume helper</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMeshVolume.cpp" line="130"/>
        <source>Divide the mesh along the Oz axis? (compute the volume per division)</source>
        <translation>Diviser le maillage le long de l&apos;axe Oz? (calculer le volume par division)</translation>
    </message>
</context>
<context>
    <name>IFP_stepComputeMnt</name>
    <message>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="75"/>
        <source>Compute the digital terrain model (IFP-LSIS)</source>
        <translation>Calcul du modèle numérique de terrain (IFP-LSIS)</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="80"/>
        <source>This step extracts the Digital Terrain Model (DTM) from a point cloud based on local minima coming from the step &lt;em&gt;IFP_stepGetMinPtsPerSurface&lt;/em&gt;. The result is provided as a triangulated irregular network (TIN)&lt;br&gt;The method is made of three main steps:&lt;ul&gt;&lt;li&gt;A quadtree construction allowing a dynamic multi-scale local approximation of the ground surface&lt;/li&gt;&lt;li&gt;A refinement step dedicated to identify and correct approximation errors in the quadtree cells&lt;/li&gt;&lt;li&gt;The computation of the implicit function and its polygonization&lt;/li&gt;&lt;/ul&gt;&lt;br&gt;The quadtree subdivision of space is used to provide the supporting neighborhood for CSRBF and parametric patches.The quadtree is iteratively built according to the quality of ”local reconstruction” with respect to data. Indeed the quadtree structure is driven by the quality of the parametric patches thus providing an optimal scale structure for digital terrain modeling using CSRBF.</source>
        <translation>Cette étape extrait le modèle numérique de terrain (DTM) d&apos;un nuage de points basé sur des minima locaux provenant de l&apos;étape &lt;em&gt; IFP_stepGetMinPtsPerSurface &lt;/em&gt;. Le résultat est fourni sous la forme d&apos;un réseau triangulaire irrégulier (TIN) &lt;br&gt; La méthode est composée de trois étapes principales: &lt;ul&gt; &lt;li&gt; Une construction en quadtree permettant une approximation locale multi-échelle dynamique de la surface du sol &lt;/li&gt; &lt;li&gt; Une étape de raffinement dédiée à l&apos;identification et à la correction des erreurs d&apos;approximation dans les cellules quadtree &lt;/li&gt; &lt;li&gt; Le calcul de la fonction implicite et sa polygonisation &lt;/li&gt; &lt;/ul&gt; &lt;br&gt; La subdivision quadtree de l&apos;espace est utilisée pour fournir le voisinage de soutien pour la CSRBF et les patches paramétriques. Le quadtree est itérativement construit selon la qualité de &quot;reconstruction locale&quot; en ce qui concerne les données. En effet, la structure quadtree est pilotée par la qualité des patchs paramétriques fournissant ainsi une structure d&apos;échelle optimale pour la modélisation de terrain numérique en utilisant le CSRBF.</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="94"/>
        <source>TY  - JOUR
TI  - Digital terrain model reconstruction from terrestrial LiDAR data using compactly supported radial basis
T2  - IEEE Computer Graphics and Applications
A1  - Morel, Jules
A2  - Bac, Alexandra
A3  - Vega, Cedric
PY  - 2017
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="114"/>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="118"/>
        <source>Scène</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="129"/>
        <source>DTM TIN</source>
        <translation>TIN MNT</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="130"/>
        <source>Scene FRBF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="184"/>
        <source>[ Computation of the models ]</source>
        <translation>[Calcul des modèles]</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="194"/>
        <source>[ Rendering ]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepComputeMnt.cpp" line="198"/>
        <source>[ MNT Points Creation ]</source>
        <translation>[ Creation Points MNT ]</translation>
    </message>
</context>
<context>
    <name>IFP_stepGetMinPtsPerSurface</name>
    <message>
        <location filename="../step/ifp_stepGetMinPtsPerSurfaceUnit.cpp" line="60"/>
        <source>Filter the point cloud to keep only the minimum points</source>
        <translation>Filter le nuage de points pour ne garder que les points minimums</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepGetMinPtsPerSurfaceUnit.cpp" line="65"/>
        <source>The point cloud is projected into a fine regular 2D (x,y) grid and further are selected the points of minimum elevation in each cell.The resulting set of points serves as input data for the digital terrain model (DTM) algorithm.Generally speaking, the size of the 2D grid should be selected according to the expected DTM accuracy. The smaller the grid size, the higher the point density and the finer the DTM.</source>
        <translation>Le nuage de points est projeté dans une fine grille 2D régulière (x, y) et les points d&apos;élévation minimum dans chaque cellule sont sélectionnés. L&apos;ensemble de points résultant sert de données d&apos;entrée pour l&apos;algorithme MNT (Modèle Numérique de Terrain). , la taille de la grille 2D doit être sélectionnée en fonction de la précision du MNT attendue. Plus la taille de la grille est petite, plus la densité de points est élevée et plus le MNT est fin.</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepGetMinPtsPerSurfaceUnit.cpp" line="81"/>
        <location filename="../step/ifp_stepGetMinPtsPerSurfaceUnit.cpp" line="85"/>
        <source>Scène</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepGetMinPtsPerSurfaceUnit.cpp" line="95"/>
        <source>Scène extraite</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepGetMinPtsPerSurfaceUnit.cpp" line="165"/>
        <source>2D grid occupancy rate: %1 %.</source>
        <translation>Taux d&apos;occupation de la grille 2D : %1 %.</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepGetMinPtsPerSurfaceUnit.cpp" line="191"/>
        <source>La scène de densité réduite comporte %1 points.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepGetMinPtsPerSurfaceUnit.cpp" line="194"/>
        <source>Aucun point conservé pour cette scène</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>IFP_stepPoisson</name>
    <message>
        <location filename="../step/ifp_stepPoisson.cpp" line="106"/>
        <source>Reconstruction of Poisson surface from an oriented point cloud</source>
        <translation>Reconstruction d&apos;une surface de poisson d&apos;un nuage de points orienté</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepPoisson.cpp" line="111"/>
        <source>This method casts the surface reconstruction from oriented points as a spatial Poisson problem.This Poisson formulation considers all the points at once, without resorting to heuristic spatial partitioning or blending, and is therefore highly resilient to data noise.This step relies on the implementation of the PCL library Poisson surface reconstruction, which is based on Kazhdan et al. (2013)</source>
        <translation>Cette méthode convertit la reconstruction de la surface à partir de points orientés en un problème de Poisson spatial. Cette formulation de Poisson considère tous les points en une seule fois, sans recourir au partitionnement heuristique ou au mélange, et est donc très résiliente au bruit de données. Cette étape s&apos;appuie sur la reconstruction de la surface de Poisson de la bibliothèque PCL, basée sur Kazhdan et al. (2013)</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepPoisson.cpp" line="118"/>
        <source>TY  - JOUR
TI  - Screened poisson surface reconstruction
T2  - ACM Transactions on Graphics (TOG)
A1  - Kazhdan, Michael
A2  - Hoppe, Hugues
PY  - 2013
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepPoisson.cpp" line="137"/>
        <source>Scène</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepPoisson.cpp" line="140"/>
        <source>Tree Point cloud</source>
        <translation>Nuage de points arbre</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepPoisson.cpp" line="141"/>
        <source>Normals</source>
        <translation>Normales</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepPoisson.cpp" line="151"/>
        <source>Continuous surface</source>
        <translation>Surface continue</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepPoisson.cpp" line="165"/>
        <source>Research of half edges ?</source>
        <translation>Recherche de demi-arêtes ?</translation>
    </message>
</context>
<context>
    <name>IFP_stepSmoothQSM</name>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="100"/>
        <source>Transform a QSM into a continuous mesh model</source>
        <translation>Transformer un QSM en maillage continu</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="105"/>
        <source>This step models the woody part of trees with an implicit surface.The method fits the points and fills surface holes by building upon the provided QSM to model the surface of the tree with local quadratic approximation merged together with radial basis functions used as partition of unity.The result is provided as a polygonal model arising from the polygonization of the resulting implicit function.</source>
        <translation>Cette méthode modélise la partie ligneuse des arbres avec une surface implicite. La méthode s&apos;ajuste aux points et remplit les trous de surface en s&apos;appuyant sur le QSM fourni pour modéliser la surface de l&apos;arbre avec une approximation quadratique locale fusionnée avec des fonctions de base radiales utilisées comme partition de l&apos;unité. Le résultat est fourni sous la forme d&apos;un modèle polygonal résultant de la polygonisation de la fonction implicite.</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="112"/>
        <source>TY  - CONF
TI  - Reconstruction of trees with cylindrical quadrics and radial basis functions
T2  - The 9th Symposium on Mobile Mapping Technology, 2015, Sydney, Australia
A1  - Morel, Jules
A2  - Bac, Alexandra
A3  - Vega, Cedric
PY  - 2015
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="132"/>
        <source>Scène</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="135"/>
        <source>Tree Point cloud</source>
        <translation>Nuage de points arbre</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="137"/>
        <source>QSM(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="140"/>
        <source>Groupe TTree</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="141"/>
        <source>Groupe segments</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="142"/>
        <source>Segment</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="148"/>
        <source>Surfaces extraites</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="151"/>
        <source>QSM cylinders</source>
        <translation>Cylindres QSM</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="152"/>
        <source>Continuous surface</source>
        <translation>Surface continue</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="160"/>
        <source>Research of half edges ? (speed up loading if unchecked)</source>
        <translation>Recherche de demi-arêtes ? (accélère le chargement si non coché)</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="204"/>
        <source>Processing of each QSM cylinders ...</source>
        <translation>Traitement de chaque cylindre QSM ...</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="213"/>
        <source>Blending of local approximations into a global model ...</source>
        <translation>Fusion d&apos;approximations locales dans un modèle global ...</translation>
    </message>
    <message>
        <location filename="../step/ifp_stepSmoothQsm.cpp" line="226"/>
        <source>Creation of the final mesh model ...</source>
        <translation>Création du modèle de maillage final ...</translation>
    </message>
</context>
</TS>
