/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef GRAPH_H
#define GRAPH_H

#include "quadtree.h"
#include "constant.h"

#include <boost/graph/adjacency_list.hpp>

typedef std::pair<quadLeaf*, quadLeaf*> Edge;

class graph
{
public:
    graph(quadTree &pTree);
    void setRadius(quadTree &pTree);
    void setNeighborhood(quadTree &pTree);
    void updateRadius(quadTree &tree);

    std::vector<quadLeaf*> getNeighbors(quadLeaf* pLeaf);

private:

    std::vector<Edge> edgeVec;

    void buildGraphOfNeighborhood(quadTree &pTree);
    void addEdge(quadTree &tree,quadLeaf* pLeaf, int deltaLevel, std::bitset<QUAD_MAX_LEVEL> pDirection);

    void print(quadTree &tree);
};

#endif // GRAPH_H
