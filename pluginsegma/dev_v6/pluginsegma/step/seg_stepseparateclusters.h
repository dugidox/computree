#ifndef SEG_STEPSEPARATECLUSTERS_H
#define SEG_STEPSEPARATECLUSTERS_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_image2d.h"
#include "ct_itemdrawable/ct_scene.h"

class SEG_StepSeparateClusters: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    SEG_StepSeparateClusters();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    CT_HandleInResultGroupCopy<>                                    _inResult;
    CT_HandleInStdZeroOrMoreGroup                                   _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                           _inGroup;
    CT_HandleInSingularItem<CT_Image2D<qint32> >                    _inClusters;
    CT_HandleInSingularItem<CT_Image2D<float> >                     _inRaster;

    CT_HandleOutStdGroup                                            _groupCluster;
    CT_HandleOutSingularItem<CT_Image2D<float> >                    _outCluster;
    CT_HandleOutStdItemAttribute<qint32>                            _outAttIDCluster;
    CT_HandleOutSingularItem<CT_Image2D<quint8> >                   _outMask;



    struct pixelsArea {
        pixelsArea()
        {
            _xmin = std::numeric_limits<int>::max();
            _xmax = std::numeric_limits<int>::min();
            _ymin = std::numeric_limits<int>::max();
            _ymax = std::numeric_limits<int>::min();
        }

        int _xmin;
        int _xmax;
        int _ymin;
        int _ymax;
    };

};

#endif // SEG_STEPSEPARATECLUSTERS_H
