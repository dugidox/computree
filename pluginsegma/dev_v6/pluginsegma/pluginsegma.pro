CT_PREFIX = ../../computreev6

#COMPUTREE += ctlibmetrics

MUST_USE_OPENCV = 1

include($${CT_PREFIX}/plugin_shared.pri)

TARGET = plug_segma

HEADERS += \
    $$CT_LIB_PREFIX/ctlibplugin/pluginentryinterface.h\
    seg_pluginentry.h \
#    metric/seg_metricrastersegma.h \
    step/seg_stepanalyzeandfitcrowns.h \
    step/seg_stepcavityfill.h \
    step/seg_stepcomputewatershed.h \
    step/seg_stepdetectmaxima.h \
    step/seg_stepextractpointsbycluster02.h \
    step/seg_stepfiltermaximabyclusterarea.h \
    step/seg_stepfiltermaximabyexclusionradius.h \
    step/seg_stepgaussianfilter.h \
    step/seg_stepreplacenabyzero.h \
    step/seg_stepseparateclusters.h \
    tools/cavityfillmain.h \
    tools/commontools.h \
    tools/list_croissance.h \
    seg_pluginmanager.h

SOURCES += \
    seg_pluginentry.cpp \
#    metric/seg_metricrastersegma.cpp \
    step/seg_stepanalyzeandfitcrowns.cpp \
    step/seg_stepcavityfill.cpp \
    step/seg_stepcomputewatershed.cpp \
    step/seg_stepdetectmaxima.cpp \
    step/seg_stepextractpointsbycluster02.cpp \
    step/seg_stepfiltermaximabyclusterarea.cpp \
    step/seg_stepfiltermaximabyexclusionradius.cpp \
    step/seg_stepgaussianfilter.cpp \
    step/seg_stepreplacenabyzero.cpp \
    step/seg_stepseparateclusters.cpp \
    tools/cavityfillmain.cpp \
    tools/commontools.cpp \
    tools/list_croissance.cpp \
    seg_pluginmanager.cpp

TRANSLATIONS += languages/pluginsegma_en.ts \
                languages/pluginsegma_fr.ts

