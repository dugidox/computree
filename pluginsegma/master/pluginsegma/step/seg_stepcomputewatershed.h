#ifndef SEG_STEPCOMPUTEWATERSHED_H
#define SEG_STEPCOMPUTEWATERSHED_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_image2d.h"

// Inclusion of auto-indexation system
#include "ct_tools/model/ct_autorenamemodels.h"


class SEG_StepComputeWatershed: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     * 
     * Create a new instance of the step
     * 
     * \param dataInit Step parameters object
     */
    SEG_StepComputeWatershed(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     * 
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     * 
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     * 
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     * 
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

    void computeWatershed(const CT_Image2D<float> *imageIn, CT_Image2D<qint32> *watershedImage, CT_Image2D<float> *mnt);

    void mergeLimitsWithClusters(const CT_Image2D<float> *imageIn, CT_Image2D<qint32> *watershedImage);

protected:

    /*! \brief Input results specification
     * 
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     * 
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     * 
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     * 
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    bool    _mergeLimits;
    double _minHeight;

    // Declaration of autoRenames Variables (groups or items added to In models copies)
    CT_AutoRenameModels    _watershedImage_ModelName;

    static void computeSumAndCount(const CT_Image2D<float> *imageIn, const CT_Image2D<qint32> *watershedImage, float centerVal, QMap<qint32, float> &sumMap, QMap<qint32, int> &countMap, size_t col, size_t lin);
    bool hasExactlyOneNeighbourCluster(size_t &col, size_t &lin, CT_Image2D<qint32> *watershedImage, qint32 &firstVal);
};

#endif // SEG_STEPCOMPUTEWATERSHED_H
