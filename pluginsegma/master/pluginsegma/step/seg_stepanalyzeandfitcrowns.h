#ifndef SEG_STEPANALYZEANDFITCROWNS_H
#define SEG_STEPANALYZEANDFITCROWNS_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_image2d.h"

// Inclusion of auto-indexation system
#include "ct_tools/model/ct_autorenamemodels.h"


class SEG_StepAnalyzeAndFitCrowns: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     * 
     * Create a new instance of the step
     * 
     * \param dataInit Step parameters object
     */
    SEG_StepAnalyzeAndFitCrowns(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     * 
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     * 
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     * 
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     * 
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);


protected:

    /*! \brief Input results specification
     * 
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     * 
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     * 
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     * 
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    double      _scoreThreshold;
    double      _resolutionOTSU;

    double _attMaxHeight;
    double _attMaxHeight_X;
    double _attMaxHeight_Y;
    double _attMinHeight;
    double _attCrownArea;
    double _attCentroidX;
    double _attCentroidY;
    double _attEccentricity;
    double _attSolidity;
    double _attHtoAratio;
    double _attCVmax;
    double _attCentroidShift;
    double _attVextent;
    double _attCrRatio;
    double _attDiameter;
    double _attCircularity;

    // Declaration of autoRenames Variables (groups or items added to In models copies)
    CT_AutoRenameModels    _outClusters_ModelName;
    CT_AutoRenameModels    _outMask_ModelName;
    CT_AutoRenameModels    _outImage_ModelName;

    CT_AutoRenameModels    _attributes_ModelName;
    CT_AutoRenameModels    _attIDCluster_ModelName;
    CT_AutoRenameModels    _attMaxHeight_ModelName;
    CT_AutoRenameModels    _attMaxHeight_X_ModelName;
    CT_AutoRenameModels    _attMaxHeight_Y_ModelName;
    CT_AutoRenameModels    _attMinHeight_ModelName;
    CT_AutoRenameModels    _attCrownArea_ModelName;
    CT_AutoRenameModels    _attCentroidX_ModelName;
    CT_AutoRenameModels    _attCentroidY_ModelName;
    CT_AutoRenameModels    _attEccentricity_ModelName;
    CT_AutoRenameModels    _attSolidity_ModelName;
    CT_AutoRenameModels    _attHtoAratio_ModelName;
    CT_AutoRenameModels    _attCVmax_ModelName;
    CT_AutoRenameModels    _attCentroidShift_ModelName;
    CT_AutoRenameModels    _attVextent_ModelName;
    CT_AutoRenameModels    _attCrRatio_ModelName;
    CT_AutoRenameModels    _attDiameter_ModelName;
    CT_AutoRenameModels    _attCircularity_ModelName;

    CT_AutoRenameModels    _attScoreCentroidShift_ModelName;
    CT_AutoRenameModels    _attScoreEccentricity_ModelName;
    CT_AutoRenameModels    _attScoreSolidity_ModelName;
    CT_AutoRenameModels    _attScoreCVmax_ModelName;
    CT_AutoRenameModels    _attScore_ModelName;
    CT_AutoRenameModels    _attOtsuThreshold_ModelName;


    void computeIndicators(CT_Image2D<float>* heightsOut, CT_Image2D<quint8>* maskOut);
    double standardize(double val, double min, double max, bool invert);
};

#endif // SEG_STEPANALYZEANDFITCROWNS_H
