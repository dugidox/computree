#include "lvox3_stepcreatemesh.h"

//In/Out
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_result/ct_resultgroup.h"

//Tools
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_view/tools/ct_configurablewidgettodialog.h"
#include "mk/tools/lvox3_errorcode.h"
#include "ct_pointcloudindex/abstract/ct_abstractpointcloudindex.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_accessor/ct_pointaccessor.h"

//Drawables
#include "mk/tools/lvox3_gridtype.h"
#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_scene.h"

//Models
#define DEF_SearchInSourceResult      "rs"
#define DEF_SearchInSourceGroup       "gs"
#define DEF_SearchInGroup             "grp"
#define DEF_SearchInGrid              "grid"
#define DEF_SearchInScene             "scn"

#ifdef USE_PCL
    #include "ctlibpcl/tools/ct_pcltools.h"
    #include <pcl/point_types.h>
    #include <pcl/point_cloud.h>

    #include <pcl/common/common.h>
    #include <pcl/features/normal_3d_omp.h>
    #include <pcl/features/boost.h>
    #include <pcl/surface/concave_hull.h>
    #include <pcl/surface/convex_hull.h>
    #include <pcl/filters/crop_hull.h>
    #include <boost/graph/graph_traits.hpp>
    #include <boost/graph/adjacency_list.hpp>
    #include <boost/graph/kruskal_min_spanning_tree.hpp>
    #include <boost/graph/depth_first_search.hpp>
    #include <boost/property_map/property_map.hpp>
#endif

LVOX3_StepCreateMesh::LVOX3_StepCreateMesh(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{

    m_isMeshType = tr("Avec maillage 3D");
    m_isMeshTypeCollection.insert(m_isMeshType,WithMesh);
    m_isMeshTypeCollection.insert(tr("Sans maillage 3D"),NoMesh);
    m_meshType = tr("Maillage convexe");
    m_meshTypeCollection.insert(m_meshType, ConvexMesh);
    m_meshTypeCollection.insert(tr("Maillage concave"), ConcaveMesh);
}

QString LVOX3_StepCreateMesh::getStepDescription() const
{
    return tr("Estimation de fraction d'écart de couronnes");
}

// Step detailed description
QString LVOX3_StepCreateMesh::getStepDetailledDescription() const
{
    return tr("Cette étape prend en entrée un nuage de points et une grille 3D LVOX pour créer une maille convexe visant à extraire les caractéristiques d'une couronne.");
}

CT_VirtualAbstractStep* LVOX3_StepCreateMesh::createNewInstance(CT_StepInitializeData &dataInit)
{
    // Creates an instance of this step
    return new LVOX3_StepCreateMesh(dataInit);
}

// Choose the grid type to give a little more flexibility in grid choice MNT or 3D Grid
void LVOX3_StepCreateMesh::createPreConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPreConfigurationDialog();

    configDialog->addStringChoice(tr("Utilisation de maillage"), "", m_isMeshTypeCollection.keys(), m_isMeshType);
    configDialog->addText(tr("Si vous n'utilisez pas de maillage, vous ne pourrez pas avoir le gap fraction de la couronne."),"", "");
}

void LVOX3_StepCreateMesh::createInResultModelListProtected()
{
    // We must have
    // - at least one grid
    CT_InResultModelGroupToCopy *inResultRefCopy = createNewInResultModelForCopy(DEF_SearchInSourceResult, tr("Grille d'entrée"), "", true);
    inResultRefCopy->setZeroOrMoreRootGroup();
    inResultRefCopy->addGroupModel("", DEF_SearchInSourceGroup, CT_AbstractItemGroup::staticGetType(), tr("Groupe"), "", CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    inResultRefCopy->addItemModel(DEF_SearchInSourceGroup, DEF_SearchInGrid, LVOX3_AbstractGrid3D::staticGetType(), tr("Grille de hits"));
    inResultRefCopy->addItemModel(DEF_SearchInSourceGroup, DEF_SearchInScene, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Scène"));
}

void LVOX3_StepCreateMesh::createPostConfigurationDialog()
{
    //Only adds post configure if you chose the 3D Grid option
    if(m_isMeshTypeCollection.value(m_isMeshType) == WithMesh){
        CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

        configDialog->addStringChoice(tr("Type de maillage 3D"), "", m_meshTypeCollection.keys(), m_meshType);
        configDialog->addText(tr("À noter que le maillage concave prend un temps de calcul considérable en comparaison au maillage convexe"),"", "");
    }
}

void LVOX3_StepCreateMesh::createOutResultModelListProtected()
{
    // create a new OUT result that is a copy of the IN result selected by the user
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_SearchInSourceResult);

    if (!resultModel)
        return;
    if(m_isMeshTypeCollection.value(m_isMeshType) == WithMesh){
        resultModel->addItemModel(DEF_SearchInSourceGroup, _grid_ModelName, new lvox::Grid3Df(), tr("Grille de masque"));
        resultModel->addItemModel(DEF_SearchInSourceGroup, _gridSurface_ModelName, new lvox::Grid3Df(), tr("Grille de surface"));
    }else{
        resultModel->addItemModel(DEF_SearchInSourceGroup, _gridSurface_ModelName, new lvox::Grid3Df(), tr("Grille de surface"));
    }
}

void LVOX3_StepCreateMesh::compute()
{
    CT_ResultGroup* outResult = getOutResultList().first();

    CT_ResultGroupIterator itRCount(outResult, this, DEF_SearchInSourceGroup);

    CT_ResultGroupIterator itR(outResult, this, DEF_SearchInSourceGroup);

    int nFilesProgress = 0; //Used to count the number of files and give estimate progress bar
    int progress = 0;


    while (itRCount.hasNext() && !isStopped())
    {
        CT_StandardItemGroup *group = dynamic_cast<CT_StandardItemGroup*>((CT_AbstractItemGroup*)itRCount.next());
        const CT_AbstractItemDrawableWithPointCloud* inScene = (CT_AbstractItemDrawableWithPointCloud*) group->firstItemByINModelName(this, DEF_SearchInScene);
        if(inScene != NULL){
            nFilesProgress++;
        }
    }

    nFilesProgress *= 2;

    //For every grid in the result
    while (itR.hasNext() && !isStopped())
    {
        CT_StandardItemGroup *group = dynamic_cast<CT_StandardItemGroup*>((CT_AbstractItemGroup*)itR.next());
        const CT_AbstractItemDrawableWithPointCloud* inScene = (CT_AbstractItemDrawableWithPointCloud*) group->firstItemByINModelName(this, DEF_SearchInScene);
        const LVOX3_AbstractGrid3D* inGrid = (const LVOX3_AbstractGrid3D*) group->firstItemByINModelName(this, DEF_SearchInGrid);

        //counters for amount of voxels with/without value in entry and in concave hull
//        size_t inHull = 0;
//        size_t inHullGoodValue = 0;

        if(inScene != NULL && inGrid != NULL){
            //Declaring output grid for the mask grid that will be an out
           lvox::Grid3Df *outGridMask = new lvox::Grid3Df(_grid_ModelName.completeName(), outResult, inGrid->minX(), inGrid->minY(), inGrid->minZ(), inGrid->xdim(), inGrid->ydim(), inGrid->zdim(), inGrid->xresolution(),inGrid->yresolution(),inGrid->zresolution(), lvox::Max_Error_Code, 0);
           lvox::Grid3Df *outGridSurface = new lvox::Grid3Df(_gridSurface_ModelName.completeName(), outResult, inGrid->minX(), inGrid->minY(), inGrid->minZ(), inGrid->xdim(), inGrid->ydim(), inGrid->zdim(), inGrid->xresolution(),inGrid->yresolution(),inGrid->zresolution(), lvox::Max_Error_Code, 0);
            #ifdef USE_PCL
                //If you're using a mesh, you calculate the mesh and the surface/mask grid
                if(m_isMeshTypeCollection.value(m_isMeshType) == WithMesh){
                    boost::shared_ptr<CT_PCLCloud> pclCloud = CT_PCLTools::staticConvertToPCLCloud(inScene->getPointCloudIndexRegistered());
                    pcl::PointCloud<pcl::PointXYZ>::Ptr pclCloudGrid(new pcl::PointCloud<pcl::PointXYZ>());

                    //Choice between convex or concave
                    pcl::ConvexHull<CT_PCLPoint> convexHull;
                    pcl::ConcaveHull<CT_PCLPoint> concaveHull;

                    std::vector<pcl::Vertices> polygons;

                    pcl::PointCloud<pcl::PointXYZ>::Ptr surface_hull(new pcl::PointCloud<pcl::PointXYZ>());
                    if(m_meshTypeCollection.value(m_meshType) == ConvexMesh){
                        convexHull.setDimension(3);
                        convexHull.setInputCloud(pclCloud);
                        convexHull.reconstruct(*surface_hull, polygons);
                    }else{
                        concaveHull.setDimension(3);
                        concaveHull.setAlpha(0.1);
                        concaveHull.setInputCloud(pclCloud);
                        concaveHull.reconstruct(*surface_hull, polygons);
                    }

                    //Percentage update after hull construction
                    progress++;
                    setProgress((100.00/nFilesProgress)*progress);

                    qDebug()<<"filling PCL point cloud from inGrid center coords data.";
                    //Iterates through the 3d ingrid
                    for(size_t col = 0;col <inGrid->xdim() && (!isStopped());col++){
                        for(size_t lin = 0;lin <inGrid->ydim() && (!isStopped());lin++){
                            for(size_t level = 0;level <inGrid->zdim() && (!isStopped());level++){
                                Eigen::Vector3d centerCoordVoxel;

                                size_t index;
                                inGrid->index(col, lin, level, index);
                                inGrid->getCellCenterCoordinates(index,centerCoordVoxel);
                                float x = centerCoordVoxel.x();
                                float y = centerCoordVoxel.y();
                                float z = centerCoordVoxel.z();

                                pclCloudGrid->push_back(pcl::PointXYZ(x,y,z));
                            }
                        }
                    }


                    qDebug()<<"point cloud has:"<< pclCloudGrid->points.size() << " points data before hull filtering";
                    pcl::CropHull<CT_PCLPoint> cropHull;

                    pcl::PointCloud<pcl::PointXYZ>::Ptr pointsInHull(new pcl::PointCloud<pcl::PointXYZ>());
                    cropHull.setDim(3);
                    cropHull.setInputCloud(pclCloudGrid);
                    cropHull.setHullIndices(polygons);
                    cropHull.setHullCloud(surface_hull);
                    cropHull.filter(*pointsInHull);
                    qDebug()<<"point cloud has:"<< pointsInHull->points.size() << " points data after hull filtering";

                    //Percentage update after grid center points filtering in hull
                    progress++;
                    setProgress((100.00/nFilesProgress)*progress);

                    group->addItemDrawable(outGridMask);
                    //Iterates through the 3d ingrid
                    for(size_t pclIndex = 0;pclIndex<pointsInHull->points.size();pclIndex++){
                        outGridMask->setValueAtXYZ(pointsInHull->at(pclIndex).x,pointsInHull->at(pclIndex).y,pointsInHull->at(pclIndex).z,1.0);
                    }
                    outGridMask->computeMinMax();
                }//ends of with mesh loop

                //grid surface for outer crown exclusion(with mesh) and inner crown exclusion
                group->addItemDrawable(outGridSurface);
                //LEFT TO RIGHT
                //Iterates through the 3d inGrid to exclude just the surface (1 for surface and 2 for the inside of the crown)
                for(size_t col = 0;col <inGrid->zdim() && (!isStopped());col++){
                    for(size_t lin = 0;lin <inGrid->ydim() && (!isStopped());lin++){
                        bool found = false;
                        for(size_t level = 0;level <inGrid->xdim() && (!isStopped());level++){
                            size_t index;
                            double value;
                            inGrid->index(level, lin, col , index); //going left to right (instead of down to up)
                            value = inGrid->valueAtIndexAsDouble(index);
                            if(!found){
                                //If mask grid volex is inside of the concave hull, then look at the value in the entry grid
                                if(value > 0){
                                    outGridSurface->setValueAtIndex(index,1.0);
                                    found = true;
                                }
                            }else{
                                outGridSurface->setValueAtIndex(index,1.0);
                            }
                        }
                    }
                }

                //RIGHT TO LEFT
                //Iterates through the 3d outGridValueLimit to exclude only the surface
                for(size_t col = 0;col <inGrid->zdim() && (!isStopped());col++){
                    for(size_t lin = 0;lin <inGrid->ydim() && (!isStopped());lin++){
                        bool found = false;
                        for(double level = inGrid->xdim()-1.0;level >= 0 && (!isStopped());level--){
                            size_t index;
                            size_t indexInternal;
                            double value;
                            inGrid->index(level, lin, col , index); //going right to left (instead of down to up)
                            value = inGrid->valueAtIndexAsDouble(index);
                            if(!found){
                                //adds first voxel with material on the edge of the grid with material
                                if(value > 0){
                                    outGridSurface->setValueAtIndex(index,1.0);
                                    found = true;
                                }else{
                                    outGridSurface->setValueAtIndex(index,0.0);
                                }
                            }else{
                                if(level > 0){
                                    outGridSurface->index(level-1.0, lin, col , indexInternal);
                                    if(outGridSurface->valueAtIndex(indexInternal) != 0){
                                        outGridSurface->setValueAtIndex(index,2.0);
                                    }
                                }
                            }
                        }
                    }
                }

                //UP TO DOWN (no calculation for DOWN to UP because it gives voxels not affected by photosynthesis)
                //Iterates through the 3d outGridValueLimit to exclude only the surface
                for(size_t col = 0;col <inGrid->xdim() && (!isStopped());col++){
                    for(size_t lin = 0;lin <inGrid->ydim() && (!isStopped());lin++){
                        bool found = false;
                        for(double level = inGrid->zdim()-1.0;level >= 0 && (!isStopped());level--){
                            size_t index;
                            double value;
                            inGrid->index(col, lin, level , index); //going up to down (instead of down to up)
                            value = inGrid->valueAtIndexAsDouble(index);
                            if(!found){
                                //adds first voxel with material on the edge of the grid with material
                                if(value > 0 || outGridSurface->valueAtIndexAsDouble(index) == 2){
                                    outGridSurface->setValueAtIndex(index,1.0);
                                    found = true;
                                }
                            }
                        }
                    }
                }

                //FRONT TO BACK
                //Iterates through the 3d outGridValueLimit to exclude only the surface
                for(size_t col = 0;col <inGrid->xdim() && (!isStopped());col++){
                    for(size_t lin = 0;lin <inGrid->zdim() && (!isStopped());lin++){
                        bool found = false;
                        for(size_t level = 0;level <inGrid->ydim() && (!isStopped());level++){
                            size_t index;
                            //size_t indexInternal;
                            double value;
                            inGrid->index(col, level, lin , index);
                            value = inGrid->valueAtIndexAsDouble(index);
                            //adds first voxel with material on the edge of the grid with material
                            if(!found){
                                if(value > 0 || outGridSurface->valueAtIndexAsDouble(index) == 2){
                                    outGridSurface->setValueAtIndex(index,1.0);
                                    found = true;
                                }
                            }
                        }
                    }
                }
                //BACK TO FRONT
                //Iterates through the 3d outGridValueLimit to exclude only the surface
                for(size_t col = 0;col <inGrid->xdim() && (!isStopped());col++){
                    for(size_t lin = 0;lin <inGrid->zdim() && (!isStopped());lin++){
                        bool found = false;
                        for(double level = inGrid->ydim()-1.0;level >= 0 && (!isStopped());level--){
                            size_t index;
                            double value;
                            inGrid->index(col, level, lin , index); //going up to down (instead of down to up)
                            value = inGrid->valueAtIndexAsDouble(index);
                            if(!found){
                                //adds first voxel with material on the edge of the grid with material
                                if(value > 0 || outGridSurface->valueAtIndexAsDouble(index) == 2){
                                    outGridSurface->setValueAtIndex(index,1.0);
                                    found = true;
                                }
                            }
                        }
                    }
                }
                outGridSurface->computeMinMax();

            #endif
        }
    }
}


