#ifndef LVOX3_STEPUNIFYSCENE_H
#define LVOX3_STEPUNIFYSCENE_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_tools/model/ct_autorenamemodels.h"

#include "../tools/3dgrid/abstract/lvox3_abstractgrid3d.h"

//This step aims to unify the scene perspectives to be able to calculate a single DTM
//for the scene(multiple perspectives create multiple dtms, which makes it useful for full scene data computing)
//To note that this step will exclusively be useful for MNT calculation and scene manipulation, calculating grids with this unified scene
//will most likely cause inaccuraries or completely wrong data

class LVOX3_StepUnifyScene : public CT_AbstractStep
{
    Q_OBJECT
public:
    LVOX3_StepUnifyScene(CT_StepInitializeData &dataInit);

    /**
     * @brief Return a short description of what do this class
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;
    /**
     * @brief Return a new empty instance of this class
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);
protected:
    /**
     * @brief This method defines what kind of input the step can accept
     */
    void createInResultModelListProtected();

    /**
     * @brief This method defines what kind of output the step produces
     */
    void createOutResultModelListProtected();

    /**
     * @brief This method do the job
     */
    void compute();
private:

    CT_AutoRenameModels _scene_ModelName;
    CT_AutoRenameModels _outGroupModelName;

};

#endif // LVOX3_STEPUNIFYSCENE_H
