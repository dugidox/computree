#include "lvox3_stepunifyscene.h"

//In/Out
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_result/ct_resultgroup.h"

//Tools
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_view/tools/ct_configurablewidgettodialog.h"
#include "mk/tools/lvox3_errorcode.h"
#include "ct_pointcloudindex/abstract/ct_abstractpointcloudindex.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_accessor/ct_pointaccessor.h"

//Drawables
#include "mk/tools/lvox3_gridtype.h"
#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_scene.h"

//Models
//In
#define DEF_SearchInSourceResult      "rs"
#define DEF_SearchInSourceGroup       "gs"
#define DEF_SearchInGroup             "grp"
#define DEF_SearchInScene             "grid"

//Out
#define DEF_resultOut_mergedScene     "rmsce"
#define DEF_groupOut_gm               "gm"
#define DEF_itemOut_mergedScene       "scm"

LVOX3_StepUnifyScene::LVOX3_StepUnifyScene(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{

}

QString LVOX3_StepUnifyScene::getStepDescription() const
{
    return tr("Unification des scènes pour le calcul du DTM");
}

// Step detailed description
QString LVOX3_StepUnifyScene::getStepDetailledDescription() const
{
    return tr("Cette étape permet d'extraire une grille 3D circulaire a partir d'une grille d'entrée, le but étant d'offrir un outil se rapprochant de la réalité forestière.");
}

CT_VirtualAbstractStep* LVOX3_StepUnifyScene::createNewInstance(CT_StepInitializeData &dataInit)
{
    // Creates an instance of this step
    return new LVOX3_StepUnifyScene(dataInit);
}

void LVOX3_StepUnifyScene::createInResultModelListProtected()
{
    // We must have
    // - at least one grid
    CT_InResultModelGroupToCopy *inResultRefCopy = createNewInResultModelForCopy(DEF_SearchInSourceResult, tr("Groupe de scènes"), "", true);
    inResultRefCopy->setZeroOrMoreRootGroup();
    inResultRefCopy->addGroupModel("", DEF_SearchInSourceGroup, CT_AbstractItemGroup::staticGetType(), tr("Groupe de référence"), "", CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    inResultRefCopy->addItemModel(DEF_SearchInSourceGroup, DEF_SearchInScene, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Scène"));
}

void LVOX3_StepUnifyScene::createOutResultModelListProtected()
{
    // create a new OUT result that is a copy of the IN result selected by the user
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_SearchInSourceResult);

    if (!resultModel)
        return;

    //Result for the merged scene, makes the two results clear cut for which can be used for further steps and which can't
    CT_OutResultModelGroup *resultMergedModel = createNewOutResultModel(DEF_resultOut_mergedScene, tr("MergedScene"));

    resultMergedModel->setRootGroup(DEF_groupOut_gm);
    resultMergedModel->addItemModel(DEF_groupOut_gm, DEF_itemOut_mergedScene, new CT_Scene(), tr("MergedScene"));

}

void LVOX3_StepUnifyScene::compute()
{
    CT_ResultGroup* outResult = getOutResultList().first();

    CT_ResultGroup* resultOut_mergedScene = getOutResultList().at(1);

    CT_ResultGroupIterator itRCount(outResult, this, DEF_SearchInSourceGroup);

    //Out point cloud related variables
    double xmin = std::numeric_limits<double>::max();
    double ymin = std::numeric_limits<double>::max();
    double zmin = std::numeric_limits<double>::max();

    double xmax = -std::numeric_limits<double>::max();
    double ymax = -std::numeric_limits<double>::max();
    double zmax = -std::numeric_limits<double>::max();
    QList<CT_PCIR> pcirlist;

    //Counts the number of files (There has to be a more efficient way) used for progress bar
    while(itRCount.hasNext()){
        CT_StandardItemGroup *groupCount = dynamic_cast<CT_StandardItemGroup*>((CT_AbstractItemGroup*)itRCount.next());

        const CT_Scene* inSceneCount = (const CT_Scene*) groupCount->firstItemByINModelName(this, DEF_SearchInScene);

        //If the scene has data
        if (inSceneCount != NULL){
            pcirlist.append(inSceneCount->getPointCloudIndexRegistered()); //Add to the list that needs to be merged

            Eigen::Vector3d min, max;
            inSceneCount->getBoundingBox(min, max); //reference change of min and max
            if (min(0) < xmin) {xmin = min(0);}
            if (max(0) > xmax) {xmax = max(0);}
            if (min(1) < ymin) {ymin = min(1);}
            if (max(1) > ymax) {ymax = max(1);}
            if (min(2) < zmin) {zmin = min(2);}
            if (max(2) > zmax) {zmax = max(2);}
        }
    }

    //Assign the merged scene with a new group and add it to the model viewer
    CT_StandardItemGroup* groupOut_mergedScene = new CT_StandardItemGroup(DEF_groupOut_gm, resultOut_mergedScene);
    //Merges the continuous point clouds(shouldnt be a problem as the point cloud are load sequentially)
    CT_Scene *mergedScene = new CT_Scene(DEF_itemOut_mergedScene, resultOut_mergedScene, PS_REPOSITORY->mergePointCloudContiguous(pcirlist));
    //Set's the bounding box based on prior looping
    mergedScene->setBoundingBox(xmin,ymin,zmin,xmax,ymax,zmax);
    //Settings for visualizing
    groupOut_mergedScene->addItemDrawable(mergedScene);
    resultOut_mergedScene->addGroup(groupOut_mergedScene);
}
