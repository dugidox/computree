#ifndef GEN_STEPGENERATESCANNER_H
#define GEN_STEPGENERATESCANNER_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGenerateScanner : public CT_AbstractStepCanBeAddedFirst
{

    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    GEN_StepGenerateScanner();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
    double    _posX;    /*!<  */
    double    _posY;    /*!<  */
    double    _posZ;    /*!<  */
    double    _initTheta;    /*!<  */
    double    _initPhi;    /*!<  */
    double    _hFov;    /*!<  */
    double    _vFov;    /*!<  */
    double    _hRes;    /*!<  */
    double    _vRes;    /*!<  */
    double    _clockWise;    /*!<  */
};

#endif // GEN_STEPGENERATESCANNER_H
