#include "gen_stepgeneratecylindermesh.h"

#include "ct_itemdrawable/ct_meshmodel.h"
#include "ct_mesh/ct_mesh.h"

#include "ct_itemdrawable/ct_standarditemgroup.h"

// Alias for indexing out models

GEN_StepGenerateCylinderMesh::GEN_StepGenerateCylinderMesh() : CT_AbstractStepCanBeAddedFirst()
{
    _height = 1;
    _radius = 1;
    _slices = 30;
    _x = _y = _z = 0;
}

QString GEN_StepGenerateCylinderMesh::description() const
{
    return tr("Créer un Maillage Cylindrique");
}

CT_VirtualAbstractStep* GEN_StepGenerateCylinderMesh::createNewInstance()
{
    return new GEN_StepGenerateCylinderMesh();
}

//////////////////// PROTECTED METHODS //////////////////

void GEN_StepGenerateCylinderMesh::declareInputModels(CT_StepInModelStructureManager& manager)
{
    // No in result is needed
    setNotNeedInputResult();
}

void GEN_StepGenerateCylinderMesh::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultMesh, tr("Generated Mesh"));

    resultModel->setRootGroup(DEF_groupOut_groupMesh, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupMesh, DEF_itemOut_mesh, new CT_MeshModel(), tr("Generated Mesh"));
}

void GEN_StepGenerateCylinderMesh::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{


    postInputConfigDialog->addDouble(tr("Height"), "", 0.0001, std::numeric_limits<double>::max(), 4, _height);
    postInputConfigDialog->addDouble(tr("Radius"), "", 0.0001, std::numeric_limits<double>::max(), 4, _radius);
    postInputConfigDialog->addInt(tr("Slices"), "", 4, 99999, _slices);
    postInputConfigDialog->addDouble("X", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _x);
    postInputConfigDialog->addDouble("Y", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _y);
    postInputConfigDialog->addDouble("Z", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _z);

}

void GEN_StepGenerateCylinderMesh::compute()
{
    CT_ResultGroup* resultOut_resultScene = getOutResultList().first();

    // ------------------------------
    // Create OUT groups and items
    CT_StandardItemGroup* groupOut_groupScene = new CT_StandardItemGroup(DEF_groupOut_groupMesh, resultOut_resultScene);

    CT_Mesh *mesh = new CT_Mesh();
    mesh->createCylinder(_radius, _height, _slices, _x, _y, _z);

    CT_MeshModel* itemOut_scene = new CT_MeshModel(DEF_itemOut_mesh, resultOut_resultScene, mesh);
    itemOut_scene->setBoundingBox(_x, (-_radius)+_y, (-_radius)+_z, _height+_x, _radius+_y, _radius+_z);

    groupOut_groupScene->addItemDrawable(itemOut_scene);
    resultOut_resultScene->addGroup(groupOut_groupScene);
}
