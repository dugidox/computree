#ifndef GEN_STEPGENERATERASTER3D_H
#define GEN_STEPGENERATERASTER3D_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGenerateRaster3DFloat  : public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    GEN_StepGenerateRaster3DFloat();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
    double    _res;    /*!<  */
    double    _botX;    /*!<  */
    double    _botY;    /*!<  */
    double    _botZ;    /*!<  */
    double    _topX;    /*!<  */
    double    _topY;    /*!<  */
    double    _topZ;    /*!<  */
    double    _valMin;    /*!<  */
    double    _valMax;    /*!<  */
};

#endif // GEN_STEPGENERATERASTER3D_H
