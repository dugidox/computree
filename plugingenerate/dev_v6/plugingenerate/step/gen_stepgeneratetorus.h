#ifndef GEN_STEPGENERATETORUS_H
#define GEN_STEPGENERATETORUS_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGenerateTorus : public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    GEN_StepGenerateTorus();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private :

    // Step parameters
    double    _centerX;
    double    _centerY;
    double    _centerZ;
    double    _radiusCircle;
    double    _distanceToCenter;
    double    _alphaMin;
    double    _alphaMax;
    double    _betaMin;
    double    _betaMax;
    double    _resAlpha;
    double    _resBeta;
    double    _noiseAlpha;
    double    _noiseBeta;
    double    _noiseRadiusCircle;
    double    _noiseDistanceToCenter;
};

#endif // GEN_STEPGENERATETORUS_H
