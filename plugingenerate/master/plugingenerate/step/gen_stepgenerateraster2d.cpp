#include "step/gen_stepgenerateraster2d.h"

#include <limits>
#include <ctime>

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

#include "ct_result/ct_resultgroup.h"

// Alias for indexing out models
#define DEF_resultOut_resultRaster2dFloat "Generated2DRaster"
#define DEF_groupOut_groupRaster2DFloat "Group"
#define DEF_itemOut_itemRaster2dFloat "Rasters2D"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_grid2dxy.h"
typedef CT_Grid2DXY<float> CT_Grid2DFloat;

GEN_StepGenerateRaster2DFloat::GEN_StepGenerateRaster2DFloat(CT_StepInitializeData &dataInit) : CT_AbstractStepCanBeAddedFirst(dataInit)
{
    _height = 0;
    _botX = -10;
    _botY = -10;
    _topX = 10;
    _topY = 10;
    _res = 1;
    _valMin = 0;
    _valMax = 100;
}

// Step description (tooltip of contextual menu)
QString GEN_StepGenerateRaster2DFloat::getStepDescription() const
{
    return tr("Créer un Raster (2D)");
}

// Step copy method
CT_VirtualAbstractStep* GEN_StepGenerateRaster2DFloat::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new GEN_StepGenerateRaster2DFloat(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void GEN_StepGenerateRaster2DFloat::createInResultModelListProtected()
{
    // No in result is needed
    setNotNeedInputResult();
}

// Creation and affiliation of OUT models
void GEN_StepGenerateRaster2DFloat::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultRaster2dFloat, tr("Generated Item"));

    resultModel->setRootGroup(DEF_groupOut_groupRaster2DFloat, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupRaster2DFloat, DEF_itemOut_itemRaster2dFloat, new CT_Grid2DFloat(), tr("Generated 2D Raster"));
}

// Semi-automatic creation of step parameters DialogBox
void GEN_StepGenerateRaster2DFloat::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Raster height"), "", -1e+10, 1e+10, 4, _height, 0);
    configDialog->addDouble(tr("Resolution"), "", 0.0001, 1e+10, 4, _res, 0);
    configDialog->addText(tr("Bottom left point"), "", "");
    configDialog->addDouble("X", "", -1e+10, 1e+10, 4, _botX, 0);
    configDialog->addDouble("Y", "", -1e+10, 1e+10, 4, _botY, 0);
    configDialog->addText(tr("Top right point"), "", "");
    configDialog->addDouble("X", "", -1e+10, 1e+10, 4, _topX, 0);
    configDialog->addDouble("Y", "", -1e+10, 1e+10, 4, _topY, 0);
    configDialog->addText(tr("Value range"), "", "");
    configDialog->addDouble(tr("Min"), "", -1e+10, 1e+10, 4, _valMin, 0);
    configDialog->addDouble(tr("Max"), "", -1e+10, 1e+10, 4, _valMax, 0);
}

void GEN_StepGenerateRaster2DFloat::compute()
{
    CT_ResultGroup* resultOut_resultRaster2dFloat = getOutResultList().first();

    CT_Grid2DFloat* itemOut_itemRaster2dFloat = CT_Grid2DFloat::createGrid2DXYFromXYCoords(DEF_itemOut_itemRaster2dFloat, resultOut_resultRaster2dFloat, _botX, _botY, _topX, _topY, _res, _height, -999, 0);

    // On initialise l'aleatoire
    srand( time(0) );

    size_t nbCellsX = itemOut_itemRaster2dFloat->colDim();
    size_t nbCellsY = itemOut_itemRaster2dFloat->linDim();

    for ( size_t i = 0 ; (i < nbCellsX) && !isStopped() ; i++ )
    {
        for ( size_t j = 0 ; (j < nbCellsY) && !isStopped() ; j++ )
        {
            itemOut_itemRaster2dFloat->setValue(i,j, _valMin + ( ((double)rand()/RAND_MAX) * (_valMax - _valMin) ) );
        }
    }

    if(!isStopped()) {
        itemOut_itemRaster2dFloat->computeMinMax();
        CT_StandardItemGroup* groupOut_groupRaster2DFloat = new CT_StandardItemGroup(DEF_groupOut_groupRaster2DFloat, resultOut_resultRaster2dFloat);

        groupOut_groupRaster2DFloat->addItemDrawable(itemOut_itemRaster2dFloat);
        resultOut_resultRaster2dFloat->addGroup(groupOut_groupRaster2DFloat);
    } else {
        delete itemOut_itemRaster2dFloat;
    }
}
