#include "gen_stepgeneratecylindermesh.h"

#include "ct_itemdrawable/ct_meshmodel.h"
#include "ct_mesh/ct_mesh.h"

#include "ct_itemdrawable/ct_standarditemgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_result/ct_resultgroup.h"

// Alias for indexing out models
#define DEF_resultOut_resultMesh "resultMesh"
#define DEF_groupOut_groupMesh "groupMesh"
#define DEF_itemOut_mesh "mesh"

GEN_StepGenerateCylinderMesh::GEN_StepGenerateCylinderMesh(CT_StepInitializeData &dataInit) : CT_AbstractStepCanBeAddedFirst(dataInit)
{
    _height = 1;
    _radius = 1;
    _slices = 30;
    _x = _y = _z = 0;
}

QString GEN_StepGenerateCylinderMesh::getStepDescription() const
{
    return tr("Créer un Maillage Cylindrique");
}

CT_VirtualAbstractStep* GEN_StepGenerateCylinderMesh::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new GEN_StepGenerateCylinderMesh(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void GEN_StepGenerateCylinderMesh::createInResultModelListProtected()
{
    // No in result is needed
    setNotNeedInputResult();
}

// Creation and affiliation of OUT models
void GEN_StepGenerateCylinderMesh::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultMesh, tr("Generated Mesh"));

    resultModel->setRootGroup(DEF_groupOut_groupMesh, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupMesh, DEF_itemOut_mesh, new CT_MeshModel(), tr("Generated Mesh"));
}

// Semi-automatic creation of step parameters DialogBox
void GEN_StepGenerateCylinderMesh::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Height"), "", 0.0001, 1e+10, 4, _height);
    configDialog->addDouble(tr("Radius"), "", 0.0001, 1e+10, 4, _radius);
    configDialog->addInt(tr("Slices"), "", 4, 99999, _slices);
    configDialog->addDouble("X", "", -1e+10, 1e+10, 4, _x);
    configDialog->addDouble("Y", "", -1e+10, 1e+10, 4, _y);
    configDialog->addDouble("Z", "", -1e+10, 1e+10, 4, _z);

}

void GEN_StepGenerateCylinderMesh::compute()
{
    CT_ResultGroup* resultOut_resultScene = getOutResultList().first();

    // ------------------------------
    // Create OUT groups and items
    CT_StandardItemGroup* groupOut_groupScene = new CT_StandardItemGroup(DEF_groupOut_groupMesh, resultOut_resultScene);

    CT_Mesh *mesh = new CT_Mesh();
    mesh->createCylinder(_radius, _height, _slices, _x, _y, _z);

    CT_MeshModel* itemOut_scene = new CT_MeshModel(DEF_itemOut_mesh, resultOut_resultScene, mesh);
    itemOut_scene->setBoundingBox(_x, (-_radius)+_y, (-_radius)+_z, _height+_x, _radius+_y, _radius+_z);

    groupOut_groupScene->addItemDrawable(itemOut_scene);
    resultOut_resultScene->addGroup(groupOut_groupScene);
}
