#include "lb_reader_bil3d.h"
#include <QFile>
#include <QIODevice>
#include <QTextStream>

#include <QDebug>

#include "ct_global/ct_context.h"
#include "ct_point.h"
#include "ct_itemdrawable/ct_circle.h"

#include <limits>
#include <QFileInfo>
#include <QDir>

LB_Reader_Bil3D::LB_Reader_Bil3D() : CT_AbstractReader()
{
    _fileName_moe = "";
    _fileName_bil = "";
    _fileName_ext = "";
    _fileName_ama = "";
    _fileName_mes = "";

    _rootGroupModel = NULL;
    _sceneOutModel = NULL;
}

bool LB_Reader_Bil3D::setFilePath(const QString &filepath)
{
    CT_AbstractReader::setFilePath(filepath);

    QFileInfo info(filepath);

    QString dir = info.absoluteDir().absolutePath();
    QString fileName = info.baseName();

    QString fileNameEnd = fileName.mid(fileName.size() - 4, 4).toLower();

    if (    (fileNameEnd == "_moe") ||
            (fileNameEnd == "_bil") ||
            (fileNameEnd == "_ext") ||
            (fileNameEnd == "_ama") ||
            (fileNameEnd == "_mes"))
    {
        fileName = fileName.mid(0, fileName.size() - 4);
    }

    _fileName_moe = QString("%1/%2_moe.mob").arg(dir).arg(fileName);
    _fileName_bil = QString("%1/%2_bil.mob").arg(dir).arg(fileName);
    _fileName_ext = QString("%1/%2_ext.mob").arg(dir).arg(fileName);
    _fileName_ama = QString("%1/%2_ama.mob").arg(dir).arg(fileName);
    _fileName_mes = QString("%1/%2_mes.mob").arg(dir).arg(fileName);

    _moe = QFile::exists(_fileName_moe);
    _bil = QFile::exists(_fileName_bil);
    _ext = QFile::exists(_fileName_ext);
    _ama = QFile::exists(_fileName_ama);
    _mes = QFile::exists(_fileName_mes);;

    if (!_moe && !_bil && !_ama && !_mes) {return false;}

    return true;
}

CT_AbstractReader* LB_Reader_Bil3D::copy() const
{
    return new LB_Reader_Bil3D();
}

void LB_Reader_Bil3D::protectedInit()
{
    addNewReadableFormat(FileFormat("mob", tr("Fichiers Bil 3D")));
}

void LB_Reader_Bil3D::protectedCreateOutItemDrawableModelList()
{
    CT_AbstractReader::protectedCreateOutItemDrawableModelList();

    _rootGroupModel = new CT_OutStdGroupModel(DEF_PB_Reader_Bil3D_rootGroup, new CT_StandardItemGroup(), tr("Billon"));
    _sceneOutModel = new CT_OutStdSingularItemModel(DEF_PB_Reader_Bil3D_sceneOut, new CT_Scene(), tr("Enveloppe"));
    _elementGroupModel = new CT_OutStdGroupModel(DEF_PB_Reader_Bil3D_elementGroup, new CT_StandardItemGroup(), tr("Element"));
    _elementCircleGroupModel = new CT_OutStdGroupModel(DEF_PB_Reader_Bil3D_elementCircleGroup, new CT_StandardItemGroup(), tr("Cercle (grp)"));
    _elementCircleModel = new CT_OutStdSingularItemModel(DEF_PB_Reader_Bil3D_elementCircle, new CT_Circle(), tr("Cercle"));

    _rootGroupModel->addItem(_sceneOutModel);
    _rootGroupModel->addGroup(_elementGroupModel);
    _elementGroupModel->addGroup(_elementCircleGroupModel);
    _elementCircleGroupModel->addItem(_elementCircleModel);

    addOutGroupModel(_rootGroupModel);
}

bool LB_Reader_Bil3D::protectedReadFile()
{
    CT_StandardItemGroup *rootGroup = new CT_StandardItemGroup(_rootGroupModel, NULL);

    // Read the _bil file
    if(QFile::exists(_fileName_bil))
    {
        QFile f(_fileName_bil);
        if (f.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream stream(&f);
            QString line;
            CT_Point pReaded;
            bool pointsReading = false;
            size_t i = 0;

            CT_AbstractUndefinedSizePointCloud* pointCloud = PS_REPOSITORY->createNewUndefinedSizePointCloud();

            double xmin = std::numeric_limits<double>::max();
            double ymin = std::numeric_limits<double>::max();
            double zmin = std::numeric_limits<double>::max();

            double xmax = -std::numeric_limits<double>::max();
            double ymax = -std::numeric_limits<double>::max();
            double zmax = -std::numeric_limits<double>::max();

            while(!stream.atEnd() && !isStopped())
            {
                line = stream.readLine();
                i++;

                if (line.contains(QString("Fin contours")))
                {
                    pointsReading = false;
                } else if (line.contains(QString("Contours"), Qt::CaseInsensitive))
                {
                    pointsReading = true;
                } else if (pointsReading)
                {
                    QStringList values = line.split(",", QString::SkipEmptyParts);

                    if (values.size() >= 3)
                    {
                        bool okx, oky, okz;

                        double x = values.at(0).trimmed().toDouble(&okx);
                        double y = values.at(1).trimmed().toDouble(&oky);
                        double z = values.at(2).trimmed().toDouble(&okz);

                        if (okx && oky && okz)
                        {
                            if (x<xmin) {xmin = x;}
                            if (x>xmax) {xmax = x;}
                            if (y<ymin) {ymin = y;}
                            if (y>ymax) {ymax = y;}
                            if (z<zmin) {zmin = z;}
                            if (z>zmax) {zmax = z;}

                            pReaded(0) = x;
                            pReaded(1) = y;
                            pReaded(2) = z;

                            pointCloud->addPoint(pReaded);
                        } else {
                            PS_LOG->addMessage(LogInterface::info, LogInterface::reader, QString("LB_Reader_Bil3D : Erreur de lecture ligne %1 du fichier %2").arg(i).arg(_fileName_bil));
                        }
                    }
                }
            }
            if (pointCloud->size() > 0)
            {
                CT_NMPCIR pcir = PS_REPOSITORY->registerUndefinedSizePointCloud(pointCloud);

                CT_Scene *scene = new CT_Scene(_sceneOutModel, NULL, pcir);
                scene->setBoundingBox(xmin, ymin, zmin, xmax, ymax, zmax);
                rootGroup->addItemDrawable(scene);
            }
            else
            {
                delete pointCloud;
            }

            f.close();
        }
    }



    CT_StandardItemGroup *elementGroup = NULL;
    Eigen::Vector3d lastCenter;

    // Read the _mes file
    if(QFile::exists(_fileName_mes))
    {
        QFile f(_fileName_mes);
        if (f.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream stream(&f);
            QString line;
            bool pointsReading = false;
            size_t i = 0;

            while(!stream.atEnd() && !isStopped())
            {
                line = stream.readLine();
                i++;

                if (line.contains(QString("Fin contours")))
                {
                    pointsReading = false;
                } else if (line.contains(QString("Contours"), Qt::CaseInsensitive))
                {
                    pointsReading = true;

                    if (elementGroup != NULL) {rootGroup->addGroup(elementGroup);}
                    elementGroup = new CT_StandardItemGroup(_elementGroupModel, NULL);

                    lastCenter = Eigen::Vector3d(NAN, NAN, NAN);

                } else if (pointsReading)
                {
                    QStringList values = line.split(",", QString::SkipEmptyParts);

                    if (values.size() >= 4)
                    {
                        bool okx, oky, okz, okr;
                        Eigen::Vector3d center;

                        center(0) = values.at(0).trimmed().toDouble(&okx);
                        center(1) = values.at(1).trimmed().toDouble(&oky);
                        center(2) = values.at(2).trimmed().toDouble(&okz);
                        double r = values.at(3).trimmed().toDouble(&okr);

                        Eigen::Vector3d direction = center - lastCenter;
                        if (std::isnan(lastCenter(0)))
                        {
                            direction = Eigen::Vector3d(0,0,1);
                        }

                        if (okx && oky && okz && okr)
                        {
                            CT_StandardItemGroup *elementCircleGroup = new CT_StandardItemGroup(_elementCircleGroupModel, NULL);
                            elementGroup->addGroup(elementCircleGroup);

                            CT_CircleData *circleData = new CT_CircleData(center, direction, r);
                            CT_Circle *circle = new CT_Circle(_elementCircleModel, NULL, circleData);
                            elementCircleGroup->addItemDrawable(circle);

                            lastCenter = center;
                        } else {
                            PS_LOG->addMessage(LogInterface::info, LogInterface::reader, QString("LB_Reader_Bil3D : Erreur de lecture ligne %1 du fichier %2").arg(i).arg(_fileName_mes));
                        }
                    }
                }
            }
            f.close();
        }
    }



    addOutGroup(DEF_PB_Reader_Bil3D_rootGroup, rootGroup);
    return true;
}
