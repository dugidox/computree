/****************************************************************************

 Copyright (C) 2014 the Institut National de Recherches Agronomique (INRA), France

 All rights reserved.

 Contact : constant@inra.fr

 Developers : Thiéry Constant (INRA/LERFOB)

 This file is part of PluginLERFOB library 1.0

 PluginLERFOB is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginLERFOB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginLERFOB.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
 **************************************************************************************/

#include "lb_stepcharacterisecurvature.h"
#include "ct_global/ct_context.h"
#include <QDebug>
#include <vector>

#include "ct_itemdrawable/ct_line.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_itemdrawable/ct_attributeslist.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "ct_math/ct_mathpoint.h"

// Alias for indexing models in models
#define DEFin_resSections "resSections"
#define DEFin_Section "Section"
#define DEFin_Cluster "Cluster"
#define DEFin_Circle "Circle"

// Alias for indexing models out models
#define DEFout_Sections "ResLogsCurv"
#define DEFout_Section "Logs"
#define DEFout_Clusters "Clus"
#define DEFout_Curvature "Curv"
#define DEFout_tan "tan"
#define DEFout_nor "nor"
#define DEFout_bin "bin"
#define DEFout_osc "osc"

#define DEFout_CurvatureAttributes "oatt"
#define DEFoutOscRadius "orad"
#define DEFoutOscHeight "ohei"
#define DEFoutOscAbscissa "oabs"

#define DEFout_iin "iin"
#define DEFout_ijn "jin"
#define DEFout_ikn "kin"


// Constructor : initialization of parameters
LB_StepCharacteriseCurvature::LB_StepCharacteriseCurvature(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _nbPtsDerivation = 5;
    _maxRadiusOsculatorCircle = 100;
}

// Step description (tooltip of contextual menu)
QString LB_StepCharacteriseCurvature::getStepDescription() const
{
    return tr("Calcule les courbures d'une section");
}

// Step detailled description
QString LB_StepCharacteriseCurvature::getStepDetailledDescription() const
{
    return tr("Cette étape permet de caractériser l'évolution de la courbure de la polyligne centrale de  billons à partir des cercles ajustés sur les clusters horizontaux."
              "On définit comme paramètres:"
              "<ul>"
              "<li> un nombre de cercles <b>n</b> qui seront pris en compte de part et d'autre de chaque centre pour calculer les vecteurs tangents et normaux à la ligne des centres en faisant appel à une méthode de régression linéaire. (N.B.: l'épaisseur des clusters horizontaux impacte aussi le résultat.)</li>"
              "<li> un <b>rayon maximal</b> des cercles osculateurs à afficher.<br>"
              "</ul>"
              "En sortie, cette étape fournit:"
              "<ul>"
              "<li> l'évolution du trièdre de Frenet le long de la polyligne composée des centres de chaque cercle, et dont se déduisent les cercles osculateurs caractérisant la courbure, en amplitude et en orientation. Chaque trièdre est composé des vecteurs tangent, normal, et binormal. Leurs normes est fixée à 10cm pour la représentation. Le cercle osculateur associé à un point de la polyligne est dans le plan défini par les vecteurs tangent et normal associés au point.<br />"
              "Les attributs permettent de relier les rayons de courbure à la hauteur, et à l'abscisse curviligne du point de la polyligne.</li>"
              "</ul>");
}

// Step copy method
CT_VirtualAbstractStep* LB_StepCharacteriseCurvature::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LB_StepCharacteriseCurvature(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LB_StepCharacteriseCurvature::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_resSections = createNewInResultModel(DEFin_resSections, tr("Billons"));
    resIn_resSections->setRootGroup(DEFin_Section, CT_AbstractItemGroup::staticGetType(), tr("Groupe Cluster"));
    resIn_resSections->addGroupModel(DEFin_Section, DEFin_Cluster, CT_AbstractItemGroup::staticGetType(), tr("Cluster(groupe)"), tr("Groupe contenant les clusters contenues dans la section"));
    resIn_resSections->addItemModel(DEFin_Cluster, DEFin_Circle, CT_Circle::staticGetType(), tr("Cercle"));

}

// Creation and affiliation of OUT models
void LB_StepCharacteriseCurvature::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_Sections = createNewOutResultModel(DEFout_Sections, tr("Courbures"));
    res_Sections->setRootGroup(DEFout_Section, new CT_StandardItemGroup(),tr("Courbure"), tr("groupe contenant les infos de courbure d'un billon "));
    //    res_Sections->addGroupModel(DEFout_Section, DEFout_Clusters, new CT_StandardItemGroup(), tr("Courbure"));
    res_Sections->addGroupModel(DEFout_Section, DEFout_Curvature, new CT_StandardItemGroup(), tr("Courbure"));
    res_Sections->addItemModel(DEFout_Curvature, DEFout_tan, new CT_Line(), tr("tangente"), tr("tangente à la ligne neutre"));
    res_Sections->addItemModel(DEFout_Curvature, DEFout_nor, new CT_Line(), tr("normale"),tr("normale à la ligne neutre"));
    res_Sections->addItemModel(DEFout_Curvature, DEFout_bin, new CT_Line(), tr("binormale"),tr("binormale à la ligne neutre"));
    res_Sections->addItemModel(DEFout_Curvature, DEFout_osc, new CT_Circle(), tr("Cercle osculateur"), tr("Cercle tangent à la ligne neutre"));
    res_Sections->addItemModel(DEFout_Curvature, DEFout_CurvatureAttributes, new CT_AttributesList(), tr("Attributs courbure"));

    res_Sections->addItemAttributeModel(DEFout_CurvatureAttributes,DEFoutOscRadius,
                                        new CT_StdItemAttributeT<float>(NULL, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), NULL, 0),
                                        tr("Rayon de Courbure"));
    res_Sections->addItemAttributeModel(DEFout_CurvatureAttributes,DEFoutOscHeight,
                                        new CT_StdItemAttributeT<float>(NULL, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), NULL, 0),
                                        tr("Hauteur de référence"));
    res_Sections->addItemAttributeModel(DEFout_CurvatureAttributes,DEFoutOscAbscissa,
                                        new CT_StdItemAttributeT<float>(NULL, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), NULL, 0),
                                        tr("Abscisse Curviligne"));

}

// Semi-automatic creation of step parameters DialogBox
void LB_StepCharacteriseCurvature::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addInt(tr("Nombre n de cercles pour lissage sur 2n +1"), " ", 1, 100, _nbPtsDerivation);
    configDialog->addDouble(tr("Rayon de courbure maximal à afficher"), " m ", 0, 1000,1,_maxRadiusOsculatorCircle );
}

void LB_StepCharacteriseCurvature::compute()
{
    Eigen::Quaterniond zero4D (0.,0.,0.,0.);

    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_Sections = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_Sections = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_Section(resIn_Sections, this, DEFin_Section);
    qint64 cptGroupIn_secs =0;
    while (itIn_Section.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_Section = (CT_AbstractItemGroup*) itIn_Section.next();
        std::vector<Eigen::Quaterniond> Polyline(2*_nbPtsDerivation+1);
        double curvedAbscissa=0.;

        cptGroupIn_secs++;

        CT_StandardItemGroup* grp_Section= new CT_StandardItemGroup(DEFout_Section, res_Sections);
        res_Sections->addGroup(grp_Section);


        // Create sorted (by Z) circle list
        QList<CT_Circle*> circleList;
        CT_GroupIterator itIn_Cluster(grpIn_Section, this, DEFin_Cluster);
        while (itIn_Cluster.hasNext() && !isStopped())
        {
            const CT_AbstractItemGroup* grpIn_Cluster = (CT_AbstractItemGroup*) itIn_Cluster.next();
            CT_Circle* circle = (CT_Circle*)grpIn_Cluster->firstItemByINModelName(this, DEFin_Circle);

            if (circle != NULL)
            {
                circleList.append(circle);
            }
        }
        std::sort(circleList.begin(), circleList.end(), LB_StepCharacteriseCurvature::orderByIncreasingZ);

        if (circleList.size() > 2*_nbPtsDerivation) { // Analyse de courbure possible

            qint64 indice = 2*_nbPtsDerivation;

            for (int ic = 0 ; ic < circleList.size() ; ic++)
            {
                CT_Circle* circle = circleList.at(ic);

                Polyline[indice].x() = circle->getCenterX();
                Polyline[indice].y() = circle->getCenterY();
                Polyline[indice].z() = circle->getCenterZ();

                if(indice <= 2*_nbPtsDerivation)
                {
                    curvedAbscissa = 0.;
                }
                else
                {
                    curvedAbscissa += distance(Polyline[indice-1],Polyline[indice]);
                }

                Polyline[indice].w() = curvedAbscissa;
                indice++;
                Polyline.push_back(zero4D);
            }
            Polyline.pop_back();

            //Complement of the polyline data by plane symetry (conservation of curvature) of   2*_nbPtsDerivation points at each end of the measured polyline
            // to provide data for derivating twice :  first the tangential vector,then the normal
            // at the beginning of the line
            qDebug() << "Completion at the beginning of the line";
            //1- Normal Definition:
            //// 1st test normal estimated by fitting a vector from _nbPtsDerivation points
            //Eigen::Quaterniond lfirst(Polyline[2*_nbPtsDerivation].scalar(),linearRegression(&Polyline[2*_nbPtsDerivation],2*_nbPtsDerivation));

            // 2d test normal vector estimated from the two first points P1P) + compensated by half the difference between the vectors  P1P2 and P2P3
            Eigen::Quaterniond lfirst = computeNormalQuaternion(Polyline[2*_nbPtsDerivation], Polyline[2*_nbPtsDerivation+1], Polyline[2*_nbPtsDerivation +2]);

            double aux = lfirst.vec().norm();

            lfirst.x() /= aux;
            lfirst.y() /= aux;
            lfirst.z() /= aux;
            lfirst.w() /= aux;

            qDebug() << "Vecteur Unitaire Début (" << lfirst.x()<< lfirst.y() << lfirst.z()<< ")";
            qDebug() << "1er point Polyline" << quaternionToString(Polyline[2*_nbPtsDerivation]);
            //2-Equation of the orthogonal Plan
            double dplanfirst = (lfirst.x()*Polyline[2*_nbPtsDerivation].x() + lfirst.y()*Polyline[2*_nbPtsDerivation].y() + lfirst.z()*Polyline[2*_nbPtsDerivation].z());
            //3-Creation of the symetrics
            for (int i= 0; i < 2*_nbPtsDerivation ; i++) //_nbPtsDerivation
            {
                Polyline[2*_nbPtsDerivation -1 -i] = symmetric(&Polyline[2*_nbPtsDerivation +1+i],lfirst.x(),lfirst.y(),lfirst.z(),dplanfirst);
                //Polyline[2*_nbPtsDerivation -1 -i] = pointReflexion(&Polyline[2*_nbPtsDerivation +1+i],&Polyline[2*_nbPtsDerivation]);

                Polyline[2*_nbPtsDerivation -1 -i].w() = Polyline[2*_nbPtsDerivation -i].w() - distance(Polyline[2*_nbPtsDerivation -i],Polyline[2*_nbPtsDerivation -1-i] );
                //          qDebug()<< Polyline[2*_nbPtsDerivation +1+i] << Polyline[2*_nbPtsDerivation -1 -i];
            }
            // at the end of the line
            qDebug() << "Completion at the end of the line";
            // Plane definition
            //1-Normal definition
            size_t lend = Polyline.size();
            //// 1st test normal estimated by fitting a vector from _nbPtsDerivation points
            //Eigen::Quaterniond llast(Polyline[lend-1].scalar(),linearRegression(&Polyline[lend-1  -_nbPtsDerivation],_nbPtsDerivation));

            // 2d test normal vector estimated from the two last points Pn-1Pn) + compensated by half the difference between the vectors  Pn-1Pn and Pn-1Pn
            Eigen::Quaterniond llast = computeNormalQuaternion(Polyline[lend-1], Polyline[lend-2], Polyline[lend-3]);

            aux = llast.vec().norm();

            llast.x() /= aux;
            llast.y() /= aux;
            llast.z() /= aux;
            llast.w() /= aux;

            //        qDebug() << "Point final" << Polyline[lend-1];
            //        qDebug() << "Vecteur Unitaire Fin (" << llast.x() << llast.y() << llast.z()<< ")";


            //2-Equation of the orthogonal Plan
            double dplanlast = (llast.x()*Polyline[lend-1].x() +llast.y()*Polyline[lend-1].y() + llast.z()*Polyline[lend-1].z());
            //3-Creation of the symetrics
            for (int i= 0; i < 2*_nbPtsDerivation ; i++) //_nbPtsDerivation
            {
                Polyline.push_back(symmetric(&Polyline[lend-2-i],llast.x(),llast.y(),llast.z(),dplanlast));//Case
                //Polyline.push_back(pointReflexion(&Polyline[lend-2-i],&Polyline[lend-1]));
                Polyline[lend +i].w() = Polyline[lend-1 +i].w() + distance(Polyline[lend - 1 +i],Polyline[lend + i] );
                //          qDebug()<< Polyline[lend-2-i] << Polyline[lend+i];
            }
            qDebug() << "nbpts ligne"<< Polyline.size();

            // end of the line completion
            // Display the whole set of points of the Polyline with the complements at the beginning and the end
            qDebug() << "Polyligne complétée ";


            ////Display the quaternions describing the polyline
            //        for (int i= 0; i < (int) (Polyline.size());i++)
            //        {
            //            qDebug() << "Point ind: " << i << "(s::" << Polyline[i].scalar() <<"/x::"<< Polyline[i].x()<<"/y::" << Polyline[i].y()<<"/z::"<< Polyline[i].z()<<")";
            //        }

            std::vector<Eigen::Quaterniond> tangent(_nbPtsDerivation+1);
            std::vector<Eigen::Quaterniond> tangentUnit(_nbPtsDerivation+1);
            std::vector<double> curvature(_nbPtsDerivation+1);


            Eigen::Vector3d T1, T2, T3, T4;

            for (qint64 i= (qint64)_nbPtsDerivation; i < (qint64)(Polyline.size() - _nbPtsDerivation); i++)
            {
                tangent[i] = derivative(&Polyline[i],_nbPtsDerivation);
                tangentUnit[i] = tangent[i];
                tangentUnit[i].vec() =  tangent[i].vec().normalized();
                tangent.push_back(zero4D);
                tangentUnit.push_back(zero4D);
                curvature.push_back(0.);

            }
            tangent.pop_back();
            tangentUnit.pop_back();
            curvature.pop_back();



            PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr(" Courbures Section %1 ").arg(cptGroupIn_secs,0,10).arg(Polyline[Polyline.size()-2*_nbPtsDerivation].w(),0,'g',4));

            /*
  Calcul des courbures locales
*/

            std::vector<Eigen::Quaterniond>  normale(2*_nbPtsDerivation+1 );
            std::vector<Eigen::Quaterniond>  normalUnit(2*_nbPtsDerivation+1 );
            std::vector<Eigen::Quaterniond>  binormalUnit(2*_nbPtsDerivation+1);


            // computation of the normal vectors to the polyline
            for (int i= 2*_nbPtsDerivation; i < ((int) Polyline.size() -2*_nbPtsDerivation) ; i++)
            {
                normale[i] = derivative(&tangentUnit[i],_nbPtsDerivation);


                //       deg = angledegrefromxy(normale[i].x(), normale[i].y());
                qDebug() << "Tangente" << quaternionToString(tangentUnit[i]);
                qDebug() << "normale" <<  quaternionToString(normale[i]);

                normale.push_back(zero4D);
                normalUnit.push_back(zero4D);
                binormalUnit.push_back(zero4D);

            }
            normale.pop_back();
            normalUnit.pop_back();
            binormalUnit.pop_back();

            double normeN (0);
            double normeT (0);
            double normeBi (0);
            Eigen::Vector3d centreOsc ;
            double rayonOsc(0);


            /*
    Generation of the lines visualizing the Frenet's vectors
    */
            for (int i= 2*_nbPtsDerivation; i < ((int) Polyline.size() -2*_nbPtsDerivation); i++)
            {
                CT_StandardItemGroup* grp_curv= new CT_StandardItemGroup(DEFout_Curvature, res_Sections);
                grp_Section->addGroup(grp_curv);
                //          normeN = normale[i].length();
                //          normeT = tangent[i].length();
                normeT = sqrt(tangent[i].vec().dot(tangent[i].vec()));;
                normeN = sqrt(normale[i].vec().dot(normale[i].vec()));

                curvature[i] = normeN;

                normalUnit[i].w() = normale[i].w();
                normalUnit[i].x() = normale[i].x()/normeN;
                normalUnit[i].y() = normale[i].y()/normeN;
                normalUnit[i].z() = normale[i].z()/normeN;

                tangentUnit[i].w() = normale[i].w();
                tangentUnit[i].x() = tangent[i].x()/normeT;
                tangentUnit[i].y() = tangent[i].y()/normeT;
                tangentUnit[i].z() = tangent[i].z()/normeT;

                binormalUnit[i].vec() =  tangentUnit[i].vec().cross(normalUnit[i].vec());
                binormalUnit[i].w() = tangentUnit[i].w();
                normeBi = sqrt(binormalUnit[i].vec().dot(binormalUnit[i].vec()));
                binormalUnit[i].vec() =  binormalUnit[i].vec() /= (normeBi);


                T1.x() = Polyline[i].x();
                T1.y() = Polyline[i].y();
                T1.z() = Polyline[i].z();

                T2.x() = Polyline[i].x() + 0.1*tangentUnit[i].x();
                T2.y() = Polyline[i].y() + 0.1*tangentUnit[i].y();
                T2.z() = Polyline[i].z() + 0.1*tangentUnit[i].z();

                T3.x() = Polyline[i].x() + 0.1*normalUnit[i].x();
                T3.y() = Polyline[i].y() + 0.1*normalUnit[i].y();
                T3.z() = Polyline[i].z() + 0.1*normalUnit[i].z();

                T4.x() = Polyline[i].x() + 0.1*binormalUnit[i].x()/normeBi;
                T4.y() = Polyline[i].y() + 0.1*binormalUnit[i].y()/normeBi;
                T4.z() = Polyline[i].z() + 0.1*binormalUnit[i].z()/normeBi;


                //         qDebug() << "T1" << T1 << "T2" << T2;
                CT_LineData *tangentData = new CT_LineData(T1,T2);
                CT_Line *item_tan = new CT_Line(DEFout_tan , res_Sections,tangentData);

                CT_LineData *normalData = new CT_LineData(T1,T3);
                CT_Line *item_nor = new CT_Line(DEFout_nor, res_Sections,normalData);

                CT_LineData *binormalData = new CT_LineData(T1,T4);
                CT_Line *item_bin = new CT_Line(DEFout_bin, res_Sections,binormalData);

                CT_AttributesList* CircleAttributes = new CT_AttributesList(DEFout_CurvatureAttributes,res_Sections);

                //        qDebug()<< "ind:" << i << " Normale " << normalUnit[i] << " Rayon " << 1/curvature[i];
                grp_curv->addItemDrawable(item_tan);
                grp_curv->addItemDrawable(item_nor);
                grp_curv->addItemDrawable(item_bin);
                grp_curv->addItemDrawable(CircleAttributes);

                if( curvature[i] > 0. )
                {
                    rayonOsc = 1/curvature[i];
                    centreOsc.x() = Polyline[i].x()+ rayonOsc*normalUnit[i].x();
                    centreOsc.y() = Polyline[i].y()+ rayonOsc*normalUnit[i].y();
                    centreOsc.z() = Polyline[i].z()+ rayonOsc*normalUnit[i].z();

                    //            rayonOsc =1.0;
                    //            centreOsc.x() = Polyline[i].x();
                    //            centreOsc.y() = Polyline[i].y();
                    //            centreOsc.z() = Polyline[i].z();


                }
                else
                {
                    rayonOsc =0.1;
                    centreOsc.x() = Polyline[i].x();
                    centreOsc.y() = Polyline[i].y();
                    centreOsc.z() = Polyline[i].z();
                }
                CircleAttributes->addItemAttribute(new CT_StdItemAttributeT<float>(DEFoutOscRadius,
                                                                                   CT_AbstractCategory::DATA_VALUE,
                                                                                   res_Sections , rayonOsc));
                CircleAttributes->addItemAttribute(new CT_StdItemAttributeT<float>(DEFoutOscHeight,
                                                                                   CT_AbstractCategory::DATA_VALUE,
                                                                                   res_Sections , Polyline[i].z()));
                CircleAttributes->addItemAttribute(new CT_StdItemAttributeT<float>(DEFoutOscAbscissa,
                                                                                   CT_AbstractCategory::DATA_VALUE,
                                                                                   res_Sections , Polyline[i].w()));

                if (rayonOsc > _maxRadiusOsculatorCircle)
                {
                    rayonOsc=0.0;
                }
                CT_CircleData *circleoscdata = new CT_CircleData(centreOsc,binormalUnit[i].vec(),rayonOsc);
                CT_Circle *itemOutOsculatorCircle = new CT_Circle(DEFout_osc, res_Sections ,circleoscdata);
                //        qDebug() << "T1" << T1 << "T2" << T2;
                qDebug()<<"i="<< i << "rayon "<< itemOutOsculatorCircle->id()<<" : " <<  circleoscdata->getRadius();

                grp_curv->addItemDrawable(itemOutOsculatorCircle);
                grp_curv->addItemDrawable(CircleAttributes);
            }

        }
        else{
            PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr(" Courbures Section %1 non calculées car trop peu de points ").arg(cptGroupIn_secs,0,10));

        }

    }


}



//Compute the distance between two 4D points(x,y,z,s)
double LB_StepCharacteriseCurvature::distance(  Eigen::Quaterniond  p1 ,  Eigen::Quaterniond  p2)
{
    double dist=0.;
    dist = sqrt(pow(p1.x() - p2.x(), 2)+ pow(p1.y() -p2.y(),2)+pow(p1.z() -p2.z(),2));
    return dist;
}

//Compute the digital derivative dx/ds dy/ds dz/ds using a local parabolic approximation fitted on p quaternions(x,y,z,s)
Eigen::Quaterniond  LB_StepCharacteriseCurvature::derivative(Eigen::Quaterniond *qion, int nbsmoothingpoints )
{
    int length = 2*nbsmoothingpoints+1;
    Eigen::Quaterniond derive = Eigen::Quaterniond();

    double *x = new double[length];
    double *y = new double[length];
    double *z = new double[length];
    double *s = new double[length];
    int j=0;
    for(int i= -nbsmoothingpoints; i< (nbsmoothingpoints+1) ; i++)
    {

        j=i+nbsmoothingpoints;
        s[j]=(qion + i)->w();
        x[j]=(qion + i)->x();
        y[j]=(qion + i)->y();
        z[j]=(qion + i)->z();
        qDebug()<<"nÃÂ°"<< i << "s ="<< s[j] << "x ="<< x[j]<< "y ="<< y[j]<< "z ="<< z[j];
    }

    double sums(0.), sumx(0.), sumy(0.), sumz(0.);
    double sums2(0.), sums3(0.), sums4(0.), saux(0.);
    double sumsx(0.), sumsy(0.),sumsz(0.);
    double sums2x(0.),sums2y(0.), sums2z(0.);
    //    double a1x(0.),a1y(0.),a1z(0.),a2x(0.),a2y(0.),a2z(0.),daux(0.),aux(0);//used for parabolic regression
    //    double dxbyds(0.),dybyds(0.),dzbyds(0.);//used for parabolic regression

    // s corresponds to the curvilinear abscissa
    // y corresponds must correspond to the x,y,z coordinate
    int k,m;

    if(length%2==0)
    {
        derive = Eigen::Quaterniond(1.,0.,0.,1.);//n'importe quoi
    }
    else
    {
        for(k=0;k<length;k++ )
        {
            sums += s[k];
            sumx += x[k];
            sumy += y[k];
            sumz += z[k];

            saux = s[k]*s[k];
            sums2 += saux;
            sums3 += saux*s[k];
            sums4 += saux*saux;

            sumsx += s[k]*x[k];
            sumsy += s[k]*y[k];
            sumsz += s[k]*z[k];

            sums2x += saux*x[k];
            sums2y += saux*y[k];
            sums2z += saux*z[k];
        }
        m = (int) ((length+1)/2);
        // régression parabolique (erreur de calcul?)

        //        qDebug()<< "m = " << m  << " length =" << length << "x="<<x[m-1]<< "y="<<y[m-1]<< "z="<<z[m-1];
        //        daux= sums4*(sums2*length-sums*sums)-sums3*(length*sums3-sums*sums2)+sums2*(sums*sums3-sums2*sums2);

        //        a2x = (sums2x*(length*sums2-sums*sums)-sums3*(length*sumsx-sums*sumx)+sums2*(sumsx*sums-sumx*sums2))/daux;
        //        a1x = (sums4*(length*sumsx-sums*sumx)-sums2x*(length*sums3-sums*sums2)+sums2*(sums3*sumx-sumsx*sums2))/daux;

        //        a2y = (sums2y*(length*sums2-sums*sums)-sums3*(length*sumsy-sums*sumy)+sums2*(sumsy*sums-sumy*sums2))/daux;
        //        a1y = (sums4*(length*sumsy-sums*sumy)-sums2y*(length*sums3-sums*sums2)+sums2*(sums3*sumy-sumsy*sums2))/daux;

        //        a2z = (sums2z*(length*sums2-sums*sums)-sums3*(length*sumsz-sums*sumz)+sums2*(sumsz*sums-sumz*sums2))/daux;
        //        a1z = (sums4*(length*sumsz-sums*sumz)-sums2z*(length*sums3-sums*sums2)+sums2*(sums3*sumz-sumsz*sums2))/daux;

        //        dxbyds = 2*a2x*x[m-1] + a1x;
        //        dybyds = 2*a2y*y[m-1] + a1y;
        //        dzbyds = 2*a2z*z[m-1] + a1z;//dérivée


        // regression linéaire

        //        a = (N.Sxy - Sx.Sy) / (N.Sxx - (Sx)ÃÂ²)

        //        Sx = somme (x)
        //        Sy = somme (y)
        //        Sxy = somme (x.y)
        //        Sxx = somme (xÃÂ²)
        //        N = nombre de points
        double   ax(0.), ay(0.), az(0.);

        ax = (length*sumsx-sums*sumx)/(length*sums2-sums*sums);
        ay = (length*sumsy-sums*sumy)/(length*sums2-sums*sums);
        az = (length*sumsz-sums*sumz)/(length*sums2-sums*sums);

        //        qDebug() << "ax"<<  ax << "ay" << ay;

        derive = Eigen::Quaterniond(s[m-1], ax,ay,az);
    }

    delete x;
    delete y;
    delete z;
    delete s;

    return derive;
}

//Calcul un vecteur par ajustement sur nbpts d'une polyligne.
Eigen::Vector3d LB_StepCharacteriseCurvature::linearRegression(Eigen::Quaterniond *qion, int nbpts )
{
    double sums(0.),sums2(0.), sumx(0.),sumsx(0.),sumy (0.),sumsy(0.),sumz(0.),sumsz(0.);
    Eigen::Vector3d resultVector(0.,0.,0.);

    for(int k=0;k<nbpts;k++ )
    {
        qDebug() << "s:" << (qion+k)->w() << "x:" <<(qion+k)->x()<< "y:" <<(qion+k)->y()<< "z:" <<(qion+k)->z();
        sums += (qion+k)->w();
        sumx += (qion+k)->x();
        sumy += (qion+k)->y();
        sumz += (qion+k)->z();
        sums2 += ((qion+k)->w())*((qion+k)->w());
        sumsx += ((qion+k)->w())*((qion+k)->x());
        sumsy += ((qion+k)->w())*((qion+k)->y());
        sumsz += ((qion+k)->w())*((qion+k)->z());
    }

    resultVector.x() = (nbpts*sumsx-sums*sumx)/(nbpts*sums2-sums*sums);
    resultVector.y() = (nbpts*sumsy-sums*sumy)/(nbpts*sums2-sums*sums);
    resultVector.z() = (nbpts*sumsz-sums*sumz)/(nbpts*sums2-sums*sums);

    return(resultVector.normalized());
}

// compute the symetric of a point defined by a quaternion (s, x,y,z) under plane refexive symmetry
Eigen::Quaterniond  LB_StepCharacteriseCurvature::symmetric(Eigen::Quaterniond *qion, double a, double b, double c, double d)
{
    Eigen::Quaterniond symetric(0.,0.,0.,0.);
    // Matrice en coordonnées homogènes de la symetrie par rapport au plan d'équation ax + by + cz = d
    //                | 1-2aÃÂ²   -2ab   -2ac  2ad |
    //                |  -2ab  1-2bÃÂ²   -2bc  2bd |
    //                |  -2ac   -2bc  1-2cÃÂ²  2cd |
    //                |    0     0      0     1  |


    symetric.x() =  (1-2*a*a)*qion->x()     -2*a*b*qion->y()         -2*a*c*qion->z()     + 2*a*d;
    symetric.y() =     -2*a*b*qion->x() +(1-2*b*b)*qion->y()         -2*b*c*qion->z()     + 2*b*d;
    symetric.z() =     -2*a*c*qion->x()     -2*b*c*qion->y()         +(1-2*c*c)*qion->z() + 2*c*d;

    return symetric;
}
// compute the symetric of a point defined by a quaternion (s, x,y,z) in a point reflexion defined by the quaternion qcentre
Eigen::Quaterniond  LB_StepCharacteriseCurvature::pointReflexion(Eigen::Quaterniond *qion, Eigen::Quaterniond *qcentre)
{
    Eigen::Quaterniond image(0.,0.,0.,0.);
    // Matrice en coordonnées homogènes de la symetrie par rapport au point qcentre
    //                | -1   0   0  2*qcentre(x) |
    //                |  0  -1   0  2*qcentre(y) |
    //                |  0   0  -1  2*qcentre(z) |
    //                |  0   0   0       1       |


    image.x() = -1*qion->x()     + 2*qcentre->x();
    image.y() = -1*qion->y()     + 2*qcentre->y();
    image.z() = -1*qion->z()     + 2*qcentre->z();

    return image;
}


/*
Compute the 3D vector orthogonal to a line defined by a 3D point as origin and a 3D unit vector as direction and joining a 3D point
*/
Eigen::Vector3d LB_StepCharacteriseCurvature::pointToLine(const Eigen::Vector3d lineOrigin, const Eigen::Vector3d  lineDirection, const Eigen::Vector3d Point3D)
{

    Eigen::Vector3d  vaux(lineOrigin.x()-Point3D.x(),lineOrigin.y()-Point3D.y(),lineOrigin.z()-Point3D.z());

    Eigen::Vector3d resultat = vaux - lineDirection*vaux.dot(lineDirection);

    return(resultat);

}


/*
Compute the area of a triangle by Héron's formula from the  coordinates of the 3 vertices
*/
double LB_StepCharacteriseCurvature::triangleArea(const Eigen::Vector3d vertexA, const Eigen::Vector3d  vertexB, const Eigen::Vector3d vertexC)
{
    double area(0), halfPerimeter (0);
    double edgeA(0), edgeB(0), edgeC(0);

    edgeA = CT_MathPoint::distance3D(vertexB, vertexC);
    edgeB = CT_MathPoint::distance3D(vertexA, vertexC);
    edgeC = CT_MathPoint::distance3D(vertexA, vertexB);

    //qDebug() << "A" << edgeA << "B" << edgeB << "C" << edgeC ;
    halfPerimeter = (edgeA+edgeB+edgeC)/2.0;
    area = sqrt(halfPerimeter*(halfPerimeter - edgeA)*(halfPerimeter - edgeB)*(halfPerimeter - edgeC));
    return(area);
}

QString LB_StepCharacteriseCurvature::quaternionToString(const Eigen::Quaterniond &quat)
{
    return QString("vector(%1;%2;%3) scalar(%4)").arg(quat.x()).arg(quat.y()).arg(quat.z()).arg(quat.w());
}

Eigen::Quaterniond LB_StepCharacteriseCurvature::computeNormalQuaternion(const Eigen::Quaterniond& q1, const Eigen::Quaterniond& q2, const Eigen::Quaterniond& q3)
{
    Eigen::Quaterniond result;

    result.x() = (q1.x() - q2.x()) + 0.5*((q1.x() - q2.x())- (q2.x() - q3.x()));
    result.y() = (q1.y() - q2.y()) + 0.5*((q1.y() - q2.y())- (q2.y() - q3.y()));
    result.z() = (q1.z() - q2.z()) + 0.5*((q1.z() - q2.z())- (q2.z() - q3.z()));
    result.w() = (q1.w() - q2.w()) + 0.5*((q1.w() - q2.w())- (q2.w() - q3.w()));

    return result;
}


