#ifndef CONVEXHULL_H
#define CONVEXHULL_H

#include "Eigen/Core"

#include "point.h"

class ConvexHull {

public:

    ConvexHull(QList<Point*> *points);

    bool pointInConvexHull(Eigen::Vector3d p);
    double addNewPointAndComputeArea(Point *p);
    double computeDistMax(Eigen::Vector3d apex);

    static double computeArea( std::vector<Eigen::Vector3d > bound);

    std::vector<Eigen::Vector3d >   _hull;
    double                          _hullArea;
    QList<Point*>*                  _points;

private:

    std::vector<Eigen::Vector3d> buildLowerConvexHull(QList<Point*> &points);
    std::vector<Eigen::Vector3d> buildUpperConvexHull(QList<Point*> &points);
    double det(Eigen::Vector3d  p, Eigen::Vector3d  q, Eigen::Vector3d  r);
    bool isRightTurn(Eigen::Vector3d p, Eigen::Vector3d q, Eigen::Vector3d r);

    static bool compare3fby1(Point* p1, Point* p2);
};




#endif // CONVEXHULL_H
