#include "foret.h"

#include <QDebug>

using namespace std;

Foret::Foret()
{   
}

Foret::~Foret()
{
}

void Foret::relabForest()
{
    Arbre treetmp;
    vector<Arbre> newlisteArbre;
   vector<Arbre> listeArbre = getlisteArbres();
   for(unsigned int i = 0;i<listeArbre.size();i++)
   {
       treetmp=listeArbre[i];
       if(i<treetmp.getlabelTree())
       {
           treetmp.setlabelTree(i);
       }
       newlisteArbre.push_back(treetmp);
   }
   setlisteArbres(newlisteArbre);
}

void Foret::setlisteArbres(vector<Arbre> &listeArbres)
{
    _listeArbres = listeArbres;
}

const vector<Arbre> &Foret::getlisteArbres() const
{
    return _listeArbres;
}

void Foret::fusionArbres(Arbre &arbre_fort,Arbre &arbre_faible,double scorefusion)
{
   // Le nouvel arbre aura le label  de l arbre fort.
    qDebug()<<"Fusion des deux arbres";

    vector<Point> listePointsFaible=arbre_faible.getlistePoints();
    qDebug()<<"listePointsFaible.size : "<<listePointsFaible.size()<<" de label "<<arbre_faible.getlabelTree();
    vector<Point> listePointsFort=arbre_fort.getlistePoints();
    qDebug()<<"listePointsFort.size : "<<listePointsFort.size()<<" de label "<<arbre_fort.getlabelTree();
    for (vector<Point>::iterator it=listePointsFaible.begin();it!=listePointsFaible.end();it++)
    {
        listePointsFort.push_back(*it);
    }

    Sommet apex = arbre_fort.getSommet();
    Eigen::Vector3d apex3d = apex.getcoord();

    arbre_fort.setlistePoints(listePointsFort);
    arbre_fort.setScoreArbre(scorefusion);
    qDebug()<<"Flag 0";
    vector<Eigen::Vector3d> arbre3d =arbre_fort.getarbre3d();
    qDebug()<<"Flag ";
    vector<Eigen::Vector3d> cvxHull =ConvexHull::buildConvexHull(arbre3d);
    arbre_fort.setbound(cvxHull);
    arbre_fort.setDistMaxArbre(ConvexHull::computeDistMax(cvxHull,apex3d));
    qDebug()<<"Distmax : "<<ConvexHull::computeDistMax(cvxHull,apex3d);
    qDebug()<<"Flag 1";
    replaceTree(arbre_fort,arbre_fort.getlabelTree());
    qDebug()<<"Flag 2";
    vector<Arbre> listeArbres=getlisteArbres();
    int index=arbre_faible.getlabelTree();
    qDebug()<<"Flag 3";
    listeArbres.erase(listeArbres.begin()+index);
    qDebug()<<"Flag 4";
    setlisteArbres(listeArbres);
    qDebug()<<"Fusion des deux arbres ok";

}

void Foret::push_newTree(Arbre &tree)
{
    std::vector<Arbre> tmp(getlisteArbres());
    tmp.push_back(tree);
    setlisteArbres(tmp);
}

Arbre Foret::extractTree(int label)
{
    Arbre Tree;

    std::vector<Arbre> list=getlisteArbres();
    Tree = list[label];

    return Tree;
}

void Foret::replaceTree(Arbre &Tree,int label)
{
    std::vector<Arbre> newlistArbres=getlisteArbres();

    newlistArbres[label]=Tree;
    setlisteArbres(newlistArbres);
}
void Foret::removeTree(int label)
{
    std::vector<Arbre> newlistArbres=getlisteArbres();

    newlistArbres.erase(newlistArbres.begin()+label);
    setlisteArbres(newlistArbres);
}


unsigned int Foret::getNbTrees()
{
    std::vector<Arbre> liste =getlisteArbres();
    return liste.size();
}

vector<Sommet> Foret::getListeSommets() const
{
    std::vector<Sommet> list;
    std::vector<Arbre> listeArbres =getlisteArbres();
    for(std::vector<Arbre>::iterator it=listeArbres.begin();it!=listeArbres.end();++it)
    {
        Arbre tree =*it;
        Sommet s = tree.getSommet();
        list.push_back(s);
    }

    return list;
}


void Foret::clearForest()
{
    std::vector<Arbre> tmp =getlisteArbres();
    tmp.clear();
    setlisteArbres(tmp);
}
