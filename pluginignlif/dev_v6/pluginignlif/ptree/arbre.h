#ifndef ARBRE_H
#define ARBRE_H

#include "point.h"
#include "sommet.h"
#include "convexHull.h"

#include <eigen/Eigen/Core>

class Arbre
{
public:
    Arbre();

    const std::vector<Point>& getlistePoints() const;
    void setlistePoints( std::vector<Point> liste);
    void push_newPoint(Point point);

    const Sommet &getSommet() const ;
    void setSommet( Sommet sommet);

    void setbound(std::vector<Eigen::Vector3d> bound);
    std::vector<Eigen::Vector3d> getbound();

    std::vector<Eigen::Vector3d> getarbre3d() const;

    double getScoreArbre();
    void setScoreArbre(double score);

    void setDistMaxArbre(double DistMax);
    double getDistMaxArbre();

    Point extractPoint(int indice);
    int getIndicePoint(int num);
    unsigned int getNbPoints();

    int getlabelTree();
    void setlabelTree(int newlab);

    int getIndiceSommet();

    std::vector< Sommet > getSommetsFils(std::vector<Sommet>& listesommets);
    void clearTree();
    ~Arbre();
protected:
    std::vector<Point> _listePoints;
    Sommet _sommet;
    std::vector<Eigen::Vector3d> _convexHull;
    double _score;
    double _Distmax;
};

#endif // ARBRE_H
