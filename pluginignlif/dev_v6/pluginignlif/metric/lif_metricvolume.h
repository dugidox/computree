/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef LIF_METRICVOLUME_H
#define LIF_METRICVOLUME_H

#include "ctlibmetrics/ct_metric/abstract/ct_abstractmetric_raster.h"
#include "ctlibmetrics/tools/ct_valueandbool.h"

class LIF_MetricVolume : public CT_AbstractMetric_Raster
{
    Q_OBJECT
public:

    struct Config {
        VaB<double>      volume_in;
        VaB<double>      volume_in_gap;
        VaB<double>      volume_out;
        VaB<double>      volume_out_gap;
        VaB<double>      volume_tot;
        VaB<double>      volume_tot_gap;
        VaB<double>      surface_gap;
        VaB<double>      surface_NA;
        VaB<double>      h_max;
        VaB<double>      h_mean;
        VaB<double>      h_sd;
        VaB<double>      rumple;
        VaB<double>      rumple_hmean;
    };

    LIF_MetricVolume();
    LIF_MetricVolume(const LIF_MetricVolume &other);

    QString getShortDescription() const;
    QString getDetailledDescription() const;

    SettingsNodeGroup* getAllSettings() const;
    bool setAllSettings(const SettingsNodeGroup *settings);
    CT_AbstractConfigurableWidget* createConfigurationWidget();


    /**
     * @brief Returns the metric configuration
     */
    LIF_MetricVolume::Config metricConfiguration() const;

    /**
     * @brief Change the configuration of this metric
     */
    void setMetricConfiguration(const LIF_MetricVolume::Config &conf);

    CT_AbstractConfigurableElement* copy() const;


protected:
    void computeMetric();
    void declareAttributes();

private:
    Config  m_configAndResults;
    double _gap_threshold;

    double computeSlope(double val, size_t yy, size_t xx, double inNA);

};


#endif // LIF_METRICVOLUME_H
