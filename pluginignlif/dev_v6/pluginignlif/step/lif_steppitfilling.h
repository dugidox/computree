/****************************************************************************
 Copyright (C) 2010-2012 the Institut National de l'information Géographique et forestière (IGN) - Laboratoire de l'Inventaire Forestier (LIF), France
                         All rights reserved.

 Contact : cedric.vega@ign.fr

 Developers : Cédric Véga (IGN)
              Alexandre PIBOULE (ONF)

 Pit filling algorithm from Vega C., Durrieu S., 2011. Multi-level filtering segmentation to measure individual tree parameters based
 on Lidar data: Application to a mountainous forest with heterogeneous stands. International Journal of Applied Earth Observation and Geoinformation 13 (2011) 646–656

 This file is part of PluginIGNLIF library.

 PluginIGNLIF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIGNLIF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginIGNLIF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef LIF_STEPPITFILLING_H
#define LIF_STEPPITFILLING_H

#include "ct_step/abstract/ct_abstractstep.h"

class LIF_StepPitFilling: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    LIF_StepPitFilling();

    QString description() const;

    QString detailledDescription() const;

    QString getStepURL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
;
    double    _deltaZ;

    float median(QList<float> &list);
};

#endif // LIF_STEPPITFILLING_H
