/****************************************************************************
 Copyright (C) 2010-2012 the Institut National de l'information Géographique et forestière (IGN) - Laboratoire de l'Inventaire Forestier (LIF), France
                         All rights reserved.

 Contact : cedric.vega@ign.fr

 Developers : Cédric Véga (IGN)
              Alexandre PIBOULE (ONF)

 Pit filling algorithm from Vega C., Durrieu S., 2011. Multi-level filtering segmentation to measure individual tree parameters based
 on Lidar data: Application to a mountainous forest with heterogeneous stands. International Journal of Applied Earth Observation and Geoinformation 13 (2011) 646–656

 This file is part of PluginIGNLIF library.

 PluginIGNLIF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIGNLIF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginIGNLIF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "lif_steppitfilling.h"

#include "ct_itemdrawable/ct_image2d.h"

#include <QFileInfo>
#include <QDir>

#include <QDebug>

LIF_StepPitFilling::LIF_StepPitFilling() : SuperClass()
{
    _deltaZ = 0.5;
}

QString LIF_StepPitFilling::description() const
{
    return tr("Pit filling");
}

QString LIF_StepPitFilling::detailledDescription() const
{
    return tr("Pit filling algorithm from:<br><b>Vega C., Durrieu S., 2011.</b><br>"
              "<em>Multi-level filtering segmentation to measure individual tree parameters based"
              "on Lidar data: Application to a mountainous forest with heterogeneous stands.</em><br>"
              "International Journal of Applied Earth Observation and Geoinformation 13 (2011) 646–656");
}

QString LIF_StepPitFilling::getStepURL() const
{
    return tr("http://www.sciencedirect.com/science/article/pii/S0303243411000535");
}

CT_VirtualAbstractStep* LIF_StepPitFilling::createNewInstance()
{
    return new LIF_StepPitFilling();
}

//////////////////// PROTECTED METHODS //////////////////

void LIF_StepPitFilling::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_InResultModelGroupToCopy *result = createNewInResultModelForCopy(DEF_InRes);
    result->setZeroOrMoreRootGroup();
    result->addGroupModel("", DEF_InGroup, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    result->addItemModel(DEF_InGroup, DEF_InItem, CT_Image2D<float>::staticGetType(), tr("Raster"));
}

void LIF_StepPitFilling::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_InRes);
    if (resultModel != nullptr)
    {
        resultModel->addItemModel(DEF_InGroup, _outCavityFillModelName, new CT_Image2D<float>(), tr("Pits filled"));
    }
}

void LIF_StepPitFilling::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    postInputConfigDialog->addDouble(tr("Depth of pits to be filled"), "m", -1e+09, 1e+09, 2, _deltaZ);
}

void LIF_StepPitFilling::compute()
{
    CT_ResultGroup *outResult = getOutResultList().first();

    CT_ResultGroupIterator it(outResult, this, DEF_InGroup);

    while(it.hasNext()) {

        CT_StandardItemGroup* group = (CT_StandardItemGroup*) it.next();
        CT_Image2D<float> *inGrid = dynamic_cast<CT_Image2D<float>*>(group->firstItemByINModelName(this, DEF_InItem));

        if(inGrid != nullptr)
        {
            float minVal = inGrid->dataMin();

            CT_Image2D<float> *tmpGrid = new CT_Image2D<float>(_outCavityFillModelName.completeName(), outResult, inGrid->minX(), inGrid->minY(), inGrid->colDim(), inGrid->linDim(), inGrid->resolution(), inGrid->level(), inGrid->NA(), inGrid->NA());
            CT_Image2D<float> *outGrid = new CT_Image2D<float>(_outCavityFillModelName.completeName(), outResult, inGrid->minX(), inGrid->minY(), inGrid->colDim(), inGrid->linDim(), inGrid->resolution(), inGrid->level(), inGrid->NA(), inGrid->NA());

            tmpGrid->getMat() = inGrid->getMat().clone();
            outGrid->getMat() = inGrid->getMat().clone();

            double numberOfFilledPixels = 1;
            float neighbourValue = 0;

            int cpt = 0;
            while (numberOfFilledPixels > 0 && cpt < 1000)
            {
                numberOfFilledPixels = 0;

                for (size_t xx = 0; xx < tmpGrid->colDim() ; xx++)
                {
                    for (size_t yy = 0; yy < tmpGrid->linDim() ; yy++)
                    {

                        float kernel8  = std::numeric_limits<float>::max();
                        float kernel4H = std::numeric_limits<float>::max();
                        float kernel4D = std::numeric_limits<float>::max();

                        QList<float> kernel8List;
                        QList<float> kernel4HList;
                        QList<float> kernel4DList;

                        float pixelValue = tmpGrid->value(xx, yy);

                        if (pixelValue == tmpGrid->NA()) {pixelValue = minVal;}

                        neighbourValue = tmpGrid->value(xx - 1, yy);
                        if (neighbourValue == tmpGrid->NA()) {neighbourValue = minVal;}
                        kernel8List.append(neighbourValue);
                        kernel4HList.append(neighbourValue);
                        if (neighbourValue < kernel8) {kernel8 = neighbourValue;}
                        if (neighbourValue < kernel4H) {kernel4H = neighbourValue;}

                        neighbourValue = tmpGrid->value(xx, yy + 1);
                        if (neighbourValue == tmpGrid->NA()) {neighbourValue = minVal;}
                        kernel8List.append(neighbourValue);
                        kernel4HList.append(neighbourValue);
                        if (neighbourValue < kernel8) {kernel8 = neighbourValue;}
                        if (neighbourValue < kernel4H) {kernel4H = neighbourValue;}

                        neighbourValue = tmpGrid->value(xx + 1, yy);
                        if (neighbourValue == tmpGrid->NA()) {neighbourValue = minVal;}
                        kernel8List.append(neighbourValue);
                        kernel4HList.append(neighbourValue);
                        if (neighbourValue < kernel8) {kernel8 = neighbourValue;}
                        if (neighbourValue < kernel4H) {kernel4H = neighbourValue;}

                        neighbourValue = tmpGrid->value(xx, yy - 1);
                        if (neighbourValue == tmpGrid->NA()) {neighbourValue = minVal;}
                        kernel8List.append(neighbourValue);
                        kernel4HList.append(neighbourValue);
                        if (neighbourValue < kernel8) {kernel8 = neighbourValue;}
                        if (neighbourValue < kernel4H) {kernel4H = neighbourValue;}

                        neighbourValue = tmpGrid->value(xx - 1, yy + 1);
                        if (neighbourValue == tmpGrid->NA()) {neighbourValue = minVal;}
                        kernel8List.append(neighbourValue);
                        kernel4DList.append(neighbourValue);
                        if (neighbourValue < kernel8) {kernel8 = neighbourValue;}
                        if (neighbourValue < kernel4D) {kernel4D = neighbourValue;}

                        neighbourValue = tmpGrid->value(xx + 1, yy + 1);
                        if (neighbourValue == tmpGrid->NA()) {neighbourValue = minVal;}
                        kernel8List.append(neighbourValue);
                        kernel4DList.append(neighbourValue);
                        if (neighbourValue < kernel8) {kernel8 = neighbourValue;}
                        if (neighbourValue < kernel4D) {kernel4D = neighbourValue;}

                        neighbourValue = tmpGrid->value(xx + 1, yy - 1);
                        if (neighbourValue == tmpGrid->NA()) {neighbourValue = minVal;}
                        kernel8List.append(neighbourValue);
                        kernel4DList.append(neighbourValue);
                        if (neighbourValue < kernel8) {kernel8 = neighbourValue;}
                        if (neighbourValue < kernel4D) {kernel4D = neighbourValue;}

                        neighbourValue = tmpGrid->value(xx - 1, yy - 1);
                        if (neighbourValue == tmpGrid->NA()) {neighbourValue = minVal;}
                        kernel8List.append(neighbourValue);
                        kernel4DList.append(neighbourValue);
                        if (neighbourValue < kernel8) {kernel8 = neighbourValue;}
                        if (neighbourValue < kernel4D) {kernel4D = neighbourValue;}

                        if (kernel8List.size() > 0 && (kernel8 - pixelValue) > _deltaZ)
                        {
                            outGrid->setValue(xx, yy, median(kernel8List));
                            numberOfFilledPixels++;

                        } else if (kernel4HList.size() > 0 && (kernel4H - pixelValue) > _deltaZ)
                        {
                            outGrid->setValue(xx, yy, median(kernel4HList));
                            numberOfFilledPixels++;

                        } else if (kernel4DList.size() > 0 && (kernel4D - pixelValue) > _deltaZ)
                        {
                            outGrid->setValue(xx, yy, median(kernel4DList));
                            numberOfFilledPixels++;

                        }
                    }

                }

                PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("Turn %2, Number of modified pixels: %1")).arg(numberOfFilledPixels).arg(cpt++));
                tmpGrid->getMat() = outGrid->getMat().clone();
            }

            outGrid->computeMinMax();
            group->addItemDrawable(outGrid);

            delete tmpGrid;
        }
    }

    setProgress(100);
}

float LIF_StepPitFilling::median(QList<float> &list)
{
    qSort(list);
    int midPosition = list.size() / 2;

    if ((list.size() % 2) == 0)
    {
        return (list.at(midPosition) + list.at(midPosition - 1)) / 2.0;
    } else {
        return list.at(midPosition);
    }
}
