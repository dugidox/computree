#include "segmentation.h"

#include <QDebug>

using namespace std;

Segmentation::Segmentation():nb_lab(0)
{
}

//// segmentation

Foret& Segmentation::segmPPL(MeshModel* m, MeshModel* mSort, CMeshO::PerVertexAttributeHandle< int > &label,Foret &forest,int k, Param &p)
{
    ////Initialisation


    PtGrid.Set( mSort->cm.vert.begin(), mSort->cm.vert.end(), m->cm.bbox, p.getbound());// on créé une grille pour la gestion des operations de voisinage
    double radius = m->cm.bbox.Diag();
    double distMax;
    //// Segmentation
    qDebug()<<"___Segmentation___";
    int numPt=0;
    Point Ptmp;
    Eigen::Vector3d ptmp3d;
    for (vector<Point>::iterator it  = _Nuage.begin();it!=_Nuage.end();it++)
    {
//        qDebug()<<"Point numero : "<<numPt<<" de label : "<<label[mSort->cm.vert[numPt]];
        Ptmp=*it;
        ptmp3d =Ptmp.getcoord();
        // Lancement du chronomètre
        //clock_t start = clock();
        ///cb((100*numPt)/(_Nuage.size()),"etape 1 : segm...");
        vector<int> glab;
        if ((label[mSort->cm.vert[numPt]] == -1) && (mSort->cm.vert[numPt].Q() > p.getminH()))
        {
            // Labelisation
            glab = Segmentation::getLabelPPL(mSort, radius,forest, ptmp3d, k, label);
            // Mise a jour de label des voisins:
            label[mSort->cm.vert[numPt]]=glab[0];

            Ptmp.setlab(glab[0]);

            // Mise a jour :
            if(glab[1]==1)
            {
                nb_lab++;
                Arbre newTree;
                newTree.push_newPoint(Ptmp);
                forest.push_newTree(newTree);
                newTree.clearTree();
//                qDebug()<<"nouveau sommet";

            }else
            {
                Arbre arbretmp = forest.extractTree(glab[0]);
                arbretmp.push_newPoint(Ptmp);
                forest.replaceTree(arbretmp,glab[0]);
            }
            //Fin Mise a jour
            glab.clear();
        }
        // Fin Labelisation


       // clock_t end  = clock();
        //double delay = double((double)(end - start) / CLOCKS_PER_SEC);
        //qDebug()<<"delay numPt "<<numPt<<" : "<< delay;
        numPt++;
    }

    //// Fin Segmentation
    qDebug()<<"La segmentation a donne "<<nb_lab<< " sommets ";

    //// Calcul des scores
    qDebug()<<"___Calcul des score___";
    vector<Eigen::Vector3d> arbre3d;
    double score;
    int nbTrees = forest.getNbTrees();
    vector <Arbre> listearbre = forest.getlisteArbres();
    int numBr=0;
    Arbre Treetmp;
    Sommet sommettmp ;
    Eigen::Vector3d sommettmp3d ;
    vector<Point> listePoints;
    vector<Eigen::Vector3d> bound;//ConvexHull

    for (vector<Arbre>::iterator it  = listearbre.begin();it!=listearbre.end();it++)
    {
        ///cb((100*numBr)/(nbTrees),"etape 2 : scores...");
        Treetmp = *it;
        listePoints = Treetmp.getlistePoints();
         Point ptmp;
         Eigen::Vector3d ptmp3d;
        for(vector<Point>::iterator it_pt = listePoints.begin();it_pt!=listePoints.end();it_pt++)
        {
            ptmp = *it_pt;
            ptmp3d =ptmp.getcoord();
            arbre3d.push_back(ptmp3d);
        }
        bound = ConvexHull::buildConvexHull(arbre3d);
        Treetmp.setbound(bound);

        sommettmp = Treetmp.getSommet();
        sommettmp3d = sommettmp.getcoord();

        distMax = ConvexHull::computeDistMax(bound,sommettmp3d);
        Treetmp.setDistMaxArbre(distMax);

        score = getScore(arbre3d, bound, k, p);
        Treetmp.setScoreArbre(score);

        forest.replaceTree(Treetmp,Treetmp.getlabelTree());
        arbre3d.clear();
        numBr++;
    }


    //// Fin Calcul des scores

    return forest;
}
vector<int> Segmentation::getLabelPPL(MeshModel* mSort, double radius,Foret& forest, Eigen::Vector3d &point3d, int k , CMeshO::PerVertexAttributeHandle< int > &label)
{
    // Recuperation des voisins
    //qDebug()<<"In getLabelPPL";
    vcg::tri::VertTmark<CMeshO> a ;
    std::vector<vcg::GridStaticPtr<CVertexO, double >::ScalarType> closestDist;
    std::vector<vcg::GridStaticPtr<CVertexO, double >::CoordType> closestPoint;
    std::vector<vcg::GridStaticPtr<CVertexO, double >::ObjPtr> closestObj;
    MyPointDistanceFunctor<vcg::GridStaticPtr<CVertexO, double >::ScalarType> myPtDist;
    int res ;
    res = PtGrid.GetKClosest(myPtDist, a, k, point3d, radius, closestObj, closestDist, closestPoint) ;
    vector<int> glab;
    ConvexHull myconvexHull; // Notre Labelisation repose essentiellement sur la theorie des convexHull
    //// Tester si le point est dans un convexHull
    int inCH = Segmentation::getCH(mSort,forest,point3d);
    if(inCH!=-1)
    {
        glab.push_back(inCH);
        glab.push_back(0);
        return glab;
    }
    //// Sinon
    // Recuperation des labels des voisins
    vector<int> labels_voisins ;
    vector<int>::iterator it_labels ;
//     Ajout bures 1/2
         double th2d;
         double dh,dc,d = 0.0;
         double max = 0.0;
         for (vector<vcg::GridStaticPtr<CVertexO, double >::ObjPtr>::iterator it = closestObj.begin() ; it != closestObj.begin()+(int)(k/3) ; ++it)
         {
             dh= sqrt(pow((*it)[0].P()[0] - point3d[0],2) + pow((*it)[0].P()[1] - point3d[1],2));
             d+=dh;
             if (max<dh){max=dh;}
         }

         th2d = 3* (d)/((int)(k/3)-1);

         for (vector<vcg::GridStaticPtr<CVertexO, double >::ObjPtr>::iterator it = closestObj.begin() ; it != closestObj.end() ; ++it)
         {
             // Ajout Bures 2/2
             dc= sqrt(pow((*it)[0].P()[0] - point3d[0],2) + pow((*it)[0].P()[1] - point3d[1],2));
             bool select = (dc<th2d);
             if (label[*it] != -1 && select)
             {
                 labels_voisins.push_back(label[*it]);
             }

             //Original
//             if (label[*it] != -1)
//             {
//                 labels_voisins.push_back(label[*it]);
//                qDebug()<<"voisin de label : "<<label[*it];
//             }
         }

    // Differenciation selon le nombre de label
    if(labels_voisins.size()>0)// Des voisins labelises existent dans le voisinage du point etudie
    {
        // Liste des labels presents
        std::sort(labels_voisins.begin(), labels_voisins.end());
        it_labels = std::unique(labels_voisins.begin(), labels_voisins.end());
        labels_voisins.resize( std::distance(labels_voisins.begin(),it_labels));
        int nbLabels = labels_voisins.size();

        if(nbLabels == 1 )
        {
            glab.push_back(labels_voisins[0]);
        }
        else
        {
            std::vector<Eigen::Vector2d> diffAreas;
            Eigen::Vector2d area;
            for (vector<vcg::GridStaticPtr<CVertexO, double >::ObjPtr>::iterator it = closestObj.begin() ; it != closestObj.end() ; ++it)
            {
                if ( label[*it]!= -1)
                {
                    int labelvoisin = label[*it];
                    Arbre Treetmp= forest.extractTree(labelvoisin);
                    int nbPoints = Treetmp.getNbPoints();
                    vector<Eigen::Vector3d> branche;
                    for(int l=0;l<nbPoints;l++)
                    {
                        Point pointtmp = Treetmp.extractPoint(l);
                        Eigen::Vector3d pointtmp3d = pointtmp.getcoord();
                        branche.push_back(pointtmp3d);
                    }
                    area[1] = myconvexHull.addNewPointAndComputeArea(point3d,branche);
                    area[0] = labelvoisin;
                    diffAreas.push_back(area);
                }
            }

            sort(diffAreas.begin(),diffAreas.end(), compare2fby2);
            glab.push_back(diffAreas[diffAreas.size()-1][0]);
        }
        glab.push_back(0);

    }
    else
    {
        // cas d'un nouveau sommet
        glab.push_back(nb_lab);
        glab.push_back(1);
        int count =0;

        //selection des points parmi les voisins a qui nous donnons le meme label
        double th3D = calculDist3D(point3d,closestObj);
        for (std::vector<vcg::GridStaticPtr<CVertexO, double >::ObjPtr>::iterator it = closestObj.begin() ; it != closestObj.end() ; it++)
        {
            bool select = selectNeighbor(point3d, (*it)[0].P(), 10.0, th3D);
            if (select)
            {
                label[*it] = glab[0];
                count++;
            }
        }

            count=0;

    }
//    qDebug()<<"label retourne : "<<glab[0];
    return glab;

}
int Segmentation::getCH(MeshModel* mSort,Foret &forest,const Eigen::Vector3d &point3d)
{

    vector< Eigen::Vector3d> branche;
    unsigned int i;
    unsigned int nbArbres=forest.getNbTrees();
    Arbre Treetmp;
    for (i=0;i<nbArbres;i++)
    {
             unsigned int l ;
             Treetmp = forest.extractTree(i);
             unsigned int nbPoints=Treetmp.getNbPoints();
             for (l=0;l<nbPoints;l++)
             {
                 int indice=Treetmp.getIndicePoint(l);
                 branche.push_back(mSort->cm.vert[indice].P());
             }
             bool in = ConvexHull::pointInConvexHull(point3d, branche);
             if(in)
             {
                 return i;
             }

         branche.clear();
    }
return -1;
}
//// Segmentation finale







void Segmentation::Fusion(int labelpere, Arbre arbrePere,vector<Sommet> &SommetsFils,Foret &ResFils,int k,Param &p)
{
    qDebug()<<"In Fusion";
    sort(SommetsFils.begin(),SommetsFils.end(),compareApexbyScore);
    vector<Sommet> listeSommets=ResFils.getListeSommets();

    qDebug()<< "Sommets fils depuis liste";
    for (int y = 0; y<SommetsFils.size();y++){
        qDebug()<<"Sommet fils  : "<<SommetsFils[y].getlab();
    }
    // Fusion is always computed for pairs. Theses variables are used to do the comparison
    Sommet Stmp1,Stmp2;
    Eigen::Vector3d Stmp1_3d, Stmp2_3d;
    Arbre arbre1, arbre2;
    vector<Eigen::Vector3d> arbre1_3d, arbre2_3d;
    vector<Eigen::Vector3d> cvxHull1,cvxHull2;
    vector<Eigen::Vector3d> cvxHullFusion;
    double Distmax1,Distmax2;
    double Dist;
    double score1,score2;
    double scoreHighestApex; // Score of the highest Apex among the two concerned by the fusion
    double scoreFusion;      // Score of the expected tree after fusion
    double areafus;          // Area of the expected tree after fusion
    int labelfusion;        // Label of the expected tree after fusion
    bool fusCvxHull;        // Convex Hull of the expected tree after fusion
    bool fusion;            // if a fusion have been done for a given apex
    int unsigned count=0;
    unsigned int i;

    while ((count<SommetsFils.size()-1)&&(SommetsFils.size()>2))
    {
        qDebug()<<"In while ";
        fusCvxHull=false;
        fusion= false;
        Stmp1  = SommetsFils[count];
        Stmp1_3d = Stmp1.getcoord();
        score1 = Stmp1.getscoresommet();
        arbre1 = ResFils.extractTree(Stmp1.getlab());
        Distmax1 = arbre1.getDistMaxArbre();
        cvxHull1 = arbre1.getbound();
        qDebug()<<"Sommet fils courant : "<<Stmp1.getlab();

        for (i=count+1;i<SommetsFils.size();i++)
        {
            qDebug()<<"In for i : "<< i;
            Stmp2  = SommetsFils[i];
            qDebug()<<"2";
            Stmp2_3d = Stmp2.getcoord();
            qDebug()<<"3";
            score2 = Stmp2.getscoresommet();
            qDebug()<<"4";
            arbre2 = ResFils.extractTree(Stmp2.getlab());
            qDebug()<<"5";
            Distmax2 = arbre2.getDistMaxArbre();
            cvxHull2 = arbre2.getbound();
            Dist = sqrt(pow(Stmp1_3d[0] - Stmp2_3d[0],2) + pow(Stmp1_3d[1] - Stmp2_3d[1],2));
            qDebug()<<"Sommet fils compare : "<<Stmp2.getlab();

            if( Dist < 1.25*(Distmax1+Distmax2) )
            {
                qDebug()<<"Sommets assez proches";
                fusCvxHull=true;
            }
            else
            {
                qDebug()<<"Sommets pas assez proches";

            }


            if (fusCvxHull==true)
            {
                //Test Fusion des deux Sommets via les scores :
                qDebug()<<"ConvexHull assez proches";
                if (Stmp1.getHeight()<Stmp2.getHeight())
                {
                    scoreHighestApex = score2;
                    labelfusion = Stmp2.getlab();
                    arbre2_3d =arbre2.getarbre3d();
                    arbre1_3d =arbre1.getarbre3d();
                    for (vector<Eigen::Vector3d>::iterator it=arbre1_3d.begin();it!=arbre1_3d.end();it++)
                    {
                        arbre2_3d.push_back(*it);
                    }
                    qDebug()<<"calcul des convexhull";
                    cvxHullFusion =ConvexHull::buildConvexHull(arbre2_3d);
                    qDebug()<<"calcul du score de la fusion ";
                    scoreFusion = getScore(arbre2_3d,cvxHullFusion,k,p);
                    qDebug()<<" score de la fusion : "<< scoreFusion;
                    qDebug()<<"comparaison entre : "<<scoreFusion<<" et le score de l'arbre le plus haut : "<<scoreHighestApex<<" et la moyenne des scores : "<<(score1+score2)/2;

                    if( ( scoreFusion > scoreHighestApex ) && ( scoreFusion > (score1+score2)/2 ) )
                    {

                        qDebug()<<"On realise la fusion";
                        fusion = true;
                        ResFils.fusionArbres(arbre2,arbre1,scoreFusion);
                        ResFils.relabForest();

                        listeSommets=ResFils.getListeSommets();
                        SommetsFils = arbrePere.getSommetsFils(listeSommets);
                        sort(SommetsFils.begin(),SommetsFils.end(),compareApexbyScore);
                        qDebug()<< "New Sommets fils depuis liste";
                        for (int y = 0; y<SommetsFils.size();y++){
                            qDebug()<<"Sommet fils  : "<<SommetsFils[y].getlab();
                        }
                        qDebug()<<"fusion done";
                        count=0;//count--;//
                        break;

                    }
                    else
                    {
                        qDebug()<<"Pas de fusion ";

                    }

                }
                else
                {
                    scoreHighestApex = score1;
                    labelfusion = Stmp1.getlab();
                    arbre1_3d =arbre1.getarbre3d();
                    arbre2_3d =arbre2.getarbre3d();
                    for (vector<Eigen::Vector3d>::iterator it=arbre2_3d.begin();it!=arbre2_3d.end();it++)
                    {
                        arbre1_3d.push_back(*it);
                    }
                    qDebug()<<"calcul des convexhull";
                    cvxHullFusion =ConvexHull::buildConvexHull(arbre2_3d);
                    qDebug()<<"calcul du score de la fusion ";
                    scoreFusion = getScore(arbre2_3d,cvxHullFusion,k,p);
                    qDebug()<<" score de la fusion : "<< scoreFusion;
                    qDebug()<<"comparaison entre : "<<scoreFusion<<" et le score de l'arbre le plus haut : "<<scoreHighestApex<<" et la moyenne des scores : "<<(score1+score2)/2;

                    if( ( scoreFusion > scoreHighestApex ) && ( scoreFusion > (score1+score2)/2 ) )
                    {
                        qDebug()<<"On realise la fusion";
                        fusion = true;
                        qDebug()<<"SommetsFils.size() = "<<SommetsFils.size();
                        ResFils.fusionArbres(arbre1,arbre2,scoreFusion);
                        ResFils.relabForest();
                        listeSommets=ResFils.getListeSommets();
                        SommetsFils = arbrePere.getSommetsFils(listeSommets);
                        for (int y = 0; y<SommetsFils.size();y++){
                            qDebug()<<"Sommet fils  : "<<SommetsFils[y].getlab();
                        }
                        qDebug()<<"SommetsFils.size() = "<<SommetsFils.size();
                        sort(SommetsFils.begin(),SommetsFils.end(),compareApexbyScore);
                        qDebug()<<"fusion done";
                        count=0;
                        break;
                    }
                    else
                    {
                        qDebug()<<"Pas de fusion ";
                    }

                }
                arbre1_3d.clear();
                arbre2_3d.clear();

            }
            else
            {
                qDebug()<<"ConvexHull pas assez proches";
            }

            qDebug()<<"Pas de fusion, passage au sommet suivant : "<<SommetsFils[i+1].getlab()<< "  count = "<<count<<" i + 1= "<<i+1;
        }
        if(fusion==false) {count++;}
        qDebug()<<"count = "<< count << " SommetsFils.size() = "<<SommetsFils.size();
    }
    qDebug()<<"Out Fusion";
}
Foret Segmentation::getApex( vector <Foret> &Resultats, Param &p )
{
    qDebug()<<"In getApex";
    int nbScales = Resultats.size();
    Foret ResPere = Resultats[nbScales-1];
    Foret ResFils ;
    Eigen::Vector2i borders=p.getneighborV();

    int scale = p.getscale();
    int knnMin = borders[1]-scale;
    qDebug()<<"nbScale = "<<nbScales;
    for (int i = 2; i<nbScales+1;i++)
    {
        ///cb((100*(i-1))/(nbScales),"Apex selection...");
        // Recuperation des informations
        qDebug()<<"Debut du traitement";
        ResFils = ResPere;
        ResPere = Resultats[nbScales-i];

        qDebug()<<"knnMin = "<<knnMin;
        compareScores(ResPere, ResFils,knnMin,p);
        knnMin-=scale;
    }
    qDebug()<<"Fin getApex";
    return ResPere;
}
void  Segmentation::compareScores(Foret &ResPere,Foret &ResFils,int k, Param &p )
{
    qDebug()<<"In compare Score";

    double CoefCompar = p.getcoefcompar();

    vector <Arbre> listeArbrePere = ResPere.getlisteArbres();
    vector<Sommet> SommetsFils;
    vector<Sommet> listeSommetsFils= ResFils.getListeSommets();
    qDebug()<<"nbre total de sommets fils : "<<listeSommetsFils.size();
    int nbArbres = ResPere.getNbTrees();
    Arbre Treetmp;
    int labelPere;

    //Test Fusion
    qDebug()<<"Debut Test Fusion";
    for (std::vector<Arbre>::iterator itTree =listeArbrePere.begin(); itTree!=listeArbrePere.end();itTree++)
    {
        Treetmp = *itTree;
        labelPere = Treetmp.getlabelTree();
        qDebug()<<"label du sommet Pere : "<<labelPere;
        SommetsFils = Treetmp.getSommetsFils(listeSommetsFils);
        qDebug()<<"Nb de sommets fils : "<<SommetsFils.size();
        if(SommetsFils.size()>2)
        {
        Fusion(labelPere,Treetmp,SommetsFils,ResFils,k,p);//modification de ResFils
        listeSommetsFils= ResFils.getListeSommets();
        }
    }
    //Mise a jour
    listeSommetsFils= ResFils.getListeSommets();
    //Fin test Fusion
    qDebug()<<"Fin Test Fusion";

    for (std::vector<Arbre>::iterator itTree =listeArbrePere.begin(); itTree!=listeArbrePere.end();itTree++)
    {
        Treetmp = *itTree;
        int labelPere = Treetmp.getlabelTree();
        qDebug()<<"label du sommet Pere : "<<labelPere;
        SommetsFils = Treetmp.getSommetsFils(listeSommetsFils);
        int nbsommetsFils =SommetsFils.size();
        qDebug()<<"nbre de sommets fils dans l arbre pere : "<<nbsommetsFils;
        if(nbsommetsFils>1)
        {
            double moy = 0;
            double scorePF=0;
            double scorePere =Treetmp.getScoreArbre();
            int indPere = Treetmp.getIndiceSommet();
            Sommet Stmp;
            for(vector<Sommet>::iterator it = SommetsFils.begin() ; it!=SommetsFils.end();it++)
            {
                Stmp = *it ;
                double scoreFils =Stmp.getscoresommet();
                int indStmp = Stmp.getIndice();
                moy += scoreFils/nbsommetsFils;
                if (indStmp==indPere)
                {
                    scorePF = scoreFils;
                }
            }
            // Le sommet pere est confirme
            if(CoefCompar*moy<scorePere || CoefCompar*scorePF<scorePere )
            { // On va renvoye ResPere donc on ne modifie pas Respere si le sommet pere est confirme par rapport a ses sommets fils
                qDebug()<<"papa gagne";
            }
            else
            {
                //Split
                Sommet s;
                for(vector<Sommet>::iterator it = SommetsFils.begin() ; it!=SommetsFils.end();it++)
                {
                    s = *it;
                    int indSf = s.getIndice();
                    int labF = s.getlab();
                    qDebug()<<"label du sommet fils : "<<labF;
                    if (indSf == indPere)
                    {
                        Arbre Treefils =ResFils.extractTree(labF);
                        Treefils.setlabelTree(labelPere);
                        ResPere.replaceTree(Treefils,labelPere);
                    }
                    else
                    {
                        Arbre TreeF = ResFils.extractTree(labF);
                        TreeF.setlabelTree(nbArbres);
                        qDebug()<<"nouveau label : "<<nbArbres;
                        nbArbres++;
                        ResPere.push_newTree(TreeF);
                    }
                }
            }
        }
    }

    qDebug()<<"out CompareScore";
}










int Segmentation::getChFIN(vector<vector <Eigen::Vector3d> > &forest3d,const Eigen::Vector3d &point3d)
{

    vector< Eigen::Vector3d> branche;
    unsigned int i;
    unsigned int nbArbres=forest3d.size();
    for (i=0;i<nbArbres;i++)
    {
        bool in = ConvexHull::pointInConvexHull(point3d, forest3d[i]);
             if(in)
             {
                 return i;
             }

         branche.clear();
    }

return -1;
}
void Segmentation::segmFIN(MeshModel* m, MeshModel* mSort, vector<Sommet> listeSommets,CMeshO::PerVertexAttributeHandle< int > &label,CMeshO::PerVertexAttributeHandle< int > &isSommet,CMeshO::PerVertexAttributeHandle< int > &isSommetv2, Param& p)
{
    qDebug()<<"Segmentation Finale";
    vector<vector <Eigen::Vector3d> > forest3d;
    vector <Eigen::Vector3d> arbre3d;
    nb_lab = listeSommets.size();
    // on créé une grille pour la gestion des operations de voisinage
    PtGrid.Set( mSort->cm.vert.begin(), mSort->cm.vert.end(), m->cm.bbox, p.getbound());
    double radius = m->cm.bbox.Diag();

    vcg::Point2i borders = p.getneighborV();
    int nbVoisins;
    int lab;
    int res;

    // Recuperation des voisins

    vcg::tri::VertTmark<CMeshO> a ;
    std::vector<vcg::GridStaticPtr<CVertexO, double >::ScalarType> closestDist;
    std::vector<vcg::GridStaticPtr<CVertexO, double >::CoordType> closestPoint;
    std::vector<vcg::GridStaticPtr<CVertexO, double >::ObjPtr> closestObj;
    MyPointDistanceFunctor<vcg::GridStaticPtr<CVertexO, double >::ScalarType> myPtDist;
    Sommet Stmp;
    Eigen::Vector3d Stmp3d;

    for (unsigned int j=0;j<listeSommets.size();j++)
    {
        Stmp = listeSommets[j];
        int indSommet =Stmp.getIndice();
        label[mSort->cm.vert[indSommet]]=j;
        Stmp3d =Stmp.getcoord();
        arbre3d.push_back(Stmp3d);
        res  = PtGrid.GetKClosest(myPtDist, a, isSommetv2[mSort->cm.vert[indSommet]], Stmp3d, radius, closestObj, closestDist, closestPoint) ;

        for (std::vector<vcg::GridStaticPtr<CVertexO, double >::ObjPtr>::iterator it = closestObj.begin() ; it != closestObj.end() ; it++)
        {
            label[*it]=j;
            Eigen::Vector3d voisin3d = (*it)[0].P();
            arbre3d.push_back(voisin3d);
        }
        forest3d.push_back(arbre3d);
        isSommet[mSort->cm.vert[indSommet]]= j;
        arbre3d.clear();
    }
    Eigen::Vector3d Ptmp3d ;
    for(unsigned int numPt=0; numPt<_Nuage.size();numPt++)
    {
        ///cb((100*(numPt))/_Nuage.size(),"Segmentation finale...");

//        qDebug()<<"Point numero : "<<numPt<<" de label : "<<label[mSort->cm.vert[numPt]];

        Ptmp3d =mSort->cm.vert[numPt].P();
        double heighitPtmp = mSort->cm.vert[numPt].Q();
        nbVoisins   = borders[0];
        lab         = -1;
        if ((label[mSort->cm.vert[numPt]] == -1) && (heightPtmp > p.getminH()))
        {
            lab = getLabelFIN(radius,forest3d,Ptmp3d,nbVoisins,label);
            while((nbVoisins < borders[1]+10) && (lab == -1))
            {
                nbVoisins += p.getscale();
                lab = getLabelFIN(radius,forest3d,Ptmp3d,nbVoisins,label);
            }
            if (lab!=-1)
            {
                label[mSort->cm.vert[numPt]] = lab;
                arbre3d=forest3d[lab];
                arbre3d.push_back(Ptmp3d);
                forest3d[lab]=arbre3d;
                arbre3d.clear();
            }
            else
            {
                qDebug()<<"nouveau label";
                arbre3d.push_back(Ptmp3d);
                forest3d.push_back(arbre3d);
                label[mSort->cm.vert[numPt]] = nb_lab;
                isSommet[mSort->cm.vert[numPt]] = nb_lab;
                nb_lab++;
            }
        }
        // traitement points sol
        if(p.getGrd()==1)
        {
            if((label[mSort->cm.vert[numPt]] == -1) && (heightPtmp< p.getminH()))
            {

                int lab = -1;
                bool isInCH;
                vector <Eigen::Vector3d> tmp;
                for(int r=0;r<nb_lab;r++)
                {
                    isInCH = ConvexHull::pointInConvexHull(Ptmp3d,forest3d[r]);
                    if(isInCH)
                    {
                        lab = r;
                        tmp = forest3d[r];
                        label[mSort->cm.vert[numPt]] = lab;
                        tmp.push_back(Ptmp3d);
                        forest3d[r]=tmp;
                        break;
                    }
                }
            }
        }
    }
}
int Segmentation::getLabelFIN(double radius,vector<vector <Eigen::Vector3d> > &forest3d, Eigen::Vector3d point3d, int k , CMeshO::PerVertexAttributeHandle< int > &label)
{

    int glab = -1;

    //// Tester si le point est dans un convexHull
    int inCH;
    inCH = Segmentation::getChFIN(forest3d,point3d);
    if(inCH!=-1)
    {
        glab=inCH;
        return glab;
    }
    //// Sinon
    // Recuperation des voisins
    int res ;
    vcg::tri::VertTmark<CMeshO> a ;
    std::vector<vcg::GridStaticPtr<CVertexO, double >::ScalarType> closestDist;
    std::vector<vcg::GridStaticPtr<CVertexO, double >::CoordType> closestPoint;
    std::vector<vcg::GridStaticPtr<CVertexO, double >::ObjPtr> closestObj;
    MyPointDistanceFunctor<vcg::GridStaticPtr<CVertexO, double >::ScalarType> myPtDist;
    res = PtGrid.GetKClosest(myPtDist, a, k, point3d, radius, closestObj, closestDist, closestPoint);
    std::vector<vcg::GridStaticPtr<CVertexO, double >::ObjPtr>::iterator it;
    ConvexHull myconvexHull;
    // Recuperation des labels des voisins
    vector<int> labels_voisins;
    vector<int>::iterator it_labels;


//    // Ajout bures 1/2
    double th2d;
    double dh,dc,d = 0.0;
    double max = 0.0;
    for (it = closestObj.begin() ; it != closestObj.begin()+(int)(k/3) ; it++)
    {
        dh= sqrt(pow((*it)[0].P()[0] - point3d[0],2) + pow((*it)[0].P()[1] - point3d[1],2));
        d+=dh;
        if (max<dh)
        {
            max=dh;
        }
    }
    th2d = 3*(d)/((int)(k/3)-1);
    for (it = closestObj.begin() ; it != closestObj.end() ; it++)
    {
        // Ajout Bures 2/2
        dc= sqrt(pow((*it)[0].P()[0] - point3d[0],2) + pow((*it)[0].P()[1] - point3d[1],2));
        bool select = (dc<th2d);
        if (label[*it] != -1 && select)
        {
            labels_voisins.push_back(label[*it]);
        }

        // Original
//        if (label[*it] != -1)
//        {
//            labels_voisins.push_back(label[*it]);
//        }
    }
    // Differenciation selon le nombre de label
    if(labels_voisins.size()>0)// Des voisins labelises existent dans le voisinage du point etudie
    {
        // Liste des labels presents
        std::sort(labels_voisins.begin(), labels_voisins.end());
        it_labels = std::unique(labels_voisins.begin(), labels_voisins.end());
        labels_voisins.resize( std::distance(labels_voisins.begin(),it_labels));
        int nbLabels = labels_voisins.size();

        if(nbLabels == 1 )
        {
            glab = labels_voisins.at(0);
        }
        else
        {
            std::vector<Eigen::Vector2d> diffAreas;
            Eigen::Vector2d area;
            for (vector<vcg::GridStaticPtr<CVertexO, double >::ObjPtr>::iterator it = closestObj.begin() ; it != closestObj.end() ; it++)
            {
                if ( label[*it]!= -1){
                    int labelvoisin = label[*it];
                    area[1] = myconvexHull.addNewPointAndComputeArea(point3d,forest3d[labelvoisin]);
                    area[0] = labelvoisin;
                    diffAreas.push_back(area);
                }
            }
            sort(diffAreas.begin(),diffAreas.end(), compare2fby2);
            glab=diffAreas[diffAreas.size()-1][0];
        }
    }
     qDebug()<<"getLab renvoie le label: "<<glab;
    return glab;
}
