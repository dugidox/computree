/****************************************************************************
 Copyright (C) 2010-2012 the Institut National de l'information Géographique et forestière (IGN) - Laboratoire de l'Inventaire Forestier (LIF), France
                         All rights reserved.

 Contact : cedric.vega@ign.fr

 Developers : Cédric Véga (IGN)
              Alexandre PIBOULE (ONF)


 Pit filling algorithm from Vega C., Durrieu S., 2011. Multi-level filtering segmentation to measure individual tree parameters based
 on Lidar data: Application to a mountainous forest with heterogeneous stands. International Journal of Applied Earth Observation and Geoinformation 13 (2011) 646–656

 This file is part of PluginIGNLIF library.

 PluginIGNLIF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIGNLIF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginIGNLIF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "lif_steppitfilling02.h"

#include "ct_view/ct_stepconfigurabledialog.h"


#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

#include "ct_itemdrawable/ct_image2d.h"
#include "ct_iterator/ct_resultgroupiterator.h"
#include "ct_result/ct_resultgroup.h"


#include <QFileInfo>
#include <QDir>

#include <QDebug>

#define DEF_InRes "r"
#define DEF_InGroup "g"
#define DEF_InItem "i"

// Constructor : initialization of parameters
LIF_StepPitFilling02::LIF_StepPitFilling02(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _deltaZ = 0.5;
}

// Step description (tooltip of contextual menu)
QString LIF_StepPitFilling02::getStepDescription() const
{
    return tr("Pit filling (v2)");
}

// Step detailled description
QString LIF_StepPitFilling02::getStepDetailledDescription() const
{
    return tr("Pit filling algorithm from:<br><b>Vega C., Durrieu S., 2011.</b><br>"
              "<em>Multi-level filtering segmentation to measure individual tree parameters based"
              "on Lidar data: Application to a mountainous forest with heterogeneous stands.</em><br>"
              "International Journal of Applied Earth Observation and Geoinformation 13 (2011) 646–656");
}

// Step URL
QString LIF_StepPitFilling02::getStepURL() const
{
    return tr("http://www.sciencedirect.com/science/article/pii/S0303243411000535");
}

// Step copy method
CT_VirtualAbstractStep* LIF_StepPitFilling02::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LIF_StepPitFilling02(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////
// Creation and affiliation of IN models
void LIF_StepPitFilling02::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *result = createNewInResultModelForCopy(DEF_InRes);
    result->setZeroOrMoreRootGroup();
    result->addGroupModel("", DEF_InGroup, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    result->addItemModel(DEF_InGroup, DEF_InItem, CT_Image2D<float>::staticGetType(), tr("Raster"));
}

// Creation and affiliation of OUT models
void LIF_StepPitFilling02::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_InRes);
    if (resultModel != NULL)
    {
        resultModel->addItemModel(DEF_InGroup, _outCavityFillModelName, new CT_Image2D<float>(), tr("Pits filled"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void LIF_StepPitFilling02::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
    configDialog->addDouble(tr("Depth of pits to be filled"), "m", -1e+09, 1e+09, 2, _deltaZ);
}

void LIF_StepPitFilling02::compute()
{
    CT_ResultGroup *outResult = getOutResultList().first();

    CT_ResultGroupIterator it(outResult, this, DEF_InGroup);

    while(it.hasNext()) {

        CT_StandardItemGroup* group = (CT_StandardItemGroup*) it.next();
        CT_Image2D<float> *inGrid = dynamic_cast<CT_Image2D<float>*>(group->firstItemByINModelName(this, DEF_InItem));

        if(inGrid != NULL)
        {
            float minVal = inGrid->dataMin() - _deltaZ*1.01;

            CT_Image2D<float> *tmpGrid = new CT_Image2D<float>(_outCavityFillModelName.completeName(), outResult, inGrid->minX(), inGrid->minY(), inGrid->colDim(), inGrid->linDim(), inGrid->resolution(), inGrid->level(), inGrid->NA(), inGrid->NA());
            CT_Image2D<float> *outGrid = new CT_Image2D<float>(_outCavityFillModelName.completeName(), outResult, inGrid->minX(), inGrid->minY(), inGrid->colDim(), inGrid->linDim(), inGrid->resolution(), inGrid->level(), inGrid->NA(), inGrid->NA());
            CT_Image2D<bool> *hasChanged = new CT_Image2D<bool>(NULL, NULL, inGrid->minX(), inGrid->minY(), inGrid->colDim(), inGrid->linDim(), inGrid->resolution(), inGrid->level(), false, false);
            CT_Image2D<bool> *hasChangedTmp = new CT_Image2D<bool>(NULL, NULL, inGrid->minX(), inGrid->minY(), inGrid->colDim(), inGrid->linDim(), inGrid->resolution(), inGrid->level(), false, false);
            CT_Image2D<bool> *hasChangedFalse = new CT_Image2D<bool>(NULL, NULL, inGrid->minX(), inGrid->minY(), inGrid->colDim(), inGrid->linDim(), inGrid->resolution(), inGrid->level(), false, false);

            tmpGrid->getMat() = inGrid->getMat().clone();
            outGrid->getMat() = inGrid->getMat().clone();

            double numberOfFilledPixels = 1;
            float neighbourValue = 0;

            int cpt = 0;
            while (numberOfFilledPixels > 0 && cpt < 1000)
            {
                numberOfFilledPixels = 0;

                for (size_t xx = 0; xx < tmpGrid->colDim() ; xx++)
                {
                    for (size_t yy = 0; yy < tmpGrid->linDim() ; yy++)
                    {

                        if (cpt == 0 ||
                                hasChanged->value(xx-1, yy-1) ||
                                hasChanged->value(xx-1, yy  ) ||
                                hasChanged->value(xx-1, yy+1) ||
                                hasChanged->value(xx  , yy-1) ||
                                hasChanged->value(xx  , yy  ) ||
                                hasChanged->value(xx  , yy+1) ||
                                hasChanged->value(xx+1, yy-1) ||
                                hasChanged->value(xx+1, yy  ) ||
                                hasChanged->value(xx+1, yy+1))
                        {

                            float kernel8  = std::numeric_limits<float>::max();
                            float kernel4H = std::numeric_limits<float>::max();
                            float kernel4D = std::numeric_limits<float>::max();

                            QList<float> kernel8List;
                            QList<float> kernel4HList;
                            QList<float> kernel4DList;

                            float pixelValue = tmpGrid->value(xx, yy);

                            if (pixelValue == tmpGrid->NA()) {pixelValue = minVal;}

                            neighbourValue = tmpGrid->value(xx - 1, yy);
                            if (neighbourValue == tmpGrid->NA()) {neighbourValue = minVal;}
                            kernel8List.append(neighbourValue);
                            kernel4HList.append(neighbourValue);
                            if (neighbourValue < kernel8) {kernel8 = neighbourValue;}
                            if (neighbourValue < kernel4H) {kernel4H = neighbourValue;}

                            neighbourValue = tmpGrid->value(xx, yy + 1);
                            if (neighbourValue == tmpGrid->NA()) {neighbourValue = minVal;}
                            kernel8List.append(neighbourValue);
                            kernel4HList.append(neighbourValue);
                            if (neighbourValue < kernel8) {kernel8 = neighbourValue;}
                            if (neighbourValue < kernel4H) {kernel4H = neighbourValue;}



                            neighbourValue = tmpGrid->value(xx + 1, yy);
                            if (neighbourValue == tmpGrid->NA()) {neighbourValue = minVal;}
                            kernel8List.append(neighbourValue);
                            kernel4HList.append(neighbourValue);
                            if (neighbourValue < kernel8) {kernel8 = neighbourValue;}
                            if (neighbourValue < kernel4H) {kernel4H = neighbourValue;}



                            neighbourValue = tmpGrid->value(xx, yy - 1);
                            if (neighbourValue == tmpGrid->NA()) {neighbourValue = minVal;}
                            kernel8List.append(neighbourValue);
                            kernel4HList.append(neighbourValue);
                            if (neighbourValue < kernel8) {kernel8 = neighbourValue;}
                            if (neighbourValue < kernel4H) {kernel4H = neighbourValue;}


                            neighbourValue = tmpGrid->value(xx - 1, yy + 1);
                            if (neighbourValue == tmpGrid->NA()) {neighbourValue = minVal;}
                            kernel8List.append(neighbourValue);
                            kernel4DList.append(neighbourValue);
                            if (neighbourValue < kernel8) {kernel8 = neighbourValue;}
                            if (neighbourValue < kernel4D) {kernel4D = neighbourValue;}


                            neighbourValue = tmpGrid->value(xx + 1, yy + 1);
                            if (neighbourValue == tmpGrid->NA()) {neighbourValue = minVal;}
                            kernel8List.append(neighbourValue);
                            kernel4DList.append(neighbourValue);
                            if (neighbourValue < kernel8) {kernel8 = neighbourValue;}
                            if (neighbourValue < kernel4D) {kernel4D = neighbourValue;}


                            neighbourValue = tmpGrid->value(xx + 1, yy - 1);
                            if (neighbourValue == tmpGrid->NA()) {neighbourValue = minVal;}
                            kernel8List.append(neighbourValue);
                            kernel4DList.append(neighbourValue);
                            if (neighbourValue < kernel8) {kernel8 = neighbourValue;}
                            if (neighbourValue < kernel4D) {kernel4D = neighbourValue;}


                            neighbourValue = tmpGrid->value(xx - 1, yy - 1);
                            if (neighbourValue == tmpGrid->NA()) {neighbourValue = minVal;}
                            kernel8List.append(neighbourValue);
                            kernel4DList.append(neighbourValue);
                            if (neighbourValue < kernel8) {kernel8 = neighbourValue;}
                            if (neighbourValue < kernel4D) {kernel4D = neighbourValue;}


                            if (kernel8List.size() > 0 && (kernel8 - pixelValue) > _deltaZ)
                            {
                                outGrid->setValue(xx, yy, median(kernel8List));
                                hasChangedTmp->setValue(xx, yy, true);
                                numberOfFilledPixels++;

                            } else if (kernel4HList.size() > 0 && (kernel4H - pixelValue) > _deltaZ)
                            {
                                outGrid->setValue(xx, yy, median(kernel4HList));
                                hasChangedTmp->setValue(xx, yy, true);
                                numberOfFilledPixels++;

                            } else if (kernel4DList.size() > 0 && (kernel4D - pixelValue) > _deltaZ)
                            {
                                outGrid->setValue(xx, yy, median(kernel4DList));
                                hasChangedTmp->setValue(xx, yy, true);
                                numberOfFilledPixels++;
                            }
                        }
                    }

                }

                PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("Turn %2, Number of modified pixels: %1")).arg(numberOfFilledPixels).arg(cpt++));
                outGrid->getMat().copyTo(tmpGrid->getMat());
                hasChangedTmp->getMat().copyTo(hasChanged->getMat());
                hasChangedFalse->getMat().copyTo(hasChangedTmp->getMat());
            }


            outGrid->computeMinMax();
            group->addItemDrawable(outGrid);

            delete tmpGrid;
            delete hasChanged;
            delete hasChangedTmp;
            delete hasChangedFalse;
        }
    }

    setProgress(100);
}


float LIF_StepPitFilling02::median(QList<float> &list)
{
    qSort(list);
    int midPosition = list.size() / 2;

    if ((list.size() % 2) == 0)
    {
        return (list.at(midPosition) + list.at(midPosition - 1)) / 2.0;
    } else {
        return list.at(midPosition);
    }
}
