#include "convexhull.h"

#include <QDebug>

ConvexHull::ConvexHull(QList<Point*>* points)
{
    _points = points;

    std::vector<Eigen::Vector3d> upper = buildUpperConvexHull(*_points);
    std::vector<Eigen::Vector3d> lower = buildLowerConvexHull(*_points);

    _hull = upper;
    unsigned int i;
    for (i = 1 ; i < lower.size() ; i++)
    {
        _hull.push_back(lower[i]);
    }

    _hullArea = computeArea(_hull);
}

double ConvexHull::det(Eigen::Vector3d p, Eigen::Vector3d q, Eigen::Vector3d r)
{
    return(q[0]*r[1] + p[0]*q[1] + r[0]*p[1]) - (q[0]*p[1] + r[0]*q[1] + p[0]*r[1]);
}


bool ConvexHull::isRightTurn(Eigen::Vector3d p, Eigen::Vector3d q, Eigen::Vector3d r)
{
    bool isRigth = false;
    if (det(p, q, r)<0){isRigth = true;}
    return isRigth;
}

bool ConvexHull::compare3fby1(Point *p1, Point *p2)
{
    return (p1->_coord(0) > p2->_coord(0));
}

std::vector<Eigen::Vector3d> ConvexHull::buildUpperConvexHull(QList<Point *> &points)
{

    std::vector<Eigen::Vector3d> upperBound;
    std::sort(points.begin(), points.end(), ConvexHull::compare3fby1);
    upperBound.push_back(points[0]->_coord);
    upperBound.push_back(points[1]->_coord);

    int length = points.size();
    bool isRight;

    for (int i = 2 ; i < length ; i++){
        upperBound.push_back(points[i]->_coord);
        int l = upperBound.size();

        isRight = isRightTurn(upperBound[l-3], upperBound[l-2], upperBound[l-1]);

        while((l > 2) && (!isRight) )
        {
            upperBound[l-2] = upperBound[l-1];
            upperBound.pop_back();
            l = upperBound.size();
            isRight = isRightTurn(upperBound[l-3],upperBound[l-2],upperBound[l-1]);
        }
    }
    return upperBound;

}

std::vector<Eigen::Vector3d> ConvexHull::buildLowerConvexHull(QList<Point *> &points)
{

    std::vector<Eigen::Vector3d> lowerBound;
    int length = points.size();
    std::sort(points.begin(), points.end(), compare3fby1);
    lowerBound.push_back(points[length-1]->_coord);
    lowerBound.push_back(points[length-2]->_coord);

    int i, l;
    bool isRight;
    for (i = length-3 ; i >= 0 ; i--)
    {
        lowerBound.push_back(points[i]->_coord);
        l = lowerBound.size();
        isRight = isRightTurn(lowerBound[l-3], lowerBound[l-2], lowerBound[l-1]);
        while((l > 2) && (!isRight) ){
            lowerBound[l-2] = lowerBound[l-1];
            lowerBound.pop_back();
            l = lowerBound.size();
            isRight=isRightTurn(lowerBound[l-3], lowerBound[l-2], lowerBound[l-1]);
        }
    }
    return lowerBound;
}


bool ConvexHull::pointInConvexHull(Eigen::Vector3d p)
{
    double x, y;
    double p1x, p2x, p1y, p2y, xinters;

    int length = _hull.size();
    bool inside = false;
    x = p[0];
    y = p[1];
    p1x = _hull[0][0];
    p1y  = _hull[0][1];

    int j;
    for (j = 1 ; j < length + 1 ; j++)
    {
        p2x = _hull[j % length][0];
        p2y = _hull[j % length][1];

        if (y > std::min(p1y, p2y))
        {
            if(y <= std::max(p1y, p2y))
            {
                if(x <= std::max(p1x,p2x))
                {
                    if (p1y != p2y)
                    {
                        xinters = (y - p1y)*(p2x - p1x) / (p2y - p1y) + p1x;
                        if ((p1x == p2x) || (x < xinters))
                        {
                            inside = !inside;
                        }
                    }
                }
            }
        }
        p1x = p2x;
        p1y = p2y;
    }
    return inside;
}


double ConvexHull::computeArea(std::vector<Eigen::Vector3d> bound)
{
    double area = 0;

    unsigned int i;
    double x1,x2,y1,y2;
    for (i=0;i<bound.size()-1;i++){
        x1 = bound[i][0];
        x2 = bound[i+1][0];
        y1 = bound[i][1];
        y2 = bound[i+1][1];
        area = area + 0.5*(x1*y2 - y1*x2);
    }
    if (area < 0){area = -area;}

    return area;
}


double ConvexHull::addNewPointAndComputeArea(Point *p)
{
    if (_hullArea <= 0) {return 0.0;}

    QList<Point*> points2;
    points2.append(*_points);
    points2.push_back(p);

    ConvexHull newBound(&points2);

    return std::abs(newBound._hullArea - _hullArea) / _hullArea;
}


double ConvexHull::computeDistMax(Eigen::Vector3d apex)
{
    unsigned int i;
    double d = 0;
    double distMax = 0;

    for(i = 0 ; i < _hull.size() ; i++)
    {
        d = sqrt(pow(apex(0) - _hull[i](0), 2) + pow(apex(1) - _hull[i](1), 2));
        if (d > distMax)
        {
            distMax = d;
        }
    }
    return distMax;
}



