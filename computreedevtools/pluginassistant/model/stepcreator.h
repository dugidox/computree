#ifndef STEPCREATOR_H
#define STEPCREATOR_H

#include <QObject>

#include "model/step/inmodelcreator.h"
#include "model/step/outmodelcreator.h"
#include "model/step/copymodelcreator.h"
#include "model/step/parameterscreator.h"


class StepCreator : public QObject
{
    Q_OBJECT
public:
    explicit StepCreator(QString directory);

    ~StepCreator();


    inline InModelCreator* getInModelCreator() {return _inModelCreator;}
    inline OutModelCreator* getOutModelCreator() {return _outModelCreator;}
    inline CopyModelCreator* getCopyModelCreator() {return _copyModelCreator;}
    inline ParametersCreator* getParametersModelCreator() {return _parametersModelCreator;}

    QString createStepFiles();
    QString getStepName();

public slots:
    void setPluginCode(const QString &pluginCode) {_pluginCode = pluginCode;}
    void setStepBaseName(const QString &stepBaseName) {_stepBaseName = stepBaseName;}
    void setShortDescription(const QString &shortDescription) {_shortDescription = shortDescription;}
    void setDetailledDescription(const QString &detailledDescription) {_detailledDescription = detailledDescription;}
    void setCanBeAddedFirst(const bool &canBeAddedFirst) {_canBeAddedFirst = canBeAddedFirst;}

private:

    QString             _directory;
    QString             _name;
    QString             _pluginCode;
    QString             _stepBaseName;
    QString             _shortDescription;
    QString             _detailledDescription;
    bool                _canBeAddedFirst;
    InModelCreator*     _inModelCreator;
    OutModelCreator*    _outModelCreator;
    CopyModelCreator*   _copyModelCreator;
    ParametersCreator*  _parametersModelCreator;

    bool create_StepFile_h();
    bool create_StepFile_cpp();

    QString getIncludes();

    QString detailledDescriptionForMethod();


};

#endif // STEPCREATOR_H
