#ifndef EXPORTERCREATOR_H
#define EXPORTERCREATOR_H

#include <QObject>

class ExporterCreator : public QObject
{
    Q_OBJECT
public:
    explicit ExporterCreator(QString directory, QString name);

    QString createExporterFiles();

private:

    QString     _directory;
    QString     _name;


    bool create_ExporterFile_h();
    bool create_ExporterFile_cpp();

};

#endif // EXPORTERCREATOR_H
