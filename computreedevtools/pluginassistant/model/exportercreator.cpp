#include "exportercreator.h"

#include <QTextStream>
#include <QDir>

ExporterCreator::ExporterCreator(QString directory, QString name) : QObject()
{
    _directory = directory;
    _name = name;
}

QString ExporterCreator::createExporterFiles()
{
    QString errors = "";

    QDir dir(_directory);

    if (!dir.exists())
    {
        return tr("Le répertoire choisi n'existe pas', veuillez en choisir un autre svp");
    }

    dir.mkdir("exporter");

    if (!create_ExporterFile_h()) {errors.append(tr("Impossible de créer le fichier exporter (.h)\n"));}
    if (!create_ExporterFile_cpp()) {errors.append(tr("Impossible de créer le fichier exporter (.cpp)\n"));}

    return errors;
}

bool ExporterCreator::create_ExporterFile_h()
{
    QFile file(QString("%1/exporter/%2.h").arg(_directory).arg(_name.toLower()));

    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);

        stream << "#ifndef " << _name.toUpper() << "_H\n";
        stream << "#define " << _name.toUpper() << "_H\n";
        stream << "\n";

        stream << "#endif // " << _name.toUpper() << "_H\n";

        file.close();
        return true;
    }
    return false;
}

bool ExporterCreator::create_ExporterFile_cpp()
{
    QFile file(QString("%1/exporter/%2.cpp").arg(_directory).arg(_name.toLower()));

    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);
        stream << "#include \"exporter/" << _name.toLower() << ".h\"\n";

        file.close();
        return true;
    }
    return false;
}

