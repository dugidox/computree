#include "ct_referencepoint.h"
#include "model/step/tools.h"

CT_ReferencePoint::CT_ReferencePoint() : AbstractItemType()
{
}

QString CT_ReferencePoint::getTypeName()
{
    return "CT_ReferencePoint";
}

QString CT_ReferencePoint::getInCode(int indent, QString name)
{
    Q_UNUSED(indent);
    Q_UNUSED(name);
    return "";
}

QString CT_ReferencePoint::getOutCode(int indent, QString name, QString modelName, QString resultName)
{
    QString result = "";
    result += getCreationCode(indent, name, modelName, resultName);
    return result;
}
