#include "ct_ellipse.h"
#include "model/step/tools.h"

CT_Ellipse::CT_Ellipse() : AbstractItemType()
{
}

QString CT_Ellipse::getTypeName()
{
    return "CT_Ellipse";
}

QString CT_Ellipse::getInCode(int indent, QString name)
{
    Q_UNUSED(indent);
    Q_UNUSED(name);
    return "";
}

QString CT_Ellipse::getOutCode(int indent, QString name, QString modelName, QString resultName)
{
    QString result = "";
    result += getCreationCode(indent, name, modelName, resultName);
    return result;
}
