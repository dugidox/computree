#ifndef PARAMETERINT_H
#define PARAMETERINT_H

#include "model/step/parameters/abstractparameter.h"
#include "view/step/parameters/widgetint.h"

class ParameterInt : public AbstractParameter
{
public:
    ParameterInt();

    virtual QString getParameterDeclaration();
    virtual QString getParameterInitialization();
    virtual QString getParameterDialogCommands();
    virtual QString getParamaterDoc();

    void virtual onAliasChange();

private:
    inline WidgetInt* widget() {return (WidgetInt*) _widget;}

};

#endif // PARAMETERINT_H
