#ifndef PARAMETERSTRINGCHOICE_H
#define PARAMETERSTRINGCHOICE_H

#include "model/step/parameters/abstractparameter.h"
#include "view/step/parameters/widgetstringchoice.h"

class ParameterStringChoice : public AbstractParameter
{
public:
    ParameterStringChoice();

    virtual QString getParameterDeclaration();
    virtual QString getParameterInitialization();
    virtual QString getParameterDialogCommands();
    virtual QString getParamaterDoc();

    void virtual onAliasChange();

private:
    inline WidgetStringChoice* widget() {return (WidgetStringChoice*) _widget;}

};

#endif // PARAMETERSTRINGCHOICE_H
