#ifndef TOOLS_H
#define TOOLS_H

#include "qstring.h"
#include "qstringlist.h"
#include "qset.h"
#include "model/step/itemtypes/abstractitemtype.h"
#include "qstandarditemmodel.h"


class Tools
{
public:

    inline static QString getQStringListConcat(QSet<QString> &list, bool sort = true)
    {
        QStringList list2(list.toList());

        if (sort)
        {
            list2.sort();
        }

        QString result = "";

        foreach (const QString &value, list2)
        {
            result += value;
        }
        return result;
    }      

    inline static QString getIndentation(int n)
    {
        QString str = "";
        for (int i = 0 ; i < n ; i++)
        {
            str.append("    ");
        }
        return str;
    }


    inline static QString getSpaceSequence(int n)
    {
        QString str = "";
        for (int i = 0 ; i < n ; i++)
        {
            str.append(" ");
        }
        return str;
    }


    inline static QString getBooleanText(bool b)
    {
        return (b?"true":"false");
    }  

    inline static QString trs(const QString &str) {return "tr(\"" + str + "\")";}

    static QMap<QString, AbstractItemType*> ITEMTYPE;
    static QMap<QString, AbstractItemType*> initItemTypes();
    static void addItemType(QMap<QString, AbstractItemType*> &map, AbstractItemType* itemType);

};

#endif // TOOLS_H
