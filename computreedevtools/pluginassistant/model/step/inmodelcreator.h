#ifndef INMODELCREATOR_H
#define INMODELCREATOR_H

#include <QObject>
#include <QStandardItemModel>

class InModelCreator : public QObject
{
    Q_OBJECT
public:
    explicit InModelCreator(QObject *parent = 0);

    inline QStandardItemModel *getStandardItemModel() {return _model;}



    void getIncludes(QSet<QString> &list);
    QString getInDefines();
    QString getCreateInResultModelListProtectedContent();
    QString getResultRecovery();
    QString getComputeContent();

private:
    QStandardItemModel *_model;

};

#endif // INMODELCREATOR_H
