#include "parameterscreator.h"
#include "model/step/parameters/abstractparameter.h"

ParametersCreator::ParametersCreator(QObject *parent) :
    QObject(parent)
{
    _model = new QStandardItemModel();
}

QString ParametersCreator::getParametersDeclaration()
{
    QString result = "";
    int count = _model->rowCount();
    for (int i = 0 ; i < count; i++)
    {
        AbstractParameter* item = (AbstractParameter*) _model->item(i);
        result += item->getParameterDeclaration();
    }
    return result;
}

QString ParametersCreator::getParametersInitialization()
{
    QString result = "";
    int count = _model->rowCount();
    for (int i = 0 ; i < count; i++)
    {
        AbstractParameter* item = (AbstractParameter*) _model->item(i);
        result += item->getParameterInitialization();
    }
    return result;
}

QString ParametersCreator::getParametersDialogCommands()
{
    QString result = "";
    int count = _model->rowCount();
    for (int i = 0 ; i < count; i++)
    {
        AbstractParameter* item = (AbstractParameter*) _model->item(i);
        result += item->getParameterDialogCommands();
    }
    return result;
}

QString ParametersCreator::getParamatersDoc()
{
    QString result = "";
    int count = _model->rowCount();
    for (int i = 0 ; i < count; i++)
    {
        AbstractParameter* item = (AbstractParameter*) _model->item(i);
        result += item->getParamaterDoc();
    }
    return result;
}
