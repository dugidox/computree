#include "model/step/models/outresultmodel.h"
#include "model/step/models/outgroupmodel.h"
#include "view/step/models/outresultwidget.h"
#include "model/step/tools.h"
#include "assert.h"

OUTResultModel::OUTResultModel() : AbstractOutModel()
{
    _widget = new OUTResultWidget(this);
    setText(getPrefixedAlias());
}

bool OUTResultModel::isValid()
{
    if (rowCount()!=1) {return false;}
    return AbstractOutModel::isValid();
}

QString OUTResultModel::getName()
{
    return "res_" + getAlias();
}

void OUTResultModel::getIncludes(QSet<QString> &list)
{
    list.insert("#include \"ct_result/ct_resultgroup.h\"\n");
    list.insert("#include \"ct_result/model/outModel/ct_outresultmodelgroup.h\"\n");

    getChildrenIncludes(list);
}

QString OUTResultModel::getCreateOutResultModelListProtectedContent(QString parentDEF, QString resultModelName, bool rootGroup)
{
    Q_UNUSED(parentDEF);
    Q_UNUSED(resultModelName);
    Q_UNUSED(rootGroup);

    QString result = "";

    result += Tools::getIndentation(1) + "CT_OutResultModelGroup *" + getName() + " = createNewOutResultModel" + "(" + getDef() + ", " + Tools::trs(getDisplayableName());

    if (getDescription() != "")
    {
        result += ", " + Tools::trs(getDescription());
    }

    result += ");";
    result += "\n";

    OUTGroupModel* rootGrp = (OUTGroupModel*) child(0);

    if (rootGrp != NULL)
    {
        result += rootGrp->getCreateOutResultModelListProtectedContent("", getName(), true);
    }

    return result;
}

QString OUTResultModel::getComputeContent(QString resultName, QString parentName)
{
    Q_UNUSED(resultName);
    Q_UNUSED(parentName);

    QString result = "";

    OUTGroupModel* rootGrp = (OUTGroupModel*) child(0);

    if (rootGrp != NULL)
    {
        result += rootGrp->getComputeContent(getName(), getName());
    }

    return result;
}
