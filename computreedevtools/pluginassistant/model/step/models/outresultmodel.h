#ifndef OUTRESULTMODEL_H
#define OUTRESULTMODEL_H

#include "model/step/models/abstractoutmodel.h"

class OUTResultModel : public AbstractOutModel
{
public:
    OUTResultModel();

    virtual AbstractOutModel::ModelType getModelType() {return AbstractOutModel::M_Result_OUT;}
    virtual bool isValid();
    QString getName();

    virtual void getIncludes(QSet<QString> &list);
    virtual QString getCreateOutResultModelListProtectedContent(QString parentDEF = "", QString resultModelName = "", bool rootGroup = "");
    virtual QString getComputeContent(QString resultName, QString parentName);


};

#endif // OUTRESULTMODEL_H
