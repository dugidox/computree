#ifndef INGROUPWIDGET_H
#define INGROUPWIDGET_H


#include "view/step/models/abstractinwidget.h"

namespace Ui {
    class INGroupWidget;
}

class INGroupWidget : public AbstractInWidget
{
    Q_OBJECT

public:

    enum ChoiceMode
    {
        C_OneIfMultiple,
        C_MultipleIfMultiple,
        C_DontChoose
    };

    enum FinderMode
    {
        F_Obligatory,
        F_Optional
    };

    explicit INGroupWidget(AbstractInModel* model, QWidget *parent = 0);
    ~INGroupWidget();

    bool isvalid();
    QString getPrefixedAliad();
    QString getAlias();
    QString getDEF();
    QString getDisplayableName();
    QString getDescription();

    INGroupWidget::FinderMode getFinderMode();
    INGroupWidget::ChoiceMode getChoiceMode();



private slots:
    void on_alias_textChanged(const QString &arg1);

private:
    Ui::INGroupWidget *ui;
};

#endif // INRESULTWIDGET_H
