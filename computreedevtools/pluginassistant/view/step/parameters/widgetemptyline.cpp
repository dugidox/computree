#include "view/step/parameters/widgetemptyline.h"
#include "ui_widgetemptyline.h"
#include "model/step/parameters/abstractparameter.h"
#include "model/step/tools.h"

WidgetEmptyLine::WidgetEmptyLine(AbstractParameter* model, QWidget *parent) :
    AbstractParameterWidget(model, parent),
    ui(new Ui::WidgetEmptyLine)
{
    ui->setupUi(this);
}

WidgetEmptyLine::~WidgetEmptyLine()
{
    delete ui;
}

bool WidgetEmptyLine::isvalid()
{
    if (getAlias().isEmpty()) {return false;}
    return true;
}

QString WidgetEmptyLine::getAlias()
{
    return "";
}
