#ifndef CREATEITEMDIALOG_H
#define CREATEITEMDIALOG_H

#include <QDialog>

namespace Ui {
class CreateItemDialog;
}

class CreateItemDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateItemDialog(QWidget *parent = 0);
    ~CreateItemDialog();

private:
    Ui::CreateItemDialog *ui;
};

#endif // CREATEITEMDIALOG_H
