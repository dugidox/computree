#include "tk_stepestimatenormalsrandom.h"

#include <time.h>

// Utilise le depot
#include "ct_global/ct_context.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/abstract/ct_abstractitemdrawable.h"
#include "ct_itemdrawable/accessibility/ct_iaccesspointcloud.h"

#include "ct_itemdrawable/ct_pointsattributesnormal.h"
#include "ct_normalcloud/ct_normalcloudstdvector.h"

// Alias for indexing in models
#define DEF_resultIn_inputResult "inputResult"
#define DEF_groupIn_inputScene "inputGroup"
#define DEF_itemIn_scene "inputScene"

// Alias for indexing out models
#define DEF_itemOut_normal "normalItem"
#define DEF_groupOut_normal "normalGroup"
#define DEF_resultOut_normal "normalResult"

// Constructor : initialization of parameters
TK_StepEstimateNormalsRandom::TK_StepEstimateNormalsRandom(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString TK_StepEstimateNormalsRandom::getStepDescription() const
{
    return tr("Créer normales aléatoires");
}

// Step copy method
CT_VirtualAbstractStep* TK_StepEstimateNormalsRandom::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new TK_StepEstimateNormalsRandom(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void TK_StepEstimateNormalsRandom::createInResultModelListProtected()
{
    CT_InResultModelGroup *resultModel = createNewInResultModel(DEF_resultIn_inputResult, tr("Scene(s)"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_groupIn_inputScene, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultModel->addItemModel(DEF_groupIn_inputScene, DEF_itemIn_scene, CT_AbstractItemDrawable::staticGetType(), tr("Item(s)"), tr("Item(s) contenant un nuage de point"));
}

// Creation and affiliation of OUT models
void TK_StepEstimateNormalsRandom::createOutResultModelListProtected()
{
    // ****************************************************
    // On sort un resultats correspondant a l'attribut de hauteur que l'on va pouvoir normalier :
    //  - un groupe d'item (groupe de hauteur)
    //      - un item (attribut hauteurs)
    // ****************************************************

    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_normal, tr("Generated normals"));

    resultModel->setRootGroup(DEF_groupOut_normal,  new CT_StandardItemGroup(), tr("Group"));
    resultModel->addItemModel(DEF_groupOut_normal, DEF_itemOut_normal, new CT_PointsAttributesNormal(), tr("Random Normals"));
}

// Semi-automatic creation of step parameters DialogBox
void TK_StepEstimateNormalsRandom::createPostConfigurationDialog()
{
    // No dialog box
}

void TK_StepEstimateNormalsRandom::compute()
{
    CT_ResultGroup* resultIn_inputResult = getInputResults().first();
    CT_ResultGroup* resultOut_normal = getOutResultList().first();

    CT_ResultItemIterator it(resultIn_inputResult, this, DEF_itemIn_scene);
    if (it.hasNext())
    {
        const CT_IAccessPointCloud* itemIn_scene = dynamic_cast<const CT_IAccessPointCloud*>(it.next());

        if(itemIn_scene != NULL)
        {
            size_t nbPoints = itemIn_scene->getPointCloudIndex()->size();

            // On declare un nuage de normales que l'on va remplir aleatoirement
            CT_NormalCloudStdVector *normalCloud = new CT_NormalCloudStdVector( nbPoints );

            srand( time(NULL) );

            for ( size_t i = 0 ; i < nbPoints && !isStopped() ; i++ )
            {
                for ( int j = 0 ; j < 3 ; j++ )
                {
                    // Tir aleatoire entre -1000 et 1000, ca donnera une precision de 3 chiffres apres la virgule pour la normale
                    normalCloud->normalAt(i)[j] = (rand() % 2001) - 1000;
                }

                normalCloud->normalAt(i)[3] = 0;
                normalCloud->normalAt(i).normalize();

                setProgress( 100.0*i /nbPoints );
                waitForAckIfInDebugMode();
            }

            // On cree les attributs que l'on met dans un groupe
            CT_StandardItemGroup*      groupOut_normal = new CT_StandardItemGroup(DEF_groupOut_normal, resultOut_normal);
            CT_PointsAttributesNormal* normalAttribute = new CT_PointsAttributesNormal(DEF_itemOut_normal, resultOut_normal, itemIn_scene->getPointCloudIndexRegistered(), normalCloud);

            groupOut_normal->addItemDrawable( normalAttribute );
            resultOut_normal->addGroup(groupOut_normal);
        }
    }
}
