CT_PREFIX = ../../computreev6

COMPUTREE += ctlibstdactions

#CHECK_CAN_USE_PCL = 1
MUST_USE_OPENCV = 1

include($${CT_PREFIX}/plugin_shared.pri)


TARGET = plug_toolkit

HEADERS += \
    $$CT_LIB_PREFIX/ctlibplugin/pluginentryinterface.h\
    tk_pluginentry.h \
    step/tk_translatecloud.h \
    step/tk_stepscalecloud.h \
    step/tk_steprotatecloud.h \
    step/tk_stepcentercloud.h \
    step/tk_stepextractbox.h \
    step/tk_stepextractcylinder.h \
    step/tk_stepextractsphere.h \
    step/tk_stepcolorbyaxis.h \
    step/tk_stepcolorrandom.h \
    step/tk_stepestimatenormalsrandom.h \
    step/tk_stepfacecolorrandom.h \
    step/tk_stepextractbboxpart.h \
    step/tk_stepextractzslicefromattributes.h \
#    step/tk_stepnormalestimator.h \
    step/tk_stepextractzslice.h \
    step/tk_stepextractverticalcloudpart.h \
    step/tk_stepmergeclouds.h \
    step/tk_stepfacenormalestimator.h \
    step/tk_stepfilterpointsbyattribute.h \
    step/tk_stepextractplot.h \
    step/tk_stepextractplotbasedondtm.h \
#    step/tk_stepextractpointsinverticalcylinders.h \
    step/tk_stepreducepointsdensity.h \
    step/tk_stepslicepointcloud.h \
    step/tk_steptransformpointcloud.h \
    actions/tk_actionslicepointcloud.h \
    views/actions/tk_actionslicepointcloudoptions.h \
    tk_steppluginmanager.h
SOURCES += \
    tk_pluginentry.cpp \
    step/tk_translatecloud.cpp \
    step/tk_stepscalecloud.cpp \
    step/tk_steprotatecloud.cpp \
    step/tk_stepcentercloud.cpp \
    step/tk_stepextractbox.cpp \
    step/tk_stepextractcylinder.cpp \
    step/tk_stepextractsphere.cpp \
    step/tk_stepcolorbyaxis.cpp \
    step/tk_stepcolorrandom.cpp \
    step/tk_stepestimatenormalsrandom.cpp \
    step/tk_stepfacecolorrandom.cpp \
    step/tk_stepextractbboxpart.cpp \
    step/tk_stepextractzslicefromattributes.cpp \
#    step/tk_stepnormalestimator.cpp \
    step/tk_stepextractzslice.cpp \
    step/tk_stepextractverticalcloudpart.cpp \
    step/tk_stepmergeclouds.cpp \
    step/tk_stepfacenormalestimator.cpp \
    step/tk_stepfilterpointsbyattribute.cpp \
    step/tk_stepextractplot.cpp \
    step/tk_stepextractplotbasedondtm.cpp \
#    step/tk_stepextractpointsinverticalcylinders.cpp \
    step/tk_stepreducepointsdensity.cpp \
    step/tk_stepslicepointcloud.cpp \
    step/tk_steptransformpointcloud.cpp \
    actions/tk_actionslicepointcloud.cpp \
    views/actions/tk_actionslicepointcloudoptions.cpp \
    tk_steppluginmanager.cpp

FORMS += \
    views/actions/tk_actionslicepointcloudoptions.ui \



TRANSLATIONS += languages/plugintoolkit_en.ts \
                languages/plugintoolkit_fr.ts

