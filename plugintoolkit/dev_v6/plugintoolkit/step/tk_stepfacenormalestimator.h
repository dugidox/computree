#ifndef TK_STEPFACENORMALESTIMATOR_H
#define TK_STEPFACENORMALESTIMATOR_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_meshmodel.h"
#include "ct_itemdrawable/ct_faceattributesnormal.h"

class TK_StepFaceNormalEstimator : public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:
    TK_StepFaceNormalEstimator();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

	CT_HandleInResultGroupCopy<>                            _inResult;
	CT_HandleInStdZeroOrMoreGroup                           _inZeroOrMoreRootGroup;
	CT_HandleInStdGroup<>                                   _inGroup;
    CT_HandleInItem<CT_MeshModel>                           _inMesh;
    CT_HandleOutSingularItem<CT_FaceAttributesNormal>       _outNormals;

};

#endif // TK_STEPFACENORMALESTIMATOR_H
