#ifndef TK_STEPSLICEPOINTCLOUD_H
#define TK_STEPSLICEPOINTCLOUD_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_pointcluster.h"
#include "actions/tk_actionslicepointcloud.h"

class TK_StepSlicePointCloud: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    TK_StepSlicePointCloud();

    ~TK_StepSlicePointCloud();

    QString description() const;

    QString detailledDescription() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

    void initManualMode();
    void useManualMode(bool quit = false);

private:

    // Step parameters
    double       _xmin;
    double       _ymin;
    double       _zmin;
    double       _xmax;
    double       _ymax;
    double       _zmax;
    bool         _manual;

    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_PointCluster>                                           _outCluster;


    TK_ActionSlicePointCloud::TK_ActionSlicePointCloud_dataContainer*     _dataContainer;

    DocumentInterface*      _m_doc;
    QList<CT_AbstractItemDrawableWithPointCloud*>* _sceneList;


};

#endif // TK_STEPSLICEPOINTCLOUD_H
