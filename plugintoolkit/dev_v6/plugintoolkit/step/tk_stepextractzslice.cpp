#include "tk_stepextractzslice.h"

TK_StepExtractZSlice::TK_StepExtractZSlice() : SuperClass()
{
    _botZ = 0.5;
    _topZ = 7.0;
}


QString TK_StepExtractZSlice::description() const
{
    return tr("Extraire les points dans une tranche horizontale");
}


CT_VirtualAbstractStep* TK_StepExtractZSlice::createNewInstance() const
{
    return new TK_StepExtractZSlice();
}

//////////////////// PROTECTED METHODS //////////////////

void TK_StepExtractZSlice::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Z Minimum"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botZ);
    postInputConfigDialog->addDouble(tr("Z Maximum"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _topZ);
}


void TK_StepExtractZSlice::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène source"));
}


void TK_StepExtractZSlice::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outScene, tr("Scène extraite"));
}

void TK_StepExtractZSlice::compute()
{

    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* inScene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            const CT_AbstractPointCloudIndex *pointCloudIndex = inScene->pointCloudIndex();
            size_t nbPoints = pointCloudIndex->size();

            // On Cree un nouveau nuage qui sera le translate
            CT_PointCloudIndexVector *extractedCloud = new CT_PointCloudIndexVector(); // 1) cretation of output pointcloud index
            extractedCloud->setSortType(CT_PointCloudIndexVector::NotSorted);

            CT_PointIterator itP(pointCloudIndex);

            size_t i = 0;
            while (itP.hasNext())
            {
                if (isStopped()) {delete extractedCloud; return;}

                const CT_Point &point = itP.next().currentPoint();
                size_t index = itP.currentGlobalIndex();

                if ( point(2) <= _topZ &&
                     point(2) >= _botZ)
                {
                    extractedCloud->addIndex(index); // 2) adding kept indices
                }

                setProgress(float(99.0*i++ /nbPoints));
            }
            extractedCloud->setSortType(CT_PointCloudIndexVector::SortedInAscendingOrder);

            if (extractedCloud->size() > 0)
            {
                CT_Scene* outScene = new CT_Scene(PS_REPOSITORY->registerPointCloudIndex(extractedCloud)); // 3) create scene, registering the pointcloudindex
                outScene->updateBoundingBox(); // 4) don't forget to update the bounding box, to be fitted to filtered points

                group->addSingularItem(_outScene, outScene);
            }
        }
    }

    setProgress(100);
}
