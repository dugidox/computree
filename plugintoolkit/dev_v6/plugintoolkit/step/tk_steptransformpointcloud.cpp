#include "tk_steptransformpointcloud.h"

TK_StepTransformPointCloud::TK_StepTransformPointCloud() : SuperClass()
{
}

QString TK_StepTransformPointCloud::description() const
{
    return tr("Appliquer une Matrice de Transformation à une Scène");
}

QString TK_StepTransformPointCloud::detailledDescription() const
{
    return tr("");
}

QString TK_StepTransformPointCloud::URL() const
{
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* TK_StepTransformPointCloud::createNewInstance() const
{
    return new TK_StepTransformPointCloud();
}

//////////////////// PROTECTED METHODS //////////////////

void TK_StepTransformPointCloud::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène(s)"));

    manager.addResult(_inResultMat, tr("Matrice de transformation"));
    manager.setZeroOrMoreRootGroup(_inResultMat, _inZeroOrMoreRootGroupMat);
    manager.addGroup(_inZeroOrMoreRootGroupMat, _inGroupMat);
    manager.addItem(_inGroupMat, _inTransformationMatrix, tr("Matrice de transformation"));
}

void TK_StepTransformPointCloud::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outScene, tr("Scène transformée"));
}

void TK_StepTransformPointCloud::compute()
{
    for (const CT_TransformationMatrix* trMat : _inTransformationMatrix.iterateInputs(_inResultMat))
    {

        for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
        {
            for (const CT_AbstractItemDrawableWithPointCloud* inScene : group->singularItems(_inScene))
            {
                if (isStopped()) {return;}

                CT_PointIterator itP(inScene->pointCloudIndex());

                if (itP.hasNext())
                {
                    // limites de la bounding-box de la scène de sortie
                    double minX = std::numeric_limits<double>::max();
                    double minY = minX;
                    double minZ = minX;

                    double maxX = -minX;
                    double maxY = -minX;
                    double maxZ = -minX;

                    CT_NMPCIR pcir = PS_REPOSITORY->createNewPointCloud(itP.size());

                    // create a mutable point iterator to change points of this cloud
                    CT_MutablePointIterator outItP(pcir);

                    while(itP.hasNext())
                    {
                        CT_Point point = itP.next().currentPoint();

                        // transform the current point
                        trMat->transform(point);

                        // set it to the new point cloud
                        outItP.next().replaceCurrentPoint(point);

                        if (point(0) < minX) {minX = point(0);}
                        if (point(1) < minY) {minY = point(1);}
                        if (point(2) < minZ) {minZ = point(2);}
                        if (point(0) > maxX) {maxX = point(0);}
                        if (point(1) > maxY) {maxY = point(1);}
                        if (point(2) > maxZ) {maxZ = point(2);}
                    }

                    CT_Scene *outScene = new CT_Scene(pcir);
                    outScene->setBoundingBox(minX, minY, minZ, maxX, maxY, maxZ);

                    group->addSingularItem(_outScene, outScene);
                }
            }
        }
    }
}
