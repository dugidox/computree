#include "tk_stepfacecolorrandom.h"

#include <time.h>

TK_StepFaceColorRandom::TK_StepFaceColorRandom() : SuperClass()
{
}


QString TK_StepFaceColorRandom::description() const
{
    return tr("Coloriser les faces aléatoirement");
}


CT_VirtualAbstractStep* TK_StepFaceColorRandom::createNewInstance() const
{
    return new TK_StepFaceColorRandom();
}

//////////////////// PROTECTED METHODS //////////////////


void TK_StepFaceColorRandom::declareInputModels(CT_StepInModelStructureManager& manager)
{

    manager.addResult(_inResult, tr("Mesh"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inMesh, tr("Mesh à coloriser"));
}


void TK_StepFaceColorRandom::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outFaceColors, tr("Couleurs aléatoires"));
}

void TK_StepFaceColorRandom::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_MeshModel* inMesh : group->singularItems(_inMesh))
        {
            if (isStopped()) {return;}

            // On recupere le nuage de face du mesh en entree et son nombre de faces
            if(inMesh->mesh()->abstractFace() != nullptr)
            {
                size_t nbFaces = inMesh->mesh()->abstractFace()->size();

                // On declare un nuage de couleur que l'on va remplir aleatoirement
                CT_ColorCloudStdVector *colorCloud = new CT_ColorCloudStdVector(false);
                CT_Color currentColor;

                for ( size_t i = 0 ; i < nbFaces ; i++ )
                {
                    if (isStopped()) {delete colorCloud; return;}

                    currentColor.r() = uchar(rand() % 256);
                    currentColor.g() = uchar(rand() % 256);
                    currentColor.b() = uchar(rand() % 256);
                    currentColor.a() = 255;

                    colorCloud->addColor( currentColor );

                    setProgress(float(100.0*i /nbFaces));
                    waitForAckIfInDebugMode();
                }

                // On cree les attributs que l'on met dans un groupe
                CT_FaceAttributesColor* colorAttribute = new CT_FaceAttributesColor(inMesh->mesh()->registeredFace(), colorCloud);
                group->addSingularItem(_outFaceColors, colorAttribute);
            }
        }
    }
}
