#ifndef TK_STEPEXTRACTPLOT_H
#define TK_STEPEXTRACTPLOT_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_scene.h"

class TK_StepExtractPlot : public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    TK_StepExtractPlot();

    QString description() const;

    QString detailledDescription() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    double _x;                            /*!< Coordonnee X du centre de la placette a extraire*/
    double _y;                            /*!< Coordonnee Y du centre de la placette a extraire*/
    double _radiusmin;                    /*!< Rayon (m) de debut de la placette a extraire*/
    double _radius;                       /*!< Rayon (m) de la placette a extraire*/
    double _azbegin;                      /*!< Azimut de debut (degres) de la placette a extraire*/
    double _azend;                        /*!< Azimut de fin (degres) de la placette a extraire*/
    double _zmin;                         /*!< Z minimum de la placette a extraire*/
    double _zmax;                         /*!< Z maximum de la placette a extraire*/

    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;


};

#endif // TK_STEPEXTRACTPLOT_H
