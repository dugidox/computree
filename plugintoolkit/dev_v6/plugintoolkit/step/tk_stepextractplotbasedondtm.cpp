#include "tk_stepextractplotbasedondtm.h"
#include "ct_log/ct_logmanager.h"


TK_StepExtractPlotBasedOnDTM::TK_StepExtractPlotBasedOnDTM() : SuperClass()
{
    _zmin = 1.0;
    _zmax = 1.6;
}

QString TK_StepExtractPlotBasedOnDTM::description() const
{
    return tr("Extraire les points dans une tranche parallèle au MNT");
}

QString TK_StepExtractPlotBasedOnDTM::detailledDescription() const
{
    return tr("Cette étape permet d'extraire les points de la scène d'entrée contenus dans une tranche de hauteur depuis le MNT.");
}

CT_VirtualAbstractStep* TK_StepExtractPlotBasedOnDTM::createNewInstance() const
{
    // cree une copie de cette etape
    return new TK_StepExtractPlotBasedOnDTM();
}

//////////////////// PROTECTED //////////////////

void TK_StepExtractPlotBasedOnDTM::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène(s)"));

    manager.addResult(_inResultDTM, tr("MNT"), tr("MNT"), true);
    manager.setZeroOrMoreRootGroup(_inResultDTM, _inZeroOrMoreRootGroupDTM);
    manager.addGroup(_inZeroOrMoreRootGroupDTM, _inGroupDTM);
    manager.addItem(_inGroupDTM, _inDTM, tr("MNT"));
}

void TK_StepExtractPlotBasedOnDTM::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("H minimum :"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 2, _zmin);
    postInputConfigDialog->addDouble(tr("H maximum :"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 2, _zmax);
}

void TK_StepExtractPlotBasedOnDTM::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outScene, tr("Scène extraite"));
}

void TK_StepExtractPlotBasedOnDTM::compute()
{
    const CT_Image2D<float>* mnt = nullptr;
    for (const CT_Image2D<float>* imageIn : _inDTM.iterateInputs(_inResultDTM))
    {
        mnt = imageIn;
    }

    if (mnt != nullptr)
    {
        float na = mnt->NA();

        for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
        {
            for (const CT_AbstractItemDrawableWithPointCloud* inScene : group->singularItems(_inScene))
            {
                if (isStopped()) {return;}

                const CT_AbstractPointCloudIndex *pointCloudIndex = inScene->pointCloudIndex();
                size_t n_points = pointCloudIndex->size();

                PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("La scène d'entrée comporte %1 points.")).arg(n_points));

                CT_PointCloudIndexVector *resPointCloudIndex = new CT_PointCloudIndexVector();
                resPointCloudIndex->setSortType(CT_PointCloudIndexVector::NotSorted);

                // Extraction des points de la placette
                size_t i = 0;

                CT_PointIterator itP(pointCloudIndex);
                while(itP.hasNext() && !isStopped())
                {
                    const CT_Point &point = itP.next().currentPoint();
                    size_t index = itP.currentGlobalIndex();

                    double hauteur;
                    float zMNT = mnt->valueAtCoords(point(0), point(1));
                    if (!qFuzzyCompare(zMNT, na)) {
                        hauteur = point(2) - double(zMNT);
                    } else {
                        hauteur = -std::numeric_limits<double>::max();
                    }

                    if (hauteur >= _zmin && hauteur <= _zmax) {

                        resPointCloudIndex->addIndex(index);

                    }

                    // progres de 0 à 100
                    setProgress(float(100.0*i/n_points));
                    ++i;
                }

                if (resPointCloudIndex->size() > 0)
                {
                    resPointCloudIndex->setSortType(CT_PointCloudIndexVector::SortedInAscendingOrder);

                    // creation et ajout de la scene
                    CT_Scene *outScene = new CT_Scene(PS_REPOSITORY->registerPointCloudIndex(resPointCloudIndex));
                    outScene->updateBoundingBox();

                    group->addSingularItem(_outScene, outScene);

                    PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("La scène extraite comporte %1 points.")).arg(outScene->pointCloudIndex()->size()));

                } else {
                    delete resPointCloudIndex;
                    PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Aucun point n'est dans l'emprise choisie"));

                }

            }
        }
    }

}

