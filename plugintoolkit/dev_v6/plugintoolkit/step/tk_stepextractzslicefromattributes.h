#ifndef TK_STEPEXTRACTZSLICEFROMATTRIBUTES_H
#define TK_STEPEXTRACTZSLICEFROMATTRIBUTES_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ctlibstructure/ct_itemattributes/abstract/ct_abstractitemattribute.h"

class TK_StepExtractZSliceFromAttributes : public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    TK_StepExtractZSliceFromAttributes();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
    double    _botZ;
    double    _topZ;
    bool      _useFixedBot;
    bool      _useFixedTop;

    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInSingularItem<CT_AbstractSingularItemDrawable>                            _inItem;
    CT_HandleInStdItemAttribute<CT_AbstractCategory::NUMBER>                            _inAttributeBot;
    CT_HandleInStdItemAttribute<CT_AbstractCategory::NUMBER>                            _inAttributeTop;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;

};

#endif // TK_STEPEXTRACTZSLICEFROMATTRIBUTES_H
