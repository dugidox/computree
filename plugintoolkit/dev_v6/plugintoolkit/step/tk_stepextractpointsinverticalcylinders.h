#ifndef TK_STEPEXTRACTPOINTSINVERTICALCYLINDERS_H
#define TK_STEPEXTRACTPOINTSINVERTICALCYLINDERS_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_view/tools/ct_textfileconfigurationdialog.h"

#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_standarditemgroup.h"

class TK_StepExtractPointsInVerticalCylinders: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    TK_StepExtractPointsInVerticalCylinders();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInSingularItem<CT_AbstractSingularItemDrawable>                            _inFileNameItem;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::STRING>     _inAttFileName;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;

    // TODOV6 - Erreur de compilation : "Impossible de convertir l'argument 1 de 'cont QString' en const 'ICategoryForModel*' with ItemAttributeT=CT_StdItemAttributeT<QString>
    CT_HandleOutStdItemAttribute<QString>                                               _outAttIDModelName;


    struct CylData {
        CylData(QString id, double x, double y, double z, double zmin, double zmax, double radius)
        {
            _id = id;
            _x = x;
            _y = y;
            _z = z;
            _zmin = zmin;
            _zmax = zmax;
            _radius = radius;
        }

        bool contains(double x, double y, double z) const
        {
            if (z >= _zmin && z <= _zmax)
            {
                double distance = sqrt(pow(x - _x, 2) + pow(y - _y, 2));
                if (distance <= _radius)
                {
                    return true;
                }
            }
            return false;
        }

        QString _id;
        double _x;
        double _y;
        double _z;
        double _zmin;
        double _zmax;
        double _radius;
    };

    // Step parameters
    QList<CT_TextFileConfigurationFields> _neededFields;

    QString _refFileName;

    bool _refHeader;
    QString _refSeparator;
    QString _refDecimal;
    QLocale _refLocale;
    int _refSkip;
    QMap<QString, int> _refColumns;
    QStringList _plotsIds;
    bool _translate;

    void extractCylindersWithoutTranslation(CT_StandardItemGroup* group, QMultiMap<QString, CylData*> cylinders, const CT_AbstractItemDrawableWithPointCloud *in_scene, QString plotName);
    void extractCylindersWithTranslation   (CT_StandardItemGroup* group, QMultiMap<QString, CylData*> cylinders, const CT_AbstractItemDrawableWithPointCloud *in_scene, QString plotName);

};

#endif // TK_STEPEXTRACTPOINTSINVERTICALCYLINDERS_H
