#ifndef TK_ACTIONSLICEPOINTCLOUDOPTIONS_H
#define TK_ACTIONSLICEPOINTCLOUDOPTIONS_H

#include "ctlibaction/ct_actions/view/abstract/ct_gabstractactionforgraphicsviewoptions.h"

class TK_ActionSlicePointCloud;

namespace Ui {
class TK_ActionSlicePointCloudOptions;
}

class TK_ActionSlicePointCloudOptions : public CT_GAbstractActionForGraphicsViewOptions
{
    Q_OBJECT

public:

    explicit TK_ActionSlicePointCloudOptions(const TK_ActionSlicePointCloud *action);
    ~TK_ActionSlicePointCloudOptions();

    double getThickness() const;
    double getSpacing() const;
    double getIncrement() const;

    void setThickness(double t) const;
    void setSpacing(double s) const;

    void increaseIncrement();
    void decreaseIncrement();

private:
    Ui::TK_ActionSlicePointCloudOptions *ui;

signals:
    void parametersChanged();

private slots:
    void changeSingleStep(int button);

};

#endif // TK_ACTIONSLICEPOINTCLOUDOPTIONS_H
