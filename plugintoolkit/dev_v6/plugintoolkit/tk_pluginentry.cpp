#include "tk_pluginentry.h"
#include "tk_steppluginmanager.h"

TK_PluginEntry::TK_PluginEntry()
{
    _stepPluginManager = new TK_StepPluginManager();
}

TK_PluginEntry::~TK_PluginEntry()
{
    delete _stepPluginManager;
}

QString TK_PluginEntry::getVersion() const
{
    return "1.0";
}

CT_AbstractStepPlugin* TK_PluginEntry::getPlugin() const
{
    return _stepPluginManager;
}

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    Q_EXPORT_PLUGIN2(plug_toolkit, TK_PluginEntry)
#endif
