#include "tk_steppluginmanager.h"
#include "ct_stepseparator.h"
#include "ct_steploadfileseparator.h"
#include "ct_stepcanbeaddedfirstseparator.h"
#include "ct_actions/ct_actionsseparator.h"

// Operations geometriques de base
#include "step/tk_translatecloud.h"
#include "step/tk_stepscalecloud.h"
#include "step/tk_steprotatecloud.h"

// Center le nuage de points
#include "step/tk_stepcentercloud.h"

// Extract sub clouds
#include "step/tk_stepextractbox.h"
#include "step/tk_stepextractcylinder.h"
#include "step/tk_stepextractsphere.h"
#include "step/tk_stepextractbboxpart.h"
#include "step/tk_stepextractzslice.h"
#include "step/tk_stepextractzslicefromattributes.h"
#include "step/tk_stepsplitscene.h"
#include "step/tk_stepextractverticalcloudpart.h"
#include "step/tk_stepmergeclouds.h"
// Thresholds
#include "step/tk_stepfilterpointsbyattribute.h"

// Colorize
#include "step/tk_stepcolorbyaxis.h"
#include "step/tk_stepcolorrandom.h"
#include "step/tk_stepfacecolorrandom.h"

// Normals
#include "step/tk_stepestimatenormalsrandom.h"
#include "step/tk_stepfacenormalestimator.h"
#ifdef USE_PCL
#include "step/tk_stepnormalestimator.h"
#endif

TK_StepPluginManager::TK_StepPluginManager() : CT_AbstractStepPlugin()
{
}

TK_StepPluginManager::~TK_StepPluginManager()
{
}

QString TK_StepPluginManager::getPluginRISCitation() const
{
    return "TY  - COMP\n"
           "TI  - Plugin ToolKit for Computree\n"
           "AU  - Ravaglia, Joris\n"
           "AU  - Krebs, Michael\n"
           "AU  - Piboule, Alexandre\n"
           "PB  - Office National des Forêts, RDI Department\n"
           "PY  - 2017\n"
           "UR  - http://rdinnovation.onf.fr/projects/plugin-toolkit/wiki\n"
           "ER  - \n";
}


bool TK_StepPluginManager::loadGenericsStep()
{    
    addNewPointsStep<TK_StepCenterCloud>(CT_StepsMenu::LP_Transform);
    addNewPointsStep<TK_TranslateCloud>(CT_StepsMenu::LP_Transform);
    addNewPointsStep<TK_StepScaleCloud>(CT_StepsMenu::LP_Transform);
    addNewPointsStep<TK_StepRotateCloud>(CT_StepsMenu::LP_Transform);

    addNewPointsStep<TK_StepExtractBBoxPart>(CT_StepsMenu::LP_Extract);
    addNewBetaStep<TK_StepExtractZSliceFromAttributes>(CT_StepsMenu::LP_Extract);
    addNewPointsStep<TK_StepExtractZSlice>(CT_StepsMenu::LP_Extract);
    addNewPointsStep<TK_StepExtractBox>(CT_StepsMenu::LP_Extract);
    addNewPointsStep<TK_StepExtractCylinder>(CT_StepsMenu::LP_Extract);
    addNewPointsStep<TK_StepExtractSphere>(CT_StepsMenu::LP_Extract);
    addNewPointsStep<TK_StepExtractVerticalCloudPart>(CT_StepsMenu::LP_Extract);
    //addNewPointsStep<TK_StepSplitScene>(CT_StepsMenu::LP_Extract);

    addNewPointsStep<TK_StepMergeClouds>(CT_StepsMenu::LP_Create);

    addNewPointsStep<TK_StepFilterPointsByAttribute>(CT_StepsMenu::LP_Filter);

    addNewPointsStep<TK_StepColorByAxis>(CT_StepsMenu::LP_Colorize);
    addNewPointsStep<TK_StepColorRandom>(CT_StepsMenu::LP_Colorize);
    addNewMeshesStep<TK_StepFaceColorRandom>(CT_StepsMenu::LP_Colorize);

    addNewOtherStep<TK_StepEstimateNormalsRandom>(QObject::tr("Générer (test)"));
    addNewMeshesStep<TK_StepFaceNormalEstimator>(CT_StepsMenu::LP_Create);

#ifdef USE_PCL
    addNewPointsStep<TK_StepNormalEstimator>(QObject::tr("Normales"));
#endif

    return true;
}

bool TK_StepPluginManager::loadOpenFileStep()
{
    return true;
}

bool TK_StepPluginManager::loadCanBeAddedFirstStep()
{
    return true;
}

bool TK_StepPluginManager::loadActions()
{
    return true;
}

bool TK_StepPluginManager::loadExporters()
{
    return true;
}

bool TK_StepPluginManager::loadReaders()
{
    return true;
}

