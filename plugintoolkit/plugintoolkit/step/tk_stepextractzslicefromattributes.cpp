#include "tk_stepextractzslicefromattributes.h"

// Utilise le depot
#include "ct_global/ct_context.h"

#include <assert.h>
#include <limits>

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_iterator/ct_pointiterator.h"

#include <QDebug>

// Alias for indexing in models
#define DEF_inputResult "inputResult"
#define DEF_inGrp "inputGroup"
#define DEF_inScene "inputScene"

#define DEF_inItem "inItem"
#define DEF_inATTBot "inATTBot"
#define DEF_inATTTop "inATTTop"


// Constructor : initialization of parameters
TK_StepExtractZSliceFromAttributes::TK_StepExtractZSliceFromAttributes(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _botZ = 0.5;
    _topZ = 5.0;

    _useFixedBot = false;
    _useFixedTop = false;
}

// Step description (tooltip of contextual menu)
QString TK_StepExtractZSliceFromAttributes::getStepDescription() const
{
    return tr("Extraire les points dans une tranche horizontale (attributs)");
}

// Step copy method
CT_VirtualAbstractStep* TK_StepExtractZSliceFromAttributes::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new TK_StepExtractZSliceFromAttributes(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////


// Semi-automatic creation of step parameters DialogBox
void TK_StepExtractZSliceFromAttributes::createPreConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPreConfigurationDialog();

    configDialog->addBool(tr("Fixer Z Minimum"), "", "", _useFixedBot);
    configDialog->addBool(tr("Fixer Z Maximum"), "", "", _useFixedTop);
    configDialog->addEmpty();
    configDialog->addTitle(tr("N.B. : Les limites non fixées seront déterminées à partir d'un attribut d'item."));
}

void TK_StepExtractZSliceFromAttributes::createPostConfigurationDialog()
{
    if (_useFixedBot || _useFixedTop)
    {
        CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

        if (_useFixedBot)
        {
            configDialog->addDouble(tr("Z Minimum"), "m", -1e+10, 1e+10, 4, _botZ);
        } else {
            configDialog->addTitle(tr("N.B. : Z Minimum déterminé à partir d'un attribut d'item"));
        }

        if (_useFixedTop)
        {
            configDialog->addDouble(tr("Z Maximum"), "m", -1e+10, 1e+10, 4, _topZ);
        } else {
            configDialog->addTitle(tr("N.B. : Z Maximum déterminé à partir d'un attribut d'item"));
        }
    }
}

// Creation and affiliation of IN models
void TK_StepExtractZSliceFromAttributes::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_inputResult, tr("Scene(s)"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_inGrp, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultModel->addItemModel(DEF_inGrp, DEF_inScene, CT_Scene::staticGetType(), tr("Scene(s)"));

    if (!_useFixedBot || !_useFixedTop)
    {
        resultModel->addItemModel(DEF_inGrp, DEF_inItem, CT_AbstractSingularItemDrawable::staticGetType(), tr("Item"));
        if (!_useFixedBot)
        {
            resultModel->addItemAttributeModel(DEF_inItem, DEF_inATTBot, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::ANY, tr("Zmin"));
        }
        if (!_useFixedTop)
        {
            resultModel->addItemAttributeModel(DEF_inItem, DEF_inATTTop, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::ANY, tr("Zmax"));
        }
    }
}

// Creation and affiliation of OUT models
void TK_StepExtractZSliceFromAttributes::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_inputResult);

    if(res != NULL)
        res->addItemModel(DEF_inGrp, _outScene_ModelName, new CT_Scene(), tr("Extracted Scene"));
}

void TK_StepExtractZSliceFromAttributes::compute()
{

    CT_ResultGroup* res = getOutResultList().first();

    CT_ResultGroupIterator it(res, this, DEF_inGrp);
    while (it.hasNext() && !isStopped())
    {        
        CT_StandardItemGroup* grp = (CT_StandardItemGroup*) it.next();
        const CT_Scene* scene = (const CT_Scene*)grp->firstItemByINModelName(this, DEF_inScene);
        const CT_AbstractSingularItemDrawable* item = NULL;

        if (!_useFixedBot || !_useFixedTop)
        {
            item = (const CT_AbstractSingularItemDrawable*)grp->firstItemByINModelName(this, DEF_inItem);
        }

        if (scene != NULL)
        {
            const CT_AbstractPointCloudIndex *pointCloudIndex = scene->getPointCloudIndex();
            size_t nbPoints = pointCloudIndex->size();

            // On Cree un nouveau nuage qui sera le translate
            CT_PointCloudIndexVector *extractedCloud = new CT_PointCloudIndexVector();

            // Cloud bounding box
            double min;
            double max;

            if (_useFixedTop || item == NULL)
            {
                max =  _topZ;
            } else {
                CT_AbstractItemAttribute* att = item->firstItemAttributeByINModelName(res, this, DEF_inATTTop);
                if (att != NULL)
                {
                    max = att->toDouble(scene, NULL);
                } else {
                    qDebug() << "Attribut Zmax non trouvé !!!";
                }
            }

            if (_useFixedBot || item == NULL)
            {
                min = _botZ;
            } else {
                CT_AbstractItemAttribute* att = item->firstItemAttributeByINModelName(res, this, DEF_inATTBot);
                if (att != NULL)
                {
                    min = att->toDouble(scene, NULL);
                } else {
                    qDebug() << "Attribut Zmin non trouvé !!!";
                }
            }

            CT_PointIterator itP(pointCloudIndex);

            size_t i = 0;
            while (itP.hasNext() && !isStopped())
            {
                const CT_Point &point = itP.next().currentPoint();
                size_t index = itP.currentGlobalIndex();

                if ( point(2) <= max &&
                     point(2) >= min)
                {
                    extractedCloud->addIndex(index);
                }

                setProgress( 100.0*i++ /nbPoints );
            }

            if (extractedCloud->size() > 0)
            {
                CT_Scene* outScene = new CT_Scene(_outScene_ModelName.completeName(), res, PS_REPOSITORY->registerPointCloudIndex(extractedCloud));
                outScene->updateBoundingBox();

                grp->addItemDrawable(outScene);
            }
        }
    }
}
