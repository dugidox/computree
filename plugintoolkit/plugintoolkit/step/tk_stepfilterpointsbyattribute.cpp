#include "tk_stepfilterpointsbyattribute.h"

// Utilise le depot
#include "ct_global/ct_context.h"

#include "ct_math/ct_mathpoint.h"

#include <limits>

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/abstract/ct_abstractpointattributesscalar.h"
#include "ct_iterator/ct_pointiterator.h"

// Alias for indexing in models
#define DEF_resultIn_inputResult "inputResult"
#define DEF_groupIn_inputScene "inputGroup"
#define DEF_itemIn_scene "inputScene"
#define DEF_itemIn_attribute "inputAttribute"

// Constructor : initialization of parameters
TK_StepFilterPointsByAttribute::TK_StepFilterPointsByAttribute(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _threshMin = -100;
    _threshMax = 100;
}

// Step description (tooltip of contextual menu)
QString TK_StepFilterPointsByAttribute::getStepDescription() const
{
    return tr("Filter les points sur un attribut");
}

// Step copy method
CT_VirtualAbstractStep* TK_StepFilterPointsByAttribute::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new TK_StepFilterPointsByAttribute(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void TK_StepFilterPointsByAttribute::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_resultIn_inputResult, tr("Scene(s)"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_groupIn_inputScene, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultModel->addItemModel(DEF_groupIn_inputScene, DEF_itemIn_scene, CT_Scene::staticGetType(), tr("Scene"));
    resultModel->addItemModel(DEF_groupIn_inputScene, DEF_itemIn_attribute, CT_AbstractPointAttributesScalar::staticGetType(), tr("Attribute"));
}

// Creation and affiliation of OUT models
void TK_StepFilterPointsByAttribute::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_resultIn_inputResult);

    if (resultModel != NULL)
    {
        resultModel->addItemModel(DEF_groupIn_inputScene, _outSceneModelName, new CT_Scene(), tr("Filtered Scene"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void TK_StepFilterPointsByAttribute::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Valeur minimum (incluse)"), "", -1e+10, 1e+10, 4, _threshMin);
    configDialog->addDouble(tr("Valeur maximum (incluse)"), "", -1e+10, 1e+10, 4, _threshMax);
}

void TK_StepFilterPointsByAttribute::compute()
{

    CT_ResultGroup* resultOut = getOutResultList().first();

    CT_ResultGroupIterator it(resultOut, this, DEF_groupIn_inputScene);
    while (it.hasNext())
    {
        CT_StandardItemGroup *group = (CT_StandardItemGroup*) it.next();

        if (group != NULL)
        {
            const CT_Scene* itemIn_scene = (const CT_Scene*)group->firstItemByINModelName(this, DEF_itemIn_scene);
            const CT_AbstractPointAttributesScalar* itemIn_Attribute = (const CT_AbstractPointAttributesScalar*)group->firstItemByINModelName(this, DEF_itemIn_attribute);

            if (itemIn_scene != NULL && itemIn_Attribute != NULL)
            {
                const CT_AbstractPointCloudIndex *pointCloudIndex = itemIn_scene->getPointCloudIndex();
                const CT_AbstractPointCloudIndex *attPointCloudIndex = itemIn_Attribute->getPointCloudIndex();

                if (pointCloudIndex != NULL && attPointCloudIndex != NULL)
                {
                    size_t nbPoints = pointCloudIndex->size();

                    // On Cree un nouveau nuage qui sera le translate
                    CT_PointCloudIndexVector *extractedCloud = new CT_PointCloudIndexVector();

                    CT_PointIterator itP(pointCloudIndex);

                    size_t i = 0;
                    while (itP.hasNext() && !isStopped())
                    {
                        size_t index = itP.next().currentGlobalIndex();
                        size_t localIndex = attPointCloudIndex->indexOf(index);

                        double attVal = itemIn_Attribute->dValueAt(localIndex);

                        if ( attVal <= _threshMax && attVal >= _threshMin )
                        {
                            extractedCloud->addIndex(index);
                        }

                        setProgress( 100.0*i++ /nbPoints );

                        waitForAckIfInDebugMode();
                    }


                    CT_Scene* itemOut_scene = new CT_Scene(_outSceneModelName.completeName(), resultOut, PS_REPOSITORY->registerPointCloudIndex(extractedCloud));
                    itemOut_scene->updateBoundingBox();
                    group->addItemDrawable(itemOut_scene);
                }
            }
        }
    }
}
