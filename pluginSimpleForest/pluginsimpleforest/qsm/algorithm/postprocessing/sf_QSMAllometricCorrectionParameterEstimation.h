/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_QSMALLOMETRICCORRECTIONPARAMETERESTIMATION_H
#define SF_QSMALLOMETRICCORRECTIONPARAMETERESTIMATION_H

#include "qsm/algorithm/sf_QSMCylinder.h"
#include "qsm/model/sf_modelQSM.h"
#include "steps/param/sf_paramAllSteps.h"

template<class ForwardIt>
ForwardIt
forestUnique(ForwardIt first, ForwardIt last)
{
  if (first == last)
    return last;

  ForwardIt result = first;
  while (++first != last) {
    if (!(*result == *first) && ++result != first) {
      *result = std::move(*first);
    }
  }
  return ++result;
}

class SF_QSMAllometricCorrectionParameterEstimation
{
  SF_ParamAllometricCorrectionNeighboring m_params;
  const float MINPERCENTAGE = 0.1;
  const float MAXPERCENTAGE = 0.99;
  float m_twigRadius = 0.;

protected:
  float getA(const std::vector<float>& x, const std::vector<float>& y, float b) const;
  void getTwigRadius(const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& bricks);
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> removeStem(
    const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& bricks) const;
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> chooseBestBricks(
    const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& bricks) const;
  bool isUnCorrectedRadiusFit(const std::shared_ptr<Sf_ModelAbstractBuildingbrick>& buildingBrick) const;

public:
  SF_QSMAllometricCorrectionParameterEstimation();
  void setParams(const SF_ParamAllometricCorrectionNeighboring& params);
  SF_ParamAllometricCorrectionNeighboring params() const;
  void compute();
};

#endif // SF_QSMALLOMETRICCORRECTIONPARAMETERESTIMATION_H
