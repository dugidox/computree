/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_qsmAllometricCorrection.h"

void
SF_QSMAllometricCorrection::setParams(const SF_ParamAllometricCorrectionNeighboring& params)
{
  m_params = params;
}

SF_ParamAllometricCorrectionNeighboring
SF_QSMAllometricCorrection::params() const
{
  return m_params;
}

void
SF_QSMAllometricCorrection::compute()
{
  if (!m_params._qsm) {
    return;
  }
  const auto translation = m_params._qsm->translateToOrigin();
  auto cylinders = m_params._qsm->getBuildingBricks();
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> branchCylinders;
  std::copy_if(cylinders.cbegin(), cylinders.cend(), std::back_inserter(branchCylinders), [](const auto& cylinder) {
    return !cylinder->isLowerStem();
  });
  std::for_each(branchCylinders.begin(), branchCylinders.end(), [&](std::shared_ptr<Sf_ModelAbstractBuildingbrick>& cylinder) {
    correct(cylinder);
  });
  m_params._qsm->translate(-translation);
}

void
SF_QSMAllometricCorrection::correct(std::shared_ptr<Sf_ModelAbstractBuildingbrick>& cylinder)
{
  auto inRange = [](float model, float predicted, float range) {
    const auto min = predicted / range;
    const auto max = predicted * range;
    return model > min && model < max;
  };
  float radiusModel = 0.f;
  if (m_params.m_useGrowthLength) {
    radiusModel = m_params._qsm->getA() * std::pow(cylinder->getGrowthLength(), m_params._qsm->getB()) + m_params._qsm->getC();
  } else if (m_params.m_useVesselVolume) {
    radiusModel = m_params._qsm->getA() * std::pow(cylinder->getVesselVolume(), m_params._qsm->getB()) + m_params._qsm->getC();
  } else {
    radiusModel = m_params._qsm->getA() * std::pow(cylinder->getGrowthVolume(), m_params._qsm->getB()) + m_params._qsm->getC();
  }
  radiusModel = std::max(radiusModel, 0.0001f);
  if (!inRange(cylinder->getRadius(), radiusModel, m_params._range) || cylinder->isPredictedRadius()) {
    if (m_params.m_useGrowthLength) {
      cylinder->setRadius(radiusModel, FittingType::ALLOMETRICGROWTHLENGTH);
    } else if (m_params.m_useVesselVolume) {
      cylinder->setRadius(radiusModel, FittingType::ALLOMETRICVESSELVOLUME);
    } else {
      cylinder->setRadius(radiusModel, FittingType::ALLOMETRICGROWTHVOLUME);
    }
  }
}
