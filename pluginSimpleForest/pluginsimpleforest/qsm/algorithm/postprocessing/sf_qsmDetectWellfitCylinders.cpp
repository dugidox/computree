/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_qsmDetectWellfitCylinders.h"

SF_QSMDetectWellfitCylinders::SF_QSMDetectWellfitCylinders() {}

void
SF_QSMDetectWellfitCylinders::setParams(const SF_ParamQSMDetectWellfitCylinders<SF_Point>& params)
{
  m_params = params;
}

SF_ParamQSMDetectWellfitCylinders<SF_Point>
SF_QSMDetectWellfitCylinders::params() const
{
  return m_params;
}

void
SF_QSMDetectWellfitCylinders::compute()
{
  if (!m_params._qsm) {
    return;
  }
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> cylinders = m_params._qsm->getBuildingBricks();
  const auto rad = m_params._qsm->getRootSegment()->getRadius();
  const auto rbopr = m_params._qsm->getRootSegment()->getRBOPR();
  const auto slopeExpeceted = rad / rbopr;
  auto isRBOPAClose = [&slopeExpeceted](std::shared_ptr<Sf_ModelAbstractBuildingbrick>& brick) -> bool {
    const auto predictedRadius = brick->getSegment()->getRBOPR() * slopeExpeceted;
    constexpr double fac = 4.0;
    return (brick->getRadius() > predictedRadius / fac && brick->getRadius() < predictedRadius * fac);
  };

  auto wasGoodFitted = [&](std::shared_ptr<Sf_ModelAbstractBuildingbrick>& brick) -> bool {
    return brick->getMaxFitQualityError() >= brick->getMean();
  };

  auto isFarFromLeave = [&](std::shared_ptr<Sf_ModelAbstractBuildingbrick>& brick) -> bool {
    const auto reverseBranchOrder = brick->getSegment()->getReverseBranchOrder();
    const auto branchOrder = brick->getSegment()->getBranchOrder();
    return reverseBranchOrder >= m_params.m_minReverseBranchOrder || branchOrder < 2;
  };
  for (auto brick : cylinders) {
    bool fitFlag = false;
    if (brick->isMeasuredRadius()) {
      if (isFarFromLeave(brick)) {
        if (wasGoodFitted(brick) && isRBOPAClose(brick)) {
          fitFlag = true;
        }
      }
    }
    brick->setGoodQuality(fitFlag);
  }
  m_params._qsm->setAllometryDirty(false);
  if (m_params.m_normalizeErrorVisualization && cylinders.size() > 1) {
    int smallest = 0;
    int nextSmallest = 1;
    if (cylinders[nextSmallest]->getVolume() < cylinders[smallest]->getVolume()) {
      nextSmallest = 0;
      smallest = 1;
    }
    for (std::uint32_t i = 2; i < cylinders.size(); i++) {
      if (cylinders[i]->getVolume() < cylinders[nextSmallest]->getVolume()) {
        nextSmallest = i;
        if (cylinders[nextSmallest]->getVolume() < cylinders[smallest]->getVolume()) {
          nextSmallest = smallest;
          smallest = i;
        }
      }
    }
    const double min = 0.001;
    const double max = 0.2;
    cylinders[smallest]->setMean(min);
    cylinders[nextSmallest]->setMean(max);
    for (auto cylinder : cylinders) {
      if (cylinder->getMean() < min) {
        cylinder->setMean(min);
      }
      if (cylinder->getMean() > max) {
        cylinder->setMean(max);
      }
    }
  }
}
