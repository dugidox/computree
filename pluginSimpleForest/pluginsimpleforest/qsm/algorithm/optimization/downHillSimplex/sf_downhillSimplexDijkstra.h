/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_DOWNHILLSIMPLEXDIJKSTRA_H
#define SF_DOWNHILLSIMPLEXDIJKSTRA_H

#include "../recursion/sf_dijkstraLightRecursive.h"

#include <gsl/gsl_blas.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_ieee_utils.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_min.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_multifit_nlinear.h>
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_test.h>

double
downhillSimplexDijkstra(const gsl_vector* v, void* params);

class SF_DownhillSimplexDijkstra
{
  SF_ParamDijkstra<SF_PointNormal> m_params;
  void serializeVec(gsl_vector* x, double fac, SF_ParamDijkstra<SF_PointNormal>& params);

public:
  SF_DownhillSimplexDijkstra();
  void compute();
  SF_ParamDijkstra<SF_PointNormal> params() const;
  void setParams(const SF_ParamDijkstra<SF_PointNormal>& params);
};

#endif // SF_DOWNHILLSIMPLEXDIJKSTRA_H
