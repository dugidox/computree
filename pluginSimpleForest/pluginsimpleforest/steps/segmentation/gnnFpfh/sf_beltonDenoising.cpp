/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_beltonDenoising.h"

#include <converters/CT_To_PCL/sf_converterCTToPCL.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/features/normal_3d.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/kdtree/kdtree.h>

#include "pcl/cloud/classification/SF_GnnFpfh.h"
#include "pcl/cloud/classification/sf_gnn.h"
#include "pcl/cloud/feature/fpfh/sf_fpfh.h"
#include "pcl/cloud/feature/pca/sf_pca.h"

#include <QDirIterator>

SF_BeltonDenoising::SF_BeltonDenoising(CT_StepInitializeData& dataInit)
  : SF_AbstractStepSegmentation(dataInit), m_maxNumberClusters(10)
{
  for (auto i = 0; i < m_maxNumberClusters; ++i) {
    m_outGrpClusters.push_back(CT_AutoRenameModels());
    m_outClusters.push_back(CT_AutoRenameModels());
  }
}

SF_BeltonDenoising::~SF_BeltonDenoising() {}

QString
SF_BeltonDenoising::getStepDescription() const
{
  return tr("Deleaving with BeltonEtAl 2013");
}

QString
SF_BeltonDenoising::getStepDetailledDescription() const
{
  return tr("Please read Belton et al: PROCESSING TREE POINT CLOUDS USING GAUSSIAN MIXTURE MODELS.");
}

QString
SF_BeltonDenoising::getStepURL() const
{
  return tr("https://www.simpleforest.org/");
}

CT_VirtualAbstractStep*
SF_BeltonDenoising::createNewInstance(CT_StepInitializeData& dataInit)
{
  return new SF_BeltonDenoising(dataInit);
}

QStringList
SF_BeltonDenoising::getStepRISCitations() const
{
  QStringList _risCitationList;
  _risCitationList.append(getRISCitationSimpleTree());
  _risCitationList.append(getRISCitationPCL());
  _risCitationList.append(
    QString("T1  - Processing Tree Point Clouds using Gaussian Mixture Models\n"
            "A1  - David Belton\n"
            "A1  - Simon Moncrieff\n"
            "A1  - Jane Chapman\n"
            "ISPRS Annals of the Photogrammetry, Remote Sensing and Spatial Information Sciences "
            "SP  - 2\n"
            "EP  - 5\n"
            "UL  - "
            "https://www.isprs-ann-photogramm-remote-sens-spatial-inf-sci.net/II-5-W2/43/2013/isprsannals-II-5-W2-43-2013.pdf\n"
            "ER  - \n"));
  return _risCitationList;
}

void
SF_BeltonDenoising::createInResultModelListProtected()
{
  CT_InResultModelGroupToCopy* resModel = createNewInResultModelForCopy(DEF_IN_RESULT, tr("Point Cloud"));
  resModel->setZeroOrMoreRootGroup();
  resModel->addGroupModel("",
                          DEF_IN_GRP_CLUSTER,
                          CT_AbstractItemGroup::staticGetType(),
                          tr("Cloud Group"),
                          "",
                          CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModel->addItemModel(DEF_IN_GRP_CLUSTER, DEF_IN_CLOUD_SEED, CT_Scene::staticGetType(), tr("Cloud"));
}

void
SF_BeltonDenoising::createPostConfigurationDialog()
{
  CT_StepConfigurableDialog* configDialog = newStandardPostConfigurationDialog();

  configDialog->addText("Please use for this step the");
  configDialog->addText(QObject::tr(" following citation:"), "Belton, D.; Moncrieff, S.; Chapman, J.");
  configDialog->addText("", "<em> Processing Tree Point Clouds using Gaussian Mixture Models.</em>");
  configDialog->addText("", "ISPRS <b>2013</b>, 2, 5.");
  configDialog->addEmpty();
  configDialog->addText("[<b>Only one input cloud allowed otherwise no manual classification is possible</b>].");
  configDialog->addText("[<b>Crash of this step under Linux was observed</b>].");
}

void
SF_BeltonDenoising::createOutResultModelListProtected()
{
  CT_OutResultModelGroupToCopyPossibilities* resModelw = createNewOutResultModelToCopy(DEF_IN_RESULT);
  if (resModelw != NULL) {
    QString grpLa = "Cluster grp number ";
    QString cloud = "Cluster number ";
    for (auto i = 0; i < m_maxNumberClusters; ++i) {
      QString grpLa2 = grpLa;
      grpLa2.append(QString::number(i));
      QString cloud2 = cloud;
      cloud2.append(QString::number(i));
      resModelw->addGroupModel(DEF_IN_GRP_CLUSTER, m_outGrpClusters[i], new CT_StandardItemGroup(), grpLa2);
      resModelw->addItemModel(m_outGrpClusters[i], m_outClusters[i], new CT_Scene(), cloud2);
    }
  }
}

void
SF_BeltonDenoising::compute()
{
  m_computationsDone = 0;
  const QList<CT_ResultGroup*>& out_result_list = getOutResultList();
  CT_ResultGroup* outResult = out_result_list.at(0);
  CT_ResultGroupIterator outResIt(outResult, this, DEF_IN_GRP_CLUSTER);
  while (!isStopped() && outResIt.hasNext()) {
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResIt.next();
    const CT_AbstractItemDrawableWithPointCloud* ctCloud = (const CT_AbstractItemDrawableWithPointCloud*)group->firstItemByINModelName(
      this, DEF_IN_CLOUD_SEED);

    Sf_ConverterCTToPCL<SF_PointNormal> converter;
    converter.setItemCpyCloudInDeprecated(ctCloud);
    converter.compute();
    SF_CloudNormal::Ptr cloud = converter.cloudTranslated();

    SF_CloudNormal::Ptr cloudDownscaled(new SF_CloudNormal());
    pcl::VoxelGrid<SF_PointNormal> sor;
    sor.setInputCloud(cloud);
    float voxelRange = static_cast<float>(0.02f);
    sor.setLeafSize(voxelRange, voxelRange, voxelRange);
    sor.filter(*cloudDownscaled);

    pcl::NormalEstimation<SF_PointNormal, SF_PointNormal> ne;

    ne.setInputCloud(cloudDownscaled);
    pcl::search::KdTree<SF_PointNormal>::Ptr tree(new pcl::search::KdTree<SF_PointNormal>());
    ne.setSearchMethod(tree);
    ne.setRadiusSearch(static_cast<float>(0.06f));
    ne.compute(*cloudDownscaled);

    std::vector<std::vector<float>> features(cloudDownscaled->points.size(), std::vector<float>());
    for (int i = 1; i < 6; ++i) {
      float rad = 0.1f * i;
      {
        SF_PCA<SF_PointNormal> sfpca(cloudDownscaled);
        sfpca.setParameters(rad, false, true);
        sfpca.computeFeatures();
        std::vector<SF_PCAValues> values = sfpca.getPcaValues();
        for (size_t j = 0; j < values.size(); ++j) {
          auto src = values[j].getDescription();
          auto& tar = features[j];
          tar.insert(tar.end(), src.begin(), src.end());
        }
      }
      {
        SF_PCA<SF_PointNormal> sfpca(cloudDownscaled);
        sfpca.setParameters(rad, true, true);
        sfpca.computeFeatures();
        std::vector<SF_PCAValues> values = sfpca.getPcaValues();
        for (size_t j = 0; j < values.size(); ++j) {
          auto src = values[j].getDescription();
          auto& tar = features[j];
          tar.insert(tar.end(), src.begin(), src.end());
        }
      }
    }

    pcl::PointCloud<pcl::FPFHSignature33>::Ptr featuresDummy(new pcl::PointCloud<pcl::FPFHSignature33>());
    SF_Gnn gnn(featuresDummy);
    gnn.setParameters(m_numberClusters, 6);
    gnn.compute(features);
    std::vector<int> indicesClusters = gnn.getIndices();
    pcl::search::KdTree<pcl::PointXYZINormal>::Ptr kdtree(new pcl::search::KdTree<pcl::PointXYZINormal>);
    kdtree->setInputCloud(cloudDownscaled);
    std::vector<CT_PointCloudIndexVector*> outIndices;
    for (std::int32_t i = 0; i < m_maxNumberClusters; ++i) {
      outIndices.push_back(new CT_PointCloudIndexVector());
    }
    const CT_AbstractPointCloudIndex* pointCloudIndex = ctCloud->getPointCloudIndex();
    CT_PointIterator itP(pointCloudIndex);
    std::uint32_t indexPCL = 0;
    while (itP.hasNext() && !isStopped()) {
      std::uint32_t indexCt = itP.currentGlobalIndex();
      pcl::PointXYZINormal point = cloud->points[indexPCL];
      indexPCL++;
      std::vector<int> pointIdxNKNSearch(1);
      std::vector<float> pointNKNSquaredDistance(1);
      if (kdtree->nearestKSearch(point, 1, pointIdxNKNSearch, pointNKNSquaredDistance) > 0) {
        int indexPoint = pointIdxNKNSearch[0];
        int indexCluster = indicesClusters[indexPoint];
        outIndices[indexCluster]->addIndex(indexCt);
      }
      itP.next();
    }

    for (std::int32_t i = 0; i < m_maxNumberClusters; ++i) {
      CT_StandardItemGroup* filterGrp = new CT_StandardItemGroup(m_outGrpClusters[i].completeName(), outResult);
      group->addGroup(filterGrp);
      CT_Scene* outScene = new CT_Scene(
        m_outClusters[i].completeName(), outResult, PS_REPOSITORY->registerPointCloudIndex(outIndices[i]));
      outScene->updateBoundingBox();
      filterGrp->addItemDrawable(outScene);
    }
  }
}
