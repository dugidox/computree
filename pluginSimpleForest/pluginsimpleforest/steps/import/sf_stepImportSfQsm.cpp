/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_stepImportSfQsm.h"

#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>
#include <fstream>
#include <sstream>
#include <string>

#include <ct_itemdrawable/ct_fileheader.h>

#include "file/import/sf_importSfQsm.h"

SF_StepImportSfQsm::SF_StepImportSfQsm(CT_StepInitializeData& dataInit) : SF_AbstractStepQSM(dataInit) {}

SF_StepImportSfQsm::~SF_StepImportSfQsm() {}

QString
SF_StepImportSfQsm::getStepDescription() const
{
  return tr("SF QSM loader.");
}

QString
SF_StepImportSfQsm::getStepDetailledDescription() const
{
  return tr("Imports geometry, topology and fitting type of SF QSMs.");
}

QString
SF_StepImportSfQsm::getStepURL() const
{
  return tr("http://simpleforest.org/");
}

CT_VirtualAbstractStep*
SF_StepImportSfQsm::createNewInstance(CT_StepInitializeData& dataInit)
{
  return new SF_StepImportSfQsm(dataInit);
}

void
SF_StepImportSfQsm::createPostConfigurationDialog()
{
  CT_StepConfigurableDialog* configDialog = newStandardPostConfigurationDialog();
  configDialog->addText("<b>SF cylinder csv files</b>:");
  configDialog->addFileChoice(tr("Select the csv files"), CT_FileChoiceButton::OneOrMoreExistingFiles, "", m_filePath);
}

void
SF_StepImportSfQsm::createInResultModelListProtected()
{
  CT_InResultModelGroup* resModelName2 = createNewInResultModelForCopy(DEF_IN_RESULT3, tr("Input for cloud"), "", true);
  resModelName2->setZeroOrMoreRootGroup();
  resModelName2->addGroupModel("",
                               DEF_IN_GRP_CLUSTER,
                               CT_AbstractItemGroup::staticGetType(),
                               tr("Cloud Group"),
                               "",
                               CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModelName2->addItemModel(DEF_IN_GRP_CLUSTER, DEF_IN_CLOUD_SEED, CT_Scene::staticGetType(), tr("QSM cloud"));
}

void
SF_StepImportSfQsm::createOutResultModelListProtected()
{
  CT_OutResultModelGroupToCopyPossibilities* resModelw = createNewOutResultModelToCopy(DEF_IN_RESULT3);
  if (resModelw != NULL) {
    QString name = tr("Imported SF Qsm");
    addQSMToOutResult(resModelw, name, QString::fromUtf8(DEF_IN_GRP_CLUSTER));
    name.append(" internal Qsm");
    resModelw->addItemModel(_QSMGrp, _outSFQSM, new SF_QSMItem(), name);
  }
}

void
SF_StepImportSfQsm::compute()
{
  using namespace std;
  using namespace boost;
  using boost::lexical_cast;
  if (m_filePath.empty() || m_filePath.first() == "") {
    QString errorMsg = "SF_StepImportSfQsm please give a path to import the qsm.";
    PS_LOG->addMessage(LogInterface::info, LogInterface::step, errorMsg);
    return;
  }
  const QList<CT_ResultGroup*>& outResultList = getOutResultList();
  CT_ResultGroup* outResult = outResultList.at(0);
  CT_ResultGroupIterator outResIt(outResult, this, DEF_IN_GRP_CLUSTER);
  std::uint32_t index = 0;
  while (!isStopped() && outResIt.hasNext()) {
    CT_StandardItemGroup* group = const_cast<CT_StandardItemGroup*>(static_cast<const CT_StandardItemGroup*>(outResIt.next()));
    QString path = m_filePath[index];
    ++index;
    SF_ImportSfQsm importer = SF_ImportSfQsm();
    try {
      auto qsm = importer.import(path);
      qsm->setName(path.toStdString());
      CT_StandardItemGroup* qsmGrp = new CT_StandardItemGroup(_QSMGrp.completeName(), outResult);
      CT_TTreeGroup* tree = constructTopology(outResult, qsm);
      group->addGroup(qsmGrp);
      qsmGrp->addGroup(tree);
      SF_QSMItem* qsmItem = new SF_QSMItem(_outSFQSM.completeName(), outResult, qsm);
      qsmGrp->addItemDrawable(qsmItem);
    } catch (const std::runtime_error& err) {
      std::cout << " err.what() " << err.what() << std::endl;
      PS_LOG->addMessage(LogInterface::error, LogInterface::step, QString::fromStdString(err.what()));
    }
  }
}
