/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_stepImportTreeQsm.h"

#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>
#include <fstream>
#include <sstream>
#include <string>

#include <ct_itemdrawable/ct_fileheader.h>

#include "file/import/sf_importTreeQsm.h"

SF_StepImportTreeQsm::SF_StepImportTreeQsm(CT_StepInitializeData& dataInit) : SF_AbstractStepQSM(dataInit) {}

SF_StepImportTreeQsm::~SF_StepImportTreeQsm() {}

QString
SF_StepImportTreeQsm::getStepDescription() const
{
  return tr("Tree QSM loader.");
}

QString
SF_StepImportTreeQsm::getStepDetailledDescription() const
{
  return tr("Tree QSM written and maintained by Pasi Raumonen can stores its QSMs in a csv format. Also export to ply can be "
            "performed.");
}

QString
SF_StepImportTreeQsm::getStepURL() const
{
  return tr("https://github.com/InverseTampere/TreeQSM/");
}

CT_VirtualAbstractStep*
SF_StepImportTreeQsm::createNewInstance(CT_StepInitializeData& dataInit)
{
  return new SF_StepImportTreeQsm(dataInit);
}

void
SF_StepImportTreeQsm::createPostConfigurationDialog()
{
  CT_StepConfigurableDialog* configDialog = newStandardPostConfigurationDialog();
  configDialog->addText("<b>treeQSM cylinder csv files</b>:");
  configDialog->addFileChoice(tr("Select the csv files"), CT_FileChoiceButton::OneOrMoreExistingFiles, "", m_filePath);
  configDialog->addBool("Load unfiltered QSM radius named", "", "UnmodRadius", m_originalRadius);
}

void
SF_StepImportTreeQsm::createInResultModelListProtected()
{
  CT_InResultModelGroup* resModelName2 = createNewInResultModelForCopy(DEF_IN_RESULT3, tr("Input for cloud"), "", true);
  resModelName2->setZeroOrMoreRootGroup();
  resModelName2->addGroupModel("",
                               DEF_IN_GRP_CLUSTER,
                               CT_AbstractItemGroup::staticGetType(),
                               tr("Cloud Group"),
                               "",
                               CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModelName2->addItemModel(DEF_IN_GRP_CLUSTER, DEF_IN_CLOUD_SEED, CT_Scene::staticGetType(), tr("QSM cloud"));
}

void
SF_StepImportTreeQsm::createOutResultModelListProtected()
{
  CT_OutResultModelGroupToCopyPossibilities* resModelw = createNewOutResultModelToCopy(DEF_IN_RESULT3);
  if (resModelw != NULL) {
    QString name = tr("TreeQSM cylinders without topology imported from Matlab csv out");
    addQSMToOutResult(resModelw, name, QString::fromUtf8(DEF_IN_GRP_CLUSTER));
  }
}

void
SF_StepImportTreeQsm::compute()
{
  using namespace std;
  using namespace boost;
  using boost::lexical_cast;
  if (m_filePath.empty() || m_filePath.first() == "") {
    QString errorMsg = "SF_StepImportTreeQsm please give a path to import the qsm.";
    PS_LOG->addMessage(LogInterface::info, LogInterface::step, errorMsg);
    return;
  }
  const QList<CT_ResultGroup*>& outResultList = getOutResultList();
  CT_ResultGroup* outResult = outResultList.at(0);
  CT_ResultGroupIterator outResIt(outResult, this, DEF_IN_GRP_CLUSTER);
  std::uint32_t index = 0;
  while (!isStopped() && outResIt.hasNext()) {
    CT_StandardItemGroup* group = const_cast<CT_StandardItemGroup*>(static_cast<const CT_StandardItemGroup*>(outResIt.next()));
    QString path = m_filePath[index];
    ++index;
    SF_ImportTreeQsm importer = SF_ImportTreeQsm();
    try {
      auto qsm = importer.import(path, m_originalRadius);
      CT_StandardItemGroup* qsmGrp = new CT_StandardItemGroup(_QSMGrp.completeName(), outResult);
      CT_TTreeGroup* tree = constructTopology(outResult, qsm);
      group->addGroup(qsmGrp);
      qsmGrp->addGroup(tree);
    } catch (const std::runtime_error& err) {
      std::cout << " err.what() " << err.what() << std::endl;
      PS_LOG->addMessage(LogInterface::error, LogInterface::step, QString::fromStdString(err.what()));
    }
  }
}
