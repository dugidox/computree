/****************************************************************************

Copyright (C) 2017-2022 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_stepQSMAllometricCorrectionAdapter.h"

SF_QSMAllometricCheckAdapter::SF_QSMAllometricCheckAdapter(const SF_QSMAllometricCheckAdapter& obj)
{
  mMutex = obj.mMutex;
}

SF_QSMAllometricCheckAdapter::SF_QSMAllometricCheckAdapter()
{
  mMutex.reset(new QMutex);
}

void
SF_QSMAllometricCheckAdapter::operator()(SF_ParamAllometricCorrectionNeighboring& params)
{
  SF_ParamAllometricCorrectionNeighboring paramsCpy;
  {
    QMutexLocker m1(&*mMutex);
    paramsCpy = params;
  }
  {
    QMutexLocker m1(&*mMutex);
    {
      std::unordered_map<std::uint32_t, std::vector<float>> histogramMap;
      auto filter = [&](const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>& cylinders) {
        for (auto& cylinder : cylinders) {
          if (cylinder->getGoodQuality()) {
            const auto x = static_cast<std::uint32_t>(std::max(1., std::ceil(cylinder->getSegment()->getRBOPR())) - 1.);
            histogramMap[x].push_back(cylinder->getRadius());
          }
        }
        std::vector<float> x;
        std::vector<float> y;
        for (auto& pair : histogramMap) {
          x.push_back(static_cast<float>(pair.first) - 0.5f);
          y.push_back(SF_Math<float>::getMedian(pair.second));
        }
        SF_FitLineWithIntercept<float> lineFit;
        lineFit.setX(x);
        lineFit.setY(y);
        constexpr double inlierDistance = 0.015;
        lineFit.setInlierDistance(inlierDistance);
        lineFit.setIterations(100);
        constexpr std::uint32_t minPts = 5;
        lineFit.setMinPts(minPts);
        lineFit.setUseWeight(true);
        constexpr double minRadius = 0.0025;
        lineFit.setIntercept(minRadius);
        lineFit.compute();
        auto equation = lineFit.equation();
        for (auto& cylinder : cylinders) {
          if (cylinder->getSegment()->getID() <= 1) {
            continue;
          }
          const auto radius = cylinder->getRadius();
          auto x = static_cast<std::uint32_t>(std::max(1., std::ceil(cylinder->getSegment()->getRBOPR())) - 1.);
          const auto radiusModel = equation.first * x + equation.second;
          constexpr double range = 0.5;
          const auto deviation = radiusModel * range;
          const auto min = radiusModel - deviation;
          const auto max = radiusModel + deviation;
          if (min > radius || max < radius) {
            cylinder->setRadius(radiusModel, FittingType::REVERSEPIPEMODEL);
          }
        }
      };
      auto cylinders = paramsCpy._qsm->getBuildingBricks();
      auto split = splitIntoStemAndNoStem(cylinders);
      filter(split.second);
    }
  }

  if (paramsCpy.m_useTaperFit) {
    {
      SF_FitLineWithIntercept<float> lineFit;
      std::vector<float> x;
      std::vector<float> y;
      for (auto& cylinder : paramsCpy._qsm->getBuildingBricks()) {
        if (cylinder->getGoodQuality() && cylinder->getSegment()->getBranchOrder() > 0) {
          x.push_back(cylinder->getDistanceToTwig());
          y.push_back(cylinder->getRadius() * cylinder->getRadius());
        }
      }
      lineFit.setX(x);
      lineFit.setY(y);
      lineFit.setInlierDistance(paramsCpy.m_inlierDistance);
      lineFit.setIterations(100);
      lineFit.setMinPts(paramsCpy.m_minPts);
      lineFit.setUseWeight(true);
      lineFit.setIntercept(0.f);
      lineFit.compute();
      auto equation = lineFit.equation();
      for (auto& cylinder : paramsCpy._qsm->getBuildingBricks()) {
        if (cylinder->getSegment()->getBranchOrder() == 0) {
          continue;
        }
        const auto radius = cylinder->getRadius();
        const auto distance = cylinder->getDistanceToTwig();
        const auto radiusModel = std::sqrt(equation.first * distance + equation.second);
        const auto min = 0.7 * radiusModel;
        const auto max = radiusModel;
        if (min > radius || max < radius) {
          cylinder->setRadius(radiusModel, FittingType::TAPERFIT);
        }
      }
      {
        QMutexLocker m1(&*mMutex);
        params = paramsCpy;
      }
    }
    {
      SF_FitLineWithIntercept<float> lineFit;
      std::vector<float> x;
      std::vector<float> y;
      for (auto& cylinder : paramsCpy._qsm->getBuildingBricks()) {
        if (cylinder->getGoodQuality() && cylinder->getSegment()->getBranchOrder() == 0) {
          x.push_back(cylinder->getDistanceToTwig());
          y.push_back(cylinder->getRadius() * cylinder->getRadius());
        }
      }
      lineFit.setX(x);
      lineFit.setY(y);
      lineFit.setInlierDistance(paramsCpy.m_inlierDistance);
      lineFit.setIterations(100);
      lineFit.setMinPts(paramsCpy.m_minPts);
      lineFit.setUseWeight(true);
      lineFit.setIntercept(0.f);
      lineFit.compute();
      auto equation = lineFit.equation();
      for (auto& cylinder : paramsCpy._qsm->getBuildingBricks()) {
        if (cylinder->getSegment()->getBranchOrder() > 0) {
          continue;
        }
        const auto radius = cylinder->getRadius();
        const auto distance = cylinder->getDistanceToTwig();
        const auto radiusModel = std::sqrt(equation.first * distance + equation.second);
        const auto deviation = radiusModel * 0.2;
        const auto min = radiusModel - deviation;
        const auto max = radiusModel + deviation;
        if (min > radius || max < radius) {
          cylinder->setRadius(radiusModel, FittingType::TAPERFIT);
        }
      }
      {
        QMutexLocker m1(&*mMutex);
        params = paramsCpy;
      }
    }
  } else {
    SF_QSMAllometricCorrectionParameterEstimation paramEst;
    {
      QMutexLocker m1(&*mMutex);
      paramsCpy._qsm->sort(SF_ModelSegment::SF_SORTTYPE::GROWTH_VOLUME);
      paramEst.setParams(paramsCpy);
    }
    try {
      paramEst.compute();
      paramsCpy.m_power = paramsCpy._qsm->getB();
      if (paramsCpy.m_power < 0.1 || paramsCpy.m_power > 0.8) {
        std::cout << " (paramsCpy.m_power < 0.2 || paramsCpy.m_power > 0.8)" << std::endl;
        paramsCpy.m_power = SF_Math<double>::_ALLOMETRIC_POWER;
        params = paramsCpy;
        return;
      }
    } catch (...) {
      std::cout << "catch" << std::endl;
      paramsCpy.m_power = SF_Math<double>::_ALLOMETRIC_POWER;
      params = paramsCpy;
      return;
    }
    SF_QSMAllometricCorrection ac;
    {
      QMutexLocker m1(&*mMutex);
      ac.setParams(paramsCpy);
    }
    ac.compute();
    if (std::abs(paramsCpy.m_power - SF_Math<double>::_ALLOMETRIC_POWER) < std::numeric_limits<double>::epsilon()) {
      QMutexLocker m1(&*mMutex);
      QString errorMsg("Allometric correction failed for one tree: ");
      paramsCpy._log->addMessage(LogInterface::error, LogInterface::step, errorMsg);
      paramsCpy._log->addMessage(LogInterface::error, LogInterface::step, QString::fromStdString(paramsCpy._qsm->getName()));
    }
    {
      QMutexLocker m1(&*mMutex);
      params = paramsCpy;
    }
  }
}
