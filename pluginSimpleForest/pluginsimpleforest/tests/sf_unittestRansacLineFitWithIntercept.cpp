/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_unittestRansacLineFitWithIntercept.h"

void
SF_UnitTestRansacLineFitWithIntercept::ransacFitWorks()
{
  std::vector<float> x{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
  std::vector<float> y{ 11, 21, 31, 41, 51, 161, 171, 181, 191, 201, 211, 221 };
  SF_FitRansacLineWithIntercept<float> ransac;
  ransac.setX(x);
  ransac.setY(y);
  ransac.setMinPts(4);
  ransac.setIterations(10);
  ransac.setInlierDistance(1.f);
  ransac.setIntercept(1.f);
  ransac.compute();
  auto equation = ransac.equation();
  QCOMPARE(equation.first, 10.f);
  QCOMPARE(equation.second, 1.f);
}

void
SF_UnitTestRansacLineFitWithIntercept::ransacExtractInliersCorrectly()
{
  std::vector<float> x{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
  std::vector<float> y{ 11, 21, 31, 41, 51, 161, 171, 181, 191, 201, 211, 221 };
  SF_FitRansacLineWithIntercept<float> ransac;
  ransac.setX(x);
  ransac.setY(y);
  ransac.setMinPts(4);
  ransac.setIterations(10);
  ransac.setInlierDistance(1.f);
  ransac.setIntercept(1.f);
  ransac.compute();
  std::vector<float> xI{ 1, 2, 3, 4, 5 };
  std::vector<float> yI{ 11, 21, 31, 41, 51 };
  auto inliersPair = ransac.inliers();
  for (size_t i = 0; i < xI.size(); i++) {
    QCOMPARE(inliersPair.first[i], xI[i]);
    QCOMPARE(inliersPair.second[i], yI[i]);
  }
}
