/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_qsmfactory.h"

SF_QSMFactory::SF_QSMFactory() {}

pcl::ModelCoefficients::Ptr
SF_QSMFactory::circle(float x, float y, float z, float rad)
{
  pcl::ModelCoefficients::Ptr circle(new pcl::ModelCoefficients);
  circle->values.push_back(x);
  circle->values.push_back(y);
  circle->values.push_back(z);
  circle->values.push_back(rad);
  return circle;
}

void
SF_QSMFactory::singleCylinder(std::shared_ptr<SF_ModelQSM> qsm)
{
  std::shared_ptr<SF_ModelSegment> root(new SF_ModelSegment(qsm));
  pcl::ModelCoefficients::Ptr circlea = circle(0, 0, 0, 1);
  pcl::ModelCoefficients::Ptr circleb = circle(0, 0, 1, 1);
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> cylinder(new Sf_ModelCylinderBuildingbrick(circlea, circleb));
  root->addBuildingBrick(cylinder);
  qsm->setRootSegment(root);
}

void
SF_QSMFactory::invertedU(std::shared_ptr<SF_ModelQSM> qsm)
{
  std::shared_ptr<SF_ModelSegment> root(new SF_ModelSegment(qsm));
  {
    pcl::ModelCoefficients::Ptr circlea = circle(0, 0, 0, 1);
    pcl::ModelCoefficients::Ptr circleb = circle(0, 0, 1, 1);
    std::shared_ptr<Sf_ModelCylinderBuildingbrick> cylinder(new Sf_ModelCylinderBuildingbrick(circlea, circleb));
    root->addBuildingBrick(cylinder);
  }
  {
    pcl::ModelCoefficients::Ptr circlea = circle(0, 0, 1, 1);
    pcl::ModelCoefficients::Ptr circleb = circle(0, 1, 1, 1);
    std::shared_ptr<Sf_ModelCylinderBuildingbrick> cylinder(new Sf_ModelCylinderBuildingbrick(circlea, circleb));
    root->addBuildingBrick(cylinder);
  }
  {
    pcl::ModelCoefficients::Ptr circlea = circle(0, 1, 1, 1);
    pcl::ModelCoefficients::Ptr circleb = circle(0, 1, 0, 1);
    std::shared_ptr<Sf_ModelCylinderBuildingbrick> cylinder(new Sf_ModelCylinderBuildingbrick(circlea, circleb));
    root->addBuildingBrick(cylinder);
  }
  qsm->setRootSegment(root);
}

void
SF_QSMFactory::largestGrowthvolumeSecond(std::shared_ptr<SF_ModelQSM> qsm)
{
  std::shared_ptr<SF_ModelSegment> root(new SF_ModelSegment(qsm));
  pcl::ModelCoefficients::Ptr circlea = circle(0, 0, 0, 1);
  pcl::ModelCoefficients::Ptr circleb = circle(0, 0, 1, 1);
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> cylinder(new Sf_ModelCylinderBuildingbrick(circlea, circleb));
  root->addBuildingBrick(cylinder);
  qsm->setRootSegment(root);
  {
    std::shared_ptr<SF_ModelSegment> childSmallGrowthVolumeLargeRadius(new SF_ModelSegment(qsm));
    pcl::ModelCoefficients::Ptr circlea = circle(0, 0, 0, 2);
    pcl::ModelCoefficients::Ptr circleb = circle(0, 0, 0.01, 2);
    std::shared_ptr<Sf_ModelCylinderBuildingbrick> cylinder(new Sf_ModelCylinderBuildingbrick(circlea, circleb));
    childSmallGrowthVolumeLargeRadius->addBuildingBrick(cylinder);
    root->addChild(childSmallGrowthVolumeLargeRadius);
  }
  {
    std::shared_ptr<SF_ModelSegment> childLargeGrowthVolumeSmallRadius(new SF_ModelSegment(qsm));
    pcl::ModelCoefficients::Ptr circlea = circle(0, 0, 0, 0.5);
    pcl::ModelCoefficients::Ptr circleb = circle(0, 0, 100, 0.5);
    std::shared_ptr<Sf_ModelCylinderBuildingbrick> cylinder(new Sf_ModelCylinderBuildingbrick(circlea, circleb));
    childLargeGrowthVolumeSmallRadius->addBuildingBrick(cylinder);
    root->addChild(childLargeGrowthVolumeSmallRadius);
  }
}

void
SF_QSMFactory::largestGrowthlengthSecond(std::shared_ptr<SF_ModelQSM> qsm)
{
  std::shared_ptr<SF_ModelSegment> root(new SF_ModelSegment(qsm));
  pcl::ModelCoefficients::Ptr circlea = circle(0, 0, 0, 1);
  pcl::ModelCoefficients::Ptr circleb = circle(0, 0, 1, 1);
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> cylinder(new Sf_ModelCylinderBuildingbrick(circlea, circleb));
  root->addBuildingBrick(cylinder);
  qsm->setRootSegment(root);
  {
    std::shared_ptr<SF_ModelSegment> childSmallGrowthLengthZeroRadius(new SF_ModelSegment(qsm));
    pcl::ModelCoefficients::Ptr circlea = circle(0, 0, 0, 0);
    pcl::ModelCoefficients::Ptr circleb = circle(0, 0, 0.01, 0);
    std::shared_ptr<Sf_ModelCylinderBuildingbrick> cylinder(new Sf_ModelCylinderBuildingbrick(circlea, circleb));
    childSmallGrowthLengthZeroRadius->addBuildingBrick(cylinder);
    root->addChild(childSmallGrowthLengthZeroRadius);
  }
  {
    std::shared_ptr<SF_ModelSegment> childLargeGrowthLengthZeroRadius(new SF_ModelSegment(qsm));
    pcl::ModelCoefficients::Ptr circlea = circle(0, 0, 0, 0);
    pcl::ModelCoefficients::Ptr circleb = circle(0, 0, 100, 0);
    std::shared_ptr<Sf_ModelCylinderBuildingbrick> cylinder(new Sf_ModelCylinderBuildingbrick(circlea, circleb));
    childLargeGrowthLengthZeroRadius->addBuildingBrick(cylinder);
    root->addChild(childLargeGrowthLengthZeroRadius);
  }
}

void
SF_QSMFactory::largestRadiusSecond(std::shared_ptr<SF_ModelQSM> qsm)
{
  std::shared_ptr<SF_ModelSegment> root(new SF_ModelSegment(qsm));
  pcl::ModelCoefficients::Ptr circlea = circle(0, 0, 0, 1);
  pcl::ModelCoefficients::Ptr circleb = circle(0, 0, 1, 1);
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> cylinder(new Sf_ModelCylinderBuildingbrick(circlea, circleb));
  root->addBuildingBrick(cylinder);
  qsm->setRootSegment(root);
  {
    std::shared_ptr<SF_ModelSegment> childSmallRadius(new SF_ModelSegment(qsm));
    pcl::ModelCoefficients::Ptr circlea = circle(0, 0, 0, 1);
    pcl::ModelCoefficients::Ptr circleb = circle(0, 0, 0, 1);
    std::shared_ptr<Sf_ModelCylinderBuildingbrick> cylinder(new Sf_ModelCylinderBuildingbrick(circlea, circleb));
    childSmallRadius->addBuildingBrick(cylinder);
    root->addChild(childSmallRadius);
  }
  {
    std::shared_ptr<SF_ModelSegment> childLargeRadius(new SF_ModelSegment(qsm));
    pcl::ModelCoefficients::Ptr circlea = circle(0, 0, 0, 2);
    pcl::ModelCoefficients::Ptr circleb = circle(0, 0, 0, 2);
    std::shared_ptr<Sf_ModelCylinderBuildingbrick> cylinder(new Sf_ModelCylinderBuildingbrick(circlea, circleb));
    childLargeRadius->addBuildingBrick(cylinder);
    root->addChild(childLargeRadius);
  }
}

void
SF_QSMFactory::largestAngleSecond(std::shared_ptr<SF_ModelQSM> qsm)
{
  std::shared_ptr<SF_ModelSegment> root(new SF_ModelSegment(qsm));
  pcl::ModelCoefficients::Ptr circlea = circle(0, 0, 0, 1);
  pcl::ModelCoefficients::Ptr circleb = circle(0, 0, 1, 1);
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> cylinder(new Sf_ModelCylinderBuildingbrick(circlea, circleb));
  root->addBuildingBrick(cylinder);
  qsm->setRootSegment(root);
  {
    std::shared_ptr<SF_ModelSegment> childSmallAngle(new SF_ModelSegment(qsm));
    pcl::ModelCoefficients::Ptr circlea = circle(0, 0, 1, 1);
    pcl::ModelCoefficients::Ptr circleb = circle(0, 1, 1, 1);
    std::shared_ptr<Sf_ModelCylinderBuildingbrick> cylinder(new Sf_ModelCylinderBuildingbrick(circlea, circleb));
    childSmallAngle->addBuildingBrick(cylinder);
    root->addChild(childSmallAngle);
  }
  {
    std::shared_ptr<SF_ModelSegment> childLargeAngle(new SF_ModelSegment(qsm));
    pcl::ModelCoefficients::Ptr circlea = circle(0, 0, 1, 1);
    pcl::ModelCoefficients::Ptr circleb = circle(0, 0, 2, 1);
    std::shared_ptr<Sf_ModelCylinderBuildingbrick> cylinder(new Sf_ModelCylinderBuildingbrick(circlea, circleb));
    childLargeAngle->addBuildingBrick(cylinder);
    root->addChild(childLargeAngle);
  }
}

std::shared_ptr<SF_ModelQSM>
SF_QSMFactory::qsm(SORTTYPE type)
{
  std::shared_ptr<SF_ModelQSM> qsm(new SF_ModelQSM(1));
  switch (type) {
    case SORTTYPE::LARGEST_GROWTHVOLUME_SECOND:
      largestGrowthvolumeSecond(qsm);
      break;
    case SORTTYPE::LARGEST_GROWTHLENGTH_SECOND:
      largestGrowthlengthSecond(qsm);
      break;
    case SORTTYPE::LARGEST_RADIUS_SECOND:
      largestRadiusSecond(qsm);
      break;
    case SORTTYPE::LARGEST_ANGLE_SECOND:
      largestAngleSecond(qsm);
      break;
    case SORTTYPE::SINGLE_CYLINDER:
      singleCylinder(qsm);
      break;
    case SORTTYPE::INVERTED_U:
      invertedU(qsm);
      break;
    default:
      throw("SF_QSMFactory: Unkown SORTTYPE.");
      break;
  }
  return qsm;
}
