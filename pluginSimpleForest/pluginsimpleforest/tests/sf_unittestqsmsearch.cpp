/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_unittestqsmsearch.h"

#include "tests/factory/sf_qsmfactory.h"

void
SF_UnitTestQSMSearch::searchWorksForNormalDistance()
{
  SORTTYPE type = SORTTYPE::SINGLE_CYLINDER;
  std::shared_ptr<SF_ModelQSM> qsm = SF_QSMFactory::qsm(type);
  SF_CloudToModelDistanceParameters params;
  params._method = SECONDMOMENTUMORDER;
  params.m_useAngle = false;
  params._k = 1;
  SF_QSMSearchKdtree<pcl::PointXYZINormal> search(qsm, params);
  pcl::PointXYZINormal p;
  p.x = 2;
  p.y = 0;
  p.z = 0;
  auto pair = search.distance(p);
  QCOMPARE(1., pair.first);
  QCOMPARE(0, pair.second);
}

void
SF_UnitTestQSMSearch::searchWorksForDistanceOnAxis()
{
  SORTTYPE type = SORTTYPE::SINGLE_CYLINDER;
  std::shared_ptr<SF_ModelQSM> qsm = SF_QSMFactory::qsm(type);
  SF_CloudToModelDistanceParameters params;
  params._method = DISTANCETOAXISSEGMENT;
  params.m_useAngle = false;
  params._k = 1;
  SF_QSMSearchKdtree<pcl::PointXYZINormal> search(qsm, params);
  pcl::PointXYZINormal p;
  p.x = 2;
  p.y = 0;
  p.z = 0;
  auto pair = search.distance(p);
  QCOMPARE(2., pair.first);
  QCOMPARE(0, pair.second);
}

void
SF_UnitTestQSMSearch::searchWorksForGrowthVolume()
{
  SORTTYPE type = SORTTYPE::INVERTED_U;
  std::shared_ptr<SF_ModelQSM> qsm = SF_QSMFactory::qsm(type);
  SF_CloudToModelDistanceParameters params;
  params._method = GROWTHDISTANCE;
  params.m_useAngle = false;
  params._k = 1;
  SF_QSMSearchKdtree<pcl::PointXYZINormal> search(qsm, params);
  pcl::PointXYZINormal p;
  p.x = 2;
  p.y = 0;
  p.z = 0;
  auto pair = search.distance(p);
  QCOMPARE(-3., pair.first);
  QCOMPARE(0, pair.second);
}

void
SF_UnitTestQSMSearch::searchRejectsWrongNormal()
{
  SORTTYPE type = SORTTYPE::SINGLE_CYLINDER;
  std::shared_ptr<SF_ModelQSM> qsm = SF_QSMFactory::qsm(type);
  SF_CloudToModelDistanceParameters params;
  params._method = DISTANCETOAXISSEGMENT;
  params.m_useAngle = true;
  params._k = 1;
  {
    SF_CloudToModelDistanceParameters params;
    params._method = DISTANCETOAXISSEGMENT;
    params.m_useAngle = true;
    params._k = 1;
    SF_QSMSearchKdtree<pcl::PointXYZINormal> search(qsm, params);
    pcl::PointXYZINormal p;
    p.x = 2;
    p.y = 0;
    p.z = 0;
    p.normal_x = 0;
    p.normal_y = 0;
    p.normal_z = 1;
    auto pair = search.distance(p);
    QCOMPARE(std::numeric_limits<double>::max(), pair.first);
    QCOMPARE(-1, pair.second);
  }
  {
    SF_CloudToModelDistanceParameters params;
    params._method = DISTANCETOAXISSEGMENT;
    params.m_useAngle = true;
    params._k = 1;
    SF_QSMSearchKdtree<pcl::PointXYZINormal> search(qsm, params);
    pcl::PointXYZINormal p;
    p.x = 2;
    p.y = 0;
    p.z = 0;
    p.normal_x = 0;
    p.normal_y = 1;
    p.normal_z = 1;
    auto pair = search.distance(p);
    QCOMPARE(2., pair.first);
    QCOMPARE(0, pair.second);
  }
  {
    SF_CloudToModelDistanceParameters params;
    params._method = DISTANCETOAXISSEGMENT;
    params.m_useAngle = true;
    params._k = 1;
    SF_QSMSearchKdtree<pcl::PointXYZINormal> search(qsm, params);
    pcl::PointXYZINormal p;
    p.x = 2;
    p.y = 0;
    p.z = 0;
    p.normal_x = 0;
    p.normal_y = 1;
    p.normal_z = 1;
    auto pair = search.distance(p);
    QCOMPARE(std::numeric_limits<double>::max(), pair.first);
    QCOMPARE(-1, pair.second);
  }
  {
    SF_CloudToModelDistanceParameters params;
    params._method = DISTANCETOAXISSEGMENT;
    params.m_useAngle = false;
    params._k = 1;
    SF_QSMSearchKdtree<pcl::PointXYZINormal> search(qsm, params);
    pcl::PointXYZINormal p;
    p.x = 2;
    p.y = 0;
    p.z = 0;
    p.normal_x = 0;
    p.normal_y = 1;
    p.normal_z = 1;
    auto pair = search.distance(p);
    QCOMPARE(2., pair.first);
    QCOMPARE(0, pair.second);
  }
}

void
SF_UnitTestQSMSearch::searchChooseCorrectNormal()
{
  SORTTYPE type = SORTTYPE::INVERTED_U;
  std::shared_ptr<SF_ModelQSM> qsm = SF_QSMFactory::qsm(type);
  {
    SF_CloudToModelDistanceParameters params;
    params._method = GROWTHDISTANCE;
    params.m_useAngle = false;
    params._k = 3;
    SF_QSMSearchKdtree<pcl::PointXYZINormal> search(qsm, params);
    pcl::PointXYZINormal p;
    p.x = 0;
    p.y = 0;
    p.z = 0.1;
    p.normal_x = 0;
    p.normal_y = 0;
    p.normal_z = 1;
    auto pair = search.distance(p);
    QCOMPARE(-3., pair.first);
    QCOMPARE(0, pair.second);
  }
  {
    SF_CloudToModelDistanceParameters params;
    params._method = GROWTHDISTANCE;
    params.m_useAngle = true;
    params._k = 3;
    SF_QSMSearchKdtree<pcl::PointXYZINormal> search(qsm, params);
    pcl::PointXYZINormal p;
    p.x = 0;
    p.y = 0;
    p.z = 0.1;
    p.normal_x = 0;
    p.normal_y = 0;
    p.normal_z = 1;
    auto pair = search.distance(p);
    QCOMPARE(-2., pair.first);
    QCOMPARE(1, pair.second);
  }
}

void
SF_UnitTestQSMSearch::searchChooseLargerGrowthLength()
{
  SORTTYPE type = SORTTYPE::INVERTED_U;
  std::shared_ptr<SF_ModelQSM> qsm = SF_QSMFactory::qsm(type);
  {
    SF_CloudToModelDistanceParameters params;
    params._method = GROWTHDISTANCE;
    params.m_useAngle = false;
    params._k = 1;
    SF_QSMSearchKdtree<pcl::PointXYZINormal> search(qsm, params);
    pcl::PointXYZINormal p;
    p.x = 0;
    p.y = 1;
    p.z = 0.1;
    p.normal_x = 0;
    p.normal_y = 0;
    p.normal_z = 1;
    auto pair = search.distance(p);
    QCOMPARE(-1., pair.first);
    QCOMPARE(2, pair.second);
  }
  {
    SF_CloudToModelDistanceParameters params;
    params._method = GROWTHDISTANCE;
    params.m_useAngle = false;
    params._k = 3;
    SF_QSMSearchKdtree<pcl::PointXYZINormal> search(qsm, params);
    pcl::PointXYZINormal p;
    p.x = 0;
    p.y = 1;
    p.z = 0.1;
    p.normal_x = 0;
    p.normal_y = 0;
    p.normal_z = 1;
    auto pair = search.distance(p);
    QCOMPARE(-3., pair.first);
    QCOMPARE(0, pair.second);
  }
}

void
SF_UnitTestQSMSearch::searchRejectsDistanceOnAxis()
{
  SORTTYPE type = SORTTYPE::INVERTED_U;
  std::shared_ptr<SF_ModelQSM> qsm = SF_QSMFactory::qsm(type);
  {
    SF_CloudToModelDistanceParameters params;
    params._method = DISTANCETOAXISSEGMENT;
    params.m_useAngle = false;
    params._k = 1;
    SF_QSMSearchKdtree<pcl::PointXYZINormal> search(qsm, params);
    pcl::PointXYZINormal p;
    p.x = 0;
    p.y = 1;
    p.z = -0.1;
    p.normal_x = 0;
    p.normal_y = 0;
    p.normal_z = 1;
    auto pair = search.distance(p);
    QCOMPARE(std::numeric_limits<double>::max(), pair.first);
    QCOMPARE(-1, pair.second);
  }
  {
    SF_CloudToModelDistanceParameters params;
    params._method = DISTANCETOAXISSEGMENT;
    params.m_useAngle = false;
    params._k = 3;
    SF_QSMSearchKdtree<pcl::PointXYZINormal> search(qsm, params);
    pcl::PointXYZINormal p;
    p.x = 0;
    p.y = 1;
    p.z = -0.1;
    p.normal_x = 0;
    p.normal_y = 0;
    p.normal_z = 1;
    auto pair = search.distance(p);
    double delta = 0.00000001;
    QVERIFY(pair.first - delta <= 1.1 && pair.first + delta >= 1.1);
    QCOMPARE(1, pair.second);
  }
}
