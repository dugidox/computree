/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_UNITTESTQSMMERGE_H
#define SF_UNITTESTQSMMERGE_H

#include "tests/factory/sf_qsmfactory.h"
#include <QtTest/QtTest>

class SF_UnitTestQsmMerge : public QObject
{
  Q_OBJECT
private:
  std::shared_ptr<SF_ModelQSM> parentQsm();
  std::shared_ptr<SF_ModelQSM> childQsm();
  std::shared_ptr<SF_ModelQSM> merge(std::shared_ptr<SF_ModelQSM> qsm1, std::shared_ptr<SF_ModelQSM> qsm2);
  void test(std::shared_ptr<SF_ModelQSM> qsm);
private slots:
  void mergeWorks();
  void mergeWorksWithSwap();
  void nullptrAsParentIsAccepted();
  void nullptrAsChildIsAccepted();
};

#endif // SF_UNITTESTQSMMERGE_H
