/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_unittestBuildingbrick.h"

void
SF_UnitTestBuildingBrick::splitWorksAtEnd()
{
  std::shared_ptr<SF_ModelSegment> parent(new SF_ModelSegment);
  std::shared_ptr<SF_ModelSegment> child1(new SF_ModelSegment);
  std::shared_ptr<SF_ModelSegment> child2(new SF_ModelSegment);
  std::shared_ptr<SF_ModelSegment> child1Child1(new SF_ModelSegment);
  std::shared_ptr<SF_ModelSegment> child1Child2(new SF_ModelSegment);
  std::shared_ptr<SF_ModelSegment> child2Child1(new SF_ModelSegment);
  parent->addChild(child1);
  parent->addChild(child2);
  child1->addChild(child1Child1);
  child1->addChild(child1Child2);
  child2->addChild(child2Child1);
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> child1B1(new Sf_ModelCylinderBuildingbrick);
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> child1B2(new Sf_ModelCylinderBuildingbrick);
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> child1B3(new Sf_ModelCylinderBuildingbrick);
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> child1B4(new Sf_ModelCylinderBuildingbrick);
  child1->addBuildingBrick(child1B1);
  child1->addBuildingBrick(child1B2);
  child1->addBuildingBrick(child1B3);
  child1B3->splitSegmentAfter();
  QCOMPARE(child1B1->getSegment(), child1);
  QCOMPARE(child1B2->getSegment(), child1);
  QCOMPARE(child1B3->getSegment(), child1);
  QCOMPARE(child1B3->getSegment()->getParent(), parent);
  auto children = child1->getChildren();
  QVERIFY(std::find(children.begin(), children.end(), child1Child1) != children.end());
  QVERIFY(std::find(children.begin(), children.end(), child1Child2) != children.end());
  QVERIFY(!child1B4->getSegment());
}

void
SF_UnitTestBuildingBrick::splitWorksAtMiddle()
{
  std::shared_ptr<SF_ModelSegment> parent(new SF_ModelSegment);
  std::shared_ptr<SF_ModelSegment> child1(new SF_ModelSegment);
  std::shared_ptr<SF_ModelSegment> child2(new SF_ModelSegment);
  std::shared_ptr<SF_ModelSegment> child1Child1(new SF_ModelSegment);
  std::shared_ptr<SF_ModelSegment> child1Child2(new SF_ModelSegment);
  std::shared_ptr<SF_ModelSegment> child2Child1(new SF_ModelSegment);
  parent->addChild(child1);
  parent->addChild(child2);
  auto children1 = parent->getChildren();
  child1->addChild(child1Child1);
  child1->addChild(child1Child2);
  child2->addChild(child2Child1);
  auto grandChildren = child1->getChildren();
  QVERIFY(children1 != grandChildren);
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> child1B1(new Sf_ModelCylinderBuildingbrick);
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> child1B2(new Sf_ModelCylinderBuildingbrick);
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> child1B3(new Sf_ModelCylinderBuildingbrick);
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> child1B4(new Sf_ModelCylinderBuildingbrick);
  child1->addBuildingBrick(child1B1);
  child1->addBuildingBrick(child1B2);
  child1->addBuildingBrick(child1B3);
  child1B2->splitSegmentAfter();
  auto parentNew = child1B1->getSegment()->getParent();
  QCOMPARE(parentNew, parent);
  auto parentNewChildren = parentNew->getChildren();
  QVERIFY(std::find(parentNewChildren.begin(), parentNewChildren.end(), child1) == parentNewChildren.end());
  QVERIFY(std::find(parentNewChildren.begin(), parentNewChildren.end(), child1B1->getSegment()) != parentNewChildren.end());
  QVERIFY(std::find(parentNewChildren.begin(), parentNewChildren.end(), child1B3->getSegment()) == parentNewChildren.end());
  auto child1B2SegmentChildren = child1B2->getSegment()->getChildren();
  QVERIFY(child1B3->getSegment()->getChildren() == grandChildren);
  QVERIFY(std::find(child1B2SegmentChildren.begin(), child1B2SegmentChildren.end(), child1B3->getSegment()) !=
          child1B2SegmentChildren.end());
}
