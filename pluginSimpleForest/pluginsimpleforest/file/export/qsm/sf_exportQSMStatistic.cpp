/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_exportQSMStatistic.h"

#include "themewidget.h"
#include "themewidgetstem.h"
#include <QPainter>
#include <QPrinter>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>

QString
SF_ExportQSMStatistic::getFullPath(QString path)
{
  path.append("/qsm/statistics/");
  return path;
}

SF_ExportQSMStatistic::SF_ExportQSMStatistic() : SF_AbstractExport() {}

void
SF_ExportQSMStatistic::exportQSM(QString path, QString qsmName, std::shared_ptr<SF_ModelQSM> qsm)
{
  QLocale::setDefault(QLocale(QLocale::English, QLocale::UnitedStates));
  m_qsm = qsm;

  {
    QString fullPath = getFullPath(path);
    QDir dir(fullPath);
    if (!dir.exists())
      dir.mkpath(".");
    QString fullName = getFullName(qsmName, "_allometry.pdf");
    fullPath.append(fullName);
    QMainWindow window;
    ThemeWidget* widget = new ThemeWidget(qsm);
    window.setCentralWidget(widget);
    window.resize(900, 900);

    QPrinter printer(QPrinter::HighResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(fullPath);
    printer.setOrientation(QPrinter::Orientation::Portrait);
    printer.setFullPage(true);
    printer.setPageMargins(12, 16, 12, 20, QPrinter::Millimeter);

    QPainter painter(&printer);

    double xscale = printer.pageRect().width() / double(window.width());
    double yscale = printer.pageRect().height() / double(window.height());
    double scale = qMin(xscale, yscale);
    painter.translate(printer.paperRect().center());
    painter.scale(scale, scale);
    painter.translate(-window.width() / 2, -window.height() / 2);
    window.render(&painter);
  }
  {
    QString fullPath = getFullPath(path);
    QDir dir(fullPath);
    if (!dir.exists())
      dir.mkpath(".");
    QString fullName = getFullName(qsmName, "_stem.pdf");
    fullPath.append(fullName);
    QMainWindow window;
    ThemeWidgetStem* widget = new ThemeWidgetStem(qsm);
    window.setCentralWidget(widget);
    window.resize(900, 900);

    QPrinter printer(QPrinter::HighResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(fullPath);
    printer.setOrientation(QPrinter::Orientation::Portrait);
    printer.setFullPage(true);
    printer.setPageMargins(12, 16, 12, 20, QPrinter::Millimeter);

    QPainter painter(&printer);

    double xscale = printer.pageRect().width() / double(window.width());
    double yscale = printer.pageRect().height() / double(window.height());
    double scale = qMin(xscale, yscale);
    painter.translate(printer.paperRect().center());
    painter.scale(scale, scale);
    painter.translate(-window.width() / 2, -window.height() / 2);
    window.render(&painter);
  }
}
