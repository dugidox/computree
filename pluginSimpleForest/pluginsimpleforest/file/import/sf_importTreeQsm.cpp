/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_importTreeQsm.h"

#include "qsm/build/sf_qsmBuildTopology.h"
#include "qsm/model/sf_modelCylinderBuildingbrick.h"

std::shared_ptr<SF_ModelQSM>
SF_ImportTreeQsm::import(const QString& path, bool originalRadius)
{
  m_originalRadius = originalRadius;
  return import(path);
}

std::shared_ptr<SF_ModelQSM>
SF_ImportTreeQsm::import(const QString& path)
{
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks;
  m_parentIndices.clear();
  QFile inputFile(path);
  if (inputFile.open(QIODevice::ReadOnly)) {
    QTextStream in(&inputFile);
    while (!in.atEnd()) {
      QString line = in.readLine();
      bricks.push_back(createBrick(line));
    }
    inputFile.close();
  }
  SF_QSMBuildTopology topologyBuilder = SF_QSMBuildTopology(bricks, m_parentIndices);
  topologyBuilder.compute();
  return topologyBuilder.qsm();
}

std::shared_ptr<Sf_ModelAbstractBuildingbrick>
SF_ImportTreeQsm::createBrick(const QString& line)
{
  QStringList list = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
  if (list.size() < 8) {
    QString errorMessage =
      "[SF_ImportTreeQsm] : expects as first entries in a line : Radius, Length, StartPoint, Axisdirection. But received: \n";
    errorMessage.append(list.join(" , "));
    throw std::runtime_error(errorMessage.toStdString());

  } else {
    std::shared_ptr<Sf_ModelCylinderBuildingbrick> cylinder(new Sf_ModelCylinderBuildingbrick);
    Eigen::Vector3d start;
    start[0] = list[2].toDouble();
    start[1] = list[3].toDouble();
    start[2] = list[4].toDouble();
    Eigen::Vector3d axis;
    axis[0] = list[5].toDouble();
    axis[1] = list[6].toDouble();
    axis[2] = list[7].toDouble();
    double length = list[1].toDouble();
    if (length < 0) {
      QString errorMessage = "[SF_ImportTreeQsm] : received negative length cylinder: \n";
      errorMessage.append(QString::number(length));
      throw std::runtime_error(errorMessage.toStdString());
    }
    double radius = m_originalRadius ? list.last().toDouble() : list[0].toDouble();
    if (radius < 0) {
      QString errorMessage = "[SF_ImportTreeQsm] : received negative length cylinder: \n";
      errorMessage.append(QString::number(radius));
      throw std::runtime_error(errorMessage.toStdString());
    }
    Eigen::Vector3d end;
    auto axisLength = axis.norm();
    if (std::abs(axisLength - 1.) > 0.01) {
      QString errorMessage = "[SF_ImportTreeQsm] : received non unit scaled cylinder axis: \n";
      std::stringstream ss;
      ss << axis;
      errorMessage.append(QString::fromStdString(ss.str()));
      throw std::runtime_error(errorMessage.toStdString());
    }
    axis *= length;
    end = start + axis;
    FittingType fittingType = FittingType::TREEQSM;
    cylinder->setStartEndRadius(start, end, radius, fittingType);
    m_parentIndices.push_back(list[8].toInt() - 1);
    return cylinder;
  }
}
