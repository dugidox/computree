/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_FITCIRCLE_HPP
#define SF_FITCIRCLE_HPP

#include "math/fit/circle/sf_fit2dCircle.h"
#include <cassert>

#include <Eigen/QR>

namespace {

template<typename T>
T
distanceToCenter(T x, T y, const SF_Point2d<T>& center)
{
  T dx = x - center.x;
  T dy = y - center.y;
  return std::sqrt(dx * dx + dy * dy);
}

}

template<typename T>
std::vector<T>
SF_Fit2dCircle<T>::x() const
{
  return m_x;
}

template<typename T>
std::vector<T>
SF_Fit2dCircle<T>::y() const
{
  return m_y;
}

template<typename T>
void
SF_Fit2dCircle<T>::setY(const std::vector<T>& y)
{
  m_y = y;
}

template<typename T>
void
SF_Fit2dCircle<T>::setX(const std::vector<T>& x)
{
  m_x = x;
}

template<typename T>
SF_Fit2dCircle<T>::SF_Fit2dCircle() : SF_Fit2dCircle<T>(30u, static_cast<T>(0.0005))
{
}

template<typename T>
SF_Fit2dCircle<T>::SF_Fit2dCircle(std::uint32_t iterations, T minDelta) : m_gaussNewtonIterations(iterations), m_minDelta(minDelta)
{
}

template<typename T>
std::vector<T>
SF_Fit2dCircle<T>::errorCircle() const
{
  return { 0.f, 0.f, std::numeric_limits<T>::min() };
}

template<typename T>
void
SF_Fit2dCircle<T>::compute()
{
  {
    // initialize
    Eigen::MatrixXd jacobian = getJacobianInitial(m_x, m_y);
    Eigen::MatrixXd residuals = getResidualsInitial(m_x, m_y);
    Eigen::FullPivHouseholderQR<Eigen::MatrixXd> jacobianSolver(jacobian);
    Eigen::MatrixXd initial = jacobianSolver.solve(residuals);
    m_circle.center.x = initial(0, 0);
    m_circle.center.y = initial(1, 0);
    m_circle.radius = std::sqrt(m_circle.center.x * m_circle.center.x + m_circle.center.y * m_circle.center.y - initial(2, 0));
  }
  // gauss newton
  std::uint32_t iteration = 0;
  while (iteration++ < m_gaussNewtonIterations) {
    Eigen::MatrixXd jacobian = getJacobian(m_x, m_y);
    Eigen::MatrixXd residuals = getResiduals(m_x, m_y);
    Eigen::FullPivHouseholderQR<Eigen::MatrixXd> jacobianSolver(jacobian);
    Eigen::MatrixXd deltas = jacobianSolver.solve(residuals);
    m_circle.center.x += deltas(0, 0);
    m_circle.center.y += deltas(1, 0);
    m_circle.radius += deltas(2, 0);
    if (deltas.norm() < m_minDelta) {
      break;
    }
  }
}

template<typename T>
std::vector<T>
SF_Fit2dCircle<T>::circle()
{
  return { m_circle.center.x, m_circle.center.y, m_circle.radius };
}

template<typename T>
Eigen::MatrixXd
SF_Fit2dCircle<T>::getResiduals(const std::vector<T>& xVec, const std::vector<T>& yVec)
{
  Eigen::MatrixXd residuals;
  residuals.resize(xVec.size(), 1);

  for (size_t index = 0; index < xVec.size(); index++) {
    T x = xVec.at(index);
    T y = yVec.at(index);
    T distance = distanceToCenter(x, y, m_circle.center);
    residuals(index, 0) = -(distance - m_circle.radius);
  }
  return residuals;
}

template<typename T>
Eigen::MatrixXd
SF_Fit2dCircle<T>::getJacobian(const std::vector<T>& xVec, const std::vector<T>& yVec)
{
  assert(!xVec.empty() && xVec.size() == yVec.size());

  Eigen::MatrixXd jacobian;
  jacobian.resize(xVec.size(), 3);
  for (size_t index = 0; index < xVec.size(); index++) {
    T x = xVec.at(index);
    T y = yVec.at(index);
    auto distance = distanceToCenter(x, y, m_circle.center);
    jacobian(index, 0) = -(x - m_circle.center.x) / distance;
    jacobian(index, 1) = -(y - m_circle.center.y) / distance;
    jacobian(index, 2) = -1;
  }
  return jacobian;
}

template<typename T>
Eigen::MatrixXd
SF_Fit2dCircle<T>::getResidualsInitial(const std::vector<T>& xVec, const std::vector<T>& yVec)
{
  Eigen::MatrixXd residuals;
  residuals.resize(xVec.size(), 1);

  for (size_t index = 0; index < xVec.size(); index++) {
    T x = xVec.at(index);
    T y = yVec.at(index);
    residuals(index, 0) = x * x + y * y;
  }
  return residuals;
}

template<typename T>
Eigen::MatrixXd
SF_Fit2dCircle<T>::getJacobianInitial(const std::vector<T>& xVec, const std::vector<T>& yVec)
{
  assert(!xVec.empty() && xVec.size() == yVec.size());

  Eigen::MatrixXd jacobian;
  jacobian.resize(xVec.size(), 3);
  for (size_t index = 0; index < xVec.size(); index++) {
    T x = xVec.at(index);
    T y = yVec.at(index);
    jacobian(index, 0) = 2 * x;
    jacobian(index, 1) = 2 * y;
    jacobian(index, 2) = -1;
  }
  return jacobian;
}

#endif // SF_FITCIRCLE_HPP
