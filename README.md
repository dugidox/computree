# About Computree

Computree is from http://rdinnovation.onf.fr/,  
There you find a forum, wiki and up-to-date svn repository
Initial commit is of Computree v5.0.221b

# Installation Windows 10

* Install CMake
* Install C++ Compiler Microsoft Visual C++ 16.4
* Install QT 5.9.9 MSVC2015 64 bit
* Install QT Creator of your choice
* Install Git

# Installation Linux Ubuntu 20.4

Install via command line:

* **PCL** : 

```
    sudo apt install libqhull-dev
    sudo apt install libpcl1-dev    
    sudo apt install libflann-dev
    sudo apt install libeigen3-dev
```

* **OpenCv**  - 3.2 compilation from source downloadable at https://opencv.org/opencv-3-2/

```
    sudo apt-get install libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libdc1394-22-dev
    sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
    sudo apt-get install build-essential
    sudo apt install cmake-qt-gui 
```

 * **GSL** 

```
    sudo apt install libgsl-dev
```

* Install Git
```
    sudo apt install git
```
* Install QT 5.9.9 64 bit with QT Installer for linux
* Install QT Creator of your choice with QT Installer for linux

# All Plaforms

Get the source code. Create a workspace folder and run from there inside command line:

```
    git clone https://gitlab.com/SimpleForest/computree.git
```

In your workspace a computree folder will appear. Inside this folder is a file named **all.pro**. Open it with QT Creator.

* Select the correct kit with the above named compiler for 5.9.9
* Under project --> build --> deactivate shadow build both for release and debug.
* run qmake on all subdirectories of all>>base, then qmake base
* right click project base and compile base first, then the remaining plugins  
* Goto Projects-> Then run configuration of your selected Kit -> Environment settings -> Add to **LD_LIBRARY_PATH**

 ```
 ~/Documents/computree/computreev5/AMKgl/compiled:
 ~/Documents/computree/computreev5/AMKgl/libQGLViewer-2.6.4/QGLViewer:
 ~/Documents/computree/ComputreeInstallRelease:
 ~/Qt/5.9.9/gcc_64/lib
 ```

# Windows specific:

Search for rc.exe in 'C:\Program Files (x86)\Windows Kits'. Add the containing folder to the **PATH** in Environment settings 
 
When starting the compiled program you can receive a crash. You can still copy the fresh compiled (and previously modified) plugin SimpleForest dll from the compilation folder to the included binary folder. In that manner you can adjust SimpleForest and run it under window. For true bug fix, stable Computree version 6 release is awaited.




![Configuration](images/computreeConfiguration.png?raw=true "Configuration")
![Configuration2](images/kitConfiguration.png?raw=true "Configuration2")



