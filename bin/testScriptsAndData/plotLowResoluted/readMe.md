## Usage

This cloud ships with the Computree platform and was used in majority during implementation.

The quality is rather poor because of:

* single scan with high occlusion
* low resolution

In general more efford into data collection is recommended. Better invest fiew more hours in the field for scanning high resoluted multiple scans.

But the cloud is really small, the provided script runs within minutes to produce QSMs. This plot can also be used to learn the behaviour of different SimpleForest steps due to incredible fast execution time.

* QSMs, DTM and the cloud at same time:

![plot](./cloudQsm.png)

* QSMs and DTM only:

![plot](./qsm.png)
