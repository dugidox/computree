## Evalutation of SimpleForest QSMs

There has been no destructive AGB data collected to have validation plots. But Screenshots show high quality models:

* QSMs of large trees colored differently per tree Id:

![plot](./QSMTreeId.png)

* QSMs of large trees colored by their GrowthVolume:

![plot](./QSMGrowthVolume.png)

* Zoom into crown with branch Id color code:

![plot](./QSMBranchOrder.png)

# The clouds's origin: 

Leipzig Canopy Crane facility financed by the German Centre for Integrative Biodiversity Research (iDiv) Halle-Jena-Leipzig.

# The clouds's owner: 

Systematic Botany and Functional Biodiversity, Institute for Biology, Leipzig University.

![plot](./LAK_Logo-full-4c-colour.jpg)

