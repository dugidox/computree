“Fieldwork was funded through the Metrology for Earth Observation and Climate project (MetEOC-2), grant number ENV55 within the European Metrology Research Programme (EMRP). The EMRP is jointly funded by the EMRP participating countries within EURAMET and the European Union.”

https://royalsocietypublishing.org/doi/10.1098/rsfs.2017.0048
