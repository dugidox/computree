## Evalutation of SimpleForest QSMs

* QSMs build on raw undenoised clouds:

![plot](./ValidationWithLeaves.png)

* QSMs build on denoised clouds:

![plot](./validationWithoutLeaves.png)

# TERMS AND CONDITIONS

This data is owned by CIFOR and Wageningen University. Use, reporting, presentation or publication of results based on this dataset requires citation as below:

## REFERENCE

> Gonzalez de Tanago, J, Lau, A, Bartholomeus, H, et al. Estimation of above‐ground biomass of large tropical trees with terrestrial LiDAR. Methods Ecol Evol. 2018; 9: 223– 234.  https://doi.org/10.1111/2041-210X.12904

This data is free to download and available for your use as long as the proper reference, as specified above, is applied. We are also open to collaboration so do not hesitate to contact us with ideas or questions to:

* alvaro.lausarmiento@wur.nl
* harm.bartholomeus@wur.nl
