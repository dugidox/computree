<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>OL_StepCreatePolylines02</name>
    <message>
        <location filename="../step/ol_stepcreatepolylines02.cpp" line="60"/>
        <source>Création d&apos;une polyligne par cluster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepcreatepolylines02.cpp" line="73"/>
        <source>Clusters (Points)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepcreatepolylines02.cpp" line="75"/>
        <source>Groupe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepcreatepolylines02.cpp" line="76"/>
        <source>Cluster (Points)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepcreatepolylines02.cpp" line="83"/>
        <source>Traitement Multithread</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepcreatepolylines02.cpp" line="92"/>
        <source>Polyligne</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>OL_StepFilterArcPolylines02</name>
    <message>
        <location filename="../step/ol_stepfilterarcpolylines02.cpp" line="66"/>
        <source>Filtrage de polylines : garde les arcs de cercles</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepfilterarcpolylines02.cpp" line="79"/>
        <source>Polylignes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepfilterarcpolylines02.cpp" line="81"/>
        <source>Groupe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepfilterarcpolylines02.cpp" line="82"/>
        <source>Polyligne</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepfilterarcpolylines02.cpp" line="90"/>
        <source>RMSE maximale acceptée </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepfilterarcpolylines02.cpp" line="92"/>
        <source>Erreur maximale acceptée</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepfilterarcpolylines02.cpp" line="94"/>
        <source>R² ajusté minimal accepté</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepfilterarcpolylines02.cpp" line="96"/>
        <source>Rayon d&apos;arc maximal</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>OL_StepThrowParticules05</name>
    <message>
        <location filename="../step/ol_stepthrowparticules05.cpp" line="83"/>
        <source>Simplification de clusters / lancé de particules</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepthrowparticules05.cpp" line="96"/>
        <location filename="../step/ol_stepthrowparticules05.cpp" line="99"/>
        <source>Groupe de points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepthrowparticules05.cpp" line="98"/>
        <source>Groupe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepthrowparticules05.cpp" line="106"/>
        <source>Nombre de points a partir duquel on opére une simplification sur le groupe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepthrowparticules05.cpp" line="107"/>
        <source>Distance moyenne entre particules sur l&apos;arc</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepthrowparticules05.cpp" line="108"/>
        <source>Nombre de voisins a prendre en compte lors de la projection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepthrowparticules05.cpp" line="109"/>
        <source>Rayon de répulsion des particules</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepthrowparticules05.cpp" line="110"/>
        <source>Constante multiplicative lors du calcul de répulsion</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepthrowparticules05.cpp" line="111"/>
        <source>Seuil de convergence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepthrowparticules05.cpp" line="112"/>
        <source>Nombre d&apos;itérations maximum sans convergence des particules</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepthrowparticules05.cpp" line="113"/>
        <source>Traitement Multithread</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepthrowparticules05.cpp" line="114"/>
        <source>Si PCL est utilisé :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepthrowparticules05.cpp" line="115"/>
        <source>Utiliser un octree pour les gros groupes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepthrowparticules05.cpp" line="116"/>
        <source>Nombre de points à partir duquel utiliser un octree</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepthrowparticules05.cpp" line="117"/>
        <source>Résolution de l&apos;octree</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/ol_stepthrowparticules05.cpp" line="126"/>
        <source>Particules</source>
        <translation></translation>
    </message>
</context>
</TS>
