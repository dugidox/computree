#include "icro_stepgetgradientnormfrom3dgridint.h"

#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

// Alias for indexing models
#define DEFin_rsltWith3Rasterd "rsltWith3Rasterd"
#define DEFin_grpWith3dRaster "grpWith3dRaster"
#define DEFin_itmRaster3D "itmRaster3D"

#define DEFout_rsltGradientNorm "rsltResultGradientNormRslt"
#define DEFout_grpGradientNorm "grpGradientNormGroup"
#define DEFout_itmGradientNormGrid "itmGradientNormGrid"

// Constructor : initialization of parameters
ICRO_StepGetGradientNormFrom3dGridInt::ICRO_StepGetGradientNormFrom3dGridInt(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString ICRO_StepGetGradientNormFrom3dGridInt::getStepDescription() const
{
    return tr("Calcule la norme du gradient avec l'operateur de sobel adapte en 3D");
}

// Step detailled description
QString ICRO_StepGetGradientNormFrom3dGridInt::getStepDetailledDescription() const
{
    return tr("Uses Sobel-Feldman kernels");
}

// Step URL
QString ICRO_StepGetGradientNormFrom3dGridInt::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ICRO_StepGetGradientNormFrom3dGridInt::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ICRO_StepGetGradientNormFrom3dGridInt(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ICRO_StepGetGradientNormFrom3dGridInt::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_rsltWith3RasterAndScene = createNewInResultModel(DEFin_rsltWith3Rasterd, tr("Result with 3d raster"));
    resIn_rsltWith3RasterAndScene->setRootGroup(DEFin_grpWith3dRaster, CT_AbstractItemGroup::staticGetType(), tr("Group with 3D raster"));
    resIn_rsltWith3RasterAndScene->addItemModel(DEFin_grpWith3dRaster, DEFin_itmRaster3D, CT_Grid3D<int>::staticGetType(), tr("3d Raster (int)"));
}

// Creation and affiliation of OUT models
void ICRO_StepGetGradientNormFrom3dGridInt::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_rsltSobelGradientNorm = createNewOutResultModel(DEFout_rsltGradientNorm, tr("Gradient norm"));
    res_rsltSobelGradientNorm->setRootGroup(DEFout_grpGradientNorm, new CT_StandardItemGroup(), tr("Gradient group"));
    res_rsltSobelGradientNorm->addItemModel(DEFout_grpGradientNorm, DEFout_itmGradientNormGrid, new CT_Grid3D<float>(), tr("Gradient norm"));
}

// Semi-automatic creation of step parameters DialogBox
void ICRO_StepGetGradientNormFrom3dGridInt::createPostConfigurationDialog()
{
}

void ICRO_StepGetGradientNormFrom3dGridInt::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_rsltWith3Rasterd = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resOut_rsltRaster3D = outResultList.at(0);

    CT_ResultGroupIterator itIn_grpWith3dRaster(resIn_rsltWith3Rasterd, this, DEFin_grpWith3dRaster);
    while (itIn_grpWith3dRaster.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_grpWith3dRaster = (CT_AbstractItemGroup*) itIn_grpWith3dRaster.next();

        CT_Grid3D<int>* itemIn_itmIntGrid = (CT_Grid3D<int>*)grpIn_grpWith3dRaster->firstItemByINModelName(this, DEFin_itmRaster3D);
        if (itemIn_itmIntGrid != NULL )
        {
            CT_Grid3D<float>* itemOut_itmSobelGrid = new CT_Grid3D<float>( DEFout_itmGradientNormGrid, resOut_rsltRaster3D,
                                                                           itemIn_itmIntGrid->minX(), itemIn_itmIntGrid->minY(), itemIn_itmIntGrid->minZ(),
                                                                           itemIn_itmIntGrid->xArraySize(), itemIn_itmIntGrid->yArraySize(), itemIn_itmIntGrid->zArraySize(),
                                                                           itemIn_itmIntGrid->resolution(),
                                                                           std::numeric_limits<float>::max(), std::numeric_limits<float>::max() );

            computeSobelGradientGrid( itemIn_itmIntGrid, itemOut_itmSobelGrid );
            normalizeGrid( itemOut_itmSobelGrid );

            CT_StandardItemGroup* grpSobelGradientNorm = new CT_StandardItemGroup(DEFout_grpGradientNorm, resOut_rsltRaster3D );
            resOut_rsltRaster3D->addGroup( grpSobelGradientNorm );
            grpSobelGradientNorm->addItemDrawable( itemOut_itmSobelGrid );
        }
    }
}

void ICRO_StepGetGradientNormFrom3dGridInt::computeSobelGradientGrid(CT_Grid3D<int> *inputGrid, CT_Grid3D<float> *outGradientGrid)
{
    int xdim = inputGrid->xArraySize();
    int ydim = inputGrid->yArraySize();
    int zdim = inputGrid->zArraySize();

    for( int x = 0 ; x < xdim ; x++ )
    {
        for( int y = 0 ; y < ydim ; y++ )
        {
            for( int z = 0 ; z < zdim ; z++ )
            {
                outGradientGrid->setValue( x, y, z, computeSobelGradientGrid(inputGrid, x, y, z) );
            }
        }
    }

    outGradientGrid->computeMinMax();
}

float ICRO_StepGetGradientNormFrom3dGridInt::computeSobelGradientGrid(CT_Grid3D<int> *inputGrid,
                                                                      int x,
                                                                      int y,
                                                                      int z)
{
    float gradx = 0;
    float grady = 0;
    float gradz = 0;

    for( int xx = -1 ; xx <= 1 ; xx++ )
    {
        for( int yy = -1 ; yy <= 1 ; yy++ )
        {
            for( int zz = -1 ; zz <= 1 ; zz++ )
            {
                gradx += _Sobel_X_Kernel[xx+1][yy+1][zz+1] * valueFromGrid( inputGrid, x+xx, y+yy, z+zz );
                grady += _Sobel_Y_Kernel[xx+1][yy+1][zz+1] * valueFromGrid( inputGrid, x+xx, y+yy, z+zz );
                gradz += _Sobel_Z_Kernel[xx+1][yy+1][zz+1] * valueFromGrid( inputGrid, x+xx, y+yy, z+zz );
            }
        }
    }

    return sqrt( gradx*gradx + grady*grady + gradz*gradz );
}

void ICRO_StepGetGradientNormFrom3dGridInt::normalizeGrid(CT_Grid3D<float> *grid,
                                                          int min,
                                                          int max)
{
    float gridMin = grid->dataMin();
    float gridDelta = grid->dataMax() - grid->dataMin();
    float outDelta = max - min;

    int xdim = grid->xArraySize();
    int ydim = grid->yArraySize();
    int zdim = grid->zArraySize();

    for( int x = 0 ; x < xdim ; x++ )
    {
        for( int y = 0 ; y < ydim ; y++ )
        {
            for( int z = 0 ; z < zdim ; z++ )
            {
                float normalizedValue = min + ( outDelta * ( grid->value(x,y,z) - gridMin )/(gridDelta) );
                grid->setValue( x, y, z, normalizedValue );
            }
        }
    }

    grid->computeMinMax();
}

int ICRO_StepGetGradientNormFrom3dGridInt::valueFromGrid(CT_Grid3D<int> *grid,
                                                         int x,
                                                         int y,
                                                         int z)
{
    int xx = x;
    if( xx < 0 )
    {
        xx = abs(x)-1;
    }

    int yy = y;
    if( yy < 0 )
    {
        yy = abs(y)-1;
    }

    int zz = z;
    if( zz < 0 )
    {
        zz = abs(z)-1;
    }

    return grid->value( xx, yy, zz );
}
