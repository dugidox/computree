/************************************************************************************
* Filename :  mathtools.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef MATHTOOLS_H
#define MATHTOOLS_H

#include <QVector>

namespace lsis_MathTools
{
    /*!
     * \brief linearInterpolation
     *
     * Interpolation lineaire entre deux valeurs
     *
     * \param xa : borne min de l'intervalle de valeurs de depart
     * \param xb : borne max de l'intervalle de valeurs de depart
     * \param ya : borne min de l'intervalle de valeurs d'arrivee
     * \param yb : borne max de l'intervalle de valeurs d'arrivee
     * \param valToInterpolate : valeur dans l'intervalle de depart a interpoler dans l'intervalle d'arrivee
     *
     * \return la valeur interpolee lineairement
     */
    template< typename DataT >
    inline DataT linearInterpolation( DataT xa, DataT xb, DataT ya, DataT yb, DataT valToInterpolate )
    {
        if( xa == xb )
        {
            return ya;
        }

        float a = (ya-yb)/((double)(xa-xb));
        return ( ( a * valToInterpolate ) + ( ya - (a*xa) ) );
    }

    /*!
     * \brief getMean
     *
     * Calcule la moyenne d'une serie de donnees discretes
     *
     * \param data : donnees
     * \return la moyenne de la serie
     */
    template< typename DataT >
    float getMean( QVector<DataT> const & data );

    /*!
     * \brief getMean
     *
     * Calcule la moyenne d'une serie de donnees discretes
     *
     * \param data : donnees
     * \return la moyenne de la serie
     */
    template< typename DataT >
    float getMean( QList<DataT> const & data );

    /*!
     * \brief getVariance
     *
     * Calcule la variance d'une serie de donnees discrètes
     *
     * \param data : donnees
     * \return la variance des donnees
     */
    template< typename DataT >
    float getVariance( QVector<DataT> const & data );

    /*!
     * \brief getVariance
     *
     * Calcule la variance d'une serie de donnees discrètes
     *
     * \param data : donnees
     * \param mean : moyenne de la serie
     * \return la variance de la serie
     */
    template< typename DataT >
    float getVariance( QVector<DataT> const & data, float mean );
}

// Implementations templatees
#include "lsis_mathtools.hpp"

#endif // MATHTOOLS_H
