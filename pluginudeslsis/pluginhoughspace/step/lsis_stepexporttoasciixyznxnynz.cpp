#include "lsis_stepexporttoasciixyznxnynz.h"

//In/Out dependencies
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

//Itemdrawable dependencies
#include "ct_shapedata/ct_circledata.h"
#include "ct_itemdrawable/ct_circle.h"
#include "ct_itemdrawable/ct_scene.h"

//Tools dependencies
#include "ct_iterator/ct_pointiterator.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_accessor/ct_pointaccessor.h"
#include "ct_view/ct_stepconfigurabledialog.h"

//Qt dependencies
#include <QFile>
#include <QTextStream>
#include <QDebug>

// Alias for indexing models
#define DEFin_rsltScene "DEFin_rsltScene"
#define DEFin_grpScene "DEFin_grpScene"
#define DEFin_itmScene "DEFin_itmScene"
#define DEFin_itmName "DEFin_itmName"
#define DEFin_itmNameField "itmNameField"

// Constructor : initialization of parameters
LSIS_StepExportToAsciiXYZNxNyNz::LSIS_StepExportToAsciiXYZNxNyNz(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _saveDir = "";
    _saveDirList.clear();
}

// Step description (tooltip of contextual menu)
QString LSIS_StepExportToAsciiXYZNxNyNz::getStepDescription() const
{
    return tr("Enregistre un fichier en xyz, NxNyNz optionnel");
}

// Step detailled description
QString LSIS_StepExportToAsciiXYZNxNyNz::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString LSIS_StepExportToAsciiXYZNxNyNz::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepExportToAsciiXYZNxNyNz::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepExportToAsciiXYZNxNyNz(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepExportToAsciiXYZNxNyNz::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_rsltHitGridSparse = createNewInResultModelForCopy(DEFin_rsltScene,"Scene Result");
    resIn_rsltHitGridSparse->setZeroOrMoreRootGroup();
    resIn_rsltHitGridSparse->addGroupModel("", DEFin_grpScene, CT_AbstractItemGroup::staticGetType(), tr("Input scene Group"));
    resIn_rsltHitGridSparse->addItemModel( DEFin_grpScene, DEFin_itmScene, CT_Scene::staticGetType(), tr("Input Scene"));
    resIn_rsltHitGridSparse->addItemModel( DEFin_grpScene, DEFin_itmName, CT_AbstractSingularItemDrawable::staticGetType(), tr("Name"), "", CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);
    resIn_rsltHitGridSparse->addItemAttributeModel(DEFin_itmName, DEFin_itmNameField, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::ANY, tr("FileName"), "", CT_InAbstractModel::C_ChooseMultipleIfMultiple, CT_InAbstractModel::F_IsOptional);
}

// Creation and affiliation of OUT models
void LSIS_StepExportToAsciiXYZNxNyNz::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities* resultModel = createNewOutResultModelToCopy(DEFin_rsltScene);
}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepExportToAsciiXYZNxNyNz::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addFileChoice( "OutputDir", CT_FileChoiceButton::OneExistingFolder, "", _saveDirList );
}

void LSIS_StepExportToAsciiXYZNxNyNz::compute()
{
    if( _saveDirList.empty() )
    {
        qDebug() << "Il faut selectionner un dossier de sortie !";
        return;
    }
    _saveDir = _saveDirList.first();

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_rsltScene = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_grpScene( res_rsltScene, this, DEFin_grpScene );
    while ( itIn_grpScene.hasNext() && !isStopped() )
    {
        CT_StandardItemGroup* grpIn_grpScene = (CT_StandardItemGroup*) itIn_grpScene.next();

        CT_Scene* itemIn_itmScene = (CT_Scene*)grpIn_grpScene->firstItemByINModelName( this, DEFin_itmScene );

        const CT_AbstractSingularItemDrawable* itemIn_name = (CT_AbstractSingularItemDrawable*)grpIn_grpScene->firstItemByINModelName(this, DEFin_itmName);
        QString baseFileName = "";
        if (itemIn_name != NULL)
        {
            baseFileName = itemIn_name->firstItemAttributeByINModelName(res_rsltScene, this, DEFin_itmNameField)->toString(itemIn_name, NULL);
            baseFileName = QFileInfo(baseFileName).baseName();
        }

        if ( itemIn_itmScene != NULL )
        {
            saveSceneAsAsciiCloud( itemIn_itmScene, _saveDir + "/" + baseFileName + ".xyz" );
        }
    }
}

void LSIS_StepExportToAsciiXYZNxNyNz::saveSceneAsAsciiCloud(CT_Scene* scene, QString outputFileName)
{
    QFile file( outputFileName );
    if( !file.open( QIODevice::WriteOnly ) )
    {
        qDebug() << "Erreur lors de l'ouverture du fichier " << outputFileName;
        return;
    }

    QTextStream txtStream( &file );

    CT_PointIterator itP(dynamic_cast<CT_IAccessPointCloud*>(scene)->getPointCloudIndex());
    size_t totalSize = itP.size();
    size_t i = 0;

    while(itP.hasNext())
    {
        const CT_Point &point = itP.next().currentPoint();

        txtStream << CT_NumericToStringConversionT<double>::toString(point(0)) << " ";
        txtStream << CT_NumericToStringConversionT<double>::toString(point(1)) << " ";
        txtStream << CT_NumericToStringConversionT<double>::toString(point(2)) << "\n";

        ++i;

        setProgress((i*100)/totalSize);
    }

    file.close();
}
