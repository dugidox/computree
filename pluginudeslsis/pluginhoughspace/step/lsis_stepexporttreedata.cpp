#include "lsis_stepexporttreedata.h"

//In/Out dependencies
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_result/ct_resultgroup.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"

//Tools dependencies
#include "ct_view/ct_stepconfigurabledialog.h"

//Qt dependencies
#include <QDebug>

//Result model
#define DEF_SearchInSourceResult      "rs"
#define DEF_SearchInSourceGroup       "gs"
#define DEF_SearchInSourceSnakeGroup  "gsnake"
#define DEF_SearchInSourceItem        "its"

LSIS_StepExportTreeData::LSIS_StepExportTreeData(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _saveDir = "";
    _filePrefix = "treeData";
    _saveDirList.clear();
}

QString LSIS_StepExportTreeData::getStepDescription() const
{
    return tr("Exporter les statistiques des arbres détectés");
}

// Step detailed description
QString LSIS_StepExportTreeData::getStepDetailledDescription() const
{
    return tr("Cette étape permet d'exporter toutes les statistiques des snakes, du parentage :parent/enfant, fourche, cercle(rayon, postion, direction, etc).\n"
              "Il est important de noter que l'ID du tuboide est un ID unique, celui du parent est -1 si le tuboide est la branche principale et l'ID du niveau\n"
              "est le niveau de fourche(0: branche principale, 1: niveau 1 de fourche, etc.)");
}

CT_VirtualAbstractStep* LSIS_StepExportTreeData::createNewInstance(CT_StepInitializeData &dataInit)
{
    // Creates an instance of this step
    return new LSIS_StepExportTreeData(dataInit);
}

void LSIS_StepExportTreeData::createPreConfigurationDialog()
{
}

void LSIS_StepExportTreeData::createInResultModelListProtected()
{
    // We must have
    // - snake group and tree parameters
    CT_InResultModelGroupToCopy *inResultCopy = createNewInResultModelForCopy(DEF_SearchInSourceResult, tr("Étape d'entrée"), "", true);
    inResultCopy->setZeroOrMoreRootGroup();
    inResultCopy->addGroupModel("", DEF_SearchInSourceGroup, CT_AbstractItemGroup::staticGetType(), tr("Groupe général"), "", CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    inResultCopy->addGroupModel(DEF_SearchInSourceGroup, DEF_SearchInSourceSnakeGroup, CT_AbstractItemGroup::staticGetType(), tr("Groupe des snakes"), "", CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    inResultCopy->addItemModel( DEF_SearchInSourceSnakeGroup, DEF_SearchInSourceItem, CT_AbstractItemDrawableWithoutPointCloud::staticGetType(), tr("Stats arbres"));
}

void LSIS_StepExportTreeData::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addFileChoice( "Dossier de sortie", CT_FileChoiceButton::OneExistingFolder, "", _saveDirList );
    configDialog->addString("Nom du fichier de sortie", "", _filePrefix );
}

void LSIS_StepExportTreeData::createOutResultModelListProtected()
{
    // create a new OUT result that is a copy of the IN result selected by the user
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_SearchInSourceResult);

    if (!resultModel)
        return;
}

void LSIS_StepExportTreeData::compute()
{
    CT_ResultGroup* outResult = getOutResultList().first();

    CT_ResultGroupIterator itR(outResult, this, DEF_SearchInSourceGroup); //Iterator for the source group

    QVector< LSIS_ActiveContour4DContinuous<int>*> rootTuboids;

    _saveDir = _saveDirList.first();

    //For every snake(tree) in the result
    while (itR.hasNext() && !isStopped())
    {
        CT_StandardItemGroup *group = dynamic_cast<CT_StandardItemGroup*>((CT_AbstractItemGroup*)itR.next());
        CT_GroupIterator itSnakeGroup( group, this, DEF_SearchInSourceSnakeGroup );
        while (itSnakeGroup.hasNext() && !isStopped())
        {
            CT_StandardItemGroup *groupSnake = dynamic_cast<CT_StandardItemGroup*>((CT_AbstractItemGroup*)itSnakeGroup.next());
            LSIS_ActiveContour4DContinuous<int>* currentSnake = (LSIS_ActiveContour4DContinuous<int>*)groupSnake->firstItemByINModelName(this, DEF_SearchInSourceItem);
            if(currentSnake != NULL){
                rootTuboids.append(currentSnake);
            }
        }
    }

    if(rootTuboids.size() > 0){
        // On enregistre toute la structure de parent/fils
        saveTreesFromRootTuboids( _saveDir,
                                  _filePrefix+"_all_snakes.csv",
                                  rootTuboids,
                                  outResult );
    }
}

void LSIS_StepExportTreeData::saveTreesFromRootTuboids(QString outDirectory,
                                                       QString outFileName,
                                                       QVector< LSIS_ActiveContour4DContinuous<int>*> rootTuboids,
                                                       CT_ResultGroup* outResult)
{
    QDir dir( outDirectory );
    if( !dir.exists() )
    {
        dir.mkpath( outDirectory );
    }

    QFile file( outDirectory + "/" + outFileName );
    if( !file.open( QIODevice::WriteOnly ) )
    {
        qDebug() << "Impossible d'ecrire le fichier " << outDirectory + "/" + outFileName;
        return;
    }

    QTextStream stream( &file );

    stream << "TuboidId TuboidParentId TuboidOrdre TuboidCircleId TuboidCircleCenterX TuboidCircleCenterY TuboidCircleCenterZ TuboidCircleCenterRadius TuboidCircleDirectionX TuboidCircleDirectionY TuboidCircleDirectionZ\n";

    foreach( LSIS_ActiveContour4DContinuous<int>* curRoot, rootTuboids )
    {
        saveSingleTreeFromRootTuboidRecursive( stream, curRoot, outResult);
    }

    file.close();
    return;
}

void LSIS_StepExportTreeData::saveSingleTreeFromRootTuboidRecursive(QTextStream& stream,
                                                                        LSIS_ActiveContour4DContinuous<int>* tuboid,
                                                                    CT_ResultGroup* outResult)
{
    QVector<CT_Circle*> tuboidCircles = tuboid->toCircles( 5,
                                                           LSIS_ActiveContour4DContinuous<int>::GLOBAL_SCORE,
                                                           "",
                                                           outResult );

    for( int i = 0 ; i < tuboidCircles.size() ; i++ )
    {
        CT_Circle* currCircle = tuboidCircles.at(i);
        int parentID = -1;
        if( tuboid->_parent != NULL )
        {
            parentID = tuboid->_parent->_id;
        }
        stream << tuboid->_id << " " <<
                  parentID << " " <<
                  tuboid->_ordre << " " <<
                  i << " " <<
                  currCircle->getCenterX() << " " <<
                  currCircle->getCenterY() << " " <<
                  currCircle->getCenterZ() << " " <<
                  currCircle->getRadius() << " " <<
                  currCircle->getDirectionX() << " " <<
                  currCircle->getDirectionY() << " " <<
                  currCircle->getDirectionZ() << "\n";
    }

    for( int i = 0 ; i < tuboid->_children.size() ; i++ )
    {
        saveSingleTreeFromRootTuboidRecursive( stream, tuboid->_children.at(i),outResult );
    }
}
