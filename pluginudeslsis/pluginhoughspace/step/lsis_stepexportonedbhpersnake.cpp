#include "lsis_stepexportonedbhpersnake.h"

//In/Out dependencies
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_referencepoint.h"
#include "ct_shapedata/ct_circledata.h"
#include "ct_itemdrawable/ct_circle.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_image2d.h"
#include "ct_itemdrawable/ct_cylinder.h"

//Tools dependencies
#include "ct_iterator/ct_pointiterator.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_accessor/ct_pointaccessor.h"
#include "ct_view/ct_stepconfigurabledialog.h"

//Qt dependencies
#include <QFile>
#include <QTextStream>

// Alias for indexing models
#define DEF_SearchInSourceResult      "rs"
#define DEF_SearchInSourceGroup       "gs"
#define DEF_SearchInSourceSnakeGroup  "gsnake"
#define DEF_SearchInSourceItem        "its"
#define DEFin_itmMnt "DEFin_itmMnt"

// Constructor : initialization of parameters
LSIS_StepExportOneDBHPerSnake::LSIS_StepExportOneDBHPerSnake(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _saveDir = "";
    _saveDirList.clear();
    _filePrefix = "hauteur";
    _minHeight = 1.30;
    _maxHeight = 1.50;
}

// Step description (tooltip of contextual menu)
QString LSIS_StepExportOneDBHPerSnake::getStepDescription() const
{
    return tr("Enregistre un DHP pour chaque snake(de niveau 0) dans la scène");
}

// Step detailled description
QString LSIS_StepExportOneDBHPerSnake::getStepDetailledDescription() const
{
    return tr("Chaque cercle obtient un DHP basé sur une moyenne pour une tranche définie par l'utilisateur. Cette étape extracte également:\n"
              "- Diamètre dans l'intervalle de hauteur\n"
              "- Hauteur de la première fourche\n");
}

// Step URL
QString LSIS_StepExportOneDBHPerSnake::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepExportOneDBHPerSnake::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepExportOneDBHPerSnake(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepExportOneDBHPerSnake::createInResultModelListProtected()
{
    // We must have
    // - snake group and tree parameters
    CT_InResultModelGroupToCopy *inResultCopy = createNewInResultModelForCopy(DEF_SearchInSourceResult, tr("Étape d'entrée"), "", true);
    inResultCopy->setZeroOrMoreRootGroup();
    inResultCopy->addGroupModel("", DEF_SearchInSourceGroup, CT_AbstractItemGroup::staticGetType(), tr("Groupe général"), "", CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    inResultCopy->addItemModel(DEF_SearchInSourceGroup, DEFin_itmMnt, CT_Image2D<float>::staticGetType(), tr("DTM"));
    inResultCopy->addGroupModel(DEF_SearchInSourceGroup, DEF_SearchInSourceSnakeGroup, CT_AbstractItemGroup::staticGetType(), tr("Groupe des snakes"), "", CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    inResultCopy->addItemModel( DEF_SearchInSourceSnakeGroup, DEF_SearchInSourceItem, CT_AbstractItemDrawableWithoutPointCloud::staticGetType(), tr("Stats arbres"));
}

// Creation and affiliation of OUT models
void LSIS_StepExportOneDBHPerSnake::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities* resultModel = createNewOutResultModelToCopy(DEF_SearchInSourceResult);

    if (!resultModel)
        return;
}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepExportOneDBHPerSnake::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
    configDialog->addDouble(tr("Hauteur minimum"), "",0, 100, 2, _minHeight,1, tr("Renseignez la hauteur de départ pour la moyenne du rayon de chaque arbre"));
    configDialog->addDouble(tr("Hauteur maximum"), "",0, 100, 2, _maxHeight,1, tr("Renseignez la hauteur de fin pour la moyenne du rayon de chaque arbre"));

    configDialog->addFileChoice( "Dossier de sortie", CT_FileChoiceButton::OneExistingFolder, "", _saveDirList );
    configDialog->addString("Nom de fichier", "", _filePrefix );
}

void LSIS_StepExportOneDBHPerSnake::compute()
{
    //Switch min with max if they're incoherent
    if(_minHeight > _maxHeight){
            double temp = _minHeight;
            _minHeight = _maxHeight;
            _maxHeight = temp;
    }

    CT_ResultGroup* outResult = getOutResultList().first();

    CT_ResultGroupIterator itR(outResult, this, DEF_SearchInSourceGroup); //Iterator for the source group

    QVector< LSIS_ActiveContour4DContinuous<int>*> rootTuboids;

    _saveDir = _saveDirList.first();

    //For every snake(tree) in the result
    while (itR.hasNext() && !isStopped())
    {
        CT_StandardItemGroup *group = dynamic_cast<CT_StandardItemGroup*>((CT_AbstractItemGroup*)itR.next());
        _mnt = (CT_Image2D<float>*)group->firstItemByINModelName(this, DEFin_itmMnt);
        CT_GroupIterator itSnakeGroup( group, this, DEF_SearchInSourceSnakeGroup );
        while (itSnakeGroup.hasNext() && !isStopped())
        {
            CT_StandardItemGroup *groupSnake = dynamic_cast<CT_StandardItemGroup*>((CT_AbstractItemGroup*)itSnakeGroup.next());
            LSIS_ActiveContour4DContinuous<int>* currentSnake = (LSIS_ActiveContour4DContinuous<int>*)groupSnake->firstItemByINModelName(this, DEF_SearchInSourceItem);
            if(currentSnake != NULL){
                rootTuboids.append(currentSnake);
            }
        }
    }

    if(rootTuboids.size() > 0){
        // On enregistre toute la structure de parent/fils
        saveTreesFromRootTuboids( _saveDir,
                                  _filePrefix+"_all_snakes.csv",
                                  rootTuboids,
                                  outResult );
    }
}

void LSIS_StepExportOneDBHPerSnake::saveTreesFromRootTuboids(QString outDirectory,
                                                       QString outFileName,
                                                       QVector< LSIS_ActiveContour4DContinuous<int>*> rootTuboids,
                                                       CT_ResultGroup* outResult)
{
    QDir dir( outDirectory );
    if( !dir.exists() )
    {
        dir.mkpath( outDirectory );
    }

    QFile file( outDirectory + "/" + outFileName );
    if( !file.open( QIODevice::WriteOnly ) )
    {
        qDebug() << "Impossible d'ecrire le fichier " << outDirectory + "/" + outFileName;
        return;
    }

    QTextStream stream( &file );

    stream << "TuboidId CenterX CenterY CenterZ Radius_m Diameter_cm Height_First_Fork\n";

    foreach( LSIS_ActiveContour4DContinuous<int>* curRoot, rootTuboids )
    {
        saveSingleTreeFromRootTuboid( stream, curRoot, outResult);
    }

    file.close();
    return;
}

void LSIS_StepExportOneDBHPerSnake::saveSingleTreeFromRootTuboid(QTextStream& stream,
                                                                        LSIS_ActiveContour4DContinuous<int>* tuboid,
                                                                    CT_ResultGroup* outResult)
{
    QVector<CT_Circle*> tuboidCircles = tuboid->toCircles( 5,
                                                           LSIS_ActiveContour4DContinuous<int>::GLOBAL_SCORE,
                                                           "",
                                                           outResult );
    QVector<CT_Circle*> circlesToUseForDBH;
    //Calculate average of values for a given interval
    double snakeRadius = 0;
    double snakeCenterX = 0;
    double snakeCenterY = 0;
    double snakeCenterZ = 0;

    double firstForkHeight = std::numeric_limits<double>::max();
    //if the tuboid is a tree, not a fork(should be done in the main loop, this is extra security)
    if(tuboid->_parent == NULL){
        for( int i = 0 ; i < tuboidCircles.size() ; i++ )
        {
            CT_Circle* currCircle = tuboidCircles.at(i);
            double mntZValue = _mnt->valueAtCoords(currCircle->getCenterX(),currCircle->getCenterY());
            double distanceZ = sqrt(pow(currCircle->getCenterZ()-mntZValue,2));
            //intermediary for radius calculation for a certain interval of height
            if(distanceZ >=_minHeight && distanceZ <= _maxHeight){
                circlesToUseForDBH.append(currCircle);
            }
        }
        //Get the average of the radius of the tree
        if(circlesToUseForDBH.size() > 0){
            for(int i=0;i<circlesToUseForDBH.size();i++){
                snakeRadius += circlesToUseForDBH.at(i)->getRadius();
                snakeCenterX += circlesToUseForDBH.at(i)->getCenterX();
                snakeCenterY += circlesToUseForDBH.at(i)->getCenterY();
                snakeCenterZ += circlesToUseForDBH.at(i)->getCenterZ();
            }
            snakeRadius /= circlesToUseForDBH.size();
            snakeCenterX /= circlesToUseForDBH.size();
            snakeCenterY /= circlesToUseForDBH.size();
            snakeCenterZ /= circlesToUseForDBH.size();
        }
        //If the tree has forks
        if(tuboid->_children.size() > 0){
            QVector<CT_Circle*> forkTuboidCircles = tuboid->_children.at(0)->toCircles(5,
                                                                                       LSIS_ActiveContour4DContinuous<int>::GLOBAL_SCORE,
                                                                                       "",
                                                                                       outResult);
            firstForkHeight = forkTuboidCircles.at(0)->getCenterZ();

            CT_Circle* currCircleHead = forkTuboidCircles.at(0);
            CT_Circle* currCircleTail = forkTuboidCircles.at(forkTuboidCircles.size()-1);
            double distanceFromTreeHead = sqrt(pow(snakeCenterX-currCircleHead->getCenterX(),2.0)+pow(snakeCenterY-currCircleHead->getCenterY(),2.0)+pow(snakeCenterZ-currCircleHead->getCenterZ(),2.0));
            double distanceFromTreeTail = sqrt(pow(snakeCenterX-currCircleTail->getCenterX(),2.0)+pow(snakeCenterY-currCircleTail->getCenterY(),2.0)+pow(snakeCenterZ-currCircleTail->getCenterZ(),2.0));
            //Takes the closest circle to the tree as the starting height
            if(distanceFromTreeHead <= distanceFromTreeTail ){
                firstForkHeight = currCircleHead->getCenterZ();
            }else{
                firstForkHeight = currCircleTail->getCenterZ();
            }
        }
        stream << tuboid->_id << " " <<
                  snakeCenterX << " " <<
                  snakeCenterY << " " <<
                  snakeCenterZ << " " <<
                  snakeRadius << " " <<
                  ((snakeRadius*2)*100) << " " <<
                  firstForkHeight << "\n";
    }
}
