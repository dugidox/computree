/************************************************************************************
* Filename :  lsis_houghspace4d.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "lsis_houghspace4d.h"

#include <QList>

// Outils de raytracing
#include "../raytracing4d/lsis_traversalalgorithm4d.h"
#include "../raytracing4d/lsis_incrementvisitor.h"
#include "../raytracing4d/lsis_fastfiltervisitor.h"
#include "../raytracing4d/lsis_effectivefiltervisitor.h"

// Iterateurs de points
#include "ct_iterator/ct_pointiterator.h"
#include "ct_accessor/ct_pointaccessor.h"

// Travail avec des pixels4D (acces aux macros de parcours, aux methodes d'acces a l'image, etc)
#include "../pixel/lsis_pixel4d.h"

#include "../streamoverload/streamoverload.h"

#include "ct_step/abstract/ct_abstractstep.h"

//#include <QtConcurrent/QtConcurrent>
#include <QElapsedTimer>

LSIS_HoughSpace4D::LSIS_HoughSpace4D()
    : CT_Grid4D_Sparse<int>(),
      _step(NULL)
{
}

LSIS_HoughSpace4D::LSIS_HoughSpace4D(const QString &modelName, const CT_AbstractResult *result,
                                     double rmin, double xmin, double ymin, double zmin,
                                     double rmax, double xmax, double ymax, double zmax,
                                     double resw, double resx, double resy, double resz,
                                     bool constructByBoundingBox,
                                     const CT_PointsAttributesNormal *normalCloud,
                                     LSIS_DebugTool *step,
                                     int progressMin,
                                     int progressMax )
    : CT_Grid4D_Sparse<int>( modelName, result,
                            rmin, xmin, ymin, zmin,
                            rmax, xmax, ymax, zmax,
                            resw, resx, resy, resz,
                            -std::numeric_limits<int>::max(), 0),
      _step( step )
{
    assert(rmin > 0);
    assert(rmin < rmax);
    assert(xmin < xmax);
    assert(ymin < ymax);
    assert(zmin < zmax);
    assert( resw > 0 );
    assert( resx > 0 );
    assert( resy > 0 );
    assert( resz > 0 );
    //qDebug() << "LSIS_HS l 68";
    Q_UNUSED( constructByBoundingBox );

    if( normalCloud != NULL )
    {
//        fillFromNormalsParallel( normalCloud, progressMin, progressMax );
        fillFromNormals( normalCloud, progressMin, progressMax );
    }
    //qDebug() << "fillNormals ok";
}

LSIS_HoughSpace4D::LSIS_HoughSpace4D(const CT_OutAbstractSingularItemModel* model, const CT_AbstractResult *result,
                                     double rmin, double xmin, double ymin, double zmin,
                                     double rmax, double xmax, double ymax, double zmax,
                                     double resw, double resx, double resy, double resz,
                                     bool constructByBoundingBox,
                                     const CT_PointsAttributesNormal *normalCloud,
                                     LSIS_DebugTool *step,
                                     int progressMin,
                                     int progressMax )
    : CT_Grid4D_Sparse<int>( model, result,
                      rmin, xmin, ymin, zmin,
                      rmax, xmax, ymax, zmax,
                      resw, resx, resy, resz,
                      -std::numeric_limits<int>::max(), 0),
      _step( step )
{
    assert(rmin > 0);
    assert(rmin < rmax);
    assert(xmin < xmax);
    assert(ymin < ymax);
    assert(zmin < zmax);
    assert( resw > 0 );
    assert( resx > 0 );
    assert( resy > 0 );
    assert( resz > 0 );
    //qDebug() << "LSIS_HS l 95";
    Q_UNUSED( constructByBoundingBox );

    if( normalCloud != NULL )
    {
//        fillFromNormalsParallel( normalCloud, progressMin, progressMax );
        fillFromNormals( normalCloud, progressMin, progressMax );
    }
    //qDebug() << "fillNormals ok";
}

LSIS_HoughSpace4D::LSIS_HoughSpace4D(const QString &modelName, const CT_AbstractResult *result,
                                     double rmin, double xmin, double ymin, double zmin,
                                     size_t dimw, size_t dimx, size_t dimy, size_t dimz,
                                     double resw, double resx, double resy, double resz,
                                     const CT_PointsAttributesNormal *normalCloud,
                                     LSIS_DebugTool *step,
                                     int progressMin,
                                     int progressMax)
    : CT_Grid4D_Sparse<int>( modelName, result,
                      rmin, xmin, ymin, zmin,
                      dimw, dimx, dimy, dimz,
                      resw, resx, resy, resz,
                      -std::numeric_limits<int>::max(), 0 ),
      _step( step )
{
    assert(rmin > 0);
    assert(dimw > 0);
    assert(dimx > 0);
    assert(dimy > 0);
    assert(dimz > 0);
    assert( resw > 0 );
    assert( resx > 0 );
    assert( resy > 0 );
    assert( resz > 0 );
    //qDebug() << "LSIS_HS l 191";

    if( normalCloud != NULL )
    {
//        fillFromNormalsParallel( normalCloud, progressMin, progressMax );
        fillFromNormals( normalCloud, progressMin, progressMax );
    }
    //qDebug() << "fillNormals ok";
}

LSIS_HoughSpace4D::LSIS_HoughSpace4D(const CT_OutAbstractSingularItemModel *model, const CT_AbstractResult *result, double rmin, double xmin, double ymin, double zmin, size_t dimw, size_t dimx, size_t dimy, size_t dimz, double resw, double resx, double resy, double resz, const CT_PointsAttributesNormal *normalCloud, LSIS_DebugTool *step, int progressMin, int progressMax)
    : CT_Grid4D_Sparse<int>( model, result,
                      rmin, xmin, ymin, zmin,
                      dimw, dimx, dimy, dimz,
                      resw, resx, resy, resz,
                      -std::numeric_limits<int>::max(), 0 ),
      _step( step )
{
    assert(rmin > 0);
    assert(dimw > 0);
    assert(dimx > 0);
    assert(dimy > 0);
    assert(dimz > 0);
    assert( resw > 0 );
    assert( resx > 0 );
    assert( resy > 0 );
    assert( resz > 0 );
    //qDebug() << "LSIS_HS l 209";

    if( normalCloud != NULL )
    {
//        fillFromNormalsParallel( normalCloud, progressMin, progressMax );
        fillFromNormals( normalCloud, progressMin, progressMax );
    }
    //qDebug() << "fillNormals ok";
}

void LSIS_HoughSpace4D::fillFromNormals(const CT_PointsAttributesNormal* normalAttribute , int progressMin, int progressMax)
{
    //qDebug() << "Hough non parallel";
    QElapsedTimer timer;
    timer.start();
    const CT_AbstractPointCloudIndex* pointCloud = normalAttribute->getPointCloudIndex();
    const CT_AbstractNormalCloud* normalCloud = normalAttribute->getNormalCloud();

    // On declare tout ce qui est necessaire pour faire le raytracing 4d
    QList< LSIS_AbstractVisitorGrid4D<int> * > visitorList;
    LSIS_IncrementVisitor<int>* incrementVisitor = new LSIS_IncrementVisitor<int>( this );
    visitorList.push_back( incrementVisitor );
    // On declare un algorithme de raytracing 4D
    LSIS_TraversalAlgorithm4D<int> traversalAlgo( this, true, visitorList );

    float normalLenght;
    size_t i = 0;
    size_t nPoints = normalCloud->size();
    CT_PointIterator itP(pointCloud);
    while(itP.hasNext())
    {
        const CT_Point &currentPoint = itP.next().currentPoint();
        const CT_Normal currentNormal = normalCloud->constNormalAt(i);

        // Length of the normal
        normalLenght = sqrt( currentNormal.x()*currentNormal.x() + currentNormal.y()*currentNormal.y() + currentNormal.z()*currentNormal.z() );

        if ( normalLenght != 0 )
        {
            LSIS_Point4DDouble direction1( normalLenght, currentNormal.x(), currentNormal.y(), currentNormal.z() );
            LSIS_Point4DDouble direction2( normalLenght, -currentNormal.x(), -currentNormal.y(), -currentNormal.z() );
            LSIS_Point4DDouble origin( 0, currentPoint(0), currentPoint(1), currentPoint(2) );

            // Raytrace both directions of the normal
            LSIS_Beam4D beam1(origin, direction1);
            traversalAlgo.compute( beam1 );

            LSIS_Beam4D beam2(origin, direction2);
            traversalAlgo.compute( beam2 );
        }

        i++;

        if( _step != NULL )
        {
            _step->setProgress( progressMin + ( (float)i * (float)(progressMax - progressMin) / (float)nPoints ) );
        }
    }

   // qDebug() << "Temps ecoule pour remplir l'espace de Hough avec les normales :" << timer.elapsed();

    // Libere la memoire
    delete incrementVisitor;

    // On renvoie l'espace de Hough rempli
    computeMinMax();
}

//void LSIS_HoughSpace4D::fillFromNormalsParallel(const CT_PointsAttributesNormal *normalAttribute, int progressMin, int progressMax)
//{
//    qDebug() << "Hough parallel";
//    QElapsedTimer timer;
//    timer.start();
//    const CT_AbstractPointCloudIndex* pointCloud = normalAttribute->getPointCloudIndex();
//    const CT_AbstractNormalCloud* normalCloud = normalAttribute->getNormalCloud();

//    /* ******************************************************************************************************************************************** */
//    /* On declare tout ce qui est necessaire pour faire le raytracing 4d                                                                            */
//    /* ******************************************************************************************************************************************** */
//    QList< LSIS_AbstractVisitorGrid4D<int> * > visitorList;
//    LSIS_IncrementVisitor<int>* incrementVisitor = new LSIS_IncrementVisitor<int>( this );
//    visitorList.push_back( incrementVisitor );
//    // On declare un algorithme de raytracing 4D
//    LSIS_TraversalAlgorithm4D<int> traversalAlgo( this, true, visitorList );

//    /* ******************************************************************************************************************************************** */
//    /* On remplit le tableau des objets a lancer en parallele                                                                                       */
//    /* ******************************************************************************************************************************************** */
//    QVector<ParallelClass> parallelObjectsVector;
//    size_t nPoints = normalCloud->size();
//    CT_PointIterator itP(pointCloud);
//    size_t i = 0;
//    float normalLenght;
//    while(itP.hasNext())
//    {
//        const CT_Point &currentPoint = itP.next().currentPoint();
//        const CT_Normal currentNormal = normalCloud->constNormalAt(i);

//        // Length of the normal
//        normalLenght = sqrt( currentNormal.x()*currentNormal.x() + currentNormal.y()*currentNormal.y() + currentNormal.z()*currentNormal.z() );

//        if ( normalLenght != 0 )
//        {
//            LSIS_Point4DDouble direction1( normalLenght, currentNormal.x(), currentNormal.y(), currentNormal.z() );
//            LSIS_Point4DDouble direction2( normalLenght, -currentNormal.x(), -currentNormal.y(), -currentNormal.z() );
//            LSIS_Point4DDouble origin( 0, currentPoint(0), currentPoint(1), currentPoint(2) );

//            LSIS_Beam4D beam1(origin, direction1);
//            LSIS_Beam4D beam2(origin, direction2);
//            parallelObjectsVector.push_back( ParallelClass( beam1, new LSIS_TraversalAlgorithm4D<int> ( this, true, visitorList ) ) );
//            parallelObjectsVector.push_back( ParallelClass( beam2, new LSIS_TraversalAlgorithm4D<int> ( this, true, visitorList ) ) );
//        }

//        i++;

//        if( _step != NULL )
//        {
//            _step->setProgress( progressMin + ( (float)i * (float)(progressMax - progressMin) / (float)nPoints ) );
//        }
//    }
//    qDebug() << "Tableau d'objets paralleles fini";

//    QFuture<void> futureRayTraced = QtConcurrent::map( parallelObjectsVector, runAlgoOnParallelObject );
//    futureRayTraced.waitForFinished();
//    qDebug() << "Temps ecoule : " << timer.elapsed();

//    // Libere la memoire
//    delete incrementVisitor;

//    // On renvoie l'espace de Hough rempli
//    computeMinMax();
//    qDebug() << "Hough parallel END";
//}

void runAlgoOnParallelObject(ParallelClass parallelObject)
{
//    qDebug() << "Beam " << parallelObject._beam << " commence";
    parallelObject._traversal->compute( parallelObject._beam );
//    qDebug() << "Beam " << parallelObject._beam << " termine";
}

void LSIS_HoughSpace4D::fillFromPointsAndNormals(const CT_AbstractPointCloudIndex *pointCloud, const CT_AbstractNormalCloud *normalCloud, int progressMin, int progressMax)
{
    // On declare tout ce qui est necessaire pour faire le raytracing 4d
    QList< LSIS_AbstractVisitorGrid4D<int> * > visitorList;
    LSIS_IncrementVisitor<int>* incrementVisitor = new LSIS_IncrementVisitor<int>( this );
    visitorList.push_back( incrementVisitor );
    // On declare un algorithme de raytracing 4D
    LSIS_TraversalAlgorithm4D<int> traversalAlgo( this, true, visitorList );

    float normalLenght;
    size_t i = 0;
    size_t nPoints = normalCloud->size();
    CT_PointIterator itP(pointCloud);
    while(itP.hasNext())
    {
        const CT_Point &currentPoint = itP.next().currentPoint();
        const CT_Normal currentNormal = normalCloud->constNormalAt(i);

        // Length of the normal
        normalLenght = sqrt( currentNormal.x()*currentNormal.x() + currentNormal.y()*currentNormal.y() + currentNormal.z()*currentNormal.z() );

        if ( normalLenght != 0 )
        {
            LSIS_Point4DDouble direction1( normalLenght, currentNormal.x(), currentNormal.y(), currentNormal.z() );
            LSIS_Point4DDouble direction2( normalLenght, -currentNormal.x(), -currentNormal.y(), -currentNormal.z() );
            LSIS_Point4DDouble origin( 0, currentPoint(0), currentPoint(1), currentPoint(2) );

            // Raytrace both directions of the normal
            LSIS_Beam4D beam1(origin, direction1);
            traversalAlgo.compute( beam1 );

            LSIS_Beam4D beam2(origin, direction2);
            traversalAlgo.compute( beam2 );
        }

        i++;

        if( _step != NULL )
        {
            _step->setProgress( progressMin + ( (float)i * (float)(progressMax - progressMin) / (float)nPoints ) );
        }
    }

    // Libere la memoire
    delete incrementVisitor;

    // On renvoie l'espace de Hough rempli
    computeMinMax();
}

void LSIS_HoughSpace4D::threshByValue(int thresh)
{
    LSIS_Pixel4D* currPix = new LSIS_Pixel4D();
    beginForAllPixelsPtrImage( currPix, _dimw, _dimx, _dimy, _dimz )
    {
        if ( currPix->valueIn( this ) < thresh )
        {
            currPix->setValueIn( this, 0 );
        }
    }
    endForAllPixelsPtrImage( currPix, _dimw, _dimx, _dimy, _dimz )

    // Ne pas oublier de mettre a jour les min max pour affichage et calcul des forces des contours actifs
    computeMinMax();

    delete currPix;
}

CT_Grid4D_Sparse<float> *LSIS_HoughSpace4D::getEnergyImage(float gamma, float globalWeight) const
{
    CT_Grid4D_Sparse<float>* rslt = new CT_Grid4D_Sparse<float>(NULL, NULL,
                                                  _bot.w(), _bot.x(), _bot.y(), _bot.z(),
                                                  _dimw, _dimx, _dimy, _dimz,
                                                  _resw, _resx, _resy, _resz,
                                                  _NAdata, 0 );

    LSIS_Pixel4D pix;
    beginForAllPixelsImage( pix, _dimw, _dimx, _dimy, _dimz )
    {
        pix.setValueIn( rslt, gamma * energyImageAtPixel(pix, globalWeight) );
    }
    endForAllPixelsImage( pix, _dimw, _dimx, _dimy, _dimz )

    return rslt;
}

void LSIS_HoughSpace4D::getEnergyGradients(CT_Grid4D_Sparse<float> *outGradW, CT_Grid4D_Sparse<float> *outGradX, CT_Grid4D_Sparse<float> *outGradY, CT_Grid4D_Sparse<float> *outGradZ,
                                           CT_Grid4D_Sparse<float>* energyImage ) const
{
    //qDebug() << "test 00";
    outGradW = new CT_Grid4D_Sparse<float>(NULL, NULL,
                                    _bot.w(), _bot.x(), _bot.y(), _bot.z(),
                                    _dimw, _dimx, _dimy, _dimz,
                                    _resw, _resx, _resy, _resz,
                                    _NAdata, 0 );

    outGradX = new CT_Grid4D_Sparse<float>(NULL, NULL,
                                    _bot.w(), _bot.x(), _bot.y(), _bot.z(),
                                    _dimw, _dimx, _dimy, _dimz,
                                    _resw, _resx, _resy, _resz,
                                    _NAdata, 0 );

    outGradY = new CT_Grid4D_Sparse<float>(NULL, NULL,
                                    _bot.w(), _bot.x(), _bot.y(), _bot.z(),
                                    _dimw, _dimx, _dimy, _dimz,
                                    _resw, _resx, _resy, _resz,
                                    _NAdata, 0 );

    outGradZ = new CT_Grid4D_Sparse<float>(NULL, NULL,
                                    _bot.w(), _bot.x(), _bot.y(), _bot.z(),
                                    _dimw, _dimx, _dimy, _dimz,
                                    _resw, _resx, _resy, _resz,
                                    _NAdata, 0 );

    LSIS_Pixel4D pix;
    LSIS_Pixel4D next;
    LSIS_Pixel4D prev;
    float pixVal;
    float prevVal;
    float nextVal;
    beginForAllPixelsImage( pix, _dimw, _dimx, _dimy, _dimz )
    {
        pixVal = pix.valueIn( energyImage );

        // Gradient en w
        next = pix;
        next.w()++;

        prev = pix;
        prev.w()--;

        if( !prev.isIn( this ) )
        {
            pix.setValueIn( outGradW, ( next.valueIn( energyImage ) - pix.valueIn( energyImage ) ) );
        }

        else if ( !next.isIn( this ) )
        {
            pix.setValueIn( outGradW, ( pix.valueIn( energyImage ) - prev.valueIn( energyImage ) ) );
        }

        else
        {
            pix.setValueIn( outGradW, (float)(( next.valueIn( energyImage ) - prev.valueIn( energyImage ) ) / 2.0 ));
        }

        // Gradient en x
        next = pix;
        next.x()++;

        prev = pix;
        prev.x()--;

        if( !prev.isIn( this ) )
        {
            pix.setValueIn( outGradX, ( next.valueIn( energyImage ) - pix.valueIn( energyImage ) ) );
        }

        else if ( !next.isIn( this ) )
        {
            pix.setValueIn( outGradX, ( pix.valueIn( energyImage ) - prev.valueIn( energyImage ) ) );
        }

        else
        {
            pix.setValueIn( outGradX, (float)(( next.valueIn( energyImage ) - prev.valueIn( energyImage ) ) / 2.0 ));
        }

        // Gradient en y
        next = pix;
        next.y()++;

        prev = pix;
        prev.y()--;

        if( !prev.isIn( this ) )
        {
            pix.setValueIn( outGradY, ( next.valueIn( energyImage ) - pix.valueIn( energyImage ) ) );
        }

        else if ( !next.isIn( this ) )
        {
            pix.setValueIn( outGradY, ( pix.valueIn( energyImage ) - prev.valueIn( energyImage ) ) );
        }

        else
        {
            pix.setValueIn( outGradY, (float)(( next.valueIn( energyImage ) - prev.valueIn( energyImage ) ) / 2.0 ));
        }

        // Gradient en z
        next = pix;
        next.z()++;

        prev = pix;
        prev.z()--;

        if( !prev.isIn( this ) )
        {
            pix.setValueIn( outGradZ, ( next.valueIn( energyImage ) - pix.valueIn( energyImage ) ) );
        }

        else if ( !next.isIn( this ) )
        {
            pix.setValueIn( outGradZ, ( pix.valueIn( energyImage ) - prev.valueIn( energyImage ) ) );
        }

        else
        {
            pix.setValueIn( outGradZ, (float)(( next.valueIn( energyImage ) - prev.valueIn( energyImage ) ) / 2.0 ));
        }
    }
    endForAllPixelsImage( pix, _dimw, _dimx, _dimy, _dimz )
}

LSIS_Point4DFloat LSIS_HoughSpace4D::gradientEnergyImageAtPixel(const LSIS_Pixel4D &p, float globalWeight) const
{
    LSIS_Point4DFloat grad;

    // Si le pixel n'est pas dans l'image
    // On renvoie un gradient nul
    if( !p.isIn( this ) )
    {
        qDebug() << "Attention ici il faut le traiter autrement ! puisqu'il est possible que le resultat soir reutilise apres";
        return LSIS_Point4DFloat(0,0,0,0);
    }

    // Calcul des composantes du gradient selon chaque direction
    for( int i =0 ; i < 4 ; i++ )
    {
        // On recupere le pixel precedent dans cette direction
        LSIS_Pixel4D prevInDirection = p;
        prevInDirection(i) -= 1;
        bool isPrevInImage = prevInDirection.isIn( this );

        // On recupere le pixel suivant dans cette direction
        LSIS_Pixel4D nextInDirection = p;
        nextInDirection(i) += 1;
        bool isNextInImage = nextInDirection.isIn( this );

        // Si aucun des deux pixels (suivant et precedent) n'est dans l'image, on renvoie une gradient nul
        // i.e. l'image n'a qu'un seul pixel d'epaisseur selon cette dimension
        if( !isPrevInImage && !isNextInImage )
        {
            grad(i) = 0;
        }

        // Si les deux pixels (suivant et precedent) sont dans l'image, on calcule le gradient par difference centree
        else if( isPrevInImage && isNextInImage )
        {
            grad(i) = ( energyImageAtPixel( nextInDirection, globalWeight ) - energyImageAtPixel( prevInDirection, globalWeight ) ) /2.0;
        }

        // Si seul le pixel precedent est dans l'image (on a atteint le bord droit de l'image), on calcule le gradient par difference a gauche (curr - prev)
        else if( isPrevInImage && !isNextInImage )
        {
            grad(i) = ( energyImageAtPixel( p, globalWeight ) - energyImageAtPixel( prevInDirection, globalWeight ) );
        }

        // Si seul le pixel suivant est dans l'image (on a atteint le bord gauche de l'image), on calcule le gradient par difference a droite (next - curr)
        else
        {
            grad(i) = ( energyImageAtPixel( nextInDirection, globalWeight ) - energyImageAtPixel( p, globalWeight ) );
        }
    }

    return grad;
}

float LSIS_HoughSpace4D::energyImageAtPixel(const LSIS_Pixel4D &p, float globalWeight) const
{
    return ( globalWeight * energyImageGlobalAtPixel(p) ) + ( ( 1 - globalWeight ) * energyImageLocalAtPixel(p) );
}

float LSIS_HoughSpace4D::energyImageGlobalAtPixel(const LSIS_Pixel4D &p) const
{
    if( dataMax() == dataMin() )
    {
        return 0;
    }

    return (float)( dataMin() - p.valueIn( this ) ) / (float)( dataMax() - dataMin() );
}

float LSIS_HoughSpace4D::energyImageLocalAtPixel(const LSIS_Pixel4D &p) const
{
    int min, max;

    // Recupere les min et max du voisinage
    p.getMinMaxInNeighbourhood( this, min, max, 1 );

    if( min == max )
    {
        return -1;
    }

    // Renvoie l'energie image locale
    return (float)( min - p.valueIn( this ) ) / (float)( max - min );
}

void LSIS_HoughSpace4D::fastFilter(const CT_PointsAttributesNormal* normalAttribute,
                                   float ratioMin )
{
    // 1 - On fait une premiere passe pour flagger les directions a filtrer
    // On se sert de deux tableaux de booleens pour ca (on pourrait le reduire a 1 tableau si on ne filtre pas selon un ratio mais qu'on prend seulement le max)
    QVector<bool> filterPositiveDirection( normalAttribute->getNormalCloud()->size() );
    QVector<bool> filterNegativeDirection( normalAttribute->getNormalCloud()->size() );
    markFilteringDirections( normalAttribute,
                             filterPositiveDirection,
                             filterNegativeDirection,
                             ratioMin );

    _step->setProgress( 50 );

    // 2 - On filtre chaque direction marquee
    filterMarkedDirections( normalAttribute,
                            filterPositiveDirection,
                            filterNegativeDirection );

    _step->setProgress( 100 );
}

void LSIS_HoughSpace4D::markFilteringDirections(const CT_PointsAttributesNormal* normalAttribute,
                                                QVector<bool>& outFilterPositiveDirection,
                                                QVector<bool>& outFilterNegativeDirection,
                                                float ratioMin)
{
    CT_PointAccessor pAccess;
    const CT_AbstractPointCloudIndex* pointCloud = normalAttribute->getPointCloudIndex();
    const CT_AbstractNormalCloud* normalCloud = normalAttribute->getNormalCloud();

    // Pour chaque normale on va faire le raytracing et voir le cote qui cumule le plus de votes
    int nNormals = normalCloud->size();
    for( int i = 0 ; i < nNormals ; i++ )
    {
        // On recupere la normale
        const CT_Normal curNormal = normalCloud->constNormalAt(i);
        const CT_Point curPoint = pAccess.pointAt( pointCloud->indexAt( i ) );

        // On fait le raytracing dans les deux directions et on regarde le cumul des votes pour chaque cote
        bool filterPositive;
        bool filterNegative;
        markFilteringDirection( curPoint,
                                curNormal,
                                filterPositive,
                                filterNegative,
                                ratioMin );

        // On remplit la marque dans le tableau correspondant
        outFilterPositiveDirection[i] = filterPositive;
        outFilterNegativeDirection[i] = filterNegative;
    }
}

void LSIS_HoughSpace4D::markFilteringDirection(const CT_Point& point,
                                               const CT_Normal normal,
                                               bool& outFilterPositiveDirection,
                                               bool& outFilterNegativeDirection,
                                               float ratioMin )
{
    // On refait le raytracing de la normale pour voir quel cote cumule le plus de votes

    // On declare tout ce qui est necessaire pour faire le raytracing 4d
    QList< LSIS_AbstractVisitorGrid4D<int> * > visitorList;
    LSIS_FastFilterVisitor<int>* fastFilterVisitor = new LSIS_FastFilterVisitor<int>( this );
    visitorList.push_back( fastFilterVisitor );

    // On declare un algorithme de raytracing 4D
    LSIS_TraversalAlgorithm4D<int> traversalAlgo( this, true, visitorList );

    // Length of the normal
    float normalLenght = sqrt( normal.x()*normal.x() + normal.y()*normal.y() + normal.z()*normal.z() );

    if ( normalLenght != 0 )
    {
        LSIS_Point4DDouble positiveDirection( normalLenght, normal.x(), normal.y(), normal.z() );
        LSIS_Point4DDouble negativeDirection( normalLenght, -normal.x(), -normal.y(), -normal.z() );
        LSIS_Point4DDouble origin( 0, point(0), point(1), point(2) );

        // Raytrace both directions of the normal
        fastFilterVisitor->setSumOfVisitedVotes( 0 );
        LSIS_Beam4D positiveBeam( origin, positiveDirection );
        traversalAlgo.compute( positiveBeam );
        int nVotesInPositiveDirection = fastFilterVisitor->sumOfVisitedVotes();

        fastFilterVisitor->setSumOfVisitedVotes( 0 );
        LSIS_Beam4D negativeBeam( origin, negativeDirection );
        traversalAlgo.compute( negativeBeam );
        int nVotesInNegativeDirection = fastFilterVisitor->sumOfVisitedVotes();

        bool isPositiveMax = ( nVotesInPositiveDirection > nVotesInNegativeDirection );
        int nVotesMax = std::max( nVotesInPositiveDirection, nVotesInNegativeDirection );
        int nVotesMin = std::min( nVotesInPositiveDirection, nVotesInNegativeDirection );
        float ratio = (float)nVotesMax / (float)nVotesMin;

        if( ratio < ratioMin )
        {
            // Le ratio n'est pas atteint on filtre les deux directions
            outFilterPositiveDirection = true;
            outFilterNegativeDirection = true;
        }
        else
        {
            if( isPositiveMax )
            {
                outFilterPositiveDirection = false;
                outFilterNegativeDirection = true;
            }
            else
            {
                outFilterPositiveDirection = true;
                outFilterNegativeDirection = false;
            }
        }
    }
    else
    {
        // Une normale inexistante ne change rien, on a qu'a dire qu'on filtre (aucun impact, cette normale ne sera pas traitee ulterieurement)
        outFilterPositiveDirection = true;
        outFilterNegativeDirection = true;
    }

    // Libere la memoire
    delete fastFilterVisitor;
}

void LSIS_HoughSpace4D::filterMarkedDirections(const CT_PointsAttributesNormal* normalAttribute,
                                               QVector<bool>& filterPositiveDirection,
                                               QVector<bool>& filterNegativeDirection)
{
    CT_PointAccessor pAccess;
    const CT_AbstractPointCloudIndex* pointCloud = normalAttribute->getPointCloudIndex();
    const CT_AbstractNormalCloud* normalCloud = normalAttribute->getNormalCloud();

    int nNormals = normalCloud->size();
    for( int i = 0 ; i < nNormals ; i++ )
    {
        const CT_Normal curNormal = normalCloud->constNormalAt(i);
        const CT_Point curPoint = pAccess.pointAt( pointCloud->indexAt( i ) );

        // Si une des deux directions doit etre filtree (ou les deux) on refait le raytracing avec le visiteur de filtre
        if( filterPositiveDirection[i] == true )
        {
            filterDirection( curPoint,
                             curNormal );
        }

        if( filterNegativeDirection[i] == true )
        {
            CT_Normal oppositeNormal;
            for( int axis = 0 ; axis < 3 ; axis++ )
            {
                oppositeNormal[axis] = -curNormal[axis];
            }
            filterDirection( curPoint,
                             oppositeNormal );
        }
    }
}

void LSIS_HoughSpace4D::filterDirection(const CT_Point& point,
                                        const CT_Normal normal)
{
    // On declare tout ce qui est necessaire pour faire le raytracing 4d
    QList< LSIS_AbstractVisitorGrid4D<int> * > visitorList;
    LSIS_EffectiveFilterVisitor<int>* effectiveFilterVisitor = new LSIS_EffectiveFilterVisitor<int>( this );
    visitorList.push_back( effectiveFilterVisitor );

    // On declare un algorithme de raytracing 4D
    LSIS_TraversalAlgorithm4D<int> traversalAlgo( this, true, visitorList );

    // Length of the normal
    float normalLenght = sqrt( normal.x()*normal.x() + normal.y()*normal.y() + normal.z()*normal.z() );

    if ( normalLenght != 0 )
    {
        LSIS_Point4DDouble positiveDirection( normalLenght, normal.x(), normal.y(), normal.z() );
        LSIS_Point4DDouble origin( 0, point(0), point(1), point(2) );

        // Raytrace the normal and filter via visitor
        LSIS_Beam4D beam( origin, positiveDirection );
        traversalAlgo.compute( beam );
    }
}

QVector<LSIS_Pixel4D>* LSIS_HoughSpace4D::getLocalMaximasInBBox(const LSIS_Pixel4D &bot,
                                                                const LSIS_Pixel4D &top,
                                                                int minValue,
                                                                int neighbourhoodSize ) const
{
    QVector<LSIS_Pixel4D>* rslt = new QVector<LSIS_Pixel4D>();

    // On met a jour la bbox pour etre sur de ne pas aller hors de l'espace de Hough (perte de temps dans le parcours car beaucoup de tests potentiels de cellules hors de l'espace)
    LSIS_Pixel4D topBBox = top;
    LSIS_Pixel4D botBBox = bot;
    for( int axe = 0 ; axe < 4 ; axe++ )
    {
        if( botBBox(axe) < 0 )
        {
            botBBox(axe) = 0;
        }

        if( botBBox(axe) >= dim()(axe) )
        {
            botBBox(axe) = dim()(axe) - 1;
        }

        if( topBBox(axe) < 0 )
        {
            topBBox(axe) = 0;
        }

        if( topBBox(axe) >= dim()(axe) )
        {
            topBBox(axe) = dim()(axe) - 1;
        }
    }

    /* *********************************************************************************** */
    cv::SparseMatConstIterator_<int> pixelIteratorBegin = this->beginIterator();
    cv::SparseMatConstIterator_<int> pixelIteratorEnd = this->endIterator();
    for( cv::SparseMatConstIterator_<int> pixelIterator( pixelIteratorBegin );
         pixelIterator != pixelIteratorEnd ;
         pixelIterator++ )
    {
        const cv::SparseMat::Node* curPixel = pixelIterator.node();
        LSIS_Pixel4D pix( curPixel->idx[0],
                          curPixel->idx[1],
                          curPixel->idx[2],
                          curPixel->idx[3]);

        if( pix.isInBBox( botBBox, topBBox ) )
        {
            if( pix.valueIn( this ) >= minValue )
            {
                if( pix.isLocalMaxima( this, neighbourhoodSize ) )
                {
                    rslt->push_back( LSIS_Pixel4D( pix ) );
                }
            }
        }
    }
    /* *********************************************************************************** */

    return rslt;
}

QVector<LSIS_Pixel4D> *LSIS_HoughSpace4D::locateLocalMaximasInBBox(const LSIS_Pixel4D &bot, const LSIS_Pixel4D &top, int minValue, int nbMaximas) const
{
    QVector<LSIS_Pixel4D>* rsltVector = new QVector<LSIS_Pixel4D>();
    QList<LSIS_Pixel4D> rsltList;
    QList<int> values;

    for( int idx = 0 ; idx < nbMaximas ; idx++ )
    {
        rsltList.append( LSIS_Pixel4D() );
        values.append(0);
    }

    LSIS_Pixel4D pix;
    beginForAllPixelsInBBox( bot, top, _dimw, _dimx, _dimy, _dimz, pix )
    {
        if( pix.valueIn( this ) >= minValue )
        {
            bool stop = false;
            for( int idx = 0 ; idx < nbMaximas && stop == false; idx++ )
            {
                if( pix.valueIn( this ) > values.at(idx) )
                {
                    rsltList.insert( idx, LSIS_Pixel4D( pix ) );
                    rsltList.removeLast();
                    values.insert( idx, pix.valueIn( this ) );
                    values.removeLast();
                    stop = true;
                }
            }
        }
    }
    endForAllPixelsInBBox( bot, top, _dimw, _dimx, _dimy, _dimz, pix )

    for( int idx = 0 ; idx < nbMaximas ; idx++ )
    {
        rsltVector->append( LSIS_Pixel4D( rsltList.at(idx) ) );
    }

    return rsltVector;
}

QVector<LSIS_Pixel4D> *LSIS_HoughSpace4D::getLocalMaximasInHeighRangeOverDTM(CT_Image2D<float> const * mnt,
                                                                             float minz,
                                                                             float maxz,
                                                                             int minValue,
                                                                             int neighbourhoodSize ) const
{
    QVector<LSIS_Pixel4D>* rslt = new QVector<LSIS_Pixel4D>();

    float pixHeight;
    float pixBotHeight;
    float pixTopHeight;
    float pixValMNT;
    float pixHeightOnMNT;

    /* *********************************************************************************** */
    cv::SparseMatConstIterator_<int> pixelIteratorBegin = this->beginIterator();
    cv::SparseMatConstIterator_<int> pixelIteratorEnd = this->endIterator();
    for( cv::SparseMatConstIterator_<int> pixelIterator( pixelIteratorBegin );
         pixelIterator != pixelIteratorEnd ;
         pixelIterator++ )
    {
        const cv::SparseMat::Node* curPixel = pixelIterator.node();
        LSIS_Pixel4D pix( curPixel->idx[0],
                          curPixel->idx[1],
                          curPixel->idx[2],
                          curPixel->idx[3]);

        pixValMNT = mnt->valueAtCoords( pix.toCartesianX(this), pix.toCartesianY(this) );

        // Si on avait NA dans le MNT on ne considere pas la colonne en z
        if( pixValMNT != mnt->NA() )
        {
            // Un pixel n'est pas dans la tranche si
            // - bot est au dessus du zmax
            // ou
            // - top est au dessous du zmax
            pixHeight = pix.toCartesianZ( this );
            pixHeightOnMNT = pixHeight - pixValMNT;

            pixBotHeight = pixHeightOnMNT - (0.5 * _resz );
            pixTopHeight = pixHeightOnMNT + (0.5 * _resz );

            if( !( pixBotHeight >= maxz || pixTopHeight <= minz ) )
            {
                if( pix.valueIn( this ) >= minValue )
                {
                    if( pix.isLocalMaxima( this, neighbourhoodSize ) )
                    {
                        rslt->push_back( LSIS_Pixel4D( pix ) );
                    }
                }
            }
        }
    }
    /* *********************************************************************************** */

    return rslt;
}

QVector<LSIS_Pixel4D> *LSIS_HoughSpace4D::getLocalMaximas( int minValue,
                                                           int neighbourhoodSize ) const
{
    LSIS_Pixel4D bot(0,0,0,0);
    LSIS_Pixel4D top( _dimw, _dimx, _dimy, _dimz );

    return getLocalMaximasInBBox( bot, top, minValue, neighbourhoodSize );
}

QVector<LSIS_Pixel4D> *LSIS_HoughSpace4D::locateLocalMaximas(int minValue, int nMaximas) const
{
    LSIS_Pixel4D bot(0,0,0,0);
    LSIS_Pixel4D top( _dimw, _dimx, _dimy, _dimz );

    return locateLocalMaximasInBBox( bot, top, minValue, nMaximas );
}

QVector<LSIS_Pixel4D> *LSIS_HoughSpace4D::getLocalMaximasWithinHeightRange(float zmin, float zmax, int minValue, int neighbourhoodSize ) const
{
    // Calcul du niveau z correspondant a minz a partir du sol (bot de l'espace de Hough)
    size_t minLevZ;
    if( !levZ( minZ() + zmin , minLevZ) )
    {
        PS_LOG->addErrorMessage( PS_LOG->itemdrawable, "La grille ne contient pas la hauteur min" + QString::number( zmin ) );
        return new QVector<LSIS_Pixel4D>();
    }

    // Calcul du niveau z correspondant a maxz
    size_t maxLevZ;
    if( !levZ( minZ() + zmax , maxLevZ  ) )
    {
        maxLevZ = zdim();
    }

    // On limite la bbox de recherche par ces deux valeurs
    LSIS_Pixel4D bot( 0, 0, 0, minLevZ );
    LSIS_Pixel4D top( wdim(), xdim(), ydim(), maxLevZ );
    return getLocalMaximasInBBox( bot, top, minValue, neighbourhoodSize );
}

QVector<LSIS_Pixel4D> *LSIS_HoughSpace4D::locateLocalMaximasWithinHeightRange(float zmin, float zmax, int minValue , int nMaximas) const
{
    // Calcul du niveau z correspondant a minz a partir du sol (bot de l'espace de Hough)
    size_t minLevZ;
    if( !levZ( minZ() + zmin , minLevZ) )
    {
        PS_LOG->addErrorMessage( PS_LOG->itemdrawable, "La grille ne contient pas la hauteur " + QString::number( zmin ) );
        qDebug() << "La grille ne contient pas la hauteur " << zmin;
        qDebug() << "Espace de Hough : ";
        qDebug() << (*this);
        return new QVector<LSIS_Pixel4D>();
    }

    // Calcul du niveau z correspondant a maxz
    size_t maxLevZ;
    if( !levZ( minZ() + zmax , maxLevZ  ) )
    {
        PS_LOG->addErrorMessage( PS_LOG->itemdrawable, "La grille ne contient pas la hauteur " + QString::number( zmax ) );
        qDebug() << "La grille ne contient pas la hauteur " << zmax;
        qDebug() << "Espace de Hough : ";
        qDebug() << (*this);
        return new QVector<LSIS_Pixel4D>();
    }

    // On limite la bbox de recherche par ces deux valeurs
    LSIS_Pixel4D bot( 0, 0, 0, minLevZ );
    LSIS_Pixel4D top( wdim(), xdim(), ydim(), maxLevZ );
    return locateLocalMaximasInBBox( bot, top, minValue, nMaximas );
}

void LSIS_HoughSpace4D::morphoClosure(int size)
{
    LSIS_HoughSpace4D* dilate = morphoDilate( size );

    LSIS_Pixel4D currPix;
    LSIS_Pixel4D currNei;
    int min;
    int currNeiValue;
    beginForAllPixelsImage( currPix, _dimw, _dimx, _dimy, _dimz )
    {
        // On nefait la fermeture que dans le masque des pixels nuls
        if( currPix.valueIn(this) == 0 )
        {
            min = std::numeric_limits<int>::max();
            beginForAllPixelsNeighbours( currPix, _dimw, _dimx, _dimy, _dimz, currNei, size )
            {
                currNeiValue = currNei.valueIn( dilate );

                if( currNeiValue > 0 )
                {
                    if( currNeiValue < min )
                    {
                        min = currNeiValue;
                    }
                }
            }
            endForAllPixelsNeighbours( currPix, _dimw, _dimx, _dimy, _dimz, currNei, size )
            currPix.setValueIn( this, min );
        }
    }
    endForAllPixelsImage( currPix, _dimw, _dimx, _dimy, _dimz )

    delete dilate;
}

LSIS_HoughSpace4D *LSIS_HoughSpace4D::morphoDilate(int size) const
{
    LSIS_HoughSpace4D* dilate = new LSIS_HoughSpace4D(NULL, NULL,
                                                      _bot(0), _bot(1), _bot(2), _bot(3),
                                                      _dimw, _dimx, _dimy, _dimz,
                                                      _resw, _resx, _resy, _resz );

    LSIS_Pixel4D currPix;
    LSIS_Pixel4D currNei;
    int max;
    int currNeiValue;

    beginForAllPixelsImage( currPix, _dimw, _dimx, _dimy, _dimz )
    {
        // On nefait la fermeture que dans le masque des pixels nuls
        if( currPix.valueIn(this) == 0 )
        {
            max = -std::numeric_limits<int>::max();
            beginForAllPixelsNeighbours( currPix, _dimw, _dimx, _dimy, _dimz, currNei, size )
            {
                currNeiValue = currNei.valueIn( this );

                if( currNeiValue > max )
                {
                    max = currNeiValue;
                }
            }
            endForAllPixelsNeighbours( currPix, _dimw, _dimx, _dimy, _dimz, currNei, size )
            currPix.setValueIn( dilate, max );
        }

        else
        {
            currPix.setValueIn( dilate, currPix.valueIn( this ) );
        }
    }
    endForAllPixelsImage( currPix, _dimw, _dimx, _dimy, _dimz )

    return dilate;
}

LSIS_HoughSpace4D *LSIS_HoughSpace4D::morphoErode(int size) const
{
    LSIS_HoughSpace4D* erode = new LSIS_HoughSpace4D(NULL, NULL,
                                                     _bot(0), _bot(1), _bot(2), _bot(3),
                                                     _dimw, _dimx, _dimy, _dimz,
                                                     _resw, _resx, _resy, _resz );

    LSIS_Pixel4D currPix;
    LSIS_Pixel4D currNei;
    int min;
    int currNeiValue;

    beginForAllPixelsImage( currPix, _dimw, _dimx, _dimy, _dimz )
    {
        // On nefait la fermeture que dans le masque des pixels nuls
        if( currPix.valueIn(this) == 0 )
        {
            min = std::numeric_limits<int>::max();
            beginForAllPixelsNeighbours( currPix, _dimw, _dimx, _dimy, _dimz, currNei, size )
            {
                currNeiValue = currNei.valueIn( this );

                if( currNeiValue < min )
                {
                    min = currNeiValue;
                }
            }
            endForAllPixelsNeighbours( currPix, _dimw, _dimx, _dimy, _dimz, currNei, size )
            currPix.setValueIn( erode, min );
        }

        else
        {
            currPix.setValueIn( erode, currPix.valueIn( this ) );
        }
    }
    endForAllPixelsImage( currPix, _dimw, _dimx, _dimy, _dimz )

            return erode;
}

//void LSIS_HoughSpace4D::meanFilter(int sizeSmooth)
//{
// TOUT EST FAUX CAR IL FAUT PASSER PAR UNE IMAGE COPIE
//    LSIS_Pixel4D pix;
//    LSIS_Pixel4D nei;
//    int curVal;
//    int sumVal;
//    int nVal;
//    beginForAllPixelsImage( pix, _dimw, _dimx, _dimy, _dimz )
//    {
//        nVal = 0;
//        sumVal = 0;
//        beginForAllPixelsNeighboursAndCenter( pix, _dimw, _dimx, _dimy, _dimz, nei, sizeSmooth )
//        {
//            if( nei.valueIn(this) > 0 )
//            {
//                sumVal += nei.valueIn( this );
//                nVal++;
//            }
//        }
//        endForAllPixelsNeighboursAndCenter( pix, _dimw, _dimx, _dimy, _dimz, nei, sizeSmooth )
//        nVal = round((float)sumVal / (float)nVal);
//    }
//    endForAllPixelsImage( pix, _dimw, _dimx, _dimy, _dimz )
//}

void LSIS_HoughSpace4D::setValueInWindow(LSIS_Pixel4D &bot, LSIS_Pixel4D &top, int val)
{
    LSIS_Pixel4D curPix;

    beginForAllPixelsInBBox( bot, top, _dimw, _dimx, _dimy, _dimz, curPix )
    {
        curPix.setValueIn( this, val );
    }
    endForAllPixelsInBBox( bot, top, _dimw, _dimx, _dimy, _dimz, curPix )
}

void LSIS_HoughSpace4D::getMinMaxInWindow(LSIS_Pixel4D& bot,
                                          LSIS_Pixel4D& top,
                                          int& outMin,
                                          int& outMax,
                                          QVector<int>& outValues )
{
    LSIS_Pixel4D curPix;
    int curVal;
    outMin = std::numeric_limits<int>::max();
    outMax = -std::numeric_limits<int>::max();

    // Parcours des pixels dans la fenetre pour recuperer le min et le max des scores
    beginForAllPixelsInBBox( bot, top, _dimw, _dimx, _dimy, _dimz, curPix )
    {
        curVal = curPix.valueIn( this );

        // Mise a jour des min max, on ne tient compte que des cases ou il y a eu au moins un vote
        if ( curVal != _NAdata && curVal > 0 )
        {
            if ( curVal < outMin )
            {
                outMin = curVal;
            }

            if ( curVal > outMax )
            {
                outMax = curVal;
            }

            outValues.push_back( curVal );
        }
    }
    endForAllPixelsInBBox( bot, top, _dimw, _dimx, _dimy, _dimz, curPix )
//    if( outMax - outMin > 10 ) qDebug() << "Min max " << outMin << outMax << outMax - outMin;
}

void LSIS_HoughSpace4D::threshInWindow(LSIS_Pixel4D& bot, LSIS_Pixel4D& top, int thresh, int threshedValue )
{
    LSIS_Pixel4D curPix;
    int curValue;

    beginForAllPixelsInBBox( bot, top, _dimw, _dimx, _dimy, _dimz, curPix )
    {
        curValue = curPix.valueIn( this );

        if ( curValue != _NAdata )
        {
            if ( curValue < thresh )
            {
                curPix.setValueIn( this, threshedValue );
            }
        }
    }
    endForAllPixelsInBBox( bot, top, _dimw, _dimx, _dimy, _dimz, curPix )
}

void LSIS_HoughSpace4D::threshWindowWithRatioAndOtsu(LSIS_Pixel4D& bot,
                                                     LSIS_Pixel4D& top,
                                                     float ratioThreshCoeff,
                                                     float otsuThreshCoeff,
                                                     int nbBinsInHistogram )
{
    assertWithMessage( bot.w() == bot.w(), "Error in LSIS_HoughSpace4D::threshWindowWithRatioAndOtsu : cette methode ne doit etre appelle que pour une bbox 3D (on reste dans les memes coordonnees en w) sinon le seuil de ratio est inadequat");

    // Calculs necessaires a l'obtention du seuil theorique pour le ratio des scores dans une fenetre valide
    float halfResX = _resw/2.0;
    float halfResY = _resy/2.0;
    float halfResZ = _resz/2.0;
    float halfDiag3D = sqrt( (halfResX * halfResX) + (halfResY * halfResY) + (halfResZ * halfResZ) );

    // Calcul du ratio entre min et max theorique et du ratio dans la fenetre courante
    // Le ratio theorique se calcule comme e / ( e + 2R ) avec e la longueur de la demi diagonale d'une case en 3D (x,y,z)
    // Si le ratio theo est plus petit qu'un ratio min fixe par l'utilisateur on le laisse a ce ration min
//    float ratioMin = 4;
    float ratioTheo = ( halfDiag3D + (2 * bot.toCartesianW( this ) ) ) / halfDiag3D;
//    if ( ratioTheo < ratioMin )
//    {
//        ratioTheo = ratioMin;
//    }

    // On recupere les valeurs min et max de la fenetre d'analyse
    // et on stocke toutes les valeurs dans un vecteur qui va servir a calculer l'histogramme
    int minValue = std::numeric_limits<int>::max();
    int maxValue = -std::numeric_limits<int>::max();
    QVector<int> values;

    // Recuperer le min et le max et l'ensemble des scores de la fenetre
    getMinMaxInWindow( bot, top, minValue, maxValue, values );
//    qDebug() << "Minvalue " << minValue << "Max value " << maxValue << "valeurs = " << values;

    // Si on n'a recupere aucun vote dans la fenetre d'analyse
    // Alors on filtre cette fenetre
    // ce qui revient a ne rien faire normalement puisque les valeurs sont deja a 0 ou a NA
    if ( values.size() < 2 )
    {
        setValueInWindow( bot, top, 0 );
    }

    // On a bien eu des votes dans la fenetre d'analyse
    // On va donc calculer le seuil d'Otsu pour sï¿œparer en deux classes les scores de l'espace de Hough
    // Puis on va analysr le ratio des valeurs moyennes des deux classes et faire passer le seuil sur le ratio
    // Finalement, si le ratio est suffisamment eleve, on applique vraiment le seuil d'otsu
    else
    {
        // Creation de l'histogramme des valeurs pour la fenetre courante
        LSIS_Histogram1D<int>* hist = new LSIS_Histogram1D<int>( minValue, maxValue, nbBinsInHistogram, true, values );

        // On calcule le seuil d'Otsu
        float otsuThresh = hist->otsuThreshold();

        // On va maintenant recuperer la valeur moyenne des deux classes
        float meanLowClass = 0;     // Moyenne de la classe du bas
        float meanHighClass = 0;    // Moyenne de la classe haute
        hist->meanOfClasses( otsuThresh, meanLowClass, meanHighClass );

        // Ajout recent (2 juin 2015)
        // Si on a qu'une seule valeur dans l'histograme on filtre tout
        if( meanLowClass == 0 )
        {
            setValueInWindow( bot, top, 0 );
            delete hist;
            return;
        }

        // Et c'est a partir de ces valeurs qu'on regarde le seuil sur le ratio
        // Calcul du ratio courant
        float curRatio = meanHighClass / meanLowClass;
//        qDebug() << "Seuil Otsu " << otsuThresh << "meanlow " << meanLowClass << "meanhigh " << meanHighClass << " curratio " << curRatio << "ratio theo " << ratioTheo;

        // Si le ratio est insuffisant alors on filtre toute la fenetre
        // Ou qu'il n'y a eu aucun pixel dans les classes
        if ( meanLowClass == 0 || meanHighClass == 0 || curRatio <= ratioTheo * ratioThreshCoeff )
        {
            setValueInWindow( bot, top, 0 );
        }

        else
        {
            // Si par contre le ratio est suffisamment eleve
            // On applique le seuil d'Otsu calcule precedemment
            threshInWindow( bot, top, ceil( otsuThresh * otsuThreshCoeff ), 0 );
        }

        // Libere la memoire
        delete hist;
    }
}

void LSIS_HoughSpace4D::threshWithRatioAndOtsuForGivenW(int w,
                                                        float ratioThreshCoeff,
                                                        float otsuThreshCoeff,
                                                        int nbBinsInHistogram)
{
    // On calcule la taille de la fenetre en cartesien, elle depend du rayon justemen
    // on prend au moins 2 fois le rayon (ici 3 par exemple)
    float winSizeCart = 5 * ( _bot(0) + ( ( (float)w + 0.5 ) * _resw ) );

    // Conversion en nombre de pixels (on en prend encore un peu plus)
    int winSizeX = ceil( winSizeCart / _resx ) + 2;
    int winSizeY = ceil( winSizeCart / _resy ) + 2;
    int winSizeZ = ceil( winSizeCart / _resz ) + 2;
    //qDebug() << "Pour le rayon de coord " << w << " taille de fenetre " << winSizeCart << " size " << winSizeX << " " << winSizeY << " " << winSizeZ;

//    // Si la taille de la fenetre est plus grande que l'espace de Hough
//    // On ne peut pas appliquer les seuils voulus (il manque le "deuxieme cercle" de scores
//    if( winSizeX >= _dimx || winSizeY >= _dimy || winSizeZ >= _dimz )
//    {
//        return;
//    }

    // Initialisation des boites englobantes des fenetres d'analayse
    LSIS_Pixel4D currentWindowStart(w,0,0,0);
    LSIS_Pixel4D currentWindowEnd(w, winSizeX-1, winSizeY-1, winSizeZ-1 );

    // Pour toutes les fenetres possibles dans l'image en 3D pour ce rayon
    // On va appliquer le seuillage local
    for ( currentWindowStart.x() = 0, currentWindowEnd.x() = winSizeX-1 ; currentWindowStart.x() < _dimx ; currentWindowStart.x() += winSizeX, currentWindowEnd.x() += winSizeX )
    {
        for ( currentWindowStart.y() = 0, currentWindowEnd.y() = winSizeY-1 ; currentWindowStart.y() < _dimy ; currentWindowStart.y() += winSizeY, currentWindowEnd.y() += winSizeY )
        {
            for ( currentWindowStart.z() = 0, currentWindowEnd.z() = winSizeZ-1 ; currentWindowStart.z() < _dimz ; currentWindowStart.z() += winSizeZ, currentWindowEnd.z() += winSizeZ )
            {
                threshWindowWithRatioAndOtsu(currentWindowStart, currentWindowEnd, ratioThreshCoeff, otsuThreshCoeff, nbBinsInHistogram );
            }
        }
    }
}

void LSIS_HoughSpace4D::threshWithRatioAndOtsu(float ratioThreshCoeff,
                                               float otsuThreshCoeff,
                                               int nbBinsInHistogram)
{
    //qDebug() << "Filtrage Otsu NON PARALLELE";
    // Il existe un seuil pour chaque dimension en rayon
    for ( int i = 0 ; i < _dimw ; i++ )
    {
        threshWithRatioAndOtsuForGivenW( i, ratioThreshCoeff, otsuThreshCoeff, nbBinsInHistogram );
    }

    // Ne pas oublier de mettre a jour les min max pour affichage et calcul des forces des contours actifs
    computeMinMax();
}

//void LSIS_HoughSpace4D::threshWithRatioAndOtsuParallel(float ratioThreshCoeff, float otsuThreshCoeff, int nbBinsInHistogram)
//{
//    qDebug() << "Filtrage Otsu parallele";
//    // Il existe un seuil pour chaque dimension en rayon
//    QVector< ParallelObjectOtsu > runs;
//    for ( int i = 0 ; i < _dimw ; i++ )
//    {
//        runs.push_back( ParallelObjectOtsu( this, i, ratioThreshCoeff, otsuThreshCoeff, nbBinsInHistogram ) );
//    }

//    QFuture<void> futureRslt = QtConcurrent::map( runs, runOtsuThreshOnParrallel );
//    futureRslt.waitForFinished();

//    // Ne pas oublier de mettre a jour les min max pour affichage et calcul des forces des contours actifs
//    computeMinMax();
//}

void LSIS_HoughSpace4D::threshByValue(float thresh)
{
    LSIS_Pixel4D currPix;
    beginForAllPixelsImage( currPix, _dimw, _dimx, _dimy, _dimz )
    {
        if ( currPix.valueIn( this ) < thresh )
        {
            currPix.setValueIn( this, 0 );
        }
    }
    endForAllPixelsImage( currPix, _dimw, _dimx, _dimy, _dimz )

    // Ne pas oublier de mettre a jour les min max pour affichage et calcul des forces des contours actifs
    computeMinMax();
}


void runOtsuThreshOnParrallel(ParallelObjectOtsu parallelObject)
{
    parallelObject._hs->threshWithRatioAndOtsuForGivenW( parallelObject._w,
                                                         parallelObject._ratioThreshCoeff,
                                                         parallelObject._otsuThreshCoeff,
                                                         parallelObject._nbBinsInHistogra );
}
