#include "lsis_pluginmanager.h"
#include "ct_stepseparator.h"
#include "ct_steploadfileseparator.h"
#include "ct_stepcanbeaddedfirstseparator.h"
#include "ct_actions/ct_actionsseparator.h"
#include "ct_exporter/ct_standardexporterseparator.h"
#include "ct_reader/ct_standardreaderseparator.h"
#include "ct_actions/abstract/ct_abstractaction.h"

// Inclure ici les entetes des classes definissant des Ã©tapes/actions/exporters ou readers

//#include "step/lsis_stepfoostep.h"
//#include "step/lsis_steptestoptionalmnt.h"

#include "step/lsis_stepcreatehoughspace.h"
//#include "step/lsis_stepmaximasfromhoughspace.h"
//#include "step/lsis_stepmaximasfromhoughspaceoptionaldtm.h"

#include "step/lsis_stepsnakesinhoughspace.h"
//#include "step/lsis_stepmultireshoughspace.h"

#include "step/lsis_steptranchesurmntoptionnel.h"
#include "step/lsis_stepprelocalisationtroncs.h"
#include "step/lsis_stepextractsubcloudsfrompositions.h"
#include "step/lsis_stepextractsubcloudsfrompositionsinfile.h"
#include "step/lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.h"

#include "step/lsis_stepaddsnake.h"
#include "step/lsis_stepaddsetsnakes.h"
#include "step/lsis_stepaddsetsnakesoverdtm.h"

#include "reader/lsis_readergrid4d.h"
#include "reader/lsis_readerhoughspace4d.h"

#include "exporter/lsis_exportergrid4d.h"

#include "step/lsis_stepexporttoasciixyznxnynz.h"

#include "step/lsis_stepexportcylindersofbillons.h"
#include "step/lsis_exportcylindersofbillonsv2.h"
#include "step/lsis_stepexportcircles.h"

#include "step/lsis_stepslicepointcloud.h"

#include "step/lsis_newstepfastfilterhoughspace.h"

#include "step/lsis_stepreadsimpletreecsv.h"

#include "step/lsis_stepfiltersnake.h"
#include "step/lsis_stepexportonedbhpersnake.h"
#include "step/lsis_stepexporttreedata.h"

LSIS_PluginManager::LSIS_PluginManager() : CT_AbstractStepPlugin()
{
}

LSIS_PluginManager::~LSIS_PluginManager()
{
}

QString LSIS_PluginManager::getPluginRISCitation() const
{
    return "TY  - COMP\n"
           "TI  - Plugin UDES-LSIS for Computree\n"
           "AU  - Ravaglia, Joris\n"
           "PB  - University of Sherbrooke, , Laboratoire des Sciences de l'Information et des Systèmes\n"
           "PY  - 2017\n"
           "UR  - http://rdinnovation.onf.fr/projects/plugin-udes-lsis/wiki\n"
           "ER  - \n";
}

bool LSIS_PluginManager::loadGenericsStep()
{

    //New submenu and steps in Geomitrical Shapes Steps
    addNewGeometricalShapesStep<LSIS_StepCreateHoughSpace>(QObject::tr("Step - Défilement"));
    addNewGeometricalShapesStep<LSIS_NewStepFastFilterHoughSpace>(QObject::tr("Step - Défilement"));
    addNewGeometricalShapesStep<LSIS_StepSnakesInHoughSpace>(QObject::tr("Step - Défilement"));
    addNewGeometricalShapesStep<LSIS_StepAddSetSnakes>(QObject::tr("Step - Défilement"));
    addNewGeometricalShapesStep<LSIS_StepSlicePointCloud>(QObject::tr("Step - Défilement"));
    addNewGeometricalShapesStep<LSIS_StepFilterSnake>(QObject::tr("Step - Défilement"));

    //New load option in Load > Geometrical Shape 3D
    addNewLoadStep<LSIS_StepReadSimpleTreeCsv>(CT_StepsMenu::LP_Items);

    //New export options in Load > Geometrical Shape 3D
    addNewExportStep<LSIS_StepExportToAsciiXYZNxNyNz>(CT_StepsMenu::LP_Items);
    addNewExportStep<LSIS_StepExportCylindersOfBillons>(CT_StepsMenu::LP_Items);
    addNewExportStep<LSIS_StepExportCylindersOfBillonsv2>(CT_StepsMenu::LP_Items);
    addNewExportStep<LSIS_StepExportOneDBHPerSnake>(CT_StepsMenu::LP_Items);
    addNewExportStep<LSIS_StepExportTreeData>(CT_StepsMenu::LP_Items);
/*
    //Export Circles is basically implemented in Export Tree Data at this point
    //addNewExportStep<LSIS_StepExportCircles>(CT_StepsMenu::LP_Items);

//    CT_StepSeparator *sep = addNewSeparator(new CT_StepSeparator("Tests"));
//    sep->addStep(new LSIS_StepCreateHoughSpace(*createNewStepInitializeData(NULL)));
//    sep->addStep(new LSIS_NewStepFastFilterHoughSpace(*createNewStepInitializeData(NULL)));
//    sep->addStep(new LSIS_StepMaximasFromHoughSpace(*createNewStepInitializeData(NULL)));
//    sep->addStep(new LSIS_StepMaximasFromHoughSpaceOptionalDtm(*createNewStepInitializeData(NULL)));
//    sep->addStep(new LSIS_StepTestOptionalMNT(*createNewStepInitializeData(NULL)));


//    sep->addStep(new LSIS_StepMultiResHoughSpace(*createNewStepInitializeData(NULL)));

//    sep = addNewSeparator(new CT_StepSeparator());
//    sep->addStep(new LSIS_StepTrancheSurMntOptionnel(*createNewStepInitializeData(NULL)));
//    sep->addStep(new LSIS_StepPreLocalisationTroncs(*createNewStepInitializeData(NULL)));
//    sep->addStep(new LSIS_StepExtractSubCloudsFromPositions(*createNewStepInitializeData(NULL)));
//    sep->addStep(new LSIS_StepExtractSubCloudsFromPositionsInFile(*createNewStepInitializeData(NULL)));
//    sep->addStep(new LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil(*createNewStepInitializeData(NULL)));

//    sep = addNewSeparator(new CT_StepSeparator());
//    sep->addStep(new LSIS_StepAddSnake(*createNewStepInitializeData(NULL)));
//    sep->addStep(new LSIS_StepAddSetSnakesOverDTM(*createNewStepInitializeData(NULL)));
*/
    return true;
}

bool LSIS_PluginManager::loadOpenFileStep()
{
    addNewLoadStep<LSIS_ReaderGrid4D>(QObject::tr("Plugin STEP"));
    addNewLoadStep<LSIS_ReaderHoughSpace4D>(QObject::tr("Plugin STEP"));

    return true;
}

bool LSIS_PluginManager::loadCanBeAddedFirstStep()
{
    return true;
}

bool LSIS_PluginManager::loadActions()
{
    return true;
}

bool LSIS_PluginManager::loadExporters()
{
    clearExporters();

    CT_StandardExporterSeparator *sep = addNewSeparator(new CT_StandardExporterSeparator("TYPE DE FICHIER"));
    sep->addExporter(new LSIS_ExporterGrid4D());

    return true;
}

bool LSIS_PluginManager::loadReaders()
{
    return true;
}

