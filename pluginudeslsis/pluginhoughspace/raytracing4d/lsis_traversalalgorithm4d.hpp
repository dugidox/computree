/************************************************************************************
* Filename :  lsis_traversalalgorithm4d.hpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_TRAVERSALALGORITHM4D_HPP
#define LSIS_TRAVERSALALGORITHM4D_HPP

#include "./lsis_traversalalgorithm4d.h"

// Utilise les assertions
#include "../assert/assertwithmessage.h"

// Possible affichage console
#include <QDebug>

// Utilise les constantes float max etc
#include <limits>

#define EPSILONE_TRAVERSAL 0.001

template< typename DataT >
LSIS_TraversalAlgorithm4D<DataT>::LSIS_TraversalAlgorithm4D()
{
    _grid = NULL;
    _gridBot = LSIS_Point4DDouble (0,0,0,0);
    _gridTop = LSIS_Point4DDouble (0,0,0,0);
    _gridRes = LSIS_Point4DDouble (0,0,0,0);
    _gridDim = LSIS_Point4DDouble (0,0,0,0);
    _keepFirst = false;
    _visitorList.clear();

    // Working variables
    _start = LSIS_Point4DDouble (0,0,0,0);
    _end = LSIS_Point4DDouble (0,0,0,0);
    _boundary = LSIS_Point4DDouble (0,0,0,0);
    _stepAxis = LSIS_Point4DDouble (0,0,0,0);
    _tMax = LSIS_Point4DDouble (0,0,0,0);
    _tDel = LSIS_Point4DDouble (0,0,0,0);
    _nextStepAxis = 0;
}

template< typename DataT >
LSIS_TraversalAlgorithm4D<DataT>::LSIS_TraversalAlgorithm4D(const CT_AbstractGrid4D *grid, bool keepFirst, QList<LSIS_AbstractVisitorGrid4D<DataT> *> &list)
{
    _visitorList = list;
    _grid = grid;
    grid->getMinCoordinates(_gridBot);
    grid->getMaxCoordinates(_gridTop);
    grid->getResolutions(_gridRes);
    grid->getDimensions(_gridDim);
    _keepFirst = keepFirst;
}

template< typename DataT >
LSIS_TraversalAlgorithm4D<DataT>::~LSIS_TraversalAlgorithm4D()
{
}

template< typename DataT >
void LSIS_TraversalAlgorithm4D<DataT>::initialise( LSIS_Beam4D& beam )
{
    // /////////////////////////////////////////////////////////////////////////////////////////////
    // On remet le start dans la grille pour pas que ca plante si il etait aux extremites
    if ( _start(1) < _gridBot(1) && _start(1) > _gridBot(1) - EPSILONE_TRAVERSAL )
    {
        _start(1) = ( _gridBot(1) + EPSILONE_TRAVERSAL );
    }

    if ( _start(2) < _gridBot(2) && _start(2) > _gridBot(2) - EPSILONE_TRAVERSAL )
    {
        _start(2) = ( _gridBot(2) + EPSILONE_TRAVERSAL );
    }

    if ( _start(3) < _gridBot(3) && _start(3) > _gridBot(3) - EPSILONE_TRAVERSAL )
    {
        _start(3) = ( _gridBot(3) + EPSILONE_TRAVERSAL );
    }

    if ( _start(0) < _gridBot(0) && _start(0) > _gridBot(0) - EPSILONE_TRAVERSAL )
    {
        _start(0) = ( _gridBot(0) + EPSILONE_TRAVERSAL );
    }

    if ( _start(1) > _gridTop(1) && _start(1) < _gridTop(1) + EPSILONE_TRAVERSAL )
    {
        _start(1) = ( _gridTop(1) - EPSILONE_TRAVERSAL );
    }

    if ( _start(2) > _gridTop(2) && _start(2) < _gridTop(2) + EPSILONE_TRAVERSAL )
    {
        _start(2) = ( _gridTop(2) - EPSILONE_TRAVERSAL );
    }

    if ( _start(3) > _gridTop(3) && _start(3) < _gridTop(3) + EPSILONE_TRAVERSAL )
    {
        _start(3) = ( _gridTop(3) - EPSILONE_TRAVERSAL );
    }

    if ( _start(0) > _gridTop(0) && _start(0) < _gridTop(0) + EPSILONE_TRAVERSAL )
    {
        _start(0) = ( _gridTop(0) - EPSILONE_TRAVERSAL );
    }
    // /////////////////////////////////////////////////////////////////////////////////////////////

    // ***********************************************************
    // Working with copies of the beam attributes avoids calling the getters
    // ***********************************************************
    LSIS_Point4DDouble  ori, dir;
    dir = beam.getDirection();
    ori = beam.getOrigin();

    // ***********************************************************
    // Initialising delta distances and the traversal directional sign
    // ***********************************************************
    qreal dirCoord;
    for( int i = 0 ; i < 4 ; i++ )
    {
        dirCoord = getQVector4DCoord(dir,i);
        if ( dirCoord > 0 )
        {
            setQVector4DCoord( _stepAxis, i, 1 );
        }

        else if ( dirCoord < 0 )
        {
            setQVector4DCoord( _stepAxis, i, -1);
        }

        else
        {
            setQVector4DCoord( _stepAxis, i, 0);
        }

        if ( dirCoord != 0 )
        {
            setQVector4DCoord( _tDel, i, getQVector4DCoord(_gridRes,i) / fabs( dirCoord ) );
        }
    }

    // ***********************************************************
    // Get the index of the voxel from which the traversal starts (so we can access its coordinates in the grid system)
    // ***********************************************************
    size_t currentVoxelW, currentVoxelX, currentVoxelY, currentVoxelZ;

    // Le voxel courant est le voxel qui contient le point _start (voxel de depart de l'algorithme)
    _grid->coordAtWXYZ( _start(0), _start(1), _start(2), _start(3),
                        currentVoxelW, currentVoxelX, currentVoxelY, currentVoxelZ );
    _currentVoxel(0) = ( currentVoxelW );
    _currentVoxel(1) = ( currentVoxelX );
    _currentVoxel(2) = ( currentVoxelY );
    _currentVoxel(3) = ( currentVoxelZ );

    // ***********************************************************
    // Compute the leaving point of the ray inside the first voxel pierced
    // And initialise the tMax values
    // ***********************************************************
    for ( int i = 0 ; i < 4 ; i++ )
    {
        if ( getQVector4DCoord(_stepAxis,i) > 0 )
        {
            setQVector4DCoord( _boundary, i, ( getQVector4DCoord(_currentVoxel,i)+1)*getQVector4DCoord(_gridRes,i) + getQVector4DCoord(_gridBot,i) );
        }

        else
        {
            setQVector4DCoord( _boundary, i, getQVector4DCoord(_currentVoxel,i)*getQVector4DCoord(_gridRes,i) + getQVector4DCoord(_gridBot,i) );
        }

        if ( getQVector4DCoord(dir,i) != 0 )
        {
            setQVector4DCoord( _tMax, i, fabs( (getQVector4DCoord(_boundary,i) - getQVector4DCoord(_start,i) ) / getQVector4DCoord(dir,i) ) );
        }

        else
        {
            setQVector4DCoord( _tMax, i, std::numeric_limits<float>::max() );
        }
    }
}

template< typename DataT >
void LSIS_TraversalAlgorithm4D<DataT>::compute(LSIS_Beam4D& beam)
{
    // ***********************************************************
    // Check for the entering point of the beam into the grid
    // ***********************************************************
    // Au passage on calcule start et end, les points d'entree et de sortie du beam dans la grille
    if ( !beam.intersect(_gridBot, _gridTop, _start, _end) )
    {
        // The beam does not even intersect the grid, the algorithm ends
        return;
    }

    // Initialise les tMax, deltaT etc
    initialise( beam );

    // ***********************************************************
    // If the first voxel has to be pierced too, visit it
    // ***********************************************************
    if ( _keepFirst )
    {
        for ( int i = 0 ; i < _visitorList.size() ; ++i )
        {
            _visitorList.at(i)->visit( (size_t)_currentVoxel(0),
                                       (size_t)_currentVoxel(1),
                                       (size_t)_currentVoxel(2),
                                       (size_t)_currentVoxel(3),
                                       &beam );
        }
    }

    // ***********************************************************
    // Start the traversal loop until the beam reaches the outside of the grid
    // ***********************************************************
    float minTMax;
    while ( 1 )
    {
        // Finds along which axis to do the next step
        // (it is the axis with the lowest tMax)
        _nextStepAxis = 0;
        minTMax = getQVector4DCoord(_tMax,0);
        for ( int i = 1 ; i < 4 ; i++ )
        {
            if ( getQVector4DCoord(_tMax,i) < minTMax )
            {
                _nextStepAxis = i;
                minTMax = getQVector4DCoord(_tMax,i);
            }
        }

        // Deplace le voxel courrant selon la direction trouvee precedement
        setQVector4DCoord( _currentVoxel, _nextStepAxis, getQVector4DCoord(_currentVoxel, _nextStepAxis) + getQVector4DCoord(_stepAxis, _nextStepAxis ) );

        // Mise a jour du tMax correspondant a la direction trouvee precedement
        setQVector4DCoord( _tMax, _nextStepAxis, getQVector4DCoord( _tMax, _nextStepAxis ) + getQVector4DCoord(_tDel, _nextStepAxis) );

        // Check if the voxel to be traversed is in the grid
        if ( getQVector4DCoord(_currentVoxel, _nextStepAxis) >= 0 && getQVector4DCoord(_currentVoxel, _nextStepAxis) < getQVector4DCoord(_gridDim, _nextStepAxis) )
        {
            // Traverse the voxel
            for (int i = 0 ; i < _visitorList.size() ; ++i)
            {
                _visitorList.at(i)->visit( (size_t)_currentVoxel(0),
                                           (size_t)_currentVoxel(1),
                                           (size_t)_currentVoxel(2),
                                           (size_t)_currentVoxel(3),
                                           &beam );
            }
        }

        else
        {
            // Traversal ends
            return;
        }
    }
}

template< typename DataT >
void LSIS_TraversalAlgorithm4D<DataT>::computeBBoxOfEnteringAndStopingVoxels(LSIS_Point4DDouble &botBBox, LSIS_Point4DDouble &topBBox )
{
    float tmp;
    if ( botBBox(0) > topBBox(0) )
    {
        tmp = topBBox(0);
        topBBox(0) = ( botBBox(0) );
        botBBox(0) = ( tmp );
    }

    if ( botBBox(1) > topBBox(1) )
    {
        tmp = topBBox(1);
        topBBox(1) = ( botBBox(1) );
        botBBox(1) = ( tmp );
    }

    if ( botBBox(2) > topBBox(2) )
    {
        tmp = topBBox(2);
        topBBox(2) = ( botBBox(2) );
        botBBox(2) = ( tmp );
    }

    if ( botBBox(3) > topBBox(3) )
    {
        tmp = topBBox(3);
        topBBox(3) = ( botBBox(3) );
        botBBox(3) = ( tmp );
    }
}

template< typename DataT >
void LSIS_TraversalAlgorithm4D<DataT>::compute(LSIS_Beam4D& beam,
                                               unsigned int stopW, unsigned int stopX, unsigned int stopY, unsigned int stopZ,
                                               bool addLastVoxel,
                                               QVector<LSIS_Point4DInt*>& traversed)
{
    // ***********************************************************
    // Check for the entering point of the beam into the grid
    // ***********************************************************
    // Au passage on calcule start et end, les points d'entree et de sortie du beam dans la grille
    if ( !beam.intersect(_gridBot, _gridTop, _start, _end) )
    {
        // The beam does not even intersect the grid, the algorithm ends
        return;
    }

    // Initialise les tMax, deltaT etc
    initialise( beam );

    // Compute the bounding box of the entering and stopping voxel
    // Casting de la coordonnee du dernier pixel limite en LSIS_Point4DDouble
    LSIS_Point4DDouble  stopVoxel( stopW, stopX, stopY, stopZ );
    LSIS_Point4DDouble  botBBox( _currentVoxel(0), _currentVoxel(1), _currentVoxel(2), _currentVoxel(3) );
    LSIS_Point4DDouble  topBBox( stopVoxel );
    computeBBoxOfEnteringAndStopingVoxels( botBBox, topBBox );

    // ***********************************************************
    // If the first voxel has to be pierced too, visit it and add it to the list of pierced voxels
    // ***********************************************************
    if (_keepFirst)
    {
        for (int i = 0 ; i < _visitorList.size() ; ++i)
        {
            _visitorList.at(i)->visit( (size_t)_currentVoxel(0),
                                       (size_t)_currentVoxel(1),
                                       (size_t)_currentVoxel(2),
                                       (size_t)_currentVoxel(3),
                                       &beam );
        }

        // Add the voxel to the list
        traversed.push_back( new LSIS_Point4DInt( _currentVoxel(0), _currentVoxel(1), _currentVoxel(2), _currentVoxel(3) ));
    }

    // ***********************************************************
    // Start the traversal loop until the beam reaches the outside of the grid
    // ***********************************************************
    float minTMax;
    while ( 1 )
    {
        // Finds along which axis to do the next step
        // (it is the axis with the lowest tMax)
        _nextStepAxis = 0;
        minTMax = getQVector4DCoord(_tMax,0);
        for ( int i = 1 ; i < 4 ; i++ )
        {
            if ( getQVector4DCoord(_tMax,i) < minTMax )
            {
                _nextStepAxis = i;
                minTMax = getQVector4DCoord(_tMax,i);
            }
        }

        // Deplace le voxel courrant selon la direction trouvee precedement
        setQVector4DCoord( _currentVoxel, _nextStepAxis, getQVector4DCoord(_currentVoxel, _nextStepAxis) + getQVector4DCoord(_stepAxis, _nextStepAxis ) );

        // Mise a jour du tMax correspondant a la direction trouvee precedement
        setQVector4DCoord( _tMax, _nextStepAxis, getQVector4DCoord( _tMax, _nextStepAxis ) + getQVector4DCoord(_tDel, _nextStepAxis) );

        // Check if the voxel to be traversed is in the grid
        if ( getQVector4DCoord(_currentVoxel, _nextStepAxis) >= 0 && getQVector4DCoord(_currentVoxel, _nextStepAxis) < getQVector4DCoord(_gridDim, _nextStepAxis) )
        {
            // Traverse the voxel
            for (int i = 0 ; i < _visitorList.size() ; ++i)
            {
                _visitorList.at(i)->visit( (size_t)_currentVoxel(0),
                                           (size_t)_currentVoxel(1),
                                           (size_t)_currentVoxel(2),
                                           (size_t)_currentVoxel(3),
                                           &beam );
            }

            // Check if the voxel to be traversed is the stopping voxel
            // The commented line should be the right one but with numerical instabilities, the following is prefered
            // if ( _currentVoxel == stopVoxel )
            // It is preferable to check if the current voxel is outside the bounding box made by the starting voxel and the stopping voxel
            if ( _currentVoxel == stopVoxel ||
                 _currentVoxel(0) < botBBox(0) || _currentVoxel(0) > topBBox(0) ||
                 _currentVoxel(1) < botBBox(1) || _currentVoxel(1) > topBBox(1) ||
                 _currentVoxel(2) < botBBox(2) || _currentVoxel(2) > topBBox(2) ||
                 _currentVoxel(3) < botBBox(3) || _currentVoxel(3) > topBBox(3) )
            {
                if ( addLastVoxel )
                {
                    qDebug() << "Ajout d'un pixel au tableau";
                    // Add the voxel to the list
                    traversed.push_back( new LSIS_Point4DInt( _currentVoxel(0), _currentVoxel(1), _currentVoxel(2), _currentVoxel(3) ));
                }

                return;
            }

            else
            {
                qDebug() << "Ajout d'un pixel au tableau";
                // Add the voxel to the list
                traversed.push_back( new LSIS_Point4DInt( _currentVoxel(0), _currentVoxel(1), _currentVoxel(2), _currentVoxel(3) ));
            }
        }

        // On est sorti de la grille on stoppe le raytracing
        else
        {
            return;
        }
    }
}

#endif // LSIS_TRAVERSALALGORITHM4D_HPP
