/************************************************************************************
* Filename :  lsis_traversalalgorithm4d.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_TRAVERSALALGORITHM4D_H
#define LSIS_TRAVERSALALGORITHM4D_H

// Traversal algorithm traverse a 4D grid
#include "ct_itemdrawable/abstract/ct_abstractgrid4d.h"

// L'algorithme implemente le design pattern visitor
#include "./lsis_abstractvisitorgrid4d.h"

// Le trajet de l'algorithme est recupere sous forme de points 4D
#include "../point4d/lsis_point4d.h"

template< typename DataT >
class LSIS_TraversalAlgorithm4D
{
public:
    /*!
     * \brief Default constructor
     * Attributes will be set to 0, NULL and cleared
     */
    LSIS_TraversalAlgorithm4D();

    /*!
     * \brief Initialisation constructor
     * \param grid : Grid to be traversed
     * \param keepFirst :
     * \param list : List of visitors (that will modify the voxels they traverse) used
     */
    LSIS_TraversalAlgorithm4D(const CT_AbstractGrid4D* grid, bool keepFirst, QList< LSIS_AbstractVisitorGrid4D<DataT> *> &list);

    /*!
      * \brief Destructor
      */
    ~LSIS_TraversalAlgorithm4D();

private :
    /*!
     * \brief initialise
     * Initialise l'algorithme (premier voxel traverse, les tMax, les deltaT, ... )
     */
    void initialise( LSIS_Beam4D& beam );

public :
    /*!
     * \brief compute method of the algorithm
     *
     * \param beam : origin and direction of the traversal
     */
    virtual void compute( LSIS_Beam4D& beam );

private :
    /*!
     * \brief computeBBoxOfEnteringAndStopingVoxels
     *
     * Remet les vecteurs dans le bon ordre (toutes les coordonnees de bot sont inferieure aux coordonnees de top)
     *
     * \param botBBox
     * \param topBBox
     */
    void computeBBoxOfEnteringAndStopingVoxels( LSIS_Point4DDouble& botBBox, LSIS_Point4DDouble& topBBox );

public :

    /*!
     * \brief compute
     *
     * Starts the algorithm
     * Each traversed voxel is visited by each visitor
     * And is then stored in a list
     * This way the list of traversed pixels is available
     *
     * \param beam : origin and direction of the traversal
     * \param stopW : coordinate of the last voxel to be traversed (in case the traversal should not traverse the entire grid but only a bounded part of it)
     * \param stopX : coordinate of the last voxel to be traversed (in case the traversal should not traverse the entire grid but only a bounded part of it)
     * \param stopY : coordinate of the last voxel to be traversed (in case the traversal should not traverse the entire grid but only a bounded part of it)
     * \param stopZ : coordinate of the last voxel to be traversed (in case the traversal should not traverse the entire grid but only a bounded part of it)
     * \param traversed : output - list of the pixels traversed during the algorithm
     */
    virtual void compute(LSIS_Beam4D& beam,
                         unsigned int stopW, unsigned int stopX, unsigned int stopY, unsigned int stopZ,
                         bool addLastVoxel,
                         QVector<LSIS_Point4DInt *> &traversed );

    /*!
     * \brief Tools to access the coordinate of a qvector4d (replacing brackets operator which does not exists in qvector4d)
     * \param vec Vector
     * \param axis Axis
     * \return The coordinate of the vector along the given axis
     */
    inline float getQVector4DCoord ( const LSIS_Point4DDouble& vec, int axis )
    {
        if ( axis == 0 ) return vec(0);
        if ( axis == 1 ) return vec(1);
        if ( axis == 2 ) return vec(2);
        if ( axis == 3 ) return vec(3);

        assert(false);
    }

    /*!
     * \brief Tools to set the coordinate of a qvector4d (replacing brackets operator which does not exists in qvector4d)
     * \param vec Vector
     * \param axis Axis
     * \param value The new coordinate
     */
    inline void setQVector4DCoord ( LSIS_Point4DDouble& vec, int axis, float value )
    {
        if ( axis == 0 ) vec(0) = value;
        if ( axis == 1 ) vec(1) = value;
        if ( axis == 2 ) vec(2) = value;
        if ( axis == 3 ) vec(3) = value;
    }

/* **************************************************************** */
/* Attributs de la classe                                           */
/* **************************************************************** */
private :
        // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    // Attributs relatifs a la grille a traverser                                                                                                   //
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    /**/    const CT_AbstractGrid4D*                _grid;          /*!< Grid to traverse */                                                        //
    /**/    LSIS_Point4DDouble                      _gridBot;       /*!< bottom left coordinates of the calibration grid*/                          //
    /**/    LSIS_Point4DDouble                      _gridTop;       /*!< upper right coordinates of the calibration grid*/                          //
    /**/    LSIS_Point4DDouble                      _gridRes;       /*!< Resolution of the calibration grid*/                                       //
    /**/    LSIS_Point4DDouble                      _gridDim;       /*!< Dimension of the calibration grid*/                                        //
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //

    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    // Attributs relatifs au comportement de l'algorithme lors de la traverse                                                                       //
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    /**/    bool                                        _keepFirst;     /*! Should the cell containing the beam origin be kept*/                    //
    /**/    QList< LSIS_AbstractVisitorGrid4D<DataT>* > _visitorList;   /*! List of visitors visiting the cell of the grid */                       //
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //

    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    // Variables d'initialisation de l'algorithme                                                                                                   //
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    /**/    LSIS_Point4DDouble  _start;           /*!< Point d'entree du rayon dans la grille */                                                    //
    /**/    LSIS_Point4DDouble  _end;             /*!< Point de sortie du rayon dans la grille */                                                   //
    /**/    LSIS_Point4DDouble  _boundary;        /*!< Point de sortie du rayon dans le premier voxel traverse */                                   //
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //

    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    // Variables utilisees lors de la traverse a proprement parler                                                                                  //
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    /**/    LSIS_Point4DDouble  _tMax;            /*!< "the value of t at which the ray crosses the first voxel boundary (along each direction)"*/            //
    /**/    LSIS_Point4DDouble  _tDel;            /*!< "how far along the ray we must move (in units of t)" for each component "of such a movement to equal the width of a voxel"*/
    /**/    LSIS_Point4DDouble  _stepAxis;        /*!< indicates for each axis wether the ray goes forward (in the same direction than the base vector => 1) or backward (the opposite direction => -1)*/
    /**/    LSIS_Point4DDouble  _currentVoxel;    /*!< Coordonnees du voxel courant */                                                                        //
    /**/    int                 _nextStepAxis;    /*!< Wich axis is going to be incremented next*/                                                  //
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
};

// Inclusion des implementations template
#include "lsis_traversalalgorithm4d.hpp"

#endif // LSIS_TRAVERSALALGORITHM4D_H
