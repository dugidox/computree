/************************************************************************************
* Filename :  lsis_incrementvisitor.hpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_INCREMENTVISITOR_HPP
#define LSIS_INCREMENTVISITOR_HPP

#include "./lsis_incrementvisitor.h"

#include <QDebug>

template < typename DataT >
LSIS_IncrementVisitor<DataT>::LSIS_IncrementVisitor(CT_Grid4D<DataT> *grid) : LSIS_AbstractVisitorGrid4D<DataT>( grid )
{
}

template < typename DataT >
LSIS_IncrementVisitor<DataT>::~LSIS_IncrementVisitor()
{
}

template < typename DataT >
void LSIS_IncrementVisitor<DataT>::visit(size_t levw, size_t levx, size_t levy, size_t levz, LSIS_Beam4D const * beam)
{
    Q_UNUSED(beam);
    LSIS_AbstractVisitorGrid4D<DataT>::_grid->addValue(levw, levx, levy, levz, 1);
}


#endif // LSIS_INCREMENTVISITOR_HPP
