/************************************************************************************
* Filename :  norm_newoctreedrawmanager.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef NORM_NEWOCTREEDRAWMANAGER_H
#define NORM_NEWOCTREEDRAWMANAGER_H

// Inherits from this class
#include "ct_itemdrawable/tools/drawmanager/ct_standardabstractitemdrawablewithoutpointclouddrawmanager.h"

class NORM_NewOctreeDrawManager : public CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager
{
public:
//********************************************//
//         Constructors/Destructors           //
//********************************************//
    /*!
     * \brief NORM_NewOctreeDrawManager
     *
     * Constructor of the NORM_NewOctreeDrawManager class
     *
     * \param drawConfigurationName : name of the configuration
     */
    NORM_NewOctreeDrawManager(QString drawConfigurationName = "");

//********************************************//
//                Drawing tools               //
//********************************************//
    /*!
     * \brief draw
     *
     * Draw an octree : draw each of the octree nodes box
     *
     * \param view : graphic view
     * \param painter : painter to paint objects in the view
     * \param itemDrawable : octree to be drawn
     */
    virtual void draw(GraphicsViewInterface& view,
                      PainterInterface& painter,
                      const CT_AbstractItemDrawable& itemDrawable) const;

    //**********************************************//
    //          Drawing configuration tools         //
    //**********************************************//
    static QString staticInitConfigDisplayAcpBasis();
    static QString staticInitConfigDisplayNodeBoxes();
    static QString staticInitConfigDisplayNodeBoxes1();
    static QString staticInitConfigDisplayNodeBoxes2();
    static QString staticInitConfigDisplayNodeBoxes3();
    static QString staticInitConfigDisplayNodeBoxes4();
    static QString staticInitConfigDisplayNodeBoxes5();
    static QString staticInitConfigDisplayNodeBoxes6();
    static QString staticInitConfigDisplayNodeBoxes7();
    static QString staticInitConfigDisplayNodeBoxes8();

    //**********************************************//
    //                  Attributes                  //
    //**********************************************//
protected :
    // Drawing configuration
    const static QString INDEX_CONFIG_DISPLAY_ACP_BASIS;
    const static QString INDEX_CONFIG_DISPLAY_NODE_BOXES;
    const static QString INDEX_CONFIG_DISPLAY_NODE_BOXES_1;
    const static QString INDEX_CONFIG_DISPLAY_NODE_BOXES_2;
    const static QString INDEX_CONFIG_DISPLAY_NODE_BOXES_3;
    const static QString INDEX_CONFIG_DISPLAY_NODE_BOXES_4;
    const static QString INDEX_CONFIG_DISPLAY_NODE_BOXES_5;
    const static QString INDEX_CONFIG_DISPLAY_NODE_BOXES_6;
    const static QString INDEX_CONFIG_DISPLAY_NODE_BOXES_7;
    const static QString INDEX_CONFIG_DISPLAY_NODE_BOXES_8;

protected :
// ******************************************** //
// Configuration tools                          //
// Member attributes                            //
// ******************************************** //
    /*!
     * \brief createDrawConfiguration
     *
     * Creates the draw configuration menu (displayed in the draw configuration window in computree)
     *
     * \param drawConfigurationName : draw configuration name
     *
     * \return the draw configuration
     */
    virtual CT_ItemDrawableConfiguration createDrawConfiguration(QString drawConfigurationName) const;
};

#endif // NORM_NEWOCTREEDRAWMANAGER_H
