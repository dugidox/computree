#include "norm_stepgeneratesphere.h"

#include "../itemdrawable/norm_sphere.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_point.h"

// Alias for indexing models
#define DEFout_ResultSphere "ResultSphere"
#define DEFout_GroupSphere "GroupSphere"
#define DEFout_ItemSphere "ItemSphere"


// Constructor : initialization of parameters
NORM_StepGenerateSphere::NORM_StepGenerateSphere(CT_StepInitializeData &dataInit) : CT_AbstractStepCanBeAddedFirst(dataInit)
{
    _centerx = 0;
    _centery = 0;
    _centerz = 0;
    _radius = 0.001;
}

// Step description (tooltip of contextual menu)
QString NORM_StepGenerateSphere::getStepDescription() const
{
    return tr("Generates a sphere");
}

// Step detailled description
QString NORM_StepGenerateSphere::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_StepGenerateSphere::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStepCanBeAddedFirst::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_StepGenerateSphere::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_StepGenerateSphere(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_StepGenerateSphere::createInResultModelListProtected()
{
    // No in result is needed
    setNotNeedInputResult();
}

// Creation and affiliation of OUT models
void NORM_StepGenerateSphere::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_ResultSphere = createNewOutResultModel(DEFout_ResultSphere, tr("Result of generation"));
    res_ResultSphere->setRootGroup(DEFout_GroupSphere, new CT_StandardItemGroup(), tr("Group"));
    res_ResultSphere->addItemModel(DEFout_GroupSphere, DEFout_ItemSphere, new NORM_Sphere(), tr("Generated sphere"));

}

// Semi-automatic creation of step parameters DialogBox
void NORM_StepGenerateSphere::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("x", "", -9999, 9999, 4, _centerx, 1);
    configDialog->addDouble("y", "", -9999, 9999, 4, _centery, 1);
    configDialog->addDouble("z", "", -9999, 9999, 4, _centerz, 1);
    configDialog->addDouble("r", "", 0.0001, 9999, 4, _radius, 1);
}

void NORM_StepGenerateSphere::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_ResultSphere = outResultList.at(0);

    // OUT results creation (move it to the appropried place in the code)
    CT_StandardItemGroup* grp_GroupSphere= new CT_StandardItemGroup(DEFout_GroupSphere, res_ResultSphere);
    res_ResultSphere->addGroup(grp_GroupSphere);
    
    CT_Point center = createCtPoint( _centerx, _centery, _centerz );
    CT_Point fooDirection = createCtPoint( 0,0,1 );
    CT_CircleData* sphereData = new CT_CircleData( center, fooDirection, _radius );
    NORM_Sphere* item_ItemSphere = new NORM_Sphere(DEFout_ItemSphere, res_ResultSphere, sphereData );
    grp_GroupSphere->addItemDrawable(item_ItemSphere);
}
