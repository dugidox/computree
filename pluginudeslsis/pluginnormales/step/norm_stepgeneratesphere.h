#ifndef NORM_STEPGENERATESPHERE_H
#define NORM_STEPGENERATESPHERE_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

/*!
 * \class NORM_StepGenerateSphere
 * \ingroup Steps_NORM
 * \brief <b>Generates a sphere.</b>
 *
 * No detailled description for this step
 *
 * \param _centerx 
 * \param _centery 
 * \param _centerz 
 * \param _radius 
 *
 */

class NORM_StepGenerateSphere: public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     * 
     * Create a new instance of the step
     * 
     * \param dataInit Step parameters object
     */
    NORM_StepGenerateSphere(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     * 
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     * 
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     * 
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     * 
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     * 
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     * 
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     * 
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     * 
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    // Step parameters
    double    _centerx;    /*!<  */
    double    _centery;    /*!<  */
    double    _centerz;    /*!<  */
    double    _radius;    /*!<  */

};

#endif // NORM_STEPGENERATESPHERE_H
