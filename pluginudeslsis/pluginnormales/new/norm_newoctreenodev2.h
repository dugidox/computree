#ifndef NORM_NEWOCTREENODE_H
#define NORM_NEWOCTREENODE_H

#include "ct_point.h"
#include "ct_accessor/ct_pointaccessor.h"

#include <QDebug>
#include <QQueue>
#include <QTextStream>

#define QCOLOR0 QColor::fromHsv( 0, 255, 255)
#define QCOLOR1 QColor::fromHsv( 45, 255, 255)
#define QCOLOR2 QColor::fromHsv( 90, 255, 255)
#define QCOLOR3 QColor::fromHsv( 135, 255, 200)
#define QCOLOR4 QColor::fromHsv( 180, 255, 255)
#define QCOLOR5 QColor::fromHsv( 225, 255, 255)
#define QCOLOR6 QColor::fromHsv( 270, 255, 255)
#define QCOLOR7 QColor::fromHsv( 315, 255, 255)

class NORM_NewOctreeV2;

template< typename DataT >
class CT_PointsAttributesScalarTemplated;

class CT_PointsAttributesNormal;

class CT_AbstractUndefinedSizePointCloud;

enum allRelativePosition
{
    LeftBackBot,
    LeftBackCentre,
    LeftBackTop,
    LeftCentreBot,
    LeftCentreCentre,
    LeftCentreTop,
    LeftFrontBot,
    LeftFrontCentre,
    LeftFrontTop,
    CentreBackBot,
    CentreBackCentre,
    CentreBackTop,
    CentreCentreBot,
    CentreCentreCentre,
    CentreCentreTop,
    CentreFrontBot,
    CentreFrontCentre,
    CentreFrontTop,
    RightBackBot,
    RightBackCentre,
    RightBackTop,
    RightCentreBot,
    RightCentreCentre,
    RightCentreTop,
    RightFrontBot,
    RightFrontCentre,
    RightFrontTop
};

enum faceRelativePosition
{
    Left,
    Right,
    Back,
    Front,
    Bot,
    Top
};

enum edgesRelativePosition
{
    LeftBot,
    LeftTop,
    LeftBack,
    LeftFront,
    RightBot,
    RightTop,
    RightBack,
    RightFront,
    BackBot,
    BackTop,
    FrontBot,
    FrontTop,
};

enum vertexRelativePosition
{
    LeftBotBack,
    LeftBotFront,
    LeftTopBack,
    LeftTopFront,
    RightBotBack,
    RightBotFront,
    RightTopBack,
    RightTopFront,
};

class NORM_NewOctreeNodeV2
{
public:
//**********************************************//
//          Constructeurs/Destructeur           //
//**********************************************//
    NORM_NewOctreeNodeV2(CT_Point& centre,
                         float dimension,
                         int depth,
                         NORM_NewOctreeNodeV2* father,
                         NORM_NewOctreeV2* octree,
                         QVector<int>* ptIndices );

    ~NORM_NewOctreeNodeV2();

//**********************************************//
//          Information sur le noeud            //
//**********************************************//
    /*!
     * \brief isLeaf
     *
     * Indique si un noeud est une feuille de l'octree (il n'a pas d'enfants)
     *
     * \return true si le noeud est une feuille, false sinon
     */
    inline bool isLeaf() const
    {
        int cpt = 0;
        foreach( NORM_NewOctreeNodeV2* child, _children )
        {
            if( child != NULL ) cpt++;
        }

        return (cpt == 0);
    }

    /*!
     * \brief nPoints
     *
     * Accede au nombre de points dans le noeud
     *
     * \return le nombre de points contenus dans le noeud
     */
    inline int nPoints()
    {
        return _ptIndices->size();
    }

    inline float getSigma3PerCent()
    {
        assert( _acpWasRan );
        return ( _l3 / (_l1+_l2+_l3) ) * 100; // du coup sigma varie entre o et 30
    }

    inline float getSigma2PerCent()
    {
        assert( _acpWasRan );
        return ( _l2 / (_l1+_l2+_l3) ) * 100; // du coup sigma varie entre o et 30
    }

//**********************************************//
//         Construction de l'octree             //
//**********************************************//
    /*!
     * \brief runACP
     *
     * Lance l'acp sur les points du noeud et remplit les attributs l1, l2, l3 et de v1, v2, v3
     */
    void runACP();

    void debugFlipAcpAndSurfaceRecursive();

    void debugFlipAcpAndSurface();

    /*!
     * \brief runQuadraticFittingRecursive
     *
     * Fitte recursivement une surface quadratique (dans la base de l'acp) a toutes feuilles atteintes a partir du noeud appelant SI l'acp a ete calculees sur ces feuilles
     *
     * \param recursionLevel : parametre de debug pour connaitre le niveau de recursion courant
     */
    void runQuadraticFittingRecursive( int recursionLevel = 0 );

    /*!
     * \brief runQuadraticFitting
     *
     * Si l'acp a ete calculee dans le noeud appelant, alors cette methode fitte une surface quadratique aux points du noeuds dans la base de l'acp
     */
    void runQuadraticFitting();

    float runQuadraticFittingAndGetRmse();

    /*!
     * \brief computeCentroid
     *
     *  Calcule le barycentre des points dans le noeud
     */
    void computeCentroid();

    /*!
     * \brief recursiveSplit
     *
     * Splitte recursivement les noeuds jusqu'a n'obtenir que des noeuds dont la pca est bonne ou des trop petits noeuds
     */
    void recursiveSplit();

    void recursiveSplitWithRmse();

    /*!
     * \brief concurrentCreateChild
     *
     * Utilise QtConcurrent::run pour calculer le split en parallele (cree le fils et relance la recursion)
     *
     * \param childId : id du noeud a creer
     * \param childPointIndices : les indices du noeud a creer
     */
    void concurrentCreateChild(int childId,
                               QVector<int>* childPointIndices);

    /*!
     * \brief sortPointsIntoChildrenNodes
     *
     * Trie les points d'un noeud en huit ensembles de points correspondant aux huit fils du noeud
     *
     * Children follow a predictable pattern to make accesses simple.
     * Here, - means less than 'origin' in that dimension, + means greater than.
     * child:  0 1 2 3 4 5 6 7
     * x:      - - - - + + + +
     * y:      - - + + - - + +
     * z:      - + - + - + - +
     *
     * \param outIndicesOfChildren : vecteur de huit ensembles de points correspondant aux huit fils du noeud
     */
    void sortPointsIntoChildrenNodes(QVector<QVector<int>*>& outIndicesOfChildren);

    /*!
     * \brief getChildIdForPoint
     *
     * Children follow a predictable pattern to make accesses simple.
     * Here, - means less than 'origin' in that dimension, + means greater than.
     * child:  0 1 2 3 4 5 6 7
     * x:      - - - - + + + +
     * y:      - - + + - - + +
     * z:      - + - + - + - +
     *
     * \param point : point dont on veut connaitre le numero du fils dans lequel il va se trouver
     *
     * \return le numero du noeud fils contenant le point demande
     */
    int getChildIdForPoint(const CT_Point& point );

    /*!
     * \brief getChildCenterFromChildNumber
     *
     * Calcule le centre d'un de ses noeuds fils
     *
     * \param child : identifiant du noeud fils
     */
    void getChildCenterFromChildNumber( int child,
                                        CT_Point& outChildCentre );

    /*!
     * \brief setFlagRecursive
     *
     * Met recursivement le booleen flag a une valeur donnee
     *
     * \param flag : valeur du flag
     */
    void setFlagRecursive( bool flag );

    void orientAllNodesRecursive(int nSamplePoints, CT_PointsAttributesScalarTemplated<int>*& debugAttribut, int& debugNumAttribut);

    // la ca commence le parcours des voisins de l'octree tout en orientant
    void orientAsBfsFromCallingNode(int nSamplePoints, CT_PointsAttributesScalarTemplated<int>*& debugAttribut, int& debugNumAttribut);

    void orientNonFlaggedFaceNeighboursAndFlagThemAddThemToQueue(int nSamplePoints,
                                                                 QQueue< NORM_NewOctreeNodeV2* >& f );

    int countPositivePointsFromPointsSampledOnFace( QVector< CT_Point >& sampledPoints );

    void getSignsOfPointsFromPointsSampledOnFace( QVector< CT_Point >& sampledPoints,
                                                  QVector< bool >& outSigns );

//**********************************************//
//                  Affichage                   //
//**********************************************//
    /*!
     * \brief drawRecursive
     *
     * Affiche recursivement les neouds (n'affiche que les feuilles)
     * La couleur est fonction du numero du noeud
     *
     * \param view : vue dans laquelle afficher
     * \param painter : painter pour dessiner
     * \param idChild : numero du noeud courant
     * \param drawBoxes : est ce qu'on doit afficher les noeuds sous forme de boites ?
     * \param drawCentroid : est ce qu'on doit afficher le barycentre des noeuds ?
     * \param drawV3 : est ce qu'on doit afficher v3 (troisieme vecteur propre de l'acp) ?
     * \param sizeV3 : longueur de l'affichage de v3
     */
    void drawRecursive(GraphicsViewInterface &view,
                       PainterInterface &painter,
                       size_t idChild,
                       bool drawBoxes,
                       bool drawCentroid,
                       bool drawV3,
                       double sizeV3) const;

    /*!
     * \brief setIdAttributeRecursive
     *
     * Remplit recursivement un tableau d'entier correspondant au numero de noeud de chaque point de l'octree
     *
     * \param attributs : attribut de sortie (tableau d'entiers qui est modifie)
     * \param currentID : numero du noeud courant
     */
    void setIdAttributeRecursive( CT_PointsAttributesScalarTemplated<int>* attributs,
                                  size_t currentID );

    /*!
     * \brief setSigmaAttributeRecursive
     *
     * Remplit recursivement un tableau de flottants correspondant au sigma du noeud de chaque point de l'octree
     *
     * \param attributs : attribut de sortie (le tableau de flottants qui est modifie)
     */
    void setSigmaAttributeRecursive( CT_PointsAttributesScalarTemplated<float>* attributs );

    void setPcaAttributesRecursive(CT_PointsAttributesScalarTemplated<float>* outCreatedL1Attribute,
                                   CT_PointsAttributesScalarTemplated<float>* outCreatedL2Attribute,
                                   CT_PointsAttributesScalarTemplated<float>* outCreatedL3Attribute,
                                   CT_PointsAttributesScalarTemplated<float>* outCreatedSigmaAttribute,
                                   CT_PointsAttributesScalarTemplated<float>* outCreatedL3L2Attribute);

//**********************************************//
//                  Normales                    //
//**********************************************//
    /*!
     * \brief setNormalsAttributeRecursive
     *
     * Remplit recursivement un tableau de flottants correspondant au sigma du noeud de chaque point de l'octree
     *
     * \param attributs : attribut de sortie (le tableau de normales qui est modifie)
     */
    void setNormalsAttributeRecursive( CT_PointsAttributesNormal* attributs );

    /*!
     * \brief setNormalsAttribute
     *
     * Calcule les normales pour les points du noeud appelant
     *
     * \param attributs : attribut de sortie (le tableau de normales qui est modifie, il fait la taille du nuage de points dans l'octree, non dans le noeud)
     */
    void setNormalsAttribute( CT_PointsAttributesNormal* attributs );

    void computeNormalAtPoint( CT_Point& pInCartBasis,
                               CT_Point& outNormal );

    void computeLocalPoint( CT_Point& pInCartBasis,
                            CT_Point& outPointInPcaBasis );

    void computeGlobalPoint( CT_Point& pInPcaBasis,
                             CT_Point& outPointInCartBasis );

    void computeProjectedPoint( CT_Point& pInCartBasis,
                                CT_Point& outProjectedPoint );

    void setNormalsAttributeWithLinearInterpolationRecursive( CT_PointsAttributesNormal* attributs );

    void setNormalsAttributeWithLinearInterpolation(CT_PointsAttributesNormal* attributs);

    void setNormalsAttributeWithWendlandInterpolationRecursive( CT_PointsAttributesNormal* attributs );

    void setNormalsAttributeWithWendlandInterpolation(CT_PointsAttributesNormal* attributs);

    float getLinearInterpolationWeight(CT_Point& pointInCartBasis,
                                       faceRelativePosition relPos,
                                       float interpolationRadius );

    float getWendlandInterpolationWeight(CT_Point& pointInCartBasis,
                                         faceRelativePosition relPos,
                                         float interpolationRadius );

//**********************************************//
//                  Surfaces                    //
//**********************************************//
    /*!
     * \brief getProjectedPointsRecursive
     *
     * Projette recursivement les points du nuage initial sur les surfaces fitees
     *
     * \param outProjectedPoints : le nuage de points qui contiendra les projetes
     */
    void getProjectedPointsRecursive( CT_AbstractUndefinedSizePointCloud* outProjectedPoints );

    void getProjectedPointsFromFileRecursive( CT_AbstractUndefinedSizePointCloud* outProjectedPoints );

    /*!
     * \brief getProjectedPoints
     *
     * Calcule les projetes des points du noeud sur la surface fitee
     *
     * \param outProjectedPoints : un tableau de points auquel on ajoute les projetes
     */
    void getProjectedPoints( CT_AbstractUndefinedSizePointCloud* outProjectedPoints );

    void getProjectedPointsFromFile( CT_AbstractUndefinedSizePointCloud* outProjectedPoints );

    void getProjectedPointsInAcpBasisFromFile( QVector< CT_Point >& outProjectedPoints );

    void getProjectedPointsWithLinearInterpolationRecursive(CT_AbstractUndefinedSizePointCloud* outProjectedPoints,
                                                            float interpolationRadius);

    void getProjectedPointsWithLinearInterpolation(CT_AbstractUndefinedSizePointCloud* outProjectedPoints,
                                                   float interpolationRadius);

    /*!
     * \brief setDistanceFromPointsToSurfaceRecursive
     *
     * Calcule recursivement la distance d'un point a la surface fitee
     *
     * \param attributs : attribut de sortie (le tableau de distances qui est modifie)
     */
    void setDistanceFromPointsToSurfaceRecursive( CT_PointsAttributesScalarTemplated<float>* attributs );

    /*!
     * \brief setDistanceFromPointsToSurface
     *
     * Calcule la distance des points du noeud a la surface fitee
     *
     * \param attributs : attribut de sortie (le tableau de distances qui est modifie)
     */
    void setDistanceFromPointsToSurface( CT_PointsAttributesScalarTemplated<float>* attributs );

    /*!
     * \brief getDistanceFromPointToSurface
     *
     * Calcule la distance d'un point a la surface fitee
     *
     * \param p : point dont on cherche la distance a la surface
     *
     * \return : la distance verticale entre p et la surface ( point - surface )
     */
    float getDistanceFromPointToSurface( const CT_Point& p );

    float getMeanSignedDistanceFromPointToSurface();

    float getMeanAbsoluteDistanceFromPointToSurface();

    /*!
     * \brief getSignOfPoint
     *
     * Donne le signe du point par rapport a la surface fitee (le point est au dessus ou au dessous?)
     *
     * \param p : point dont on cherche le signe
     *
     * \return : false si le point est en dessous de la surface, true sinon
     */
    bool getSignOfPoint( const CT_Point& p );

    void setCurvaturesRecursive(CT_PointsAttributesScalarTemplated<float>* outGaussianCurvature,
                                CT_PointsAttributesNormal* outDir1,
                                CT_PointsAttributesNormal* outDir2 );

    void setCurvatures(CT_PointsAttributesScalarTemplated<float>* outGaussianCurvature,
                       CT_PointsAttributesNormal* outDir1,
                       CT_PointsAttributesNormal* outDir2 );

    void setGaussianCurvatureRecursive(CT_PointsAttributesScalarTemplated<float>* attributs , float& outMinCurvature, float& outMaxCurvature);

    void setGaussianCurvature(CT_PointsAttributesScalarTemplated<float>* attributs , float& outMinCurvature, float& outMaxCurvature);

    void getCurvatures(QVector< CT_Point >& outDir1, QVector< float >& outK1,
                       QVector< CT_Point >& outDir2, QVector< float >& outK2);

    void getCurvatureAtPoint(CT_Point& pInAcpBasis,
                             CT_Point& normalAtPInAcpBasis,
                             Eigen::Vector2d& outDir1, float& outK1,
                             Eigen::Vector2d& outDir2, float& outK2);

    void getPremiereFormeFondamentale( Eigen::Matrix2d& outputMatrix, CT_Point& pInAcpBasis );

    void getSecondeFormeFondamentale(Eigen::Matrix2d& outputMatrix, CT_Point& normalAtPointInAcpBasis );

    /*!
     * \brief sortEigenValues
     *
     * Sort eigenvalues and correspondingeigenvectors in decresent order of absolute value (i.e. l1 > l2)
     *
     * \param eigenValues : eigenvalues from a pca
     * \param eigenVectors : corresponding eigenvectors (each column of this matrix is n eigenvector)
     */
    void sortEigenValues(Eigen::Vector2cd& eigenValues,
                            Eigen::Matrix2cd& eigenVectors);

//**********************************************//
//      Recherche de points et voisinage        //
//**********************************************//
    /*!
     * \brief getNodeContainingPointRecursive
     *
     * Recherche recursive pour trouver le noeud le plus bas contenant un point
     *
     * La recursion s'arete si le noeud contenant ce point est vide (on renvoie NULL et un level de -1)
     *
     * \param querryPoint : point a chercher
     * \param outputNode : noeud resultat (feuille ou internet) contenant le point
     * \param outputLevel : profondeur du noeud contenant le point
     * \param curLevel : niveau de recursion courant
     * \param level : niveau de profondeur souhaitee pour le noeud (la recursion s'arete a la profondeur level ou avant si une feuille a ete rencontree)
     */
    void getNodeContainingPointRecursive(const CT_Point& querryPoint,
                                         NORM_NewOctreeNodeV2*& outputNode,
                                         int& outputLevel,
                                         int curLevel = 0,
                                         int level = -1 );

    /*!
     * \brief getNeighbour
     *
     * Cherche le noeud voisin dans une direction donnee et a une profondeur donnee.
     * Si une feuille est rencontree avant la recherche s'arete.
     * Si le voisin n'existe pas on renvoie NULL
     * La direction s'indique par les trois booleens direction
     *
     * \param directionx : 0 si on veut a gauche, 1 pour a droite
     * \param directiony : 0 si on veut a gauche, 1 pour a droite
     * \param directionz : 0 si on veut a gauche, 1 pour a droite
     * \param usex : doit on tenir compte de l'indication de direction en x ?
     * \param usey : doit on tenir compte de l'indication de direction en y ?
     * \param usez : doit on tenir compte de l'indication de direction en z ?
     * \param level : niveau maximum souhaite du voisin (peut etre non atteint si une feuille est rencontree avant ou si un noeud n'existe pas)
     * \param outputLevel : niveau du noeud voisin trouve
     *
     * \return : le noeud voisin dans la direction souhaitee ou NULL
     */
    NORM_NewOctreeNodeV2* getNeighbourAtLevel(bool directionx,
                                              bool directiony,
                                              bool directionz,
                                              bool usex,
                                              bool usey,
                                              bool usez,
                                              int level,
                                              int& outputLevel );

    /*!
     * \brief getAllNeighbours
     *
     * Cherche tous les noeuds feuilles voisins du noeud appelant
     *
     * \param outputNeighbours : tableau des noeuds voisins
     */
    void getAllNeighbours(QVector<NORM_NewOctreeNodeV2*>& outputNeighbours );

    /*!
     * \brief getLeavesNeighboursRecursive
     *
     * Cherche recursivement les feuilles de l'octree et calcule leurs voisins
     *
     */
    void getLeavesNeighboursRecursive();

    /*!
     * \brief getFacesNeighbours
     *
     * Cherche tous les noeuds feuilles voisins de face du noeud appelant
     *
     * \param outputNeighbours : tableau des noeuds voisins
     */
    void getFacesNeighbours(QVector<NORM_NewOctreeNodeV2*>& outputNeighbours );

    // Ne recupere que les voisins qui touchent le noeud appelant par des faces paralleles a v3
    // Ce sont les faces qui sont les plus censees couper la surface fitee
    void getFacesNeighboursAroundv3(QVector<NORM_NewOctreeNodeV2*>& outputNeighbours );

    /*!
     * \brief getEdgesNeighbours
     *
     * Cherche tous les noeuds feuilles voisins d'arete du noeud appelant
     *
     * \param outputNeighbours : tableau des noeuds voisins
     */
    void getEdgesNeighbours(QVector<NORM_NewOctreeNodeV2*>& outputNeighbours );

    /*!
     * \brief getVertexNeighbours
     *
     * Cherche tous les noeuds feuilles voisins de sommet du noeud appelant
     *
=     * \param outputNeighbours : tableau des noeuds voisins
     */
    void getVertexNeighbours(QVector<NORM_NewOctreeNodeV2*>& outputNeighbours );

    /*!
     * \brief getNeighboursLeavesAtLevel
     *
     * Cherche tous les noeuds feuille voisins du noeud appelant dans une direction donnee
     *
     * \param directionx : 0 si on veut a gauche, 1 pour a droite
     * \param directiony : 0 si on veut a gauche, 1 pour a droite
     * \param directionz : 0 si on veut a gauche, 1 pour a droite
     * \param usex : doit on tenir compte de l'indication de direction en x ?
     * \param usey : doit on tenir compte de l'indication de direction en y ?
     * \param usez : doit on tenir compte de l'indication de direction en z ?
     *
     * \param outputNeighbours : tableau des noeuds voisins
     */
    void getNeighboursLeavesAtLevel(bool directionx,
                                    bool directiony,
                                    bool directionz,
                                    bool usex,
                                    bool usey,
                                    bool usez,
                                    QVector<NORM_NewOctreeNodeV2*>& outputNeighbours );

    /*!
     * \brief getLeavesFromNodeInDirectionRecursive
     *
     * Parcours recursivement les enfants dans une direction (e.g. "enfants a droite", "enfants a droite en bas" ou "enfants a droite en bas devant")
     * et recupere toutes les feuilles dans cette direction
     *
     * \param directionx : 0 si on veut a gauche, 1 pour a droite
     * \param directiony : 0 si on veut a gauche, 1 pour a droite
     * \param directionz : 0 si on veut a gauche, 1 pour a droite
     * \param usex : doit on tenir compte de l'indication de direction en x ?
     * \param usey : doit on tenir compte de l'indication de direction en y ?
     * \param usez : doit on tenir compte de l'indication de direction en z ?
     * \param outputLeaves : tableau des fils feuilles du noeud dans la direction souhaitee
     *
     * Example :
     * - Chercher les "enfants a droite" : getLeavesFromNodeInDirection( 1, 0, 0,
     *                                                                   1, 0, 0 )
     * - Chercher les "enfants a gauche en haut" : getLeavesFromNodeInDirection( 0, 0, 1,
     *                                                                           1, 0, 1 )
     * - Chercher les "enfants a gauche en bas devant" : getLeavesFromNodeInDirection( 0, 1, 0,
     *                                                                                 1, 1, 1 )
     */
    void getLeavesFromNodeInDirectionRecursive(bool directionx,
                                               bool directiony,
                                               bool directionz,
                                               bool usex,
                                               bool usey,
                                               bool usez,
                                               QVector<NORM_NewOctreeNodeV2*>& outputLeaves);

    /*!
     * \brief getChildrenIdInDirections
     *
     * Recupere les identifiants de tous les enfants correspondant a la combinaison de direction donnee
     *
     * \param directionx : 0 si on veut a gauche, 1 pour a droite
     * \param directiony : 0 si on veut a gauche, 1 pour a droite
     * \param directionz : 0 si on veut a gauche, 1 pour a droite
     * \param usex : doit on tenir compte de l'indication de direction en x ?
     * \param usey : doit on tenir compte de l'indication de direction en y ?
     * \param usez : doit on tenir compte de l'indication de direction en z ?
     * \param outputChildrenId : tableau contenant les indices des enfants correspondant a la combinaison de direction donnee
     *
     * child:  0 1 2 3 4 5 6 7
     * x:      - - - - + + + +
     * y:      - - + + - - + +
     * z:      - + - + - + - +
     *
     * Exemples :
     * - Les "enfants a droite" : 4,5,6,7
     * - Les "enfants a gauche en haut" : 1,3
     * - Les "enfants a gauche en bas devant" : 2
     */
    void getChildrenIdInDirections( bool directionx,
                                    bool directiony,
                                    bool directionz,
                                    bool usex,
                                    bool usey,
                                    bool usez,
                                    QVector<int>& outputChildrenId );

    /*!
     * \brief valueOfIthBit
     *
     * Renvoie la valeur du ieme bit d'un nombre
     *
     * \param num : nombre
     * \param i : indice du bit a chercher
     * \return : true si le ieme bit de num est a 1, false sinon
     */
    inline bool valueOfIthBit(int num, int i)
    {
        return 1 == ( (num >> i) & 1);
    }

    /*!
     * \brief getFaceRelativePosition
     *
     * Retrouve la position relative d'un noeud voisin par face
     *
     * \param node : noeud voisin
     *
     * \return : la position relative du noeud par rapport au noeud appelant
     */
    faceRelativePosition getFaceRelativePosition( NORM_NewOctreeNodeV2* node );

    /*!
     * \brief isNeighbourInRangeOfFace
     *
     * Est ce que le centre d'un noeud est dans le range en (x,y,z dependemment de la face demandee) du cube du noeud appelant
     *
     * \param nei : noeud voisin
     * \param face : axe pour lequel verifier l'appartenance du centre du noeud voisin
     * \return
     */
    bool isNeighbourInRangeOfFace( NORM_NewOctreeNodeV2* nei, int face );

//**********************************************//
//          Orientation des normales            //
//**********************************************//
    /*!
     * \brief samplePointsOnFace
     *
     * Sample des points sur une face du noeud
     * Les points sont samples a intervale regulier sur toute la surface.
     * Il y a au final nPointsOnEdge^2 points samples sur la face
     *
     * \param nPointsOnEdge : nombre de points a sampler sur un cote de la face
     * \param outputSampledPoints : tableau qui contiendra les points samples
     */
    void samplePointsOnFace( faceRelativePosition position,
                             int nPointsOnEdge,
                             QVector< CT_Point >& outputSampledPoints );

    /*!
     * \brief samplePointsOnFace
     *
     * Sample des points sur une face du noeud
     * Les points sont samples a intervale regulier sur toute la surface.
     * Il y a au final nPointsOnEdge^2 points samples sur la face
     *
     * \param axisFace : [0,1,2] pour [x,y,z]. La face x est parallele au plan yOz, la face y parallele au plan xOz et la face z parallele au plan xOy
     * \param sup : booleen indiquant quelle face de l'axe sampler : false pour (left,back,bot) et true pour (right, front, top)
     * \param nPointsOnEdge : nombre de points a sampler sur un cote de la face
     * \param outputSampledPoints : tableau qui contiendra les points samples
     */
    void samplePointsOnFace( int axisFace,
                             bool sup,
                             int nPointsOnEdge,
                             QVector< CT_Point >& outputSampledPoints );

//**********************************************//
//              Acces aux points                //
//**********************************************//
    /*!
     * \brief getPoints
     *
     * Accede a tous les points du noeud
     *
     * \return : un tableau de tous les points du noeud
     */
    CT_AbstractUndefinedSizePointCloud* getPoints();

    /*!
     * \brief addPoints
     *
     * Ajoute tous les points du noeud a un tableau
     *
     * \param outPoints : tableau de points dans lequel ajouter les points du noeud
     */
    void addPoints( CT_AbstractUndefinedSizePointCloud* outPoints );

    /*!
     * \brief getSamplePointsOnFace
     * Sample des points sur une face du noeud
     * Les points sont samples a intervale regulier sur toute la surface.
     * Il y a au final nPointsOnEdge^2 points samples sur la face
     *
     * \param axisFace : [0,1,2] pour [x,y,z]. La face x est parallele au plan yOz, la face y parallele au plan xOz et la face z parallele au plan xOy
     * \param sup : booleen indiquant quelle face de l'axe sampler : false pour (left,back,bot) et true pour (right, front, top)
     * \param nPointsOnEdge : nombre de points a sampler sur un cote de la face
     *
     * \return : un tableau qui contient les points samples
     */
    CT_AbstractUndefinedSizePointCloud* getSamplePointsOnFace( int axisFace,
                                                               bool sup,
                                                               int nPointsOnEdge);

//**********************************************//
//          Point Cloud Compression             //
//**********************************************//
    void saveToFileRecursive( QTextStream& stream );

    void saveToFile( QTextStream& stream );

    void getLocalBbox( Eigen::Vector2f& outBot,
                       Eigen::Vector2f& outTop );

    static NORM_NewOctreeNodeV2* loadRecursive(QTextStream& stream,
                                               CT_Point& centre,
                                               float dimension,
                                               int depth,
                                               NORM_NewOctreeNodeV2* father,
                                               NORM_NewOctreeV2* octree, int& nLinesRead);

    bool containsPoint(CT_Point& p);

//**********************************************//
//                Debug displays                //
//**********************************************//
    void printCentreRecursive();

private :
    CT_Point                        _centre;        /*!< Centre du noeud */
    float                           _dimension;     /*!< Demi-dimension du noeud */
    int                             _depth;         /*!< Profondeur du noeud dans l'octree */
    bool                            _flag;          /*!< Attribut booleen permettant de flager un noeud pour x raisons */

    NORM_NewOctreeNodeV2*           _father;        /*!< Noeud pere */
    QVector<NORM_NewOctreeNodeV2*>  _children;      /*!< Tableau des huit fils */
    NORM_NewOctreeV2*               _octree;        /*!< Octree auquel appartient le noeud (pour avoir acces aux different seuils) */
    QVector<NORM_NewOctreeNodeV2*>* _neighbours;    /*!< Tableau des voisins du noeud */

    QVector<int>*                   _ptIndices;     /*!< Tableau des indices des points dans le noeud (NULL en dehors des feuilles) */

    bool        _surfaceFittingWasRan;              /*!< Indique si le fitting de surface a eu lieu dans ce noeud */
    float       _a;                                 /*!< Coefficient de la quadrique fitee aux points de la cellule : f(x,y) = ax^2 + bxy + cy^2 + dx + ey + f */
    float       _b;                                 /*!< Coefficient de la quadrique fitee aux points de la cellule : f(x,y) = ax^2 + bxy + cy^2 + dx + ey + f */
    float       _c;                                 /*!< Coefficient de la quadrique fitee aux points de la cellule : f(x,y) = ax^2 + bxy + cy^2 + dx + ey + f */
    float       _d;                                 /*!< Coefficient de la quadrique fitee aux points de la cellule : f(x,y) = ax^2 + bxy + cy^2 + dx + ey + f */
    float       _e;                                 /*!< Coefficient de la quadrique fitee aux points de la cellule : f(x,y) = ax^2 + bxy + cy^2 + dx + ey + f */
    float       _f;                                 /*!< Coefficient de la quadrique fitee aux points de la cellule : f(x,y) = ax^2 + bxy + cy^2 + dx + ey + f */
    float       _rmse;                              /*!< Rmse du fitting de quadrique */

    bool        _acpWasRan;                         /*!< Indique si l'acp eu lieu dans ce noeud */
    CT_Point    _centroid;                          /*!< Barycentre des points dans le noeud */
    float       _l1;                                /*!< Premiere valeur propre de l'ACP sur les points du noeud : l1 > l2 > l3 */
    float       _l2;                                /*!< Premiere valeur propre de l'ACP sur les points du noeud : l1 > l2 > l3 */
    float       _l3;                                /*!< Premiere valeur propre de l'ACP sur les points du noeud : l1 > l2 > l3 */

    CT_Point    _v1;                                /*!< Vecteur propre associe a la premiere valeur propre de l'ACP sur les points du noeud */
    CT_Point    _v2;                                /*!< Vecteur propre associe a la seconde valeur propre de l'ACP sur les points du noeud */
    CT_Point    _v3;                                /*!< Vecteur propre associe a la troisieme valeur propre de l'ACP sur les points du noeud */

    Eigen::Vector2f     _surfBot;
    Eigen::Vector2f     _surfTop;
    int                 _nPointsForReconstruction;

    const static QVector<QColor>    _qColorChild;   /*!< Color map pour les noeuds en fonction de leur identifiant (entre 0 et 7 inclu) */
    const static CT_PointAccessor   _pAccessor;
};

#endif // NORM_NEWOCTREENODE_H
