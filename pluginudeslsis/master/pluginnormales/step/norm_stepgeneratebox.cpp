#include "norm_stepgeneratebox.h"

#include "ct_point.h"
#include "../itemdrawable/norm_box.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

// Alias for indexing models
#define DEFout_ResultBox "ResultBox"
#define DEFout_GroupBox "GroupBox"
#define DEFout_ItemBox "ItemBox"

// Constructor : initialization of parameters
NORM_StepGenerateBox::NORM_StepGenerateBox(CT_StepInitializeData &dataInit) : CT_AbstractStepCanBeAddedFirst(dataInit)
{
    _botx = -1;
    _boty = -1;
    _botz = -1;
    _topx = 1;
    _topy = 1;
    _topz = 1;
}

// Step description (tooltip of contextual menu)
QString NORM_StepGenerateBox::getStepDescription() const
{
    return tr("Generates a box");
}

// Step detailled description
QString NORM_StepGenerateBox::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_StepGenerateBox::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStepCanBeAddedFirst::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_StepGenerateBox::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_StepGenerateBox(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_StepGenerateBox::createInResultModelListProtected()
{
    // No in result is needed
    setNotNeedInputResult();
}

// Creation and affiliation of OUT models
void NORM_StepGenerateBox::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_ResultBox = createNewOutResultModel(DEFout_ResultBox, tr("Resul of the generation"));
    res_ResultBox->setRootGroup(DEFout_GroupBox, new CT_StandardItemGroup(), tr("Group"));
    res_ResultBox->addItemModel(DEFout_GroupBox, DEFout_ItemBox, new NORM_Box(), tr("Generated box"));
}

// Semi-automatic creation of step parameters DialogBox
void NORM_StepGenerateBox::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("botx", "", -9999, 9999, 4, _botx, 1);
    configDialog->addDouble("boty", "", -9999, 9999, 4, _boty, 1);
    configDialog->addDouble("botz", "", -9999, 9999, 4, _botz, 1);
    configDialog->addDouble("topx", "", -9999, 9999, 4, _topx, 1);
    configDialog->addDouble("topy", "", -9999, 9999, 4, _topy, 1);
    configDialog->addDouble("topz", "", -9999, 9999, 4, _topz, 1);
}

void NORM_StepGenerateBox::compute()
{
    // DONT'T FORGET TO ADD THIS STEP TO THE PLUGINMANAGER !!!!!

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_ResultBox = outResultList.at(0);

    // IN results browsing


    // COPIED results browsing


    // OUT results creation (move it to the appropried place in the code)
    CT_StandardItemGroup* grp_GroupBox= new CT_StandardItemGroup(DEFout_GroupBox, res_ResultBox);
    res_ResultBox->addGroup(grp_GroupBox);
    
    CT_Point center = createCtPoint( (_topx + _botx)/2.0, (_topy + _boty)/2.0, (_topz + _botz)/2.0 );
    CT_Point fooDirection;
    CT_Point widthDirection = createCtPoint( (_topx - _botx), (_topy - _boty), (_topz - _botz) );
    CT_BoxData* boxData = new CT_BoxData( center, fooDirection, widthDirection, (_topx - _botx), (_topy - _boty), (_topz - _botz) );
    NORM_Box* item_ItemBox = new NORM_Box(DEFout_ItemBox, res_ResultBox, boxData );
    grp_GroupBox->addItemDrawable(item_ItemBox);
}
