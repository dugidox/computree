#include "norm_pairint.h"

#include <assert.h>

NORM_PairInt::NORM_PairInt()
    : QPair( 0, 0 )
{
}

NORM_PairInt::NORM_PairInt(int firstInt, int secondInt)
    : QPair( firstInt, secondInt )
{
}
