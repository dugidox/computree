#include "norm_pluginmanager.h"
#include "ct_stepseparator.h"
#include "ct_steploadfileseparator.h"
#include "ct_stepcanbeaddedfirstseparator.h"
#include "ct_actions/ct_actionsseparator.h"
#include "ct_exporter/ct_standardexporterseparator.h"
#include "ct_reader/ct_standardreaderseparator.h"
#include "ct_actions/abstract/ct_abstractaction.h"

// Inclure ici les entetes des classes definissant des Ã©tapes/actions/exporters ou readers
#include "step/norm_stepoctreenodefromquerrypoint.h"
#include "step/norm_stepneighbournodesfromquerrypoint.h"
#include "step/norm_stepsphericalneighbourhood.h"
#include "step/norm_stepapproximatesphericalneighbourhood.h"

#include "step/norm_stepcreatestdoctree.h"
#include "step/norm_stepcreateoctreepca.h"

#include "step/norm_stepcomputesigma.h"
#include "step/norm_stepfilterpca.h"
#include "step/norm_stepfilteronsigmaoctree.h"
#include "step/norm_stepfilteronsigmaoctreeandestimatenormals.h"

#include "step/norm_stepcomputenormalspca.h"

#include "step/norm_stepgeneratesphere.h"
#include "step/norm_stepgeneratebox.h"

#include "exporter/icro_ascnormexporter.h"

#include "step/norm_stepcreatenewoctree.h"
#include "step/norm_stepgetnewoctreenodecontainingpoint.h"
#include "step/norm_stepgetnewoctreeneighbournodesfromquerrypoint.h"
#include "step/norm_stepsegmentfromnewoctree.h"

#include "step/norm_newstepcreateoctree.h"
#include "step/norm_newstepfittsurfaces.h"
#include "step/norm_newstepcomputenormalswithoutinterpolation.h"
#include "step/norm_newstepcomputenormalswithlinearinterpolation.h"
#include "step/norm_newstepcomputenormalswithwendlandinterpolation.h"
#include "step/norm_newstepgetprojectedpoints.h"
#include "step/norm_newstepgetprojectedpointsfromfile.h"
#include "step/norm_newstepgetprojectedpointswithlinearinterpolation.h"
#include "step/norm_newstepcomparenormals.h"
#include "step/norm_newstepcomparesetofnormals.h"
#include "step/norm_newstepquerrynodefrompoint.h"
#include "step/norm_newstepcomputeleavesneighbours.h"
#include "step/norm_newstepsamplepointonnodeface.h"
#include "step/norm_newsteporientsurfaces.h"

#include "exporter/norm_octreeexporter.h"
#include "step/norm_newsteploadoctree.h"

NORM_PluginManager::NORM_PluginManager() : CT_AbstractStepPlugin()
{
}

NORM_PluginManager::~NORM_PluginManager()
{
}

bool NORM_PluginManager::loadGenericsStep()
{
    CT_StepSeparator *sep = addNewSeparator(new CT_StepSeparator());
    // Ajouter ici les etapes
//    sep->addStep(new NORM_StepCreateStdOctree(*createNewStepInitializeData(NULL)));
//    sep->addStep(new NORM_StepCreateOctreePCA(*createNewStepInitializeData(NULL)));
//    sep = addNewSeparator(new CT_StepSeparator());
//    sep->addStep(new NORM_StepComputeSigma(*createNewStepInitializeData(NULL)));
//    sep->addStep(new NORM_StepFilterPCA(*createNewStepInitializeData(NULL)));
//    sep->addStep(new NORM_StepFilterOnSigmaOctree(*createNewStepInitializeData(NULL)));
//    sep->addStep(new NORM_StepFilterOnSigmaOctreeAndEstimateNormals(*createNewStepInitializeData(NULL)));
//    sep = addNewSeparator(new CT_StepSeparator());
//    sep->addStep(new NORM_StepComputeNormalsPCA(*createNewStepInitializeData(NULL)));
//    sep = addNewSeparator(new CT_StepSeparator());
//    sep->addStep(new NORM_StepOctreeNodeFromQuerryPoint(*createNewStepInitializeData(NULL)));
//    sep->addStep(new NORM_StepNeighbourNodesFromQuerryPoint(*createNewStepInitializeData(NULL)));
//    sep->addStep(new NORM_StepSphericalNeighbourhood(*createNewStepInitializeData(NULL)));
//    sep->addStep(new NORM_StepApproximateSphericalNeighbourhood(*createNewStepInitializeData(NULL)));

//    sep = addNewSeparator(new CT_StepSeparator());
//    sep->addStep(new NORM_StepCreateNewOctree(*createNewStepInitializeData(NULL)));
//    sep->addStep(new NORM_StepGetNewOctreeNodeContainingPoint(*createNewStepInitializeData(NULL)));
//    sep->addStep(new NORM_StepGetNewOctreeNeighbourNodesFromQuerryPoint(*createNewStepInitializeData(NULL)));
//    sep->addStep(new NORM_StepSegmentFromNewOctree(*createNewStepInitializeData(NULL)));

    sep->addStep(new NORM_NewStepLoadOctree(*createNewStepInitializeData(NULL)));

    sep->addStep(new NORM_NewStepCreateOctree(*createNewStepInitializeData(NULL)));
    sep->addStep(new NORM_NewStepFittSurfaces(*createNewStepInitializeData(NULL)));

    sep->addStep(new NORM_NewStepComputeNormalsWithoutInterpolation(*createNewStepInitializeData(NULL)));
    sep->addStep(new NORM_NewStepComputeNormalsWithLinearInterpolation(*createNewStepInitializeData(NULL)));
    sep->addStep(new NORM_NewStepComputeNormalsWithWendlandInterpolation(*createNewStepInitializeData(NULL)));

    sep->addStep(new NORM_NewStepGetProjectedPoints(*createNewStepInitializeData(NULL)));
    sep->addStep(new NORM_NewStepGetProjectedPointsFromFile(*createNewStepInitializeData(NULL)));
    sep->addStep(new NORM_NewStepGetProjectedPointsWithLinearInterpolation(*createNewStepInitializeData(NULL)));

    sep->addStep(new NORM_NewStepCompareNormals(*createNewStepInitializeData(NULL)));
    sep->addStep(new NORM_NewStepCompareSetOfNormals(*createNewStepInitializeData(NULL)));

    sep->addStep(new NORM_NewStepQuerryNodeFromPoint(*createNewStepInitializeData(NULL)));
    sep->addStep(new NORM_NewStepComputeLeavesNeighbours(*createNewStepInitializeData(NULL)));
    sep->addStep(new NORM_NewStepSamplePointOnNodeFace(*createNewStepInitializeData(NULL)));
    sep->addStep(new NORM_NewStepOrientSurfaces(*createNewStepInitializeData(NULL)));

    return true;
}

bool NORM_PluginManager::loadOpenFileStep()
{
    clearOpenFileStep();

    CT_StepLoadFileSeparator *sep = addNewSeparator(new CT_StepLoadFileSeparator(("TYPE DE FICHIER")));
    //sep->addStep(new NomDeLEtape(*createNewStepInitializeData(NULL)));

    return true;
}

bool NORM_PluginManager::loadCanBeAddedFirstStep()
{
    clearCanBeAddedFirstStep();

    CT_StepCanBeAddedFirstSeparator *sep = addNewSeparator(new CT_StepCanBeAddedFirstSeparator());
    sep->addStep(new NORM_StepGenerateSphere(*createNewStepInitializeData(NULL)));
    sep->addStep(new NORM_StepGenerateBox(*createNewStepInitializeData(NULL)));

    return true;
}

bool NORM_PluginManager::loadActions()
{
    clearActions();

    CT_ActionsSeparator *sep = addNewSeparator(new CT_ActionsSeparator(CT_AbstractAction::TYPE_SELECTION));
    //sep->addAction(new NomDeLAction());

    return true;
}

bool NORM_PluginManager::loadExporters()
{
    clearExporters();

    CT_StandardExporterSeparator *sep = addNewSeparator(new CT_StandardExporterSeparator("TYPE DE FICHIER"));
    //sep->addNORMporter(new NomDeLNORMporter());
    sep->addExporter(new ICRO_ASCNormExporter());
    sep->addExporter(new NORM_OctreeExporter());

    return true;
}

bool NORM_PluginManager::loadReaders()
{
    clearReaders();

    CT_StandardReaderSeparator *sep = addNewSeparator(new CT_StandardReaderSeparator("TYPE DE FICHIER"));
    //sep->addReader(new NomDuReader());

    return true;
}

