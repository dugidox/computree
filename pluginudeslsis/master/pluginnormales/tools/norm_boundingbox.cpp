/************************************************************************************
* Filename :  norm_boundingbox.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "norm_boundingbox.h"

void NORM_Tools::NORM_BBox::updateBoundingBox(float &xmin, float &ymin, float &zmin,
                                              float &xmax, float &ymax, float &zmax,
                                              const CT_Point &p)
{
    if( p(0) < xmin )
    {
        xmin = p(0);
    }

    if( p(0) > xmax )
    {
        xmax = p(0);
    }

    if( p(1) < ymin )
    {
        ymin = p(1);
    }

    if( p(1) > ymax )
    {
        ymax = p(1);
    }

    if( p(2) < zmin )
    {
        zmin = p(2);
    }

    if( p(2) > zmax )
    {
        zmax = p(2);
    }
}
