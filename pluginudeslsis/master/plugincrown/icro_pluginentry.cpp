#include "icro_pluginentry.h"
#include "icro_pluginmanager.h"

ICRO_PluginEntry::ICRO_PluginEntry()
{
    _pluginManager = new ICRO_PluginManager();
}

ICRO_PluginEntry::~ICRO_PluginEntry()
{
    delete _pluginManager;
}

QString ICRO_PluginEntry::getVersion() const
{
    return "1.0";
}

CT_AbstractStepPlugin* ICRO_PluginEntry::getPlugin() const
{
    return _pluginManager;
}

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    Q_EXPORT_PLUGIN2(plug_isolatecrown, ICRO_PluginEntry)
#endif
