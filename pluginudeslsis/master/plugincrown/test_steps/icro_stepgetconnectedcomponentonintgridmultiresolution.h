#ifndef ICRO_STEPGETCONNECTEDCOMPONENTONINTGRIDMULTIRESOLUTION_H
#define ICRO_STEPGETCONNECTEDCOMPONENTONINTGRIDMULTIRESOLUTION_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_grid3d.h"
#include "ct_itemdrawable/ct_circle.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "../tools/connectedcomponent.h"

/*!
 * \class ICRO_StepGetConnectedComponentOnIntGridMultiResolution
 * \ingroup Steps_ICRO
 * \brief <b>Segmente une grille 3D en composantes connexes.</b>
 *
 * No detailled description for this step
 *
 * \param _minValue
 *
 */

class ICRO_StepGetConnectedComponentOnIntGridMultiResolution: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    ICRO_StepGetConnectedComponentOnIntGridMultiResolution(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     *
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

    void labelSuperConnectedComponents(CT_Grid3D<int>* inputGrid,
                                       CT_Grid3D<int>* outLabeledGrid,
                                       double minValue,
                                       int minLabelSize,
                                       int resolution,
                                       QVector<ConnectedComponent *>& outConnectedComponents);

    void labelSuperConnectedComponentFromSeed(CT_Grid3D<int> *inputGrid,
                                              CT_Grid3D<int> *outLabeledGrid,
                                              double minValue,
                                              int startx,
                                              int starty,
                                              int startz,
                                              int label,
                                              int resolution,
                                              ConnectedComponent* outSuperComponent);

    void markSuperComponent( ConnectedComponent* superComponent,
                             CT_Grid3D<int>* grid,
                             int label );

    bool superPixelContainsValuablePixel(Eigen::Vector3i& superPixel,
                                         CT_Grid3D<int>* hitGrid,
                                         CT_Grid3D<int> *labelGrid,
                                         int minValue,
                                         int resolution);

    void addPixelsToSuperConnectedComponent(SuperPixel &superPixel,
                                            ConnectedComponent* outSuperComponent);

    QVector<CT_CircleData*> getCirclesFromFile(QString dir, QString fileName);

    QVector<CT_CircleData*> getCirclesFromFile2(QString dir, QString fileName);

    bool circleIntersect2D( QVector< CT_CircleData* >& circles, CT_CircleData* c );

    bool circleIntersect2D( CT_CircleData* c1, CT_CircleData* c2 );

    ConnectedComponent* getComponentFromLabel(QVector< ConnectedComponent* >& components,
                                              int label);

    void updateComponentsTypeAndLabelTypeGrid(QVector< ConnectedComponent* >& components,
                                              CT_Grid3D<int>* outTypeGrid,
                                              int minComponentSize);

    void getComponentsType(CT_Grid3D<int> *labeledGrid,
                           QVector< CT_CircleData* >& circles,
                           QVector< ConnectedComponent* >& components,
                           int minLabelSize);

    void labelComponentInGridFromPixels( ConnectedComponent* component,
                                         CT_Grid3D<int>* grid,
                                         int label );

    void splitComplexComponent(ConnectedComponent* component,
                               int initialResolution,
                               CT_Grid3D<int>* valueGrid,
                               int minValue,
                               CT_Grid3D<int>* labelGrid,
                               CT_Grid3D<int>* typeGrid,
                               int minLabelSize,
                               QVector<ConnectedComponent *> &outComponents);

    void splitComponentUntilSimplexes(ConnectedComponent* component,
                                      int initialResolution,
                                      CT_Grid3D<int>* valueGrid,
                                      int minValue,
                                      CT_Grid3D<int>* labelGrid,
                                      CT_Grid3D<int>* typeGrid,
                                      int minLabelSize,
                                      QVector<ConnectedComponent *> &outComponents);

    void countComponentsByType(QVector<ConnectedComponent *>& components,
                                int& outNTotal,
                                int &outNNA,
                                int& outNTooSmall,
                                int& outNBigEnough,
                                int& outNEmpty,
                                int& outNSimplex,
                                int& outNComplex );

    void printComponentsResolution( QVector<ConnectedComponent *>& components );

    void splitSceneIntoComponents(const CT_Scene* inputScene,
                                  CT_Grid3D<int> *typeGrid,
                                  CT_PointCloudIndexVector* outTooSmall,
                                  CT_PointCloudIndexVector* outEmpty,
                                  CT_PointCloudIndexVector* outSimplex,
                                  CT_PointCloudIndexVector* outComplex);

    void aggregEmptyComponents(int maxDilatation,
                               CT_Grid3D<int> *valueImage,
                               CT_Grid3D<int> *labelImage,
                               CT_Grid3D<int> *typeImage,
                               QVector<ConnectedComponent *> &components);

    void aggregEmptyComponents2(int maxDilatation,
                                CT_Grid3D<int> *valueImage,
                                CT_Grid3D<int> *labelImage,
                                CT_Grid3D<int> *typeImage,
                                QVector<ConnectedComponent *> &components);

    void aggregEmptyComponents3(int maxDilatation,
                                CT_Grid3D<int> *valueImage,
                                CT_Grid3D<int> *labelImage,
                                CT_Grid3D<int> *typeImage,
                                QVector<ConnectedComponent *> &components);

    void aggregEmptyComponentsSingleDilatation(CT_Grid3D<int> *valueImage,
                                               CT_Grid3D<int> *labelImage,
                                               CT_Grid3D<int> *typeImage,
                                               QVector<ConnectedComponent *> &components);

    void aggregEmptyComponentsSingleDilatation2(CT_Grid3D<int> *valueImage,
                                                CT_Grid3D<int> *labelImage,
                                                CT_Grid3D<int> *typeImage,
                                                QVector<ConnectedComponent *> &components);

    void aggregEmptyComponentsSingleDilatation3(CT_Grid3D<int> *valueImage,
                                                CT_Grid3D<int> *labelImage,
                                                CT_Grid3D<int> *typeImage,
                                                CT_Grid3D<bool> *markImage,
                                                QVector<ConnectedComponent *> &components);

    void markPixelsInGrid( QVector<Pixel>& pixels,
                           CT_Grid3D<int>* grid,
                           int value );

    void getExternalPixelsOfConnectedComponent (CT_Grid3D<int>* labelGrid,
                                                CT_Grid3D<bool> *markGrid,
                                                ConnectedComponent* component,
                                                QVector< Pixel >& outPixels );

    int countNonAggregatedPixels(QVector<ConnectedComponent*>& components, bool countComplex = false);

    void getSimplexNonAgregated(QVector<ConnectedComponent*>& allComponents ,
                                QVector<ConnectedComponent *> &outSimplexNonAggregatedComponents);

    void splitSceneIntoComponentsFomNegativeLabels(const CT_Scene* inputScene,
                                                   CT_Grid3D<int> *labelGrid,
                                                   QVector< CT_PointCloudIndexVector* > outScenes);

private:

    // Step parameters
    double      _minValue;    /*!<  */
    int         _minPixelsInComponent;
    int         _resolution;
    int         _nStepsAggreg;
};

#endif // ICRO_STEPGETCONNECTEDCOMPONENTONINTGRIDMULTIRESOLUTION_H
