#ifndef LSIS_FASTFILTERVISITOR_HPP
#define LSIS_FASTFILTERVISITOR_HPP

#include "lsis_fastfiltervisitor.h"

template < typename DataT >
LSIS_FastFilterVisitor<DataT>::LSIS_FastFilterVisitor(CT_Grid4D<DataT> *grid) :
    LSIS_AbstractVisitorGrid4D<DataT>( grid ),
    _sumOfVisitedVotes(0)
{
}

template < typename DataT >
LSIS_FastFilterVisitor<DataT>::~LSIS_FastFilterVisitor()
{
}

template < typename DataT >
void LSIS_FastFilterVisitor<DataT>::visit(size_t levw, size_t levx, size_t levy, size_t levz, LSIS_Beam4D const * beam)
{
    Q_UNUSED(beam);
    _sumOfVisitedVotes += LSIS_AbstractVisitorGrid4D<DataT>::_grid->value(levw, levx, levy, levz);
}

template < typename DataT >
int LSIS_FastFilterVisitor<DataT>::sumOfVisitedVotes() const
{
    return _sumOfVisitedVotes;
}

template < typename DataT >
void LSIS_FastFilterVisitor<DataT>::setSumOfVisitedVotes(int sumOfVisitedVotes)
{
    _sumOfVisitedVotes = sumOfVisitedVotes;
}


#endif // LSIS_FASTFILTERVISITOR_HPP
