#include "lsis_stepprelocalisationtroncs.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_pointsattributesnormal.h"
#include "ct_itemdrawable/ct_circle.h"

//Tools dependencies
#include "ct_point.h"
#include "../pixel/lsis_pixel4ddecrescentvaluesorter.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "streamoverload/streamoverload.h"

// Alias for indexing models
#define DEFin_rsltNormalCloud "rsltNormalCloud"
#define DEFin_grpNormalCloud "grpNormalCloud"
#define DEFin_itmNormalCloud "itmNormalCloud"

#define DEFout_rsltCircles "rsltCircles"
#define DEFout_grpGrpCircles "grpGrpCircles"
#define DEFout_grpCircles "grpCircles"
#define DEFout_itmCircle "itmCircle"


// Constructor : initialization of parameters
LSIS_StepPreLocalisationTroncs::LSIS_StepPreLocalisationTroncs(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _rmin = 0.05;
    _rmax = 0.2;

    _resw = 0.01;
    _resx = 0.03;
    _resy = 0.03;

    _seuilMode = false;
    _otsuCoeff = 0.5;
    _ratioCoeff = 0.5;
    _nBinsInHist = 20;

    _neighbourhoodSizeForLocalMaxima = 1;

    _saveMode = false;
    _dirPath = ".";
    _fileName = "output-circles.txt";

    setDebuggable(true);
}

// Step description (tooltip of contextual menu)
QString LSIS_StepPreLocalisationTroncs::getStepDescription() const
{
    return tr("Fait une transformee de Hough avec une seule case en Z (s'applique sur une tranche uniquement) On prend les maximas locaux uniquement (pas de contours actifs)");
}

// Step detailled description
QString LSIS_StepPreLocalisationTroncs::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString LSIS_StepPreLocalisationTroncs::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepPreLocalisationTroncs::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepPreLocalisationTroncs(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepPreLocalisationTroncs::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_rsltNormalCloud = createNewInResultModel(DEFin_rsltNormalCloud, tr("Result Normal Cloud"));
    resIn_rsltNormalCloud->setRootGroup(DEFin_grpNormalCloud, tr("Group"), tr(""), CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    resIn_rsltNormalCloud->addItemModel(DEFin_grpNormalCloud, DEFin_itmNormalCloud, CT_PointsAttributesNormal::staticGetType(), tr("Normal Cloud"));

}

// Creation and affiliation of OUT models
void LSIS_StepPreLocalisationTroncs::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_rsltCircles = createNewOutResultModel(DEFout_rsltCircles, tr("Result Position"));
    res_rsltCircles->setRootGroup(DEFout_grpGrpCircles, new CT_StandardItemGroup(), tr("Group"));
    res_rsltCircles->addGroupModel(DEFout_grpGrpCircles, DEFout_grpCircles, new CT_StandardItemGroup(), tr("Group"));
    res_rsltCircles->addItemModel(DEFout_grpCircles, DEFout_itmCircle, new CT_Circle(), tr("Circles (stems location)"));

}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepPreLocalisationTroncs::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("rMin", "", 0.0001, 9999, 4, _rmin, 1);
    configDialog->addDouble("rMax", "", 0.0001, 9999, 4, _rmax, 1);
    configDialog->addDouble("ResR", "", 0.0001, 9999, 4, _resw, 1);
    configDialog->addDouble("ResX", "", 0.0001, 9999, 4, _resx, 1);
    configDialog->addDouble("ResY", "", 0.0001, 9999, 4, _resy, 1);
    configDialog->addInt("NeiSizeForMaxima","",1,500,_neighbourhoodSizeForLocalMaxima);
    configDialog->addBool("Seuillage","","",_seuilMode);
    configDialog->addDouble("CoeffOtsu", "", 0.0001, 1000, 4, _otsuCoeff, 1 );
    configDialog->addDouble("CoeffRatio", "", 0.0001, 1000, 4, _ratioCoeff, 1 );
    configDialog->addInt("nBinsInHist","",1,500,_nBinsInHist);
    configDialog->addBool("Save","","",_saveMode);
    configDialog->addString("DirPath","",_dirPath);
    configDialog->addString("FileName","",_fileName);
}

void LSIS_StepPreLocalisationTroncs::compute()
{
    // Recupere le resultat de sortie que l'on va remplir avec le(s) cercle(s)
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    _rsltCircles = outResultList.at(0);
    _grpGrpCircles = new CT_StandardItemGroup( DEFout_grpGrpCircles, _rsltCircles );
    _rsltCircles->addGroup(_grpGrpCircles);

    // Recupere les resultats d'entree
    QVector< CT_PointsAttributesNormal* > normalClouds;
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_rsltNormalCloud = inResultList.at(0);
    CT_ResultGroupIterator itIn_grpNormals(resIn_rsltNormalCloud, this, DEFin_grpNormalCloud);
    while (itIn_grpNormals.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_grpNormals = (CT_AbstractItemGroup*) itIn_grpNormals.next();

        CT_PointsAttributesNormal* itemIn_itmNormals = (CT_PointsAttributesNormal*)grpIn_grpNormals->firstItemByINModelName(this, DEFin_itmNormalCloud );
        if (itemIn_itmNormals != NULL)
        {
            normalClouds.push_back( itemIn_itmNormals );
        }
    }

    // Pour chaque nuage de normales on lance le traitement
    int nNormalClouds = normalClouds.size();
    for( int i = 0 ; i < nNormalClouds ; i++ )
    {
        if( _saveMode )
        {
            computeAndSave( normalClouds.at(i), _dirPath, _fileName );
        }

        else
        {
            compute( normalClouds.at(i) );
        }
    }
}

void LSIS_StepPreLocalisationTroncs::compute(const CT_PointsAttributesNormal *normalCloud)
{
    float minx = normalCloud->minX();
    float miny = normalCloud->minY();
    float minz = normalCloud->minZ();
    float maxx = normalCloud->maxX();
    float maxy = normalCloud->maxY();
    float maxz = normalCloud->maxZ();
    float resz = (maxz - minz) * 2;

    // On cree un espace de Hough a partir de la bounding box du nuage de points
    LSIS_HoughSpace4D* hs = new LSIS_HoughSpace4D( NULL, NULL,
                                                   _rmin,
                                                   minx,
                                                   miny,
                                                   minz,
                                                   _rmax,
                                                   maxx,
                                                   maxy,
                                                   maxz,
                                                   _resw,
                                                   _resx,
                                                   _resy,
                                                   resz,
                                                   true,
                                                   normalCloud,
                                                   this );

    _debugHoughSpace = hs;
    waitForAckIfInDebugMode();

    qDebug() << "Espace de Hough " << (*hs);
    hs->threshByValue( 10 );
    waitForAckIfInDebugMode();

    // Seuillage optionnel
    if( _seuilMode )
    {
        hs->threshWithRatioAndOtsu( _ratioCoeff, _otsuCoeff, _nBinsInHist);
        waitForAckIfInDebugMode();
    }

    // Cree une image de repulsion
    CT_Grid4D_Sparse<bool> repulseImage( NULL, NULL,
                                  hs->minW(), hs->minX(), hs->minY(), hs->minZ(),
                                  hs->maxW(), hs->maxX(), hs->maxY(), hs->maxZ(),
                                  hs->wres(), hs->xres(), hs->yres(), hs->zres(),
                                  false, false);

    // On recupere les maximas de l'espace de Hough
    QVector< LSIS_Pixel4D >* maximas= hs->getLocalMaximas( 5, _neighbourhoodSizeForLocalMaxima );
    int nMaximas = maximas->size();

    // On les trie par ordre decroissant
    maximas = sortPixelsByValueDecrescentOrder( maximas, hs );

    // On recupere l'ensemble des maximas locaux qui ne s'intersectent pas
    LSIS_Pixel4D curPixel;
    CT_Point orientation = createCtPoint(0,0,1);
    for( int i = 0 ; i < nMaximas ; i++ )
    {
        curPixel = maximas->at(i);

        if( curPixel.valueIn( repulseImage ) == false )
        {
            // On applique la repulsion
            markRepulsePixel( curPixel, repulseImage, 10 );

            // Et on ajoute le cercle dans la sortie d'etape
            CT_StandardItemGroup* grpCircles= new CT_StandardItemGroup(DEFout_grpCircles, _rsltCircles);
            _grpGrpCircles->addGroup(grpCircles);

            CT_Point center = createCtPoint( curPixel.toCartesianX( repulseImage ),
                                             curPixel.toCartesianY( repulseImage ),
                                             curPixel.toCartesianZ( repulseImage ) );
            float radius = curPixel.toCartesianW( repulseImage );
            CT_CircleData* circleData_item_itmCircle = new CT_CircleData( center, orientation, radius);
            CT_Circle* item_itmCircle = new CT_Circle(DEFout_itmCircle, _rsltCircles, circleData_item_itmCircle);

            grpCircles->addItemDrawable(item_itmCircle);
        }
    }

    delete maximas;
    delete hs;
}

void LSIS_StepPreLocalisationTroncs::computeAndSave(const CT_PointsAttributesNormal *normalCloud, QString dirPath, QString fileName)
{
    // Cree le dossier au besoin
    QDir dir( dirPath );
    if( !dir.exists() )
    {
        dir.mkdir(".");
    }

    QFile file( dirPath + "/" + fileName );
    if( !file.open( QIODevice::WriteOnly ) )
    {
        qDebug() << "Impossible d'ecrire le fichier " << dirPath + "/" + fileName;
        exit(0);
    }
    QTextStream stream( &file );

    float minx = normalCloud->minX();
    float miny = normalCloud->minY();
    float minz = normalCloud->minZ();
    float maxx = normalCloud->maxX();
    float maxy = normalCloud->maxY();
    float maxz = normalCloud->maxZ();
    float resz = (maxz - minz) * 2;

    // On cree un espace de Hough a partir de la bounding box du nuage de points
    LSIS_HoughSpace4D* hs = new LSIS_HoughSpace4D( NULL, NULL,
                                                   _rmin,
                                                   minx,
                                                   miny,
                                                   minz,
                                                   _rmax,
                                                   maxx,
                                                   maxy,
                                                   maxz,
                                                   _resw,
                                                   _resx,
                                                   _resy,
                                                   resz,
                                                   true,
                                                   normalCloud,
                                                   this );

    _debugHoughSpace = hs;
    waitForAckIfInDebugMode();

    qDebug() << "Espace de Hough " << (*hs);
    hs->threshByValue( 10 );
    waitForAckIfInDebugMode();

    // Seuillage optionnel
    if( _seuilMode )
    {
        hs->threshWithRatioAndOtsu( _ratioCoeff, _otsuCoeff, _nBinsInHist);
        waitForAckIfInDebugMode();
    }

    // Cree une image de repulsion
    CT_Grid4D_Sparse<bool> repulseImage( NULL, NULL,
                                  hs->minW(), hs->minX(), hs->minY(), hs->minZ(),
                                  hs->maxW(), hs->maxX(), hs->maxY(), hs->maxZ(),
                                  hs->wres(), hs->xres(), hs->yres(), hs->zres(),
                                  false, false);

    // On recupere les maximas de l'espace de Hough
    QVector< LSIS_Pixel4D >* maximas= hs->getLocalMaximas( 5, _neighbourhoodSizeForLocalMaxima );
    int nMaximas = maximas->size();

    // On les trie par ordre decroissant
    maximas = sortPixelsByValueDecrescentOrder( maximas, hs );

    // On recupere l'ensemble des maximas locaux qui ne s'intersectent pas
    LSIS_Pixel4D curPixel;
    int nCircles = 0;
    for( int i = 0 ; i < nMaximas ; i++ )
    {
        curPixel = maximas->at(i);

        if( curPixel.valueIn( repulseImage ) == false )
        {
            // On applique la repulsion
            markRepulsePixel( curPixel, repulseImage, 3 );

            if( nCircles != 0 )
            {
                stream << "\n";
            }

            stream << nCircles << " "
                   << curPixel.toCartesianW( repulseImage ) << " "
                   << curPixel.toCartesianX( repulseImage ) << " "
                   << curPixel.toCartesianY( repulseImage ) << " "
                   << curPixel.toCartesianZ( repulseImage );

            nCircles++;
        }
    }

    delete maximas;
    delete hs;

    file.close();
}

void LSIS_StepPreLocalisationTroncs::markRepulsePixel( const LSIS_Pixel4D &pixel, CT_Grid4D_Sparse<bool>& repulseImage, float coeffRadiusRepulse )
{
    // Les elements qui intersectent pixel verifient : r_p + r_elt > dist( pixel, elt )
    // On a une bbox des elements qui intersectent pour chaque rayon
    // Elle est donnee par : (p+(r_elt+r_p)) et (p-(r_elt+r_p))
    float radius = pixel.toCartesianW( repulseImage );
    LSIS_Pixel4D bot;
    LSIS_Pixel4D top;
    LSIS_Pixel4D curInBBox;
    float rElt;
    float sumRadius;

    // Pour chaque rayon on calcule alors cette bbox
    for ( int currW = 0 ; currW < repulseImage.wdim() ; currW++ )
    {
        // On calcule le rayon cartesien des elements de ce rayon
        rElt = repulseImage.minW() + ( currW + 0.5 ) * repulseImage.wres();

        // Ainsi on a acces a (r_elt+r_p) et on peut avoir la bbox en cartesien
        sumRadius = (rElt + radius)*coeffRadiusRepulse;

        // On transforme la bbox cartesienne en bbox de la grille
        bot.setFromCartesian( LSIS_Point4DFloat( rElt,
                                                 pixel.toCartesianX( repulseImage ) - sumRadius,
                                                 pixel.toCartesianY( repulseImage ) - sumRadius,
                                                 pixel.toCartesianZ( repulseImage ) - sumRadius ),
                                                 repulseImage );

        top.setFromCartesian( LSIS_Point4DFloat( rElt,
                                                 pixel.toCartesianX( repulseImage ) + sumRadius,
                                                 pixel.toCartesianY( repulseImage ) + sumRadius,
                                                 pixel.toCartesianZ( repulseImage ) + sumRadius ),
                                                 repulseImage );

        // On elargi un peu la bbox pour etre sur de ne rien intersecter
        // (voir les histoires avec les positions de centre vs les positions des coins des pixels)
        bot -= LSIS_Pixel4D(2,2,2,2);
        top += LSIS_Pixel4D(2,2,2,2);

        // Il ne reste plus qu'a tester les intersections des pixels a l'interieur de la bbox
        // On parcour donc toute la bbox
        beginForAllPixelsInBBox( bot, top, repulseImage.wdim(), repulseImage.xdim(), repulseImage.ydim(), repulseImage.zdim(), curInBBox )
        {
            curInBBox.setValueIn( repulseImage, true );
        }
        endForAllPixelsInBBox( bot, top, repulseImage.wdim(), repulseImage.xdim(), repulseImage.ydim(), repulseImage.zdim(), curInBBox )
    }
}

void LSIS_StepPreLocalisationTroncs::preWaitForAckIfInDebugMode()
{
    // Si il est possible d'acceder a la GUI (ce qui est le cas uniquement en mode debug)
    if ( getGuiContext() != NULL )
    {
        // On choisi un document dans lequel afficher
        setDocumentForDebugDisplay( getGuiContext()->documentManager()->documents() );

        // Si un document est disponible pour l'affichage de la grille
        if ( _debugDocumentHoughSpace != NULL )
        {
            // Si on a un espace de Hough a afficher en debug mode on l'ajoute au document
            if ( _debugHoughSpace != NULL )
            {
                _debugDocumentHoughSpace->addItemDrawable( *(_debugHoughSpace) );
            }

            // On actualise l'affichage du document
            _debugDocumentHoughSpace->redrawGraphics();
        }
    }
}

void LSIS_StepPreLocalisationTroncs::postWaitForAckIfInDebugMode()
{
    // Si un document etait disponible pour affichage
    if(_debugDocumentHoughSpace != NULL)
    {
        if ( _debugHoughSpace != NULL )
        {
            // On enleve la grille des elements du document
            _debugDocumentHoughSpace->removeItemDrawable(*_debugHoughSpace);
        }
    }

    // Et on lui demande de redessinner le tout (sans la grille)
    _debugDocumentHoughSpace->redrawGraphics();
}

void LSIS_StepPreLocalisationTroncs::setDocumentForDebugDisplay( QList<DocumentInterface *> docList )
{
    // On choisit le premier document disponible pour l'affichage des infos de debug
    // Si aucun n'est disponible on recupere null
    if(docList.isEmpty())
    {
        _debugHoughSpace = NULL;
    }

    else
    {
        _debugDocumentHoughSpace = docList.first();
    }
}
