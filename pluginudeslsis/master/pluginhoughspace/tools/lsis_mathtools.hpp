/************************************************************************************
* Filename :  lsis_mathtools.hpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_MATHTOOLS_HPP
#define LSIS_MATHTOOLS_HPP

#include "lsis_mathtools.h"

template< typename DataT >
float lsis_MathTools::getMean( QVector<DataT> const & data )
{
    if( data.empty() )
    {
        return 0;
    }

    float rslt = 0;
    int ndata = data.size();

    for( int i = 0 ; i < ndata ; i++ )
    {
        rslt += data.at(i);
    }

    return (rslt / ndata);
}

template< typename DataT >
float lsis_MathTools::getMean( QList<DataT> const & data )
{
    if( data.empty() )
    {
        return 0;
    }

    float rslt = 0;
    int ndata = data.size();

    for( int i = 0 ; i < ndata ; i++ )
    {
        rslt += data.at(i);
    }

    return (rslt / ndata);
}

template< typename DataT >
float lsis_MathTools::getVariance( QVector<DataT> const & data )
{
    return getVariance( data, getMean(data) );
}

template< typename DataT >
float lsis_MathTools::getVariance( QVector<DataT> const & data, float mean )
{
    if( data.empty() )
    {
        return 0;
    }

    float rslt = 0;
    int ndata = data.size();

    for( int i = 0 ; i < ndata ; i++ )
    {
        rslt += ( data.at(i) - mean )*( data.at(i) - mean );
    }

    return (rslt / ndata);
}

#endif // LSIS_MATHTOOLS_HPP
