#include "norm_newoctreenodev2.h"

#include "norm_newoctreev2.h"
#include "tools/norm_acp.h"
#include "tools/norm_leastsquarefitting.h"

#include <QtConcurrent/QtConcurrentRun>

#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_pointsattributesnormal.h"

#include "streamoverload/streamoverload.h"

// Using the point cloud deposit
#include "ct_global/ct_context.h"

const CT_PointAccessor NORM_NewOctreeNodeV2::_pAccessor = CT_PointAccessor();

const QVector<QColor> NORM_NewOctreeNodeV2::_qColorChild = QVector<QColor>() << QCOLOR0
                                                                             << QCOLOR1
                                                                             << QCOLOR2
                                                                             << QCOLOR3
                                                                             << QCOLOR4
                                                                             << QCOLOR5
                                                                             << QCOLOR6
                                                                             << QCOLOR7;

NORM_NewOctreeNodeV2::NORM_NewOctreeNodeV2(CT_Point& centre,
                                           float dimension,
                                           int depth,
                                           NORM_NewOctreeNodeV2* father,
                                           NORM_NewOctreeV2* octree,
                                           QVector<int>* ptIndices) :
    _centre( centre ),
    _dimension( dimension ),
    _depth( depth ),
    _flag( false ),
    _father( father ),
    _octree( octree ),
    _neighbours( NULL ),
    _ptIndices( ptIndices ),
    _surfaceFittingWasRan( false ),
    _acpWasRan( false ),
    _l1(-1),
    _l2(-1),
    _l3(-1)
{
    _children.resize(8);
    for( int i = 0 ; i < 8 ; i++ )
    {
        _children[i] = NULL;
    }
}

NORM_NewOctreeNodeV2::~NORM_NewOctreeNodeV2()
{
    if( _ptIndices != NULL )
    {
        delete _ptIndices;
    }
    if( _neighbours != NULL )
    {
        delete _neighbours;
    }

    foreach( NORM_NewOctreeNodeV2* child, _children )
    {
        delete child;
    }
}

void NORM_NewOctreeNodeV2::runACP()
{
    // On met le flag a true
    _acpWasRan = true;

    // On calcule le centroide
    computeCentroid();

    // On lance l'acp sur tous les points du noeud
    NORM_Tools::NORM_PCA::pca( _ptIndices,
                               _centroid,
                               _l1, _l2, _l3,
                               _v1, _v2, _v3 );
}

void NORM_NewOctreeNodeV2::debugFlipAcpAndSurfaceRecursive()
{
    // Si on est sur une feuille alors on fait le flip
    if( isLeaf() && _acpWasRan && _surfaceFittingWasRan )
    {
        debugFlipAcpAndSurface();
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->debugFlipAcpAndSurfaceRecursive();
            }
        }
    }
}

void NORM_NewOctreeNodeV2::debugFlipAcpAndSurface()
{
    _v1 = -_v1;
    _v2 = -_v2;
    _v3 = -_v3;

    _a = -_a;
    _b = -_b;
    _c = -_c;
    _f = -_f;
}

void NORM_NewOctreeNodeV2::runQuadraticFittingRecursive(int recursionLevel)
{
    // Si on est sur une feuille alors on fait le fitting de surface
    // On fait gaffe a avoir fait une acp avant
    if( isLeaf() && _acpWasRan )
    {
        runQuadraticFitting();
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->runQuadraticFittingRecursive( recursionLevel++ );
//                QFuture< void > future = QtConcurrent::run( _children[i], &NORM_NewOctreeNodeV2::runQuadraticFittingRecursive, recursionLevel++ );
//                future.waitForFinished();
            }
        }
    }
}

void NORM_NewOctreeNodeV2::runQuadraticFitting()
{
    assert( _acpWasRan );

    // On ne peut faire le fitting que si la pca a ete faite et que le sigma est suffisemment bas
    if( getSigma3PerCent() < _octree->sigma3Max() )
    {
        // On met le flag a true
        _surfaceFittingWasRan = true;

        // On recupere la matrice de changement de base
        CT_Point x; x << 1,0,0;
        CT_Point y; y << 0,1,0;
        CT_Point z; z << 0,0,1;

        Eigen::Matrix3d changeBaseMatrix = NORM_Tools::NORM_ChangeBasis::changeBasisMatrix( x, y, z,
                                                                                            _v1, _v2, _v3 );

        // On fait le surface fitting
        NORM_Tools::NORM_LeastSquareFitting::quadraticSurfaceFitting( _ptIndices,
                                                                      _pAccessor,
                                                                      _centroid,
                                                                      changeBaseMatrix,
                                                                      _a, _b, _c, _d, _e, _f );
    }
}

float NORM_NewOctreeNodeV2::runQuadraticFittingAndGetRmse()
{
    assert( _acpWasRan );

    // On met le flag a true
    _surfaceFittingWasRan = true;

    // On recupere la matrice de changement de base
    CT_Point x; x << 1,0,0;
    CT_Point y; y << 0,1,0;
    CT_Point z; z << 0,0,1;

    Eigen::Matrix3d changeBaseMatrix = NORM_Tools::NORM_ChangeBasis::changeBasisMatrix( x, y, z,
                                                                                        _v1, _v2, _v3 );

    // On fait le surface fitting
    return NORM_Tools::NORM_LeastSquareFitting::quadraticSurfaceFittingWithRMSE( _ptIndices,
                                                                                 _pAccessor,
                                                                                 _centroid,
                                                                                 changeBaseMatrix,
                                                                                 _a, _b, _c, _d, _e, _f );
}

void NORM_NewOctreeNodeV2::computeCentroid()
{
    _centroid.setValues(0,0,0);

    if( _ptIndices->empty() )
    {
        return;
    }

    foreach( int index, (*_ptIndices) )
    {
        _centroid += _pAccessor.constPointAt( index );
    }

    _centroid /= (float)(_ptIndices->size());
}

void NORM_NewOctreeNodeV2::recursiveSplit()
{
    // Si il n'y a pas assez de points pour faire une acp alors on arete ici aussu
    if( _ptIndices->size() < _octree->nbPointsMinForPca() )
    {
        return;
    }

    float sigma2 = std::numeric_limits<float>::max();
    float sigma3 = std::numeric_limits<float>::max();

    // Si la dimension est assez petite pour faire une acp (on ne veut pas faire l'acp sur des cellules qui font des dizaines de metres cubes)
    if( _dimension < _octree->getDimensionMaxForPca() )
    {
        // On lance l'acp
        runACP();

        // Pour recuperer le sigma
        sigma2 = getSigma2PerCent();
        sigma3 = getSigma3PerCent();
    }

    // Si le sigma etait trop grand OU que sigma2 est trop petit (cas d'une courbe)
    // - alors il faut diviser le noeud
    if( ( sigma3 > _octree->sigma3Max() )
        ||
        ( sigma2 < _octree->getSigma2Min() ) )
    {
        // On regarde si il est divisible en taille
        if( _dimension > _octree->dimensionMin() )
        {
            // On trie les points dans les huit noeuds fils
            QVector< QVector<int>* > indicesInChildren;
            sortPointsIntoChildrenNodes( indicesInChildren );

            // On libere la memoire
            delete _ptIndices;
            _ptIndices = NULL;

            // Pour chaque fils qui n'est pas vide on cree un noeud
            _children.resize(8);

            // ************************************************************************************************************************** */
            // Version sequentielle
            // ************************************************************************************************************************** */
            for( int i = 0 ; i < 8 ; i++ )
            {
                // Si on en a un (fils) qui n'est pas vide
                if( !indicesInChildren.at(i)->empty() )
                {
                    // On cree le noeud fils
                    // Mais d'abord on calcule son centre
                    CT_Point childCentre;
                    getChildCenterFromChildNumber( i, childCentre );
                    _children[i] = new NORM_NewOctreeNodeV2( childCentre,
                                                             _dimension/2.0,
                                                             _depth+1,
                                                             this,
                                                             _octree,
                                                             indicesInChildren.at(i) );

                    // Et on regarde si il faut subdiviser ce fils egalement
                    _children[i]->recursiveSplit();
                }
                else
                {
                    // Si le fils etait vide on le met a NULL
                    _children[i] = NULL;
                }
          }
          // ************************************************************************************************************************** */
          // ************************************************************************************************************************** */

//            // ************************************************************************************************************************** */
//            // Version parallele
//            // ************************************************************************************************************************** */
//            QFuture< void > future0 = QtConcurrent::run( this, &NORM_NewOctreeNodeV2::concurrentCreateChild, 0, indicesInChildren.at(0) );
//            QFuture< void > future1 = QtConcurrent::run( this, &NORM_NewOctreeNodeV2::concurrentCreateChild, 1, indicesInChildren.at(1) );
//            QFuture< void > future2 = QtConcurrent::run( this, &NORM_NewOctreeNodeV2::concurrentCreateChild, 2, indicesInChildren.at(2) );
//            QFuture< void > future3 = QtConcurrent::run( this, &NORM_NewOctreeNodeV2::concurrentCreateChild, 3, indicesInChildren.at(3) );
//            QFuture< void > future4 = QtConcurrent::run( this, &NORM_NewOctreeNodeV2::concurrentCreateChild, 4, indicesInChildren.at(4) );
//            QFuture< void > future5 = QtConcurrent::run( this, &NORM_NewOctreeNodeV2::concurrentCreateChild, 5, indicesInChildren.at(5) );
//            QFuture< void > future6 = QtConcurrent::run( this, &NORM_NewOctreeNodeV2::concurrentCreateChild, 6, indicesInChildren.at(6) );
//            QFuture< void > future7 = QtConcurrent::run( this, &NORM_NewOctreeNodeV2::concurrentCreateChild, 7, indicesInChildren.at(7) );
//            future0.waitForFinished();
//            future1.waitForFinished();
//            future2.waitForFinished();
//            future3.waitForFinished();
//            future4.waitForFinished();
//            future5.waitForFinished();
//            future6.waitForFinished();
//            future7.waitForFinished();
//            // ************************************************************************************************************************** */
//            // ************************************************************************************************************************** */

            // Fin de recursion
            return;
        }
        else
        {
            // Si il n'est pas divisible en taille la recursion s'arete la
            return;
        }
    }

    else
    {
        // Si le sigma etait sous la barre acceptee alors la recursion s'arete
        return;
    }
}

void NORM_NewOctreeNodeV2::recursiveSplitWithRmse()
{
    // Si il n'y a pas assez de points pour faire une acp alors on arete ici aussu
    if( _ptIndices->size() < _octree->nbPointsMinForPca() )
    {
        return;
    }

    // Si la dimension est assez petite pour faire une acp (on ne veut pas faire l'acp sur des cellules qui font des dizaines de metres cubes)
    if( _dimension < _octree->getDimensionMaxForPca() )
    {
        // On lance l'acp
        runACP();

        // Et on fait le fitting de surface
        _rmse = runQuadraticFittingAndGetRmse();
    }
    // Sinon on continue la recursion jusqu'a atteindre des cellules suffisamment petites pour y faire une acp
    else
    {
        // On trie les points dans les huit noeuds fils
        QVector< QVector<int>* > indicesInChildren;
        sortPointsIntoChildrenNodes( indicesInChildren );

        // On libere la memoire
        delete _ptIndices;
        _ptIndices = NULL;

        // Pour chaque fils qui n'est pas vide on cree un noeud
        _children.resize(8);

        // ************************************************************************************************************************** */
        // Version sequentielle
        // ************************************************************************************************************************** */
        for( int i = 0 ; i < 8 ; i++ )
        {
            // Si on en a un (fils) qui n'est pas vide
            if( !indicesInChildren.at(i)->empty() )
            {
                // On cree le noeud fils
                // Mais d'abord on calcule son centre
                CT_Point childCentre;
                getChildCenterFromChildNumber( i, childCentre );
                _children[i] = new NORM_NewOctreeNodeV2( childCentre,
                                                         _dimension/2.0,
                                                         _depth+1,
                                                         this,
                                                         _octree,
                                                         indicesInChildren.at(i) );

                // Et on regarde si il faut subdiviser ce fils egalement
                _children[i]->recursiveSplitWithRmse();
            }
            else
            {
                // Si le fils etait vide on le met a NULL
                _children[i] = NULL;
            }
        }

        // Fin de recursion
        return;
    }

    // Si la rmse est trop grande
    // - alors il faut diviser le noeud
    if( _rmse > _octree->getMaxRMSE() )
    {
        // On regarde si il est divisible en taille
        if( _dimension > _octree->dimensionMin() )
        {
            // On trie les points dans les huit noeuds fils
            QVector< QVector<int>* > indicesInChildren;
            sortPointsIntoChildrenNodes( indicesInChildren );

            // On libere la memoire
            delete _ptIndices;
            _ptIndices = NULL;

            // Pour chaque fils qui n'est pas vide on cree un noeud
            _children.resize(8);

            // ************************************************************************************************************************** */
            // Version sequentielle
            // ************************************************************************************************************************** */
            for( int i = 0 ; i < 8 ; i++ )
            {
                // Si on en a un (fils) qui n'est pas vide
                if( !indicesInChildren.at(i)->empty() )
                {
                    // On cree le noeud fils
                    // Mais d'abord on calcule son centre
                    CT_Point childCentre;
                    getChildCenterFromChildNumber( i, childCentre );
                    _children[i] = new NORM_NewOctreeNodeV2( childCentre,
                                                             _dimension/2.0,
                                                             _depth+1,
                                                             this,
                                                             _octree,
                                                             indicesInChildren.at(i) );

                    // Et on regarde si il faut subdiviser ce fils egalement
                    _children[i]->recursiveSplitWithRmse();
                }
                else
                {
                    // Si le fils etait vide on le met a NULL
                    _children[i] = NULL;
                }
            }

            // Fin de recursion
            return;
        }
        else
        {
            // Si il n'est pas divisible en taille la recursion s'arete la
            return;
        }
    }

    else
    {
        // Si la rmse etait sous le seuil demande alors la recursion s'arete
        return;
    }
}

void NORM_NewOctreeNodeV2::concurrentCreateChild(int childId,
                                                 QVector<int>* childPointIndices)
{
    // Si on en a un (fils) qui n'est pas vide
    if( !childPointIndices->empty() )
    {
        // On cree le noeud fils
        // Mais d'abord on calcule son centre
        CT_Point childCentre;
        getChildCenterFromChildNumber( childId, childCentre );
        _children[childId] = new NORM_NewOctreeNodeV2( childCentre,
                                                       _dimension/2.0,
                                                       _depth+1,
                                                       this,
                                                       _octree,
                                                       childPointIndices );
        // Et on regarde si il faut subdiviser ce fils egalement
        _children[childId]->recursiveSplit();
    }
    else
    {
        // Si le fils etait vide on le met a NULL
        _children[childId] = NULL;
    }
}

void NORM_NewOctreeNodeV2::sortPointsIntoChildrenNodes( QVector< QVector<int>* >& outIndicesOfChildren )
{
    outIndicesOfChildren.clear();
    outIndicesOfChildren.resize(8);
    for( int i = 0 ; i < 8 ; i++ )
    {
        outIndicesOfChildren[i] = new QVector<int>();
    }

    // Pour chaque point (indice) du noeud
    CT_Point p;
    foreach( int indice, (*_ptIndices) )
    {
        // On recupere le numero du fils qui va le contenir
        p = _pAccessor.pointAt( indice );
        int childId = getChildIdForPoint( p );

        // On l'ajoute au fils correspondant
        outIndicesOfChildren.at( childId )->append( indice );
    }
}

int NORM_NewOctreeNodeV2::getChildIdForPoint(const CT_Point& point)
{
    /*
     * Children follow a predictable pattern to make accesses simple.
     * Here, - means less than 'origin' in that dimension, + means greater than.
     * child:  0 1 2 3 4 5 6 7
     * x:      - - - - + + + +
     * y:      - - + + - - + +
     * z:      - + - + - + - +
     *
     * Soit x code le 2eme bit (4), y code le 1er bit (2) et z code le 0eme bit (0)
     */
    int rslt = 0;

    rslt = rslt | ( ( point.x() > _centre.x() ) << 2 );
    rslt = rslt | ( ( point.y() > _centre.y() ) << 1 );
    rslt = rslt | ( point.z() > _centre.z() );

    return rslt;
}

void NORM_NewOctreeNodeV2::getChildCenterFromChildNumber(int child,
                                                         CT_Point& outChildCentre)
{
    float shift = _dimension / 4.0;

    // C.F. table de correspondance en commentaire de la methode "int NORM_NewOctreeNodeV2::getChildIdForPoint(CT_Point& point)"
    if( child & 4 )
    {
        outChildCentre.x() = _centre.x() + shift;
    }
    else
    {
        outChildCentre.x() = _centre.x() - shift;
    }

    if( child & 2 )
    {
        outChildCentre.y() = _centre.y() + shift;
    }
    else
    {
        outChildCentre.y() = _centre.y() - shift;
    }

    if( child & 1 )
    {
        outChildCentre.z() = _centre.z() + shift;
    }
    else
    {
        outChildCentre.z() = _centre.z() - shift;
    }
}

void NORM_NewOctreeNodeV2::setFlagRecursive(bool flag)
{
    // On met a jour le flag
    _flag = flag;

    // On continue la recursion
    for( size_t i = 0 ; i < 8 ; i++ )
    {
        if( _children[i] != NULL )
        {
            _children[i]->setFlagRecursive(flag);
        }
    }
}

void NORM_NewOctreeNodeV2::orientAllNodesRecursive(int nSamplePoints, CT_PointsAttributesScalarTemplated<int>*& debugAttribut, int& debugNumAttribut )
{
    // Si on est sur une feuille et que le noeud n'est pas marque alors on part une orientation par propagation
    if( isLeaf() )
    {
        if( _surfaceFittingWasRan == true && _flag == false )
        {
            debugNumAttribut++;
            orientAsBfsFromCallingNode( nSamplePoints, debugAttribut, debugNumAttribut );
            qDebug() << debugNumAttribut;
        }
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->orientAllNodesRecursive( nSamplePoints, debugAttribut, debugNumAttribut );
            }
        }
    }
}

void NORM_NewOctreeNodeV2::orientAsBfsFromCallingNode( int nSamplePoints, CT_PointsAttributesScalarTemplated<int>*& debugAttribut, int& debugNumAttribut )
{
    QQueue< NORM_NewOctreeNodeV2* > f;

    // On flagge le noeud appelant et on l'insere dans la queue
    _flag = true;
    f.enqueue( this );

    // Tant que la queue n'est pas vide
    while( !f.empty() )
    {
        // On prend un sommet de la queue
        NORM_NewOctreeNodeV2* curNode = f.dequeue();

        for( int i = 0 ; i < curNode->_ptIndices->size() ; i++ )
        {
            debugAttribut->setValueAt( curNode->_ptIndices->at(i), debugNumAttribut );
        }
//        debugNumAttribut++;

        // On recupere les voisins de face ce noeud
        QVector< NORM_NewOctreeNodeV2* > faceNeis;
        curNode->getFacesNeighbours( faceNeis );
        qDebug() << "Le changement ici";
//        curNode->getFacesNeighboursAroundv3( faceNeis );

        // On oriente les voisins non marques et on les ajoute a la queue
        curNode->orientNonFlaggedFaceNeighboursAndFlagThemAddThemToQueue( nSamplePoints,
                                                                          f );
    }
}

void NORM_NewOctreeNodeV2::orientNonFlaggedFaceNeighboursAndFlagThemAddThemToQueue(int nSamplePoints,
                                                                                   QQueue<NORM_NewOctreeNodeV2*>& f)
{
    // On emet l'hypothese que le noeud appelant est dej oriente et flage

    // On recupere les voisins de face ce noeud
    QVector< NORM_NewOctreeNodeV2* > faceNeis;
    getFacesNeighbours( faceNeis );

    // Pour chacun on va sampler des points sur la face de contact du plus petit des deux
    foreach( NORM_NewOctreeNodeV2* nei, faceNeis )
    {
        // Si le voisin n'est pas encore flage et que la surface a ete fitee avant
        if( nei->_flag == false && nei->_surfaceFittingWasRan == true )
        {
//            float scalarProduct = ( _v3.x()*nei->_v3.x() +
//                                    _v3.y()*nei->_v3.y() +
//                                    _v3.z()*nei->_v3.z() );
//            if( scalarProduct < 0 )
//            {
//                nei->debugFlipAcpAndSurface();
//            }

            // On recupere le plus petit et le plus grand
            NORM_NewOctreeNodeV2* smallest = nei;
            NORM_NewOctreeNodeV2* largest = this;
            if( _dimension < nei->_dimension )
            {
                smallest = this;
                largest = nei;
            }

            // On regarde par quelle face le plus petit touche le plus grand
            faceRelativePosition relPos = smallest->getFaceRelativePosition( largest );

            // On sample les points sur la face de contact
            QVector< CT_Point > sampledPoints;
            smallest->samplePointsOnFace( relPos, nSamplePoints, sampledPoints );

            // On regarde le signe des points sur chaque noeud
            QVector< bool > signsCallingNode;
            QVector< bool > signsNeighbourNode;
            getSignsOfPointsFromPointsSampledOnFace( sampledPoints,
                                                     signsCallingNode );

            nei->getSignsOfPointsFromPointsSampledOnFace( sampledPoints,
                                                          signsNeighbourNode );

//            qDebug() << "----------------------------";
//            qDebug() << "Centre appelant" << _centre;
//            qDebug() << "Centre voisin a orienter" << nei->_centre;
//            qDebug() << "Ils se touchent par la face" << relPos;

            // Compte le nombre de differences de signe
            int nDiff = 0;
            int nSampledPoints = sampledPoints.size();
            for( int i = 0 ; i < nSampledPoints ; i++ )
            {
//                qDebug() << "Point " << sampledPoints.at(i) << "signe appelant =" << signsCallingNode[i] << " signe voisin = " << signsNeighbourNode[i];
                if( signsCallingNode[i] != signsNeighbourNode[i] )
                {
                    nDiff++;
                }
            }
//            qDebug() << "nDiff " << nDiff;

//            // Si le nombre de difference est environ egal au nombre de matches
//            if( ( (float)nDiff * 2.0 / (float)nSampledPoints ) > 0.1 &&
//                ( (float)nDiff * 2.0 / (float)nSampledPoints ) < 1.9 )
//            {
//                qDebug() << "nDiff " << nDiff << " sur " << (float)nSampledPoints;
//            }

            // Si on a plus de differences que de match alors on inverse le voisin
            if( nDiff > ( (float)nSampledPoints / 2.0 ) )
            {
                nei->debugFlipAcpAndSurface();
            }

            // Et on flage le voisin
            nei->_flag = true;

            // Et on l'ajoute a la queue
            f.enqueue( nei );
        }
    }
}

int NORM_NewOctreeNodeV2::countPositivePointsFromPointsSampledOnFace(QVector<CT_Point>& sampledPoints)
{
    int rslt = 0;

    // Calcule les matrices de passage cartesien vers acp et acp vers cartesien
    Eigen::Matrix3d toCartBasis;
    toCartBasis << _v1(0) , _v2(0) , _v3(0),
                   _v1(1) , _v2(1) , _v3(1),
                   _v1(2) , _v2(2) , _v3(2);
    Eigen::Matrix3d toPcaBasis = toCartBasis.inverse();

    foreach( CT_Point p, sampledPoints )
    {
        // On le transforme pour l'avoir dans la base de l'acp :
        // 1 - On considere le centroide du noeud comme l'origine du repere de l'acp donc on fait une translation
        // 2 - On applique la matrice de passage
        p = p - _centroid;
        CT_Point curPointInPcaBasis = toPcaBasis * p;

        // On regarde si le point se situe sur la surface
        if( curPointInPcaBasis.z() - ( _a * curPointInPcaBasis.x() * curPointInPcaBasis.x() +
                                       _b * curPointInPcaBasis.x() * curPointInPcaBasis.y() +
                                       _c * curPointInPcaBasis.y() * curPointInPcaBasis.y() +
                                       _d * curPointInPcaBasis.x() +
                                       _e * curPointInPcaBasis.y() +
                                       _f ) > 0 )
        {
            rslt++;
        }
    }

    return rslt;
}

void NORM_NewOctreeNodeV2::getSignsOfPointsFromPointsSampledOnFace(QVector<CT_Point>& sampledPoints,
                                                                   QVector<bool>& outSigns)
{
    outSigns.resize( sampledPoints.size() );

    // Calcule les matrices de passage cartesien vers acp et acp vers cartesien
    Eigen::Matrix3d toCartBasis;
    toCartBasis << _v1(0) , _v2(0) , _v3(0),
                   _v1(1) , _v2(1) , _v3(1),
                   _v1(2) , _v2(2) , _v3(2);
    Eigen::Matrix3d toPcaBasis = toCartBasis.inverse();

    int nSamplePoints = sampledPoints.size();
    for( int i = 0 ; i < nSamplePoints ; i++ )
    {
        CT_Point p = sampledPoints.at(i);

        // On le transforme pour l'avoir dans la base de l'acp :
        // 1 - On considere le centroide du noeud comme l'origine du repere de l'acp donc on fait une translation
        // 2 - On applique la matrice de passage
        p = p - _centroid;
        CT_Point curPointInPcaBasis = toPcaBasis * p;

        // On regarde si le point se situe sur la surface
        if( curPointInPcaBasis.z() - ( _a * curPointInPcaBasis.x() * curPointInPcaBasis.x() +
                                       _b * curPointInPcaBasis.x() * curPointInPcaBasis.y() +
                                       _c * curPointInPcaBasis.y() * curPointInPcaBasis.y() +
                                       _d * curPointInPcaBasis.x() +
                                       _e * curPointInPcaBasis.y() +
                                       _f ) > 0 )
        {
            outSigns[i] = true;
        }
        else
        {
            outSigns[i] = false;
        }
    }
}

void NORM_NewOctreeNodeV2::drawRecursive(GraphicsViewInterface& view,
                                         PainterInterface& painter,
                                         size_t idChild,
                                         bool drawBoxes,
                                         bool drawCentroid,
                                         bool drawV3,
                                         double sizeV3 ) const
{
    // Draw each leaf and stop recursion
    if ( isLeaf() )
    {
        // Si la noeud contient des points
        if( drawBoxes )
        {
            float halfDim = _dimension / 2.0;
            painter.setColor( _qColorChild[idChild] );
            painter.drawCube( _centre(0) - halfDim, _centre(1) - halfDim, _centre(2) - halfDim,
                              _centre(0) + halfDim, _centre(1) + halfDim, _centre(2) + halfDim);
        }

        // Si on a lance l'acp sur ce noeud alors on peut afficher v3
        if( drawV3 && _acpWasRan )
        {
            if( drawCentroid )
            {
                painter.drawPoint( _centroid.x(), _centroid.y(), _centroid.z() );
            }

            painter.drawLine( _centroid.x(), _centroid.y(), _centroid.z(),
                              _centroid.x() + ( _v3.x() * sizeV3 ), _centroid.y() + ( _v3.y() * sizeV3 ), _centroid.z() + ( _v3.z() * sizeV3 ) );
        }
    }

    // else continue recursion over children
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->drawRecursive( view,
                                             painter,
                                             i,
                                             drawBoxes,
                                             drawCentroid,
                                             drawV3,
                                             sizeV3 );
                /*! Ici on ne peut pas faire de multithread car PainterInterface contient des methodes virtuelles ?! */
            }
        }
    }
}

void NORM_NewOctreeNodeV2::setIdAttributeRecursive(CT_PointsAttributesScalarTemplated<int>* attributs,
                                                   size_t currentID)
{
    // Si on est sur une feuille alors on met a jour les identifiants
    if( isLeaf() )
    {
        foreach( int index, (*_ptIndices) )
        {
            attributs->setValueAt( index, currentID );
        }
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->setIdAttributeRecursive( attributs, i );
//                QFuture< void > future = QtConcurrent::run( _children[i], &NORM_NewOctreeNodeV2::setIdAttributeRecursive, attributs, i );
//                future.waitForFinished();
            }
        }
    }
}

void NORM_NewOctreeNodeV2::setSigmaAttributeRecursive(CT_PointsAttributesScalarTemplated<float>* attributs)
{

    // Si on est sur une feuille alors on met a jour les identifiants
    if( isLeaf() )
    {
        // Si on n'avait pas lance l'acp sur ce noeud (trop peu de points)
        if( !_acpWasRan )
        {
            // Alors on met a -1
            foreach( int index, (*_ptIndices) )
            {
                attributs->setValueAt( index, -1 );
            }
        }
        // Sinon on peut ajouter les sigmas trouves
        else
        {
            foreach( int index, (*_ptIndices) )
            {
                attributs->setValueAt( index, getSigma3PerCent() );
            }
        }
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->setSigmaAttributeRecursive( attributs );
//                QFuture< void > future = QtConcurrent::run( _children[i], &NORM_NewOctreeNodeV2::setSigmaAttributeRecursive, attributs );
//                future.waitForFinished();
            }
        }
    }
}

void NORM_NewOctreeNodeV2::setPcaAttributesRecursive(CT_PointsAttributesScalarTemplated<float>* outCreatedL1Attribute,
                                                     CT_PointsAttributesScalarTemplated<float>* outCreatedL2Attribute,
                                                     CT_PointsAttributesScalarTemplated<float>* outCreatedL3Attribute,
                                                     CT_PointsAttributesScalarTemplated<float>* outCreatedSigmaAttribute,
                                                     CT_PointsAttributesScalarTemplated<float>* outCreatedL3L2Attribute)
{
    // Si on est sur une feuille alors on met a jour les identifiants
    if( isLeaf() && _acpWasRan )
    {
        foreach( int index, (*_ptIndices) )
        {
            outCreatedL1Attribute->setValueAt( index, _l1 );
            outCreatedL2Attribute->setValueAt( index, _l2 );
            outCreatedL3Attribute->setValueAt( index, _l3 );
            outCreatedSigmaAttribute->setValueAt( index, getSigma3PerCent() );
            outCreatedL3L2Attribute->setValueAt( index, _l3 / _l2 );
        }
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->setPcaAttributesRecursive( outCreatedL1Attribute,
                                                         outCreatedL2Attribute,
                                                         outCreatedL3Attribute,
                                                         outCreatedSigmaAttribute,
                                                         outCreatedL3L2Attribute );
            }
        }
    }
}

void NORM_NewOctreeNodeV2::setNormalsAttributeRecursive(CT_PointsAttributesNormal* attributs)
{
    // Si on est sur une feuille et que la surface a ete fittee
    if( isLeaf() && _acpWasRan && _surfaceFittingWasRan )
    {
        setNormalsAttribute( attributs );
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->setNormalsAttributeRecursive( attributs );
//                QFuture< void > future = QtConcurrent::run( _children[i], &NORM_NewOctreeNodeV2::setNormalsAttributeRecursive, attributs );
//                future.waitForFinished();
            }
        }
    }
}

void NORM_NewOctreeNodeV2::setNormalsAttribute(CT_PointsAttributesNormal* attributs)
{
    // On est oblige de passer par le abstractnormalcloud pour modifier les normales
    CT_AbstractNormalCloud* outNormalCloud = attributs->getNormalCloud();

    // On s'assure que l'acp et le fitting de surface ont bien ete fait pour ce noeud
    assert( _acpWasRan && _surfaceFittingWasRan );

    // Calcule les matrices de passage cartesien vers acp et acp vers cartesien
    Eigen::Matrix3d toCartBasis;
    toCartBasis << _v1(0) , _v2(0) , _v3(0),
                   _v1(1) , _v2(1) , _v3(1),
                   _v1(2) , _v2(2) , _v3(2);
    Eigen::Matrix3d toPcaBasis = toCartBasis.inverse();

    foreach( int index, (*_ptIndices) )
    {
        // On accede au point
        CT_Point curPointInCartBasis = _pAccessor.pointAt( index );

        // On le transforme pour l'avoir dans la base de l'acp :
        // 1 - On considere le centroide du noeud comme l'origine du repere de l'acp donc on fait une translation
        // 2 - On applique la matrice de passage
        curPointInCartBasis = curPointInCartBasis - _centroid;
        CT_Point curPointInPcaBasis = toPcaBasis * curPointInCartBasis;

        // On calcule la normale du point projete sur la surface (projection selon l'axe z simplement)
        CT_Point curNormalInPcaBasis;
        curNormalInPcaBasis(0) = ( 2 * _a * curPointInPcaBasis.x() ) + ( _b * curPointInPcaBasis.y() ) + _d;
        curNormalInPcaBasis(1) = ( 2 * _c * curPointInPcaBasis.y() ) + ( _b * curPointInPcaBasis.x() ) + _e;
        curNormalInPcaBasis(2) = -1;

        // On normalise la normale
        curNormalInPcaBasis.normalize();

        // On repasse la normale dans le repere cartesien
        CT_Point curNormalInCartBasis = toCartBasis * curNormalInPcaBasis;

        // Transformation en CT_Normal
        CT_Normal outNormal;
        for( int i = 0 ; i < 3 ; i++ )
        {
            outNormal(i) = curNormalInCartBasis(i);
        }

        // Et on modifie le tableau des normales
        outNormalCloud->replaceNormal( index, outNormal );
    }
}

void NORM_NewOctreeNodeV2::computeNormalAtPoint(CT_Point& pInCartBasis,
                                                CT_Point& outNormal)
{
    // On s'assure que l'acp et le fitting de surface ont bien ete fait pour ce noeud
    assert( _acpWasRan && _surfaceFittingWasRan );

    // Calcule les matrices de passage cartesien vers acp et acp vers cartesien
    Eigen::Matrix3d toCartBasis;
    toCartBasis << _v1(0) , _v2(0) , _v3(0),
                   _v1(1) , _v2(1) , _v3(1),
                   _v1(2) , _v2(2) , _v3(2);
    Eigen::Matrix3d toPcaBasis = toCartBasis.inverse();

    // On le transforme pour l'avoir dans la base de l'acp :
    // 1 - On considere le centroide du noeud comme l'origine du repere de l'acp donc on fait une translation
    // 2 - On applique la matrice de passage
    CT_Point curPointInCartBasis = pInCartBasis - _centroid;
    CT_Point curPointInPcaBasis = toPcaBasis * curPointInCartBasis;

    // On calcule la normale du point projete sur la surface (projection selon l'axe z simplement)
    CT_Point curNormalInPcaBasis;
    curNormalInPcaBasis(0) = ( 2 * _a * curPointInPcaBasis.x() ) + ( _b * curPointInPcaBasis.y() ) + _d;
    curNormalInPcaBasis(1) = ( 2 * _c * curPointInPcaBasis.y() ) + ( _b * curPointInPcaBasis.x() ) + _e;
    curNormalInPcaBasis(2) = -1;

    // On normalise la normale
    curNormalInPcaBasis.normalize();

    // On repasse la normale dans le repere cartesien
    outNormal = toCartBasis * curNormalInPcaBasis;
}

void NORM_NewOctreeNodeV2::computeLocalPoint(CT_Point& pInCartBasis, CT_Point& outPointInPcaBasis)
{
    // Calcule les matrices de passage cartesien vers acp et acp vers cartesien
    Eigen::Matrix3d toCartBasis;
    toCartBasis << _v1(0) , _v2(0) , _v3(0),
                   _v1(1) , _v2(1) , _v3(1),
                   _v1(2) , _v2(2) , _v3(2);
    Eigen::Matrix3d toPcaBasis = toCartBasis.inverse();

    // On le transforme pour l'avoir dans la base de l'acp :
    // 1 - On considere le centroide du noeud comme l'origine du repere de l'acp donc on fait une translation
    // 2 - On applique la matrice de passage
    CT_Point curPointInCartBasis = pInCartBasis - _centroid;
    outPointInPcaBasis = toPcaBasis * curPointInCartBasis;
}

void NORM_NewOctreeNodeV2::computeGlobalPoint(CT_Point& pInPcaBasis,
                                              CT_Point& outPointInCartBasis)
{
    // Calcule les matrices de passage cartesien vers acp et acp vers cartesien
    Eigen::Matrix3d toCartBasis;
    toCartBasis << _v1(0) , _v2(0) , _v3(0),
                   _v1(1) , _v2(1) , _v3(1),
                   _v1(2) , _v2(2) , _v3(2);

    // On le transforme pour l'avoir dans la base cartesienne :
    // 2 - On applique la matrice de passage
    // 1 - On refait la translation au centroide
    CT_Point curPointInCartBasis = toCartBasis * pInPcaBasis;
    outPointInCartBasis = curPointInCartBasis + _centroid;
}

void NORM_NewOctreeNodeV2::computeProjectedPoint(CT_Point& pInCartBasis,
                                                 CT_Point& outProjectedPoint)
{
    // On recupere le point dans la base locale
    CT_Point pInPcaBasis;
    computeLocalPoint( pInCartBasis,
                       pInPcaBasis );

    // On le projette sur la surface
    pInPcaBasis.z() = ( _a * pInPcaBasis.x() * pInPcaBasis.x() +
                        _b * pInPcaBasis.x() * pInPcaBasis.y() +
                        _c * pInPcaBasis.y() * pInPcaBasis.y() +
                        _d * pInPcaBasis.x() +
                        _e * pInPcaBasis.y() +
                        _f );

    // Retour a la base cartesienne
    computeGlobalPoint( pInPcaBasis,
                        outProjectedPoint );
}

void NORM_NewOctreeNodeV2::setNormalsAttributeWithLinearInterpolationRecursive(CT_PointsAttributesNormal* attributs)
{
    // Si on est sur une feuille et que la surface a ete fittee
    if( isLeaf() && _acpWasRan && _surfaceFittingWasRan )
    {
        setNormalsAttributeWithLinearInterpolation( attributs );
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->setNormalsAttributeWithLinearInterpolationRecursive( attributs );
            }
        }
    }
}

void NORM_NewOctreeNodeV2::setNormalsAttributeWithLinearInterpolation(CT_PointsAttributesNormal* attributs)
{
    // On s'assure que l'acp et le fitting de surface ont bien ete fait pour ce noeud
    assert( _acpWasRan && _surfaceFittingWasRan );

    // On est oblige de passer par le abstractnormalcloud pour modifier les normales
    CT_AbstractNormalCloud* outNormalCloud = attributs->getNormalCloud();

    // On recupere tous les voisins de face
    QVector< NORM_NewOctreeNodeV2* > facesNei;
    QVector< faceRelativePosition > posNei;
    getFacesNeighbours( facesNei );

    // Ainsi que leur position face au noeud appelant
    foreach( NORM_NewOctreeNodeV2* n, facesNei )
    {
        posNei.push_back( getFaceRelativePosition( n ) );
    }

    // On va faire une interpolation lineaire pour tous les points du noeud appelant
    foreach( int index, (*_ptIndices) )
    {
        // On recupere le point dans la base cartesienne
        CT_Point curPoint = _pAccessor.pointAt( index );

        // Calcul de la normale pour le noeud appelant
        CT_Point normalCallingNode;
        computeNormalAtPoint( curPoint,
                              normalCallingNode );

        // On va parcourir tous les voisins pour faire l'interpolation
        float sumWeight = 0;
        CT_Point sumNormal;
        sumNormal.setValues(0,0,0);
        int nNei = facesNei.size();
        for( int i = 0 ; i < nNei ; i++ )
        {
            NORM_NewOctreeNodeV2* nei = facesNei.at(i);

            // On fait attention a ce que les voisins pris en compte ont bien ete fites eux aussi
            if( nei->_acpWasRan && nei->_surfaceFittingWasRan )
            {
                // On calcule le rayon d'interpolation :
                // On prend 1/3 de la taille de la cellule la plus petite entre les deux
                float nodesMinDim = std::min( _dimension, nei->_dimension );
                float localInterpolationRadius = nodesMinDim / 3.0;

                // Calcul des poids pour la paire de voisins
                float weightCallingNode = getLinearInterpolationWeight( curPoint,
                                                                        posNei.at(i),
                                                                        localInterpolationRadius );

                float weightNeighbourNode = nei->getLinearInterpolationWeight( curPoint,
                                                                               posNei.at(i),
                                                                               localInterpolationRadius );

                // Calcul de la normale pour le noeud voisin
                CT_Point normalNeighbourNode;
                nei->computeNormalAtPoint( curPoint,
                                           normalNeighbourNode );

                // On ajoute les normales ponderees par les poids trouves
                sumWeight += weightCallingNode + weightNeighbourNode;
                sumNormal = sumNormal + ( weightCallingNode * normalCallingNode );
                sumNormal = sumNormal + ( weightNeighbourNode * normalNeighbourNode );
            }
        }

        // On divise la somme des normales par la somme des poids
        CT_Normal outNormal;
        for( int i = 0 ; i < 3 ; i++ )
        {
            outNormal[i] = sumNormal[i] / sumWeight;
        }
        outNormal.normalize();

        // On affecte la normale de sortie
        outNormalCloud->replaceNormal( index, outNormal );
    }
}

void NORM_NewOctreeNodeV2::setNormalsAttributeWithWendlandInterpolationRecursive(CT_PointsAttributesNormal* attributs)
{
    // Si on est sur une feuille et que la surface a ete fittee
    if( isLeaf() && _acpWasRan && _surfaceFittingWasRan )
    {
        setNormalsAttributeWithWendlandInterpolation( attributs );
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->setNormalsAttributeWithWendlandInterpolationRecursive( attributs );
            }
        }
    }
}

void NORM_NewOctreeNodeV2::setNormalsAttributeWithWendlandInterpolation(CT_PointsAttributesNormal* attributs)
{
    // On s'assure que l'acp et le fitting de surface ont bien ete fait pour ce noeud
    assert( _acpWasRan && _surfaceFittingWasRan );

    // On est oblige de passer par le abstractnormalcloud pour modifier les normales
    CT_AbstractNormalCloud* outNormalCloud = attributs->getNormalCloud();

    // On recupere tous les voisins de face
    QVector< NORM_NewOctreeNodeV2* > facesNei;
    QVector< faceRelativePosition > posNei;
    getFacesNeighbours( facesNei );

    // Ainsi que leur position face au noeud appelant
    foreach( NORM_NewOctreeNodeV2* n, facesNei )
    {
        posNei.push_back( getFaceRelativePosition( n ) );
    }

    // On va faire une interpolation lineaire pour tous les points du noeud appelant
    foreach( int index, (*_ptIndices) )
    {
        // On recupere le point dans la base cartesienne
        CT_Point curPoint = _pAccessor.pointAt( index );

        // Calcul de la normale pour le noeud appelant
        CT_Point normalCallingNode;
        computeNormalAtPoint( curPoint,
                              normalCallingNode );

        // On va parcourir tous les voisins pour faire l'interpolation
        float sumWeight = 0;
        CT_Point sumNormal;
        sumNormal.setValues(0,0,0);
        int nNei = facesNei.size();
        for( int i = 0 ; i < nNei ; i++ )
        {
            NORM_NewOctreeNodeV2* nei = facesNei.at(i);

            // On calcule le rayon d'interpolation :
            // On prend 1/3 de la taille de la cellule la plus petite entre les deux
            float nodesMinDim = std::min( _dimension, nei->_dimension );
            float localInterpolationRadius = nodesMinDim / 3.0;

            // On fait attention a ce que les voisins pris en compte ont bien ete fites eux aussi
            if( nei->_acpWasRan && nei->_surfaceFittingWasRan )
            {
                // Calcul des poids pour la paire de voisins
                float weightCallingNode = getWendlandInterpolationWeight( curPoint,
                                                                          posNei.at(i),
                                                                          localInterpolationRadius );

                float weightNeighbourNode = nei->getWendlandInterpolationWeight( curPoint,
                                                                                 posNei.at(i),
                                                                                 localInterpolationRadius );

                // Calcul de la normale pour le noeud voisin
                CT_Point normalNeighbourNode;
                nei->computeNormalAtPoint( curPoint,
                                           normalNeighbourNode );

                // On ajoute les normales ponderees par les poids trouves
                sumWeight += weightCallingNode + weightNeighbourNode;
                sumNormal = sumNormal + ( weightCallingNode * normalCallingNode );
                sumNormal = sumNormal + ( weightNeighbourNode * normalNeighbourNode );
            }
        }

        // On divise la somme des normales par la somme des poids
        CT_Normal outNormal;
        for( int i = 0 ; i < 3 ; i++ )
        {
            outNormal[i] = sumNormal[i] / sumWeight;
        }
        outNormal.normalize();

        // On affecte la normale de sortie
        outNormalCloud->replaceNormal( index, outNormal );
    }
}

float NORM_NewOctreeNodeV2::getLinearInterpolationWeight(CT_Point& pointInCartBasis,
                                                         faceRelativePosition relPos,
                                                         float interpolationRadius)
{
    float weight = 0;

    // On regarde la distance du point a la face correspondante
    float dist;
    switch ( relPos )
    {
        case Left :
        {
            dist = ( pointInCartBasis.x() - ( _centre.x() - ( _dimension / 2.0 ) ) );
            break;
        }
        case Right :
        {
            dist = ( ( _centre.x() + ( _dimension / 2.0 ) ) ) - pointInCartBasis.x();
            break;
        }
        case Back :
        {
            dist = ( pointInCartBasis.y() - ( _centre.y() - ( _dimension / 2.0 ) ) );
            break;
        }
        case Front :
        {
            dist = ( ( _centre.y() + ( _dimension / 2.0 ) ) ) - pointInCartBasis.y();
            break;
        }
        case Bot :
        {
            dist = ( pointInCartBasis.z() - ( _centre.z() - ( _dimension / 2.0 ) ) );
            break;
        }
        case Top :
        {
            dist = ( ( _centre.z() + ( _dimension / 2.0 ) ) ) - pointInCartBasis.z();
            break;
        }
    }

    // Le point est dans le noeud
    if( dist > 0 )
    {
        // Si la distance est superieure au rayon / 2 alors le point est impacte a fond
        if( dist > ( interpolationRadius / 2.0 ) )
        {
            weight = 1;
        }
        // Sinon le point est impacte, on interpole lineairement un poids entre 1 et 0.5
        else
        {
            weight = 0.5 + ( ( dist / ( interpolationRadius / 2.0 ) ) * 0.5 );
        }
    }
    // Ou le point est hors du noeud
    else
    {
        // Si la distance est superieure au rayon / 2 alors le point n'est pas impacte
        if( -dist > ( interpolationRadius / 2.0 ) )
        {
            weight = 0;
        }
        // Sinon le point est impacte, on interpole lineairement un poids entre 0 et 0.5
        else
        {
            weight = ( dist / ( interpolationRadius / 2.0 ) ) * 0.5;
        }
    }

    return weight;
}

float NORM_NewOctreeNodeV2::getWendlandInterpolationWeight(CT_Point& pointInCartBasis,
                                                           faceRelativePosition relPos,
                                                           float interpolationRadius)
{
    float weight = 0;

    // On regarde la distance du point a la face correspondante
    float dist;
    switch ( relPos )
    {
        case Left :
        {
            dist = ( pointInCartBasis.x() - ( _centre.x() - ( _dimension / 2.0 ) ) );
            break;
        }
        case Right :
        {
            dist = ( ( _centre.x() + ( _dimension / 2.0 ) ) ) - pointInCartBasis.x();
            break;
        }
        case Back :
        {
            dist = ( pointInCartBasis.y() - ( _centre.y() - ( _dimension / 2.0 ) ) );
            break;
        }
        case Front :
        {
            dist = ( ( _centre.y() + ( _dimension / 2.0 ) ) ) - pointInCartBasis.y();
            break;
        }
        case Bot :
        {
            dist = ( pointInCartBasis.z() - ( _centre.z() - ( _dimension / 2.0 ) ) );
            break;
        }
        case Top :
        {
            dist = ( ( _centre.z() + ( _dimension / 2.0 ) ) ) - pointInCartBasis.z();
            break;
        }
    }

    // Le point est dans le noeud
    if( dist > 0 )
    {
        float ratioRadiusSupport = dist / interpolationRadius;
        // Si la distance est superieure au rayon / 2 alors le point est impacte a fond
        if( dist > ( interpolationRadius / 2.0 ) )
        {
            weight = 1;
        }
        // Sinon le point est impacte, on interpole avec la fonction de wendland
        else
        {
            weight = ( (1 - ratioRadiusSupport )*(1 - ratioRadiusSupport )*(1 - ratioRadiusSupport )*(1 - ratioRadiusSupport )
                     *
                     (4 * ratioRadiusSupport + 1 ) );
        }
    }
    // Ou le point est hors du noeud
    else
    {
        float ratioRadiusSupport = -dist / interpolationRadius;
        // Si la distance est superieure au rayon / 2 alors le point n'est pas impacte
        if( -dist > ( interpolationRadius / 2.0 ) )
        {
            weight = 0;
        }
        // Sinon le point est impacte, on interpole avec la fonction de wendland
        else
        {
            weight = ( (1 - ratioRadiusSupport )*(1 - ratioRadiusSupport )*(1 - ratioRadiusSupport )*(1 - ratioRadiusSupport )
                     *
                     (4 * ratioRadiusSupport + 1 ) );
        }
    }

    return weight;
}

void NORM_NewOctreeNodeV2::getProjectedPointsRecursive(CT_AbstractUndefinedSizePointCloud* outProjectedPoints)
{
    // Si on est sur une feuille et que la surface a ete fittee
    if( isLeaf() && _acpWasRan && _surfaceFittingWasRan )
    {
        getProjectedPoints( outProjectedPoints );
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->getProjectedPointsRecursive( outProjectedPoints );
//                QFuture< void > future = QtConcurrent::run( _children[i], &NORM_NewOctreeNodeV2::getProjectedPointsRecursive, outProjectedPoints );
//                future.waitForFinished();
            }
        }
    }
}

void NORM_NewOctreeNodeV2::getProjectedPointsFromFileRecursive(CT_AbstractUndefinedSizePointCloud* outProjectedPoints)
{
    // Si on est sur une feuille et que la surface a ete fittee
    if( isLeaf() && _acpWasRan && _surfaceFittingWasRan )
    {
        getProjectedPointsFromFile( outProjectedPoints );
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->getProjectedPointsFromFileRecursive( outProjectedPoints );
            }
        }
    }
}

void NORM_NewOctreeNodeV2::getProjectedPoints(CT_AbstractUndefinedSizePointCloud* outProjectedPoints)
{
    // On s'assure que l'acp et le fitting de surface ont bien ete fait pour ce noeud
    assert( _acpWasRan && _surfaceFittingWasRan );

    // Calcule les matrices de passage cartesien vers acp et acp vers cartesien
    Eigen::Matrix3d toCartBasis;
    toCartBasis << _v1(0) , _v2(0) , _v3(0),
                   _v1(1) , _v2(1) , _v3(1),
                   _v1(2) , _v2(2) , _v3(2);
    Eigen::Matrix3d toPcaBasis = toCartBasis.inverse();

    foreach( int index, (*_ptIndices) )
    {
        // On accede au point
        CT_Point curPointInCartBasis = _pAccessor.pointAt( index );

        // On le transforme pour l'avoir dans la base de l'acp :
        // 1 - On considere le centroide du noeud comme l'origine du repere de l'acp donc on fait une translation
        // 2 - On applique la matrice de passage
        curPointInCartBasis = curPointInCartBasis - _centroid;
        CT_Point curPointInPcaBasis = toPcaBasis * curPointInCartBasis;

        // 3 - Le projette du point sur la surface a le meme x et y mais son z correspond au z de la surface en (x,y)
        curPointInPcaBasis.z() = ( _a * curPointInPcaBasis.x() * curPointInPcaBasis.x() +
                                   _b * curPointInPcaBasis.x() * curPointInPcaBasis.y() +
                                   _c * curPointInPcaBasis.y() * curPointInPcaBasis.y() +
                                   _d * curPointInPcaBasis.x() +
                                   _e * curPointInPcaBasis.y() +
                                   _f );

        CT_Point projectedPointInCartesianBasis = ( toCartBasis * curPointInPcaBasis );
        projectedPointInCartesianBasis = projectedPointInCartesianBasis + _centroid;

        outProjectedPoints->addPoint( projectedPointInCartesianBasis );
    }
}

void NORM_NewOctreeNodeV2::getProjectedPointsFromFile(CT_AbstractUndefinedSizePointCloud* outProjectedPoints)
{
    // On s'assure que l'acp et le fitting de surface ont bien ete fait pour ce noeud
    assert( _acpWasRan && _surfaceFittingWasRan );

    // Calcule les matrices de passage cartesien vers acp et acp vers cartesien
    Eigen::Matrix3d toCartBasis;
    toCartBasis << _v1(0) , _v2(0) , _v3(0),
                   _v1(1) , _v2(1) , _v3(1),
                   _v1(2) , _v2(2) , _v3(2);

    // On sample des points dans le plan xy de la base locale et on cherche son z
    int nPointsToBeSampled = ceil( sqrt(_nPointsForReconstruction) );
    float deltaX = ( _surfTop.x() - _surfBot.x() ) / (nPointsToBeSampled-1);
    float deltaY = ( _surfTop.y() - _surfBot.y() ) / (nPointsToBeSampled-1);
    for( int xx = 0 ; xx < nPointsToBeSampled ; xx++ )
    {
        for( int yy = 0 ; yy < nPointsToBeSampled ; yy++ )
        {
            CT_Point curPointInPcaBasis;
            curPointInPcaBasis.setValues( xx * deltaX + _surfBot.x(),
                                          yy * deltaY + _surfBot.y(),
                                          0 );
            curPointInPcaBasis.z() = ( _a * curPointInPcaBasis.x() * curPointInPcaBasis.x() +
                                       _b * curPointInPcaBasis.x() * curPointInPcaBasis.y() +
                                       _c * curPointInPcaBasis.y() * curPointInPcaBasis.y() +
                                       _d * curPointInPcaBasis.x() +
                                       _e * curPointInPcaBasis.y() +
                                       _f );

            CT_Point projectedPointInCartesianBasis = ( toCartBasis * curPointInPcaBasis );
            projectedPointInCartesianBasis = projectedPointInCartesianBasis + _centroid;

            if( containsPoint( projectedPointInCartesianBasis ) )
            {
                outProjectedPoints->addPoint( projectedPointInCartesianBasis );
            }
        }
    }
}

void NORM_NewOctreeNodeV2::getProjectedPointsInAcpBasisFromFile(QVector<CT_Point>& outProjectedPoints)
{
    // On s'assure que l'acp et le fitting de surface ont bien ete fait pour ce noeud
    assert( _acpWasRan && _surfaceFittingWasRan );

    // Calcule les matrices de passage cartesien vers acp et acp vers cartesien
    Eigen::Matrix3d toCartBasis;
    toCartBasis << _v1(0) , _v2(0) , _v3(0),
                   _v1(1) , _v2(1) , _v3(1),
                   _v1(2) , _v2(2) , _v3(2);
    Eigen::Matrix3d toPcaBasis = toCartBasis.inverse();

    foreach( int index, (*_ptIndices) )
    {
        // On accede au point
        CT_Point curPointInCartBasis = _pAccessor.pointAt( index );

        // On le transforme pour l'avoir dans la base de l'acp :
        // 1 - On considere le centroide du noeud comme l'origine du repere de l'acp donc on fait une translation
        // 2 - On applique la matrice de passage
        curPointInCartBasis = curPointInCartBasis - _centroid;
        CT_Point curPointInPcaBasis = toPcaBasis * curPointInCartBasis;

        // 3 - Le projette du point sur la surface a le meme x et y mais son z correspond au z de la surface en (x,y)
        curPointInPcaBasis.z() = ( _a * curPointInPcaBasis.x() * curPointInPcaBasis.x() +
                                   _b * curPointInPcaBasis.x() * curPointInPcaBasis.y() +
                                   _c * curPointInPcaBasis.y() * curPointInPcaBasis.y() +
                                   _d * curPointInPcaBasis.x() +
                                   _e * curPointInPcaBasis.y() +
                                   _f );

        outProjectedPoints.push_back( curPointInPcaBasis );
    }
}

void NORM_NewOctreeNodeV2::getProjectedPointsWithLinearInterpolationRecursive(CT_AbstractUndefinedSizePointCloud* outProjectedPoints,
                                                                              float interpolationRadius )
{
    // Si on est sur une feuille et que la surface a ete fittee
    if( isLeaf() && _acpWasRan && _surfaceFittingWasRan )
    {
        getProjectedPointsWithLinearInterpolation( outProjectedPoints, interpolationRadius );
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->getProjectedPointsWithLinearInterpolationRecursive( outProjectedPoints, interpolationRadius );
            }
        }
    }
}

void NORM_NewOctreeNodeV2::getProjectedPointsWithLinearInterpolation(CT_AbstractUndefinedSizePointCloud* outProjectedPoints,
                                                                     float interpolationRadius )
{
    // On s'assure que l'acp et le fitting de surface ont bien ete fait pour ce noeud
    assert( _acpWasRan && _surfaceFittingWasRan );

    // On recupere tous les voisins de face
    QVector< NORM_NewOctreeNodeV2* > facesNei;
    QVector< faceRelativePosition > posNei;
    getFacesNeighbours( facesNei );

    // Ainsi que leur position face au noeud appelant
    foreach( NORM_NewOctreeNodeV2* n, facesNei )
    {
        posNei.push_back( getFaceRelativePosition( n ) );
    }

    // On va faire une interpolation lineaire pour tous les points du noeud appelant
    foreach( int index, (*_ptIndices) )
    {
        // On recupere le point projete sur la surface du noeud apellant
        CT_Point curPointCartBasis = _pAccessor.pointAt( index );
        CT_Point curProjectedPoint;
        computeProjectedPoint( curPointCartBasis,
                               curProjectedPoint );

        // On va parcourir tous les voisins pour faire l'interpolation
        float sumWeight = 0;
        CT_Point sumPoint;
        sumPoint.setValues(0,0,0);
        int nNei = facesNei.size();
        for( int i = 0 ; i < nNei ; i++ )
        {
            NORM_NewOctreeNodeV2* nei = facesNei.at(i);

            // On fait attention a ce que les voisins pris en compte ont bien ete fites eux aussi
            if( nei->_acpWasRan && nei->_surfaceFittingWasRan )
            {
                // Calcul des poids pour la paire de voisins
                float weightCallingNode = getLinearInterpolationWeight( curPointCartBasis,
                                                                        posNei.at(i),
                                                                        interpolationRadius );

                float weightNeighbourNode = nei->getLinearInterpolationWeight( curPointCartBasis,
                                                                               posNei.at(i),
                                                                               interpolationRadius );

                // Calcul du point projete dans le voisin
                CT_Point curProjectedPointNei;
                nei->computeProjectedPoint( curPointCartBasis,
                                            curProjectedPointNei );

                // On ajoute les normales ponderees par les poids trouves
                sumWeight += weightCallingNode + weightNeighbourNode;
                sumPoint = sumPoint + ( weightCallingNode * curProjectedPoint );
                sumPoint = sumPoint + ( weightNeighbourNode * curProjectedPointNei );
            }
        }

        // On divise la somme des normales par la somme des poids
        CT_Point outPoint;
        for( int i = 0 ; i < 3 ; i++ )
        {
            outPoint[i] = ( sumPoint[i] / sumWeight );
        }

        // On affecte la normale de sortie
        outProjectedPoints->addPoint( outPoint );
    }
}

void NORM_NewOctreeNodeV2::setDistanceFromPointsToSurfaceRecursive(CT_PointsAttributesScalarTemplated<float>* attributs)
{
    // Si on est sur une feuille et que la surface a ete fittee
    if( isLeaf() && _acpWasRan && _surfaceFittingWasRan )
    {
        setDistanceFromPointsToSurface( attributs );
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->setDistanceFromPointsToSurfaceRecursive( attributs );
//                QFuture< void > future = QtConcurrent::run( _children[i], &NORM_NewOctreeNodeV2::setDistanceFromPointToSurfaceRecursive, attributs );
//                future.waitForFinished();
            }
        }
    }
}

void NORM_NewOctreeNodeV2::setDistanceFromPointsToSurface(CT_PointsAttributesScalarTemplated<float>* attributs)
{
    foreach( int index, (*_ptIndices) )
    {
        attributs->setValueAt( index, getDistanceFromPointToSurface( _pAccessor.pointAt(index) ) );
    }
}

float NORM_NewOctreeNodeV2::getDistanceFromPointToSurface(const CT_Point& p)
{
    // On s'assure que l'acp et le fitting de surface ont bien ete fait pour ce noeud
    assert( _acpWasRan && _surfaceFittingWasRan );

    // Calcule les matrices de passage cartesien vers acp et acp vers cartesien
    Eigen::Matrix3d toCartBasis;
    toCartBasis << _v1(0) , _v2(0) , _v3(0),
                   _v1(1) , _v2(1) , _v3(1),
                   _v1(2) , _v2(2) , _v3(2);
    Eigen::Matrix3d toPcaBasis = toCartBasis.inverse();

    // On cherche p dans la base de l'acp :
    // 1 - On considere le centroide du noeud comme l'origine du repere de l'acp donc on fait une translation
    // 2 - On applique la matrice de passage
    CT_Point pInCartBasis = p - _centroid;
    CT_Point curPointInPcaBasis = toPcaBasis * pInCartBasis;

    // 3 - Le projette du point sur la surface a le meme x et y mais son z correspond au z de la surface en (x,y)
    float surfaceHeight = ( _a * curPointInPcaBasis.x() * curPointInPcaBasis.x() +
                            _b * curPointInPcaBasis.x() * curPointInPcaBasis.y() +
                            _c * curPointInPcaBasis.y() * curPointInPcaBasis.y() +
                            _d * curPointInPcaBasis.x() +
                            _e * curPointInPcaBasis.y() +
                            _f );

    return ( curPointInPcaBasis.z() - surfaceHeight );
}

float NORM_NewOctreeNodeV2::getMeanSignedDistanceFromPointToSurface()
{
    // On s'assure que l'acp et le fitting de surface ont bien ete fait pour ce noeud
    assert( _acpWasRan && _surfaceFittingWasRan );

    float meanDistance = 0;

    foreach( int index, (*_ptIndices) )
    {
        float curDistance = getDistanceFromPointToSurface( _pAccessor.pointAt( index ) );
        meanDistance += curDistance;
    }

    meanDistance /= _ptIndices->size();

    return meanDistance;
}

float NORM_NewOctreeNodeV2::getMeanAbsoluteDistanceFromPointToSurface()
{
    // On s'assure que l'acp et le fitting de surface ont bien ete fait pour ce noeud
    assert( _acpWasRan && _surfaceFittingWasRan );

    float meanDistance = 0;

    foreach( int index, (*_ptIndices) )
    {
        float curDistance = getDistanceFromPointToSurface( _pAccessor.pointAt( index ) );
        meanDistance += fabs( curDistance );
    }

    meanDistance /= _ptIndices->size();

    return meanDistance;
}

bool NORM_NewOctreeNodeV2::getSignOfPoint(const CT_Point& p)
{
    // On s'assure que l'acp et le fitting de surface ont bien ete fait pour ce noeud
    assert( _acpWasRan && _surfaceFittingWasRan );

    // Calcule les matrices de passage cartesien vers acp et acp vers cartesien
    Eigen::Matrix3d toCartBasis;
    toCartBasis << _v1(0) , _v2(0) , _v3(0),
                   _v1(1) , _v2(1) , _v3(1),
                   _v1(2) , _v2(2) , _v3(2);
    Eigen::Matrix3d toPcaBasis = toCartBasis.inverse();

    // On cherche p dans la base de l'acp :
    // 1 - On considere le centroide du noeud comme l'origine du repere de l'acp donc on fait une translation
    // 2 - On applique la matrice de passage
    CT_Point pInCartBasis = p - _centroid;
    CT_Point curPointInPcaBasis = toPcaBasis * pInCartBasis;

    // 3 - Le projette du point sur la surface a le meme x et y mais son z correspond au z de la surface en (x,y)
    float surfaceHeight = ( _a * curPointInPcaBasis.x() * curPointInPcaBasis.x() +
                            _b * curPointInPcaBasis.x() * curPointInPcaBasis.y() +
                            _c * curPointInPcaBasis.y() * curPointInPcaBasis.y() +
                            _d * curPointInPcaBasis.x() +
                            _e * curPointInPcaBasis.y() +
                            _f );

    return ( curPointInPcaBasis.z() > surfaceHeight );
}

void NORM_NewOctreeNodeV2::setCurvaturesRecursive(CT_PointsAttributesScalarTemplated<float>* outGaussianCurvature,
                                                  CT_PointsAttributesNormal* outDir1,
                                                  CT_PointsAttributesNormal* outDir2)
{
    // Si on est sur une feuille et que la surface a ete fittee
    if( isLeaf() && _acpWasRan && _surfaceFittingWasRan )
    {
        setCurvatures( outGaussianCurvature,
                       outDir1,
                       outDir2 );
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->setCurvaturesRecursive( outGaussianCurvature,
                                                      outDir1,
                                                      outDir2 );
            }
        }
    }
}

void NORM_NewOctreeNodeV2::setCurvatures(CT_PointsAttributesScalarTemplated<float>* outGaussianCurvature,
                                         CT_PointsAttributesNormal* outDir1,
                                         CT_PointsAttributesNormal* outDir2)
{
    // On est oblige de passer par le abstractnormalcloud pour modifier les normales
    CT_AbstractNormalCloud* outDir1Cloud = outDir1->getNormalCloud();
    CT_AbstractNormalCloud* outDir2Cloud = outDir2->getNormalCloud();

    // Calcule les matrices de passage cartesien vers acp et acp vers cartesien
    Eigen::Matrix3d toCartBasis;
    toCartBasis << _v1(0) , _v2(0) , _v3(0),
                   _v1(1) , _v2(1) , _v3(1),
                   _v1(2) , _v2(2) , _v3(2);
    Eigen::Matrix3d toPcaBasis = toCartBasis.inverse();

    foreach( int index, (*_ptIndices) )
    {
        // On accede au point dans la base de la pca
        CT_Point pInCartBasis = _pAccessor.pointAt( index ) - _centroid;
        CT_Point pInPcaBasis = toPcaBasis * pInCartBasis;

        // On calcule la normale du point projete sur la surface (projection selon l'axe z simplement)
        // Et la base du plan tangent
        CT_Point dfdx;
        dfdx(0) = 1;
        dfdx(1) = 0;
        dfdx(2) = ( 2 * _a * pInPcaBasis.x() ) + ( _b * pInPcaBasis.y() ) + _d;
        dfdx.normalize();
        CT_Point dfdy;
        dfdy(0) = 0;
        dfdy(1) = 1;
        dfdy(2) = ( 2 * _c * pInPcaBasis.y() ) + ( _b * pInPcaBasis.x() ) + _e;
        dfdy.normalize();
        CT_Point curNormalInPcaBasis;
        curNormalInPcaBasis(0) = ( 2 * _a * pInPcaBasis.x() ) + ( _b * pInPcaBasis.y() ) + _d;
        curNormalInPcaBasis(1) = ( 2 * _c * pInPcaBasis.y() ) + ( _b * pInPcaBasis.x() ) + _e;
        curNormalInPcaBasis(2) = -1;

        curNormalInPcaBasis.normalize();

        // On calcule les courbures
        Eigen::Vector2d dir1;
        float k1;
        Eigen::Vector2d dir2;
        float k2;
        getCurvatureAtPoint( pInPcaBasis,
                             curNormalInPcaBasis,
                             dir1, k1,
                             dir2, k2 );

        // On remet les directions de courbures dans R3 (jusque la elles sont dans le plan tangent)
        CT_Point finalDir1;
        CT_Point finalDir2;
        finalDir1 = ( dir1.x() * dfdx ) + ( dir1.y() * dfdy );
        finalDir2 = ( dir2.x() * dfdx ) + ( dir2.y() * dfdy );

        // Normalise
        finalDir1 = toCartBasis * finalDir1;
        finalDir2 = toCartBasis * finalDir2;
        finalDir1.normalize();
        finalDir2.normalize();

        // Transforme en CT_Normal
        CT_Normal ndir1;
        ndir1.set( finalDir1.x(), finalDir1.y(), finalDir1.z(), 0 );
        CT_Normal ndir2;
        ndir2.set( finalDir2.x(), finalDir2.y(), finalDir2.z(), 0 );

        // Que l'on ajoute au tableau de sortie
        outGaussianCurvature->setValueAt( index, k1*k2 );
        outDir1Cloud->replaceNormal( index, ndir1 );
        outDir2Cloud->replaceNormal( index, ndir2 );
    }
}

void NORM_NewOctreeNodeV2::setGaussianCurvatureRecursive(CT_PointsAttributesScalarTemplated<float>* attributs, float& outMinCurvature, float& outMaxCurvature )
{
    // Si on est sur une feuille et que la surface a ete fittee
    if( isLeaf() && _acpWasRan && _surfaceFittingWasRan )
    {
        setGaussianCurvature( attributs, outMinCurvature, outMaxCurvature );
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->setGaussianCurvatureRecursive( attributs, outMinCurvature, outMaxCurvature );
//                QFuture< void > future = QtConcurrent::run( _children[i], &NORM_NewOctreeNodeV2::setGaussianCurvatureRecursive, attributs );
//                future.waitForFinished();
            }
        }
    }
}

void NORM_NewOctreeNodeV2::setGaussianCurvature(CT_PointsAttributesScalarTemplated<float>* attributs, float& outMinCurvature, float& outMaxCurvature)
{
    // On calcule les courbures
    QVector<CT_Point> dir1;
    QVector<float> k1;
    QVector<CT_Point> dir2;
    QVector<float> k2;
    getCurvatures( dir1, k1, dir2, k2 );

    // Pour chaque point on calcule la courbure gaussienne
    int nPoints = _ptIndices->size();
    for( int i = 0 ; i < nPoints ; i++ )
    {
        attributs->setValueAt( _ptIndices->at(i), k1.at(i)*k2.at(i) );

        if( k1.at(i)*k2.at(i) < outMinCurvature )
        {
            outMinCurvature = k1.at(i)*k2.at(i);
        }
        if( k1.at(i)*k2.at(i) > outMaxCurvature )
        {
            outMaxCurvature = k1.at(i)*k2.at(i);
        }
    }
}

void NORM_NewOctreeNodeV2::getCurvatures(QVector<CT_Point>& outDir1,
                                         QVector<float>& outK1,
                                         QVector<CT_Point>& outDir2,
                                         QVector<float>& outK2)
{
    // Calcule les matrices de passage cartesien vers acp et acp vers cartesien
    Eigen::Matrix3d toCartBasis;
    toCartBasis << _v1(0) , _v2(0) , _v3(0),
                   _v1(1) , _v2(1) , _v3(1),
                   _v1(2) , _v2(2) , _v3(2);
    Eigen::Matrix3d toPcaBasis = toCartBasis.inverse();

    foreach( int index, (*_ptIndices) )
    {
        // On accede au point dans la base de la pca
        CT_Point pInCartBasis = _pAccessor.pointAt( index ) - _centroid;
        CT_Point pInPcaBasis = toPcaBasis * pInCartBasis;

        // On calcule la normale du point projete sur la surface (projection selon l'axe z simplement)
        // Et la base du plan tangent
        CT_Point dfdx;
        dfdx(0) = 1;
        dfdx(1) = 0;
        dfdx(2) = ( 2 * _a * pInPcaBasis.x() ) + ( _b * pInPcaBasis.y() ) + _d;
        CT_Point dfdy;
        dfdy(0) = 0;
        dfdy(1) = 1;
        dfdy(2) = ( 2 * _c * pInPcaBasis.y() ) + ( _b * pInPcaBasis.x() ) + _e;
        CT_Point curNormalInPcaBasis;
        curNormalInPcaBasis(0) = ( 2 * _a * pInPcaBasis.x() ) + ( _b * pInPcaBasis.y() ) + _d;
        curNormalInPcaBasis(1) = ( 2 * _c * pInPcaBasis.y() ) + ( _b * pInPcaBasis.x() ) + _e;
        curNormalInPcaBasis(2) = -1;

        curNormalInPcaBasis.normalize();

        // On calcule les courbures
        Eigen::Vector2d dir1;
        float k1;
        Eigen::Vector2d dir2;
        float k2;
        getCurvatureAtPoint( pInPcaBasis,
                             curNormalInPcaBasis,
                             dir1, k1,
                             dir2, k2 );

        // On remet les directions de courbures dans R3 (jusque la elles sont dans le plan tangent)
        CT_Point finalDir1;
        CT_Point finalDir2;
        finalDir1 = ( dir1.x() * dfdx ) + ( dir1.y() * dfdy );
        finalDir2 = ( dir2.x() * dfdx ) + ( dir2.y() * dfdy );

        // Normalise
        finalDir1.normalize();
        finalDir2.normalize();

        // Que l'on ajoute au tableau de sortie
        outDir1.push_back( finalDir1 );
        outK1.push_back( k1 );
        outDir2.push_back( finalDir2 );
        outK2.push_back( k2 );
    }
}

void NORM_NewOctreeNodeV2::getCurvatureAtPoint(CT_Point& pInAcpBasis,
                                               CT_Point& normalAtPInAcpBasis,
                                               Eigen::Vector2d& outDir1,
                                               float& outK1,
                                               Eigen::Vector2d& outDir2,
                                               float& outK2 )
{
    // Calcule la premiere forme fondamentale
    Eigen::Matrix2d forme1;
    getPremiereFormeFondamentale( forme1, pInAcpBasis );

    // Calcule la seconde forme fondamentale
    Eigen::Matrix2d forme2;
    getSecondeFormeFondamentale( forme2, normalAtPInAcpBasis );

    // Calcule le tenseur de courbure
    Eigen::Matrix2d forme1Inv = forme1.inverse();
    Eigen::Matrix2d tenseurCourbure = forme1Inv * forme2;

    // Les courbure et les directions de courbures sont les valeurs propres et les vecteurs propre du tenseur
    // Compute eigenvalues and eigenvectors
    Eigen::EigenSolver< Eigen::Matrix2d > eigenSolver;
    eigenSolver.compute( tenseurCourbure );

    // Get the eigenvalues and eigenvectors
    Eigen::Vector2cd eigenValues = eigenSolver.eigenvalues();   // Each row of the vector is an eigenvalue of the covariance matrix
    Eigen::Matrix2cd eigenVectors = eigenSolver.eigenvectors(); // The columnsof this matrix are the associated eigenvectors

    // Sort eigenvalues and eigenvectors in decrescent order
    sortEigenValues( eigenValues, eigenVectors );

    // Parse the results into float and CT_Point
    // Eigenvalues of a covariancematrix are always real
    outK1 = eigenValues(0).real();
    outK2 = eigenValues(1).real();

    for ( int i = 0 ; i < 2 ; i++ )
    {
        outDir1(i) = eigenVectors(i,0).real();
        outDir2(i) = eigenVectors(i,1).real();
    }
}

void NORM_NewOctreeNodeV2::getPremiereFormeFondamentale(Eigen::Matrix2d& outputMatrix,
                                                        CT_Point& pInAcpBasis)
{
    // (2ax+by+d)^2
    float calc1 = ( (2*_a*pInAcpBasis.x()) + (_b*pInAcpBasis.y()) + _d );

    // (2cy+bx+e)^2
    float calc2 = ( (2*_c*pInAcpBasis.y()) + (_b*pInAcpBasis.x()) + _e );

    outputMatrix( 0, 0 ) = 1 + (calc1*calc1);
    outputMatrix( 0, 1 ) = calc1*calc2;
    outputMatrix( 1, 0 ) = calc1*calc2;
    outputMatrix( 1, 1 ) = 1 + (calc2*calc2);
}

void NORM_NewOctreeNodeV2::getSecondeFormeFondamentale(Eigen::Matrix2d& outputMatrix,
                                                       CT_Point& normalAtPointInAcpBasis)
{
    outputMatrix( 0, 0 ) = 2*_a*normalAtPointInAcpBasis.z();
    outputMatrix( 0, 1 ) = _b*normalAtPointInAcpBasis.z();
    outputMatrix( 1, 0 ) = _b*normalAtPointInAcpBasis.z();
    outputMatrix( 1, 1 ) = 2*_c*normalAtPointInAcpBasis.z();
}

void NORM_NewOctreeNodeV2::sortEigenValues(Eigen::Vector2cd& eigenValues,
                                              Eigen::Matrix2cd& eigenVectors)
{
    if( fabs( eigenValues(0).real() ) < fabs( eigenValues(1).real() ) )
    {
        // Ordre decroissant il faut switcher
        std::complex<float> swapVal;            // swap value for permutation
        Eigen::Vector2cd    swapVec;            // swap vector for permutation

        swapVal = eigenValues(0);
        swapVec = eigenVectors.col(0);

        eigenValues(0) = eigenValues(1);
        eigenVectors.col(0) = eigenVectors.col(1);

        eigenValues(1) = swapVal;
        eigenVectors.col(1) = swapVec;
    }
}

void NORM_NewOctreeNodeV2::getNodeContainingPointRecursive(const CT_Point& querryPoint,
                                                           NORM_NewOctreeNodeV2*& outputNode,
                                                           int &outputLevel,
                                                           int curLevel,
                                                           int level)
{
    if( !_octree->boundingCubeContainsPoint( querryPoint ) )
    {
        // Si le point cherche n'est pas dans l'octree on renvoie NULL
        outputNode = NULL;
        outputLevel = -1;
        return;
    }

    // Si le noeud est une feuille
    if( isLeaf() || curLevel == level )
    {
        // La recursion se termine
        outputNode = this;
        outputLevel = curLevel;
        return;
    }

    // Si le noeud n'est pas une feuille
    // 1 - On calcule le fils contenant le point demande
    int childID = getChildIdForPoint( querryPoint );

    // 2 - On regarde si ce fils existe
    if( _children[childID] == NULL )
    {
        // Si il n'existe pas on renvoie NULL
        outputNode = NULL;
        outputLevel = -1;
        return;
    }
    else
    {
        // Si il existe, on continue la recursion
        _children[ childID ]->getNodeContainingPointRecursive( querryPoint,
                                                               outputNode,
                                                               outputLevel,
                                                               curLevel+1,
                                                               level );
//        QFuture< void > future = QtConcurrent::run( _children[childID],
//        future.waitForFinished();
//                                                    &NORM_NewOctreeNodeV2::getNodeContainingPointRecursive,
//                                                    querryPoint,
//                                                    outputNode,
//                                                    outputLevel,
//                                                    curLevel+1,
//                                                    level );
        return;
    }
}

NORM_NewOctreeNodeV2* NORM_NewOctreeNodeV2::getNeighbourAtLevel(bool directionx,
                                                                bool directiony,
                                                                bool directionz,
                                                                bool usex,
                                                                bool usey,
                                                                bool usez,
                                                                int level,
                                                                int& outputLevel)
{
    if( !usex && !usey && !usez )
    {
        qDebug() << "Erreur dans getNeighbourAtLevel : pas de direction donnee";
        assert(false);
    }

    // Regarde le centre du voisin
    CT_Point centreOfNeighbour;
    centreOfNeighbour = _centre;


    if( usex )
    {
        if( directionx )
        {
            centreOfNeighbour.x() += _dimension;
        }
        else
        {
            centreOfNeighbour.x() -= _dimension;
        }
    }

    if( usey )
    {
        if( directiony )
        {
            centreOfNeighbour.y() += _dimension;
        }
        else
        {
            centreOfNeighbour.y() -= _dimension;
        }
    }

    if( usez )
    {
        if( directionz )
        {
            centreOfNeighbour.z() += _dimension;
        }
        else
        {
            centreOfNeighbour.z() -= _dimension;
        }
    }

    // Cherche le noeud de meme profondeur (level) contenant le point
    NORM_NewOctreeNodeV2* rslt;
    _octree->getRoot()->getNodeContainingPointRecursive( centreOfNeighbour,
                                                         rslt,
                                                         outputLevel,
                                                         0,
                                                         level );

    return rslt;
}

void NORM_NewOctreeNodeV2::getAllNeighbours(QVector<NORM_NewOctreeNodeV2*>& outputNeighbours)
{
    // Ajoute les voisins de face
    getFacesNeighbours( outputNeighbours );

    // Ajoute les voisins d'arete
    getEdgesNeighbours( outputNeighbours );

    // Ajoute les voisins de sommet
    getVertexNeighbours( outputNeighbours );
}

void NORM_NewOctreeNodeV2::getLeavesNeighboursRecursive()
{
    // Si le noeud appelant est une feuille alors on calcule ses voisins et on termine la recursion
    if( isLeaf() )
    {
        // Alloue le tableau de voisins
        _neighbours = new QVector< NORM_NewOctreeNodeV2* >();
        getAllNeighbours( *_neighbours );
        return;
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->getLeavesNeighboursRecursive();
//                QFuture< void > future = QtConcurrent::run( _children[i], &NORM_NewOctreeNodeV2::getLeavesNeighboursRecursive );
//                future.waitForFinished();
            }
        }
    }
}

void NORM_NewOctreeNodeV2::getFacesNeighbours(QVector<NORM_NewOctreeNodeV2*>& outputNeighbours)
{
    // Ajoute les 6 voisins de face

    // Ajoute les voisins gauche
    getNeighboursLeavesAtLevel( 0, 0, 0,
                                1, 0, 0,
                                outputNeighbours );
    if( outputNeighbours.contains( this ) )
    {
        qDebug() << "1";
        assert(false);
    }

    // Ajoute les voisins droite
    getNeighboursLeavesAtLevel( 1, 0, 0,
                                1, 0, 0,
                                outputNeighbours );
    if( outputNeighbours.contains( this ) )
    {
        qDebug() << "2";
        assert(false);
    }

    // Ajoute les voisins back
    getNeighboursLeavesAtLevel( 0, 0, 0,
                                0, 1, 0,
                                outputNeighbours );
    if( outputNeighbours.contains( this ) )
    {
        qDebug() << "3";
        assert(false);
    }

    // Ajoute les voisins front
    getNeighboursLeavesAtLevel( 0, 1, 0,
                                0, 1, 0,
                                outputNeighbours );
    if( outputNeighbours.contains( this ) )
    {
        qDebug() << "4";
        assert(false);
    }

    // Ajoute les voisins bot
    getNeighboursLeavesAtLevel( 0, 0, 0,
                                0, 0, 1,
                                outputNeighbours );
    if( outputNeighbours.contains( this ) )
    {
        qDebug() << "5";
        assert(false);
    }

    // Ajoute les voisins top
    getNeighboursLeavesAtLevel( 0, 0, 1,
                                0, 0, 1,
                                outputNeighbours );
    if( outputNeighbours.contains( this ) )
    {
        qDebug() << "6";
        assert(false);
    }
}

void NORM_NewOctreeNodeV2::getFacesNeighboursAroundv3(QVector<NORM_NewOctreeNodeV2*>& outputNeighbours)
{
    bool xPlanes = true;
    bool yPlanes = true;
    bool zPlanes = true;
    // On regarde la direction principale de v3 pour eliminer des faces
    if( fabs(_v3.x()) > fabs(_v3.y()) && fabs(_v3.x()) > fabs(_v3.z()) )
    {
        xPlanes = false;
    }
    else if( fabs(_v3.y()) > fabs(_v3.x()) && fabs(_v3.y()) > fabs(_v3.z()) )
    {
        yPlanes = false;
    }
    else
    {
        zPlanes = false;
    }

    if( xPlanes )
    {
        // Ajoute les voisins gauche
        getNeighboursLeavesAtLevel( 0, 0, 0,
                                    1, 0, 0,
                                    outputNeighbours );
        if( outputNeighbours.contains( this ) )
        {
            qDebug() << "1";
            assert(false);
        }

        // Ajoute les voisins droite
        getNeighboursLeavesAtLevel( 1, 0, 0,
                                    1, 0, 0,
                                    outputNeighbours );
        if( outputNeighbours.contains( this ) )
        {
            qDebug() << "2";
            assert(false);
        }
    }

    if( yPlanes )
    {
        // Ajoute les voisins back
        getNeighboursLeavesAtLevel( 0, 0, 0,
                                    0, 1, 0,
                                    outputNeighbours );
        if( outputNeighbours.contains( this ) )
        {
            qDebug() << "3";
            assert(false);
        }

        // Ajoute les voisins front
        getNeighboursLeavesAtLevel( 0, 1, 0,
                                    0, 1, 0,
                                    outputNeighbours );
        if( outputNeighbours.contains( this ) )
        {
            qDebug() << "4";
            assert(false);
        }
    }

    if( zPlanes )
    {
        // Ajoute les voisins bot
        getNeighboursLeavesAtLevel( 0, 0, 0,
                                    0, 0, 1,
                                    outputNeighbours );
        if( outputNeighbours.contains( this ) )
        {
            qDebug() << "5";
            assert(false);
        }

        // Ajoute les voisins top
        getNeighboursLeavesAtLevel( 0, 0, 1,
                                    0, 0, 1,
                                    outputNeighbours );
        if( outputNeighbours.contains( this ) )
        {
            qDebug() << "6";
            assert(false);
        }
    }
}

void NORM_NewOctreeNodeV2::getEdgesNeighbours(QVector<NORM_NewOctreeNodeV2*>& outputNeighbours)
{
    // Ajoute les 12 voisins d'aretes

    // Ajoute les voisins gauche bot
    getNeighboursLeavesAtLevel( 0, 0, 0,
                                1, 0, 1,
                                outputNeighbours );

    // Ajoute les voisins gauche top
    getNeighboursLeavesAtLevel( 0, 0, 1,
                                1, 0, 1,
                                outputNeighbours );

    // Ajoute les voisins droite bot
    getNeighboursLeavesAtLevel( 1, 0, 0,
                                1, 0, 1,
                                outputNeighbours );

    // Ajoute les voisins droite top
    getNeighboursLeavesAtLevel( 1, 0, 1,
                                1, 0, 1,
                                outputNeighbours );

    // Ajoute les voisins back bot
    getNeighboursLeavesAtLevel( 0, 0, 0,
                                0, 1, 1,
                                outputNeighbours );

    // Ajoute les voisins back top
    getNeighboursLeavesAtLevel( 0, 0, 1,
                                0, 1, 1,
                                outputNeighbours );

    // Ajoute les voisins front bot
    getNeighboursLeavesAtLevel( 0, 1, 0,
                                0, 1, 1,
                                outputNeighbours );

    // Ajoute les voisins front top
    getNeighboursLeavesAtLevel( 0, 1, 1,
                                0, 1, 1,
                                outputNeighbours );

    // Ajoute les voisins gauche back
    getNeighboursLeavesAtLevel( 0, 0, 0,
                                1, 1, 0,
                                outputNeighbours );

    // Ajoute les voisins gauche front
    getNeighboursLeavesAtLevel( 0, 1, 0,
                                1, 1, 0,
                                outputNeighbours );

    // Ajoute les voisins droite back
    getNeighboursLeavesAtLevel( 1, 0, 0,
                                1, 1, 0,
                                outputNeighbours );

    // Ajoute les voisins droite front
    getNeighboursLeavesAtLevel( 1, 1, 0,
                                1, 1, 0,
                                outputNeighbours );
}

void NORM_NewOctreeNodeV2::getVertexNeighbours(QVector<NORM_NewOctreeNodeV2*>& outputNeighbours)
{
    // Ajoute les 8 voisins de sommet

    // Ajoute les voisins gauche back bot
    getNeighboursLeavesAtLevel( 0, 0, 0,
                                1, 1, 1,
                                outputNeighbours );

    // Ajoute les voisins gauche back top
    getNeighboursLeavesAtLevel( 0, 0, 1,
                                1, 1, 1,
                                outputNeighbours );

    // Ajoute les voisins droite back bot
    getNeighboursLeavesAtLevel( 1, 0, 0,
                                1, 1, 1,
                                outputNeighbours );

    // Ajoute les voisins droite back top
    getNeighboursLeavesAtLevel( 1, 0, 1,
                                1, 1, 1,
                                outputNeighbours );

    // Ajoute les voisins gauche front bot
    getNeighboursLeavesAtLevel( 0, 1, 0,
                                1, 1, 1,
                                outputNeighbours );

    // Ajoute les voisins gauche front top
    getNeighboursLeavesAtLevel( 0, 1, 1,
                                1, 1, 1,
                                outputNeighbours );

    // Ajoute les voisins droite front bot
    getNeighboursLeavesAtLevel( 1, 1, 0,
                                1, 1, 1,
                                outputNeighbours );

    // Ajoute les voisins droite front top
    getNeighboursLeavesAtLevel( 1, 1, 1,
                                1, 1, 1,
                                outputNeighbours );
}

void NORM_NewOctreeNodeV2::getNeighboursLeavesAtLevel(bool directionx,
                                                      bool directiony,
                                                      bool directionz,
                                                      bool usex,
                                                      bool usey,
                                                      bool usez,
                                                      QVector<NORM_NewOctreeNodeV2*>& outputNeighbours )
{
    // 1 - On recupere le voisin de meme niveau dans la direction souhaitee
    int neiLevel = -2;
    NORM_NewOctreeNodeV2* rNei = getNeighbourAtLevel( directionx, directiony, directionz,
                                                      usex, usey, usez,
                                                      _depth,
                                                      neiLevel );

    // 2 - On recupere tous les fils de ce voisin dans la direction opposee a celle demandee
    if( rNei != NULL )
    {
        rNei->getLeavesFromNodeInDirectionRecursive( !directionx, !directiony, !directionz,
                                                     usex, usey, usez,
                                                     outputNeighbours );
    }
}

void NORM_NewOctreeNodeV2::getLeavesFromNodeInDirectionRecursive(bool directionx, bool directiony, bool directionz,
                                                                 bool usex, bool usey, bool usez,
                                                                 QVector<NORM_NewOctreeNodeV2*>& outputLeaves)
{
    // Si le noeud est une feuille on l'ajoute au resultat et on termine la recurrence
    if( isLeaf() )
    {
        outputLeaves.push_back( this );
        return;
    }

    // Sinon on cherche les enfants dans la direction souhaitee
    QVector<int> childrenInDir;
    getChildrenIdInDirections( directionx, directiony, directionz,
                               usex, usey, usez,
                               childrenInDir );

    // Et on continue la recurrence sur ces enfants la
    foreach( int childIndex, childrenInDir )
    {
        if( _children[ childIndex ] != NULL )
        {
            _children[ childIndex ]->getLeavesFromNodeInDirectionRecursive( directionx, directiony, directionz,
                                                                            usex, usey, usez,
                                                                            outputLeaves );
//            QFuture< void > future = QtConcurrent::run( _children[ childIndex ],
//            future.waitForFinished();
//                                                        &NORM_NewOctreeNodeV2::getLeavesFromNodeInDirectionRecursive,
//                                                        directionx, directiony, directionz,
//                                                        usex, usey, usez,
//                                                        outputLeaves );
        }
    }
}

void NORM_NewOctreeNodeV2::getChildrenIdInDirections(bool directionx, bool directiony, bool directionz,
                                                     bool usex, bool usey, bool usez,
                                                     QVector<int>& outputChildrenId)
{
    // On fait attention d'avoir une direction indiquee
    if( !usex && !usey && !usez )
    {
        // Ne devrait jamais arriver
        qDebug() << "Error : getChildInDirections with no direction";
        assert(false);
    }

    for( int i = 0 ; i < 8 ; i++ )
    {
        bool accept = true;
        if( usex )
        {
            // Teste l'egalite du bit x avec la direction souhaitee
            if( valueOfIthBit( i, 2 ) != directionx )
            {
                // Les bits sont differents alors on n'accepte pas cet enfant (il n'est pas dans la direction souhaitee)
                accept = false;
            }

        }
        if( usey )
        {
            // Teste l'egalite du bit y avec la direction souhaitee
            if( valueOfIthBit( i, 1 ) != directiony )
            {
                // Les bits sont differents alors on n'accepte pas cet enfant (il n'est pas dans la direction souhaitee)
                accept = false;
            }
        }
        if( usez )
        {
            // Teste l'egalite du bit z avec la direction souhaitee
            if( valueOfIthBit( i, 0 ) != directionz )
            {
                // Les bits sont differents alors on n'accepte pas cet enfant (il n'est pas dans la direction souhaitee)
                accept = false;
            }
        }
        // Si le fils etait dans la bonne direction
        if( accept )
        {
            // On l'ajoute au tableau
            outputChildrenId.push_back( i );
        }
    }
}

faceRelativePosition NORM_NewOctreeNodeV2::getFaceRelativePosition(NORM_NewOctreeNodeV2* node)
{
    float halfDim = _dimension / 2.0;
    float halfDimNei = node->_dimension / 2.0;

    if( fabs( ( node->_centre.x() + halfDimNei ) - ( _centre.x() - halfDim ) ) < 1e-6 )
    {
        return faceRelativePosition::Left;
    }
    else if( fabs( ( node->_centre.x() - halfDimNei ) - ( _centre.x() + halfDim ) ) < 1e-6 )
    {
        return faceRelativePosition::Right;
    }
    if( fabs( ( node->_centre.y() + halfDimNei ) - ( _centre.y() - halfDim ) ) < 1e-6 )
    {
        return faceRelativePosition::Back;
    }
    else if( fabs( ( node->_centre.y() - halfDimNei ) - ( _centre.y() + halfDim ) ) < 1e-6 )
    {
        return faceRelativePosition::Front;
    }
    if( fabs( ( node->_centre.z() + halfDimNei ) - ( _centre.z() - halfDim ) ) < 1e-6 )
    {
        return faceRelativePosition::Bot;
    }
    else if( fabs( ( node->_centre.z() - halfDimNei ) - ( _centre.z() + halfDim ) ) < 1e-6 )
    {
        return faceRelativePosition::Top;
    }
    else
    {
        qDebug() << "Noeud appelant " << _centre.x() << _centre.y() << _centre.z() << " de dimension " << _dimension;
        qDebug() << "Node " << node->_centre.x() << node->_centre.y() << node->_centre.z();
        assert(false);
    }
}

bool NORM_NewOctreeNodeV2::isNeighbourInRangeOfFace(NORM_NewOctreeNodeV2* nei,
                                                    int face)
{
    if( ( _centre[face] - _dimension/2.0 < nei->_centre[face] ) &&
        ( nei->_centre[face] < _centre[face] + _dimension/2.0 ) )
    {
        return true;
    }

    return false;
}

void NORM_NewOctreeNodeV2::samplePointsOnFace(faceRelativePosition position,
                                              int nPointsOnEdge,
                                              QVector<CT_Point>& outputSampledPoints)
{
    switch ( position )
    {
        case Left :
        {
            samplePointsOnFace( 0, 0, nPointsOnEdge, outputSampledPoints );
            break;
        }
        case Right :
        {
            samplePointsOnFace( 0, 1, nPointsOnEdge, outputSampledPoints );
            break;
        }
        case Back :
        {
            samplePointsOnFace( 1, 0, nPointsOnEdge, outputSampledPoints );
            break;
        }
        case Front :
        {
            samplePointsOnFace( 1, 1, nPointsOnEdge, outputSampledPoints );
            break;
        }
        case Bot :
        {
            samplePointsOnFace( 2, 0, nPointsOnEdge, outputSampledPoints );
            break;
        }
        case Top :
        {
            samplePointsOnFace( 2, 1, nPointsOnEdge, outputSampledPoints );
            break;
        }
    }
}

void NORM_NewOctreeNodeV2::samplePointsOnFace(int axisFace,
                                              bool sup,
                                              int nPointsOnEdge,
                                              QVector<CT_Point>& outputSampledPoints)
{
    // Cas particulier a traiter ici
    if( nPointsOnEdge == 1 )
    {
        assert( false );
        // Cas particulier pas implemente (qui ferait une division par 0) :
        // Le point doit se trouver au centre de la face
    }

    // Recupere les deux autres axes (axes de la face)
    int axis1;
    int axis2;
    if( axisFace == 0 )
    {
        axis1 = 1;
        axis2 = 2;
    }
    else if( axisFace == 1 )
    {
        axis1 = 0;
        axis2 = 2;
    }
    else if( axisFace == 2 )
    {
        axis1 = 0;
        axis2 = 1;
    }

    // Calcule la position du plan
    CT_Point sample;
    if( !sup )
    {
        sample[ axisFace ] = _centre[ axisFace ] - ( _dimension / 2.0 );
    }
    else
    {
        sample[ axisFace ] = _centre[ axisFace ] + ( _dimension / 2.0 );
    }

    // Calcule l'increment dans l'espace
    float spaceIncrement = ( _dimension / (nPointsOnEdge-1) );

    sample[axis1] = _centre[axis1] - ( _dimension / 2.0 );
    for( int nPointsOnEdge1 = 0 ; nPointsOnEdge1 < nPointsOnEdge ; nPointsOnEdge1++ )
    {
        // Repositionne le deuxieme axe au debut
        sample[axis2] = _centre[axis2] - ( _dimension / 2.0 );
        for( int nPointsOnEdge2 = 0 ; nPointsOnEdge2 < nPointsOnEdge ; nPointsOnEdge2++ )
        {
            // Ajoute le point
            outputSampledPoints.push_back( sample );

            // Incremente le second axe
            sample[axis2] += spaceIncrement;
        }

        // Incremente le premier axe
        sample[axis1] += spaceIncrement;
    }
}

CT_AbstractUndefinedSizePointCloud* NORM_NewOctreeNodeV2::getPoints()
{
    CT_AbstractUndefinedSizePointCloud* rslt = PS_REPOSITORY->createNewUndefinedSizePointCloud();
    CT_Point p;
    foreach( int index, (*_ptIndices) )
    {
        p = _pAccessor.pointAt( index );
        rslt->addPoint( p );
    }
    return rslt;
}

void NORM_NewOctreeNodeV2::addPoints(CT_AbstractUndefinedSizePointCloud* outPoints)
{
    CT_Point p;
    foreach( int index, (*_ptIndices) )
    {
        p = _pAccessor.pointAt( index );
        outPoints->addPoint( p );
    }
}

CT_AbstractUndefinedSizePointCloud*NORM_NewOctreeNodeV2::getSamplePointsOnFace(int axisFace,
                                                                               bool sup,
                                                                               int nPointsOnEdge)
{
    CT_AbstractUndefinedSizePointCloud* rslt = PS_REPOSITORY->createNewUndefinedSizePointCloud();

    // On recupere les points
    QVector< CT_Point > points;
    samplePointsOnFace( axisFace,
                        sup,
                        nPointsOnEdge,
                        points );

    // Ajout au resultat
    foreach( CT_Point p, points )
    {
        rslt->addPoint( p );
    }

    return rslt;
}

void NORM_NewOctreeNodeV2::saveToFileRecursive(QTextStream& stream)
{
    saveToFile( stream );

    if( !isLeaf() )
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                _children[i]->saveToFileRecursive( stream );
            }
            else
            {
                stream << "NULL\n";
            }
        }
    }
}

void NORM_NewOctreeNodeV2::saveToFile(QTextStream& stream)
{
    if( _depth == 0 )
    {
        // La racine ne s'ecrit pas
        return;
    }

    if( isLeaf() )
    {
        if( _acpWasRan && _surfaceFittingWasRan )
        {
            Eigen::Vector2f bot;
            Eigen::Vector2f top;

            getLocalBbox( bot, top );

            stream << _centroid.x() << " " << _centroid.y() << " " << _centroid.z() << " "
                   << _v1.x() << " " << _v1.y() << " " << _v1.z() << " "
                   << _v2.x() << " " << _v2.y() << " " << _v2.z() << " "
                   << _v3.x() << " " << _v3.y() << " " << _v3.z() << " "
                   << _a << " " << _b << " " << _c << " " << _d << " " << _e << " " << _f << " "
                   << bot.x() << " " << bot.y() << " "
                   << top.x() << " " << top.y() << " "
                   << _ptIndices->size()
                   << "\n";
        }
        else
        {
            stream << "NULL\n";
        }
    }
    else
    {
        stream << "\n";
    }
}

void NORM_NewOctreeNodeV2::getLocalBbox(Eigen::Vector2f& outBot,
                                        Eigen::Vector2f& outTop)
{
    // On recupere tous les points projetes
    QVector<CT_Point> projectedPoints;
    getProjectedPointsInAcpBasisFromFile( projectedPoints );

    // Initialise la bbox
    for( int i = 0 ; i < 2 ; i++ )
    {
        outBot[i] = std::numeric_limits<float>::max();
        outTop[i] = -std::numeric_limits<float>::max();
    }

    // Et on calcule la bbox en xy
    foreach( CT_Point curProjectedPoint, projectedPoints )
    {
        for( int i = 0 ; i < 2 ; i++ )
        {
            if( curProjectedPoint[i] < outBot[i] )
            {
                outBot[i] = curProjectedPoint[i];
            }
            if( curProjectedPoint[i] > outTop[i] )
            {
                outTop[i] = curProjectedPoint[i];
            }
        }
    }
}

NORM_NewOctreeNodeV2* NORM_NewOctreeNodeV2::loadRecursive(QTextStream& stream,
                                                          CT_Point& centre,
                                                          float dimension,
                                                          int depth,
                                                          NORM_NewOctreeNodeV2* father,
                                                          NORM_NewOctreeV2* octree, int& nLinesRead )
{
    NORM_NewOctreeNodeV2* rslt = new NORM_NewOctreeNodeV2(centre,
                                                          dimension,
                                                          depth,
                                                          father,
                                                          octree,
                                                          NULL );
    rslt->_children.resize(8);
    for( int i = 0 ; i < 8 ; i++ )
    {
        rslt->_children[i] = NULL;
    }

    QString line;
    QStringList listWords;
    for( int i = 0 ; i < 8 ; i++ )
    {
        // On lit une ligne
        if( !stream.atEnd() )
        {
            line = stream.readLine();
            nLinesRead++;
            listWords = line.split(" ", QString::SkipEmptyParts);

            CT_Point childCentre;
            rslt->getChildCenterFromChildNumber( i,
                                                 childCentre );

            if( listWords.empty() )
            {
                // On a lu un noeud interne
                rslt->_children[i] = NORM_NewOctreeNodeV2::loadRecursive(stream,
                                                                         childCentre,
                                                                         dimension / 2.0,
                                                                         depth + 1,
                                                                         rslt,
                                                                         rslt->_octree, nLinesRead );
            }
            else
            {
                // On a lu une feuille soit car elle n'existe pas soit car c'est une vraie feuille
                if( listWords.first() == "NULL" )
                {
                    // C'est une feuille inexistante
                    rslt->_children[i] = NULL;
                }
                else
                {
                    // C'est une vraie feuille il faut lire ses attributs surfaciques
                    if( listWords.size() != 23 )
                    {
                        // On a pas lu le bon nombre de champs
                        qDebug() << "Erreur lors du chargement de l'octree, une feuille n'a pas le bon nombre de champs";
                        assert(false);
                    }
                    else
                    {
                        // On peut creer le noeud fils
                        rslt->_children[i] = new NORM_NewOctreeNodeV2(childCentre,
                                                                      dimension / 2.0,
                                                                      depth + 1,
                                                                      rslt,
                                                                      rslt->_octree,
                                                                      NULL );

                        // Et on remplit ses champs relatifs a la surface
                        rslt->_children[i]->_centroid.x() = listWords.at(0).toFloat();
                        rslt->_children[i]->_centroid.y() = listWords.at(1).toFloat();
                        rslt->_children[i]->_centroid.z() = listWords.at(2).toFloat();
                        rslt->_children[i]->_v1.x() = listWords.at(3).toFloat();
                        rslt->_children[i]->_v1.y() = listWords.at(4).toFloat();
                        rslt->_children[i]->_v1.z() = listWords.at(5).toFloat();
                        rslt->_children[i]->_v2.x() = listWords.at(6).toFloat();
                        rslt->_children[i]->_v2.y() = listWords.at(7).toFloat();
                        rslt->_children[i]->_v2.z() = listWords.at(8).toFloat();
                        rslt->_children[i]->_v3.x() = listWords.at(9).toFloat();
                        rslt->_children[i]->_v3.y() = listWords.at(10).toFloat();
                        rslt->_children[i]->_v3.z() = listWords.at(11).toFloat();
                        rslt->_children[i]->_a = listWords.at(12).toFloat();
                        rslt->_children[i]->_b = listWords.at(13).toFloat();
                        rslt->_children[i]->_c = listWords.at(14).toFloat();
                        rslt->_children[i]->_d = listWords.at(15).toFloat();
                        rslt->_children[i]->_e = listWords.at(16).toFloat();
                        rslt->_children[i]->_f = listWords.at(17).toFloat();
                        rslt->_children[i]->_surfBot.x() = listWords.at(18).toFloat();
                        rslt->_children[i]->_surfBot.y() = listWords.at(19).toFloat();
                        rslt->_children[i]->_surfTop.x() = listWords.at(20).toFloat();
                        rslt->_children[i]->_surfTop.y() = listWords.at(21).toFloat();
                        rslt->_children[i]->_nPointsForReconstruction = listWords.at(22).toInt();
                        rslt->_children[i]->_acpWasRan = true;
                        rslt->_children[i]->_surfaceFittingWasRan = true;
                    }
                }
            }
        }
    }

    return rslt;
}

bool NORM_NewOctreeNodeV2::containsPoint(CT_Point& p)
{
    float halfDim = _dimension / 2.0;
    if( p.x() < _centre.x() - halfDim ||
        p.x() > _centre.x() + halfDim ||
        p.y() < _centre.y() - halfDim ||
        p.y() > _centre.y() + halfDim ||
        p.z() < _centre.z() - halfDim ||
        p.z() > _centre.z() + halfDim )
    {
        return false;
    }
    return true;
}

void NORM_NewOctreeNodeV2::printCentreRecursive()
{
    int nValidChildren = 0;
    for( int i = 0 ; i < 8 ; i++ )
    {
        if( _children[i] != NULL )
        {
            nValidChildren++;
        }
    }

    for( size_t i = 0 ; i < 8 ; i++ )
    {
        if( _children[i] != NULL )
        {
            _children[i]->printCentreRecursive();
        }
    }
}
