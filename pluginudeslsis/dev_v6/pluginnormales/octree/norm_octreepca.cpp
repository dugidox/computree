/************************************************************************************
* Filename :  norm_octreeacp.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "norm_octreepca.h"

NORM_OctreePCA::NORM_OctreePCA()
    : NORM_Octree()
{
}

NORM_OctreePCA::NORM_OctreePCA(float minNodeSize,
                               size_t minNbPoints,
                               CT_Point bot,
                               CT_Point top,
                               float threshPCA,
                               CT_PCIR inputIndexCloud,
                               int minPcaDepth)
    : NORM_Octree( minNodeSize,
                   minNbPoints,
                   bot,
                   top,
                   inputIndexCloud),
      _threshPCA( threshPCA ),
      _minPcaDepth( minPcaDepth )
{
}

NORM_OctreePCA::NORM_OctreePCA(float minNodeSize,
                               size_t minNbPoints,
                               CT_Point bot,
                               CT_Point top,
                               float threshPCA,
                               CT_PCIR inputIndexCloud,
                               const CT_OutAbstractSingularItemModel *model,
                               const CT_AbstractResult *result,
                               int minPcaDepth)
    : NORM_Octree( minNodeSize,
                   minNbPoints,
                   bot,
                   top,
                   inputIndexCloud,
                   model,
                   result),
          _threshPCA( threshPCA ),
          _minPcaDepth( minPcaDepth )
{
}

NORM_OctreePCA::NORM_OctreePCA(float minNodeSize,
                               size_t minNbPoints,
                               CT_Point bot,
                               CT_Point top,
                               float threshPCA,
                               CT_PCIR inputIndexCloud,
                               const QString &modelName,
                               const CT_AbstractResult *result,
                               int minPcaDepth)
    : NORM_Octree( minNodeSize,
                   minNbPoints,
                   bot,
                   top,
                   inputIndexCloud,
                   modelName,
                   result),
      _threshPCA( threshPCA ),
      _minPcaDepth( minPcaDepth )
{
}

void NORM_OctreePCA::createOctree()
{
    initRoot();

    // Et on lance la creation de l'octree par recursio
    _root->createOctreeRecursive( _minPcaDepth );
}

void NORM_OctreePCA::initRoot()
{
    bool computePca = (_minPcaDepth == 0 );
    _root = new NORM_OctreeNodePCA( this,
                                    NULL,
                                    0,
                                    _indexCloud->size(),
                                    (_bot + _top) / 2.0,
                                    0, 0, 0,
                                    computePca );
}

AttributFloat *NORM_OctreePCA::getSigmaAttributeFromNodes(QString modelName, CT_AbstractResult *result)
{
    // On parcours l'arbre jusqu'a trouver les feuilles
    // Tout en gardant leur id de fils (entre 0 et 7)
    // Et pour chaque point de chaque feuille on affecte cet identifiant
    AttributFloat* rslt = new AttributFloat( modelName, result, _indexCloud );

    root()->setSigmaAttributeFromNodeRecursive( rslt );

    rslt->setBoundingBox( _inputBot(0), _inputBot(1), _inputBot(2),
                          _inputTop(0), _inputTop(1), _inputTop(2) );

    return rslt;
}

NORM_OctreeNodePCA *NORM_OctreePCA::nodeContainingPoint(CT_Point &p) const
{
    return ((NORM_OctreeNodePCA*)NORM_Octree::nodeContainingPoint(p));
}

NORM_OctreeNodePCA *NORM_OctreePCA::nodeAtCode(size_t codex, size_t codey, size_t codez) const
{
    return ((NORM_OctreeNodePCA*)NORM_Octree::nodeAtCode( codex, codey, codez ));
}

NORM_OctreeNodePCA *NORM_OctreePCA::nodeAtCodeAndMaxDepth(size_t codex, size_t codey, size_t codez, size_t maxDepth) const
{
    return ((NORM_OctreeNodePCA*)NORM_Octree::nodeAtCodeAndMaxDepth( codex, codey, codez, maxDepth ));
}

NORM_OctreeNodePCA *NORM_OctreePCA::deepestNodeContainingBBBox(const CT_Point &bot, const CT_Point &top) const
{
    return ((NORM_OctreeNodePCA*)NORM_Octree::deepestNodeContainingBBBox( bot, top ));
}

NORM_OctreeNodePCA *NORM_OctreePCA::root() const
{
    return ((NORM_OctreeNodePCA*)NORM_Octree::root());
}

float NORM_OctreePCA::threshPCA() const
{
    return _threshPCA;
}

void NORM_OctreePCA::setThreshPCA(float threshPCA)
{
    _threshPCA = threshPCA;
}
int NORM_OctreePCA::minPcaDepth() const
{
    return _minPcaDepth;
}

void NORM_OctreePCA::setMinPcaDepth(int minPcaDepth)
{
    _minPcaDepth = minPcaDepth;
}

