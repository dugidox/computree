/************************************************************************************
* Filename :  norm_octreenodepca.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "norm_octreepca.h"
#include "norm_octreenodepca.h"
#include "tools/norm_acp.h"
#include "norm_octreettools.h"
#include "tools/norm_leastsquarefitting.h"
#include <ct_accessor/ct_pointaccessor.h>

NORM_OctreeNodePCA::NORM_OctreeNodePCA( NORM_OctreePCA* octree,
                                        NORM_OctreeNodePCA* father,
                                        size_t first,
                                        size_t last,
                                        const CT_Point& center,
                                        size_t codex,
                                        size_t codey,
                                        size_t codez,
                                        bool computePCA )
    : NORM_OctreeNode(octree,
                      father,
                      first,
                      last,
                      center,
                      codex,
                      codey,
                      codez),
      _lambda1(-1),
      _lambda2(-1),
      _lambda3(-1),
      _sigma(std::numeric_limits<float>::max()),
      _a(std::numeric_limits<float>::max()),
      _b(std::numeric_limits<float>::max()),
      _c(std::numeric_limits<float>::max()),
      _d(std::numeric_limits<float>::max()),
      _e(std::numeric_limits<float>::max()),
      _f(std::numeric_limits<float>::max()),
      _v1( createCtPoint() ),
      _v2( createCtPoint() ),
      _v3( createCtPoint() ),
      _centroid( createCtPoint() )
{
    // Calcul du centroid
    _centroid = NORM_Tools::NORM_PCA::getCentroid( _octree->modifiablepointCloudIndexRegistered(),
                                                   first,
                                                   last );

    if( computePCA &&
        last > first &&
        last - first >= _octree->minNbPoints() )
    {
        // Calcul de la PCA
        NORM_Tools::NORM_PCA::pca( _octree->modifiablepointCloudIndexRegistered(),
                                   _centroid,
                                   _first,
                                   _last,
                                   _lambda1,
                                   _lambda2,
                                   _lambda3,
                                   _v1,
                                   _v2,
                                   _v3 );

        // On s'assure qu'on est pas lineaire
        if( _lambda2 / _lambda1 > 0.25 )
        {
            // Affectation de sigma
            _sigma = _lambda3 / ( _lambda1 + _lambda2 + _lambda3 );

            // Conversion en pourcentage
            _sigma *= ( 100.0 / (1.0/3.0));
        }
    }
}

void NORM_OctreeNodePCA::createOctreeRecursive(int minPcaDepth)
{
    // Si le noeud est divisible
    if ( isSubdivisible() )
    {
        // On le divise effectivement
        subdivide( minPcaDepth );

        // Et on lance la recursion sur les huit fils
        for ( int i = 0 ; i < 8 ; i++ )
        {
            _children[i]->createOctreeRecursive(minPcaDepth);
        }
    }
}

bool NORM_OctreeNodePCA::isSubdivisible()
{
    // Redefinition from NORM_OctreeNode class

    // On prend les memes criteres qu'un octree standard
    bool rslt = NORM_OctreeNode::isSubdivisible();

    // Auxquels on rajoute le critere sur l'acp (seuil sur sigma)
    if( _sigma < ((NORM_OctreePCA*)_octree)->threshPCA() )
    {
        rslt = false;
    }

    return rslt;
}

void NORM_OctreeNodePCA::subdivide()
{
    // Dans un premier temps on trie les points par ordre de morton
    size_t bornes[9];
    NORM_OctreeTools::sortMortonOrder( _first, _last,
                                       _center, bornes,
                                       _octree->modifiablepointCloudIndexRegistered() );

    // Dans un second temps on cree les fils
    CT_Point centerChild;
    size_t codexChild, codeyChild, codezChild;
    for( size_t i = 0 ; i < 8 ; i++ )
    {
        // Calcul du centre du fils
        getChildCenterFromID( i, centerChild );

        // Calcul du code du fils
        getChildCodeFromID(i, codexChild, codeyChild, codezChild );

        // Instanciation du fils
        _children[i] = new NORM_OctreeNodePCA( (NORM_OctreePCA*)_octree,
                                               this,
                                               bornes[i],
                                               bornes[i+1],
                                               centerChild,
                                               codexChild,
                                               codeyChild,
                                               codezChild,
                                               true );
    }
}

void NORM_OctreeNodePCA::subdivide(int minPcaDepth)
{
    // Dans un premier temps on trie les points par ordre de morton
    size_t bornes[9];
    NORM_OctreeTools::sortMortonOrder( _first, _last,
                                       _center, bornes,
                                       _octree->modifiablepointCloudIndexRegistered() );

    // Dans un second temps on cree les fils
    CT_Point centerChild;
    size_t codexChild, codeyChild, codezChild;
    for( size_t i = 0 ; i < 8 ; i++ )
    {
        // Calcul du centre du fils
        getChildCenterFromID( i, centerChild );

        // Calcul du code du fils
        getChildCodeFromID(i, codexChild, codeyChild, codezChild );

        // Instanciation du fils
        bool computePca = true;
        // Si minPcaDepth == 0 alors on reste vrai
        // Si la profondeur des noeuds fils (_depth+1) est plus petite que la profondeur min alors on passe a faux
        if( minPcaDepth > 0 && _depth+1 < minPcaDepth )
        {
            computePca = false;
        }

        _children[i] = new NORM_OctreeNodePCA( (NORM_OctreePCA*)_octree,
                                               this,
                                               bornes[i],
                                               bornes[i+1],
                                               centerChild,
                                               codexChild,
                                               codeyChild,
                                               codezChild,
                                               computePca );
    }
}

void NORM_OctreeNodePCA::setSigmaAttributeFromNodeRecursive(AttributFloat *attributs )
{
    // Si on est sur une feuille alors on met a jour les identifiants
    if( isLeaf() )
    {
        for( size_t index = _first ; index < _last ; index++ )
        {
            attributs->setValueAt( index, _sigma );
        }
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            getChild(i)->setSigmaAttributeFromNodeRecursive( attributs );
        }
    }
}

float NORM_OctreeNodePCA::lambda1() const
{
    return _lambda1;
}

void NORM_OctreeNodePCA::setLambda1(float lambda1)
{
    _lambda1 = lambda1;
}
float NORM_OctreeNodePCA::lambda2() const
{
    return _lambda2;
}

void NORM_OctreeNodePCA::setLambda2(float lambda2)
{
    _lambda2 = lambda2;
}
float NORM_OctreeNodePCA::lambda3() const
{
    return _lambda3;
}

void NORM_OctreeNodePCA::setLambda3(float lambda3)
{
    _lambda3 = lambda3;
}
float NORM_OctreeNodePCA::sigma() const
{
    return _sigma;
}

void NORM_OctreeNodePCA::setSigma(float sigma)
{
    _sigma = sigma;
}
CT_Point NORM_OctreeNodePCA::v1() const
{
    return _v1;
}

void NORM_OctreeNodePCA::setV1(const CT_Point &v1)
{
    _v1 = v1;
}
CT_Point NORM_OctreeNodePCA::v2() const
{
    return _v2;
}

void NORM_OctreeNodePCA::setV2(const CT_Point &v2)
{
    _v2 = v2;
}
CT_Point NORM_OctreeNodePCA::v3() const
{
    return _v3;
}

void NORM_OctreeNodePCA::setV3(const CT_Point &v3)
{
    _v3 = v3;
}
CT_Point NORM_OctreeNodePCA::centroid() const
{
    return _centroid;
}

void NORM_OctreeNodePCA::setCentroid(const CT_Point &centroid)
{
    _centroid = centroid;
}

QVector<CT_Point> *NORM_OctreeNodePCA::getPointCloudInAcpBasis()
{
    QVector<CT_Point>* rslt = new QVector<CT_Point>();
    CT_MPCIR cloudIndex = _octree->modifiablepointCloudIndexRegistered();
    CT_PointAccessor ptAccessor;
    CT_Point curPoint;

    CT_Point v1Normalized = _v1.normalized();
    CT_Point v2Normalized = _v2.normalized();
    CT_Point v3Normalized = _v3.normalized();

    Eigen::Matrix3d changeBasisMatrix;
    changeBasisMatrix << v1Normalized(0), v2Normalized(0), v3Normalized(0),
                         v1Normalized(1), v2Normalized(1), v3Normalized(1),
                         v1Normalized(2), v2Normalized(2), v3Normalized(2);
    Eigen::Matrix3d changeBasisMatrixInverse;
    changeBasisMatrixInverse = changeBasisMatrix.inverse();

    CT_Point curPointInNewBasis;
    for( size_t i = _first ; i < _last ; i++ )
    {
        curPoint = ptAccessor.pointAt( cloudIndex->indexAt(i) );

        // On recentre au barycentre
        for( int i = 0 ; i < 3 ; i++ )
        {
            curPoint(i) -= _centroid(i);
        }

        // On prend le point dans la nouvelle base
        curPointInNewBasis = NORM_Tools::NORM_ChangeBasis::pointInNewBasis( changeBasisMatrixInverse,
                                                                            curPoint );

        // On l'ajoute au vecteur de points de sortie
        rslt->push_back( curPointInNewBasis );
    }

    return rslt;
}

QVector<CT_Point> *NORM_OctreeNodePCA::getPointCloudInGlobalBasis()
{
    QVector<CT_Point>* rslt = new QVector<CT_Point>();
    CT_MPCIR cloudIndex = _octree->modifiablepointCloudIndexRegistered();
    CT_PointAccessor ptAccessor;
    CT_Point curPoint;

    for( size_t i = _first ; i < _last ; i++ )
    {
        curPoint = ptAccessor.pointAt( cloudIndex->indexAt(i) );

        // On l'ajoute au vecteur de points de sortie
        rslt->push_back( curPoint );
    }

    return rslt;
}

void NORM_OctreeNodePCA::getQuadraticSurface()
{
    // On recupere un vecteur des points dans le noeud
    QVector<CT_Point>* pointCloud = getPointCloudInAcpBasis();

    // On fitte une quadrique dessus et on stocke les coefficients
    NORM_Tools::NORM_LeastSquareFitting::quadraticSurfaceFitting( pointCloud,
                                                                  _a, _b, _c, _d, _e, _f );

    pointCloud->clear();
    delete pointCloud;
}

QVector<float>* NORM_OctreeNodePCA::getErrorOnQuadraticSurface()
{
    QVector<float>* rslt = new QVector<float>();

    // On recupere un vecteur des points dans le noeud
    QVector<CT_Point>* pointCloud = getPointCloudInAcpBasis();

    // Pour chaque point on calcule sa distance a la surface
    int nPts = pointCloud->size();
    CT_Point curPoint;

    assert( _a != std::numeric_limits<float>::max() );

    for( int i = 0 ; i < nPts ; i++ )
    {
        curPoint = pointCloud->at(i);
        rslt->push_back( curPoint.z() - ( _a*curPoint.x()*curPoint.x() +
                                          _b*curPoint.x()*curPoint.y() +
                                          _c*curPoint.y()*curPoint.y() +
                                          _d*curPoint.x() +
                                          _e*curPoint.y() +
                                          _f ) );
    }

    pointCloud->clear();
    delete pointCloud;

    return rslt;
}

float NORM_OctreeNodePCA::a() const
{
    return _a;
}

void NORM_OctreeNodePCA::setA(float a)
{
    _a = a;
}
float NORM_OctreeNodePCA::b() const
{
    return _b;
}

void NORM_OctreeNodePCA::setB(float b)
{
    _b = b;
}
float NORM_OctreeNodePCA::c() const
{
    return _c;
}

void NORM_OctreeNodePCA::setC(float c)
{
    _c = c;
}
float NORM_OctreeNodePCA::e() const
{
    return _e;
}

void NORM_OctreeNodePCA::setE(float e)
{
    _e = e;
}
float NORM_OctreeNodePCA::d() const
{
    return _d;
}

void NORM_OctreeNodePCA::setD(float d)
{
    _d = d;
}
float NORM_OctreeNodePCA::f() const
{
    return _f;
}

void NORM_OctreeNodePCA::setF(float f)
{
    _f = f;
}






