/************************************************************************************
* Filename :  norm_octreeacp.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef NORM_OCTREEPCA_H
#define NORM_OCTREEPCA_H

#include "norm_octree.h"
#include "norm_octreenodepca.h"

typedef CT_PointsAttributesScalarTemplated<float> AttributFloat;

/*!
 * \brief The NORM_OctreePCA class
 *
 * Octree with a subdivision criteria based on PCA result
 * Uses NORM_OctreeNodePCA
 */
class NORM_OctreePCA : public NORM_Octree
{
public :
/* **************************************************************** */
/* Constructuors and destructors                                    */
/* **************************************************************** */
    NORM_OctreePCA();

    NORM_OctreePCA(float minNodeSize,
                    size_t minNbPoints,
                    CT_Point bot,
                    CT_Point top,
                    float threshPCA,
                    CT_PCIR inputIndexCloud,
                    int minPcaDepth );

    NORM_OctreePCA(float minNodeSize,
                    size_t minNbPoints,
                    CT_Point bot,
                    CT_Point top,
                    float threshPCA,
                    CT_PCIR inputIndexCloud,
                    const CT_OutAbstractSingularItemModel *model,
                    const CT_AbstractResult *result,
                    int minPcaDepth );

    NORM_OctreePCA( float minNodeSize,
                    size_t minNbPoints,
                    CT_Point bot,
                    CT_Point top,
                    float threshPCA,
                    CT_PCIR inputIndexCloud,
                    const QString &modelName,
                    const CT_AbstractResult *result,
                    int minPcaDepth );

    void createOctree();

/* **************************************************************** */
/* Construction tools                                               */
/* **************************************************************** */
    /*!
     * \brief initRoot
     * See NORM_AbstractOctree::initRoot()
     * \sa NORM_AbstractOctree::initRoot()
     */
    virtual void initRoot();

/* **************************************************************** */
/* Visualisation tools                                              */
/* **************************************************************** */
    AttributFloat* getSigmaAttributeFromNodes(QString modelName,
                                              CT_AbstractResult* result);

/* ******************************************************************** */
/* NORM_AbstractOctree wrapping methods in order to manage return type  */
/* ******************************************************************** */
    NORM_OctreeNodePCA* nodeContainingPoint( CT_Point& p ) const;

    NORM_OctreeNodePCA* nodeAtCode( size_t codex,
                                    size_t codey,
                                    size_t codez ) const;

    NORM_OctreeNodePCA* nodeAtCodeAndMaxDepth( size_t codex,
                                               size_t codey,
                                               size_t codez,
                                               size_t maxDepth ) const;

    NORM_OctreeNodePCA* deepestNodeContainingBBBox(const CT_Point &bot,
                                                   const CT_Point &top) const;

    NORM_OctreeNodePCA* root() const;

    virtual CT_AbstractItemDrawable* copy(const CT_OutAbstractItemModel *model,
                                          const CT_AbstractResult *result,
                                          CT_ResultCopyModeList copyModeList )
    {
        Q_UNUSED( copyModeList );
        CT_PCIR foopcir;
        CT_Point foobot;
        CT_Point footop;
        return new NORM_OctreePCA( 0, 0, foobot, footop, 0, foopcir, 0 );
    }

/* **************************************************************** */
/* Getters and setters                                              */
/* **************************************************************** */
    float threshPCA() const;
    void setThreshPCA(float threshPCA);

    int minPcaDepth() const;
    void setMinPcaDepth(int minPcaDepth);

protected :
    /* **************************************************************** */
/* Member attributes                                                */
/* **************************************************************** */
    float _threshPCA;
    int   _minPcaDepth;
};

#endif // NORM_OCTREEPCA_H
