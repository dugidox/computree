#ifndef NORM_PLUGINMANAGER_H
#define NORM_PLUGINMANAGER_H

#include "ct_abstractstepplugin.h"

class NORM_PluginManager : public CT_AbstractStepPlugin
{
public:
    NORM_PluginManager();
    ~NORM_PluginManager();

    QString getPluginURL() const {return QString("http://rdinnovation.onf.fr/projects/PLUGINS-PROJECT-NAME-HERE/wiki");}

protected:

    bool loadGenericsStep();
    bool loadOpenFileStep();
    bool loadCanBeAddedFirstStep();
    bool loadActions();
    bool loadExporters();
    bool loadReaders();

};

#endif // NORM_PLUGINMANAGER_H
