#ifndef NORM_NEWSTEPCOMPARENORMALS_H
#define NORM_NEWSTEPCOMPARENORMALS_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_tools/model/ct_autorenamemodels.h"

class NORM_NewStepCompareNormals : public CT_AbstractStep
{
    Q_OBJECT

public:
    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    NORM_NewStepCompareNormals(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     *
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

    float angle3D( float x1, float y1, float z1,
                   float x2, float y2, float z2 );

private :
    CT_AutoRenameModels     _autoRenameModelGroup;
    CT_AutoRenameModels     _autoRenameModelScene;
    CT_AutoRenameModels     _autoRenameModelAngles;

    QStringList             _logFileList;
    QString                 _logFile;

    QStringList             _fileList1;
    QString                 _file1;
    int                     _skipLinesFile1;
    int                     _fileType1;
    QString                 _separator1;

    QStringList             _fileList2;
    QString                 _file2;
    int                     _skipLinesFile2;
    int                     _fileType2;
    QString                 _separator2;

};

#endif // NORM_NEWSTEPCOMPARENORMALS_H
