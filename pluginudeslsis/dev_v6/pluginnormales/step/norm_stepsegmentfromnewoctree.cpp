/************************************************************************************
* Filename :  norm_stepsegmentfromnewoctree.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "norm_stepsegmentfromnewoctree.h"

#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "newoctree/norm_newoctree.h"

// Alias for indexing models
#define DEFin_RsltOctree "inRslt_Octree"
#define DEFin_GrpOctree "inGrp_Octree"
#define DEFin_ItmOctree "inItm_Octree"

#define DEFout_RsltScenes "outRslt_Scenes"
#define DEFout_GrpGrpScenes "outGrp_GrpGrpScenes"
#define DEFout_GrpScenes "outGrp_Scene"
#define DEFout_ItmScene "outItm_Scene"

// Constructor : initialization of parameters
NORM_StepSegmentFromNewOctree::NORM_StepSegmentFromNewOctree(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _angleMax = 20;
}

// Step description (tooltip of contextual menu)
QString NORM_StepSegmentFromNewOctree::getStepDescription() const
{
    return tr("Segmenta en fonction du voisinage et de l'angle entre les vecteurs propres de l'acp");
}

// Step detailled description
QString NORM_StepSegmentFromNewOctree::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_StepSegmentFromNewOctree::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_StepSegmentFromNewOctree::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_StepSegmentFromNewOctree(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_StepSegmentFromNewOctree::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy* resIn_Result = createNewInResultModelForCopy(DEFin_RsltOctree, tr("inRsltOctree"), tr(""), true);
    resIn_Result->setRootGroup(DEFin_GrpOctree, CT_AbstractItemGroup::staticGetType(), tr("inGrpOctree"));
    resIn_Result->addItemModel(DEFin_GrpOctree, DEFin_ItmOctree, NORM_NewOctree::staticGetType(), tr("inItmOctree"));
}

// Creation and affiliation of OUT models
void NORM_StepSegmentFromNewOctree::createOutResultModelListProtected()
{
    CT_OutResultModelGroup* res_ResultOctree = createNewOutResultModel(DEFout_RsltScenes, tr("Resultat segmentation"));
    res_ResultOctree->setRootGroup( DEFout_GrpGrpScenes, new CT_StandardItemGroup(), tr("GrpGrpScenes"));
    res_ResultOctree->addGroupModel( DEFout_GrpGrpScenes, DEFout_GrpScenes, new CT_StandardItemGroup(), tr("GrpScene"));
    res_ResultOctree->addItemModel( DEFout_GrpScenes, DEFout_ItmScene, new CT_Scene(), tr("Scene"));
}

// Semi-automatic creation of step parameters DialogBox
void NORM_StepSegmentFromNewOctree::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Angle max", "[Deg]", 0.0001, 360, 4, _angleMax, 1);
}

void NORM_StepSegmentFromNewOctree::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_Result = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_ResultScenes = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_Group(resIn_Result, this, DEFin_GrpOctree);
    while (itIn_Group.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_Group = (CT_AbstractItemGroup*) itIn_Group.next();

        NORM_NewOctree* itmIn_NewOctree = (NORM_NewOctree*)grpIn_Group->firstItemByINModelName(this, DEFin_ItmOctree);

        if( itmIn_NewOctree != NULL )
        {
            CT_StandardItemGroup* grp_GrpGrpScene = new CT_StandardItemGroup(DEFout_GrpGrpScenes, res_ResultScenes);
            res_ResultScenes->addGroup(grp_GrpGrpScene);

            QVector< CT_Scene* >* segments = itmIn_NewOctree->segmentFromPcaDirections( _angleMax,
                                                                                        DEFout_ItmScene,
                                                                                        res_ResultScenes );

//            QVector< CT_Scene* >* segments = itmIn_NewOctree->segmentFromNormalDirections( _angleMax,
//                                                                                           DEFout_ItmScene,
//                                                                                           res_ResultScenes );

            int nbScenes = segments->size();
            for( int i = 0 ; i < nbScenes ; i++ )
            {
                CT_StandardItemGroup* grp_GrpScene = new CT_StandardItemGroup(DEFout_GrpScenes, res_ResultScenes);
                grp_GrpScene->addItemDrawable( segments->at(i) );
                grp_GrpGrpScene->addGroup( grp_GrpScene );
            }
        }
    }
}
