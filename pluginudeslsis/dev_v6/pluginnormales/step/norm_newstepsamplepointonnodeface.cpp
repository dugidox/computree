#include "norm_newstepsamplepointonnodeface.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"

#include "new/norm_newoctreev2.h"
#include "new/norm_newoctreenodev2.h"
#include "ct_itemdrawable/ct_scene.h"

// Alias for indexing models
#define DEFin_rsltOctree "RsltOctree"
#define DEFin_grpOctree "grpOctree"
#define DEFin_itmOctree "itmOctree"

// Constructor : initialization of parameters
NORM_NewStepSamplePointOnNodeFace::NORM_NewStepSamplePointOnNodeFace(CT_StepInitializeData &dataInit) :
    CT_AbstractStep(dataInit)
{
    _x = 0;
    _y = 0;
    _z = 0;

    _face = 0;
    _sup = 0;
    _nPointsOnEdge = 10;
}

// Step description (tooltip of contextual menu)
QString NORM_NewStepSamplePointOnNodeFace::getStepDescription() const
{
    return tr("b - Debug - Sample des points sur une face donnee du noeud contenant un point");
}

// Step detailled description
QString NORM_NewStepSamplePointOnNodeFace::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_NewStepSamplePointOnNodeFace::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_NewStepSamplePointOnNodeFace::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_NewStepSamplePointOnNodeFace(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_NewStepSamplePointOnNodeFace::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_rsltOctree = createNewInResultModelForCopy(DEFin_rsltOctree, "Octree Result");
    resIn_rsltOctree->setZeroOrMoreRootGroup();

    resIn_rsltOctree->addGroupModel("", DEFin_grpOctree, CT_AbstractItemGroup::staticGetType(), tr("Input octree Group"));
    resIn_rsltOctree->addItemModel( DEFin_grpOctree, DEFin_itmOctree, NORM_NewOctreeV2::staticGetType(), tr("Input Octree"));
}

// Creation and affiliation of OUT models
void NORM_NewStepSamplePointOnNodeFace::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res_RsltOctree = createNewOutResultModelToCopy( DEFin_rsltOctree );

    if(res_RsltOctree != NULL)
    {
        res_RsltOctree->addItemModel( DEFin_grpOctree,
                                      _autoRenameModelQuerryPointScene,
                                      new CT_Scene(),
                                      "Querry Point");
        res_RsltOctree->addItemModel( DEFin_grpOctree,
                                      _autoRenameModelPointsOfNodeScene,
                                      new CT_Scene(),
                                      "Points of Node");
        res_RsltOctree->addItemModel( DEFin_grpOctree,
                                      _autoRenameModelSampledPointsOnFace,
                                      new CT_Scene(),
                                      "Points sampled on face");
    }
}

// Semi-automatic creation of step parameters DialogBox
void NORM_NewStepSamplePointOnNodeFace::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble( "Querry point x", "", -std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), 4, _x );
    configDialog->addDouble( "Querry point y", "", -std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), 4, _y );
    configDialog->addDouble( "Querry point z", "", -std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), 4, _z );

    configDialog->addInt( "NbPointsOnEdge", "", 2, 1000, _nPointsOnEdge );

    CT_ButtonGroup& bg_face = configDialog->addButtonGroup( _face );
    configDialog->addExcludeValue("", "", tr("x"), bg_face, 0);
    configDialog->addExcludeValue("", "", tr("y"), bg_face, 1);
    configDialog->addExcludeValue("", "", tr("z"), bg_face, 2);

    CT_ButtonGroup& bg_sup = configDialog->addButtonGroup( _sup );
    configDialog->addExcludeValue("", "", tr("Inf"), bg_sup, 0);
    configDialog->addExcludeValue("", "", tr("Sup"), bg_sup, 1);
}

void NORM_NewStepSamplePointOnNodeFace::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_RsltOctree = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_grpOctree( res_RsltOctree, this, DEFin_grpOctree );
    while ( itIn_grpOctree.hasNext()
            &&
            !isStopped())
    {
        qDebug() << _sup;

        CT_StandardItemGroup* grpIn_grpOctree = (CT_StandardItemGroup*) itIn_grpOctree.next();

        NORM_NewOctreeV2* itemIn_itmOctree = (NORM_NewOctreeV2*)grpIn_grpOctree->firstItemByINModelName( this, DEFin_itmOctree );
        if ( itemIn_itmOctree != NULL )
        {
            CT_Point querryPoint;
            querryPoint.setValues( _x, _y, _z );

            // Cree une scene ne contenant qu'un point (le querry point)
            CT_AbstractUndefinedSizePointCloud* querryPointPointCloud = PS_REPOSITORY->createNewUndefinedSizePointCloud();
            querryPointPointCloud->addPoint( querryPoint );
            CT_NMPCIR depositQuerryPointPointCloud = PS_REPOSITORY->registerUndefinedSizePointCloud(querryPointPointCloud);
            CT_Scene* querryPointScene = new CT_Scene( _autoRenameModelQuerryPointScene.completeName(),
                                                       res_RsltOctree,
                                                       depositQuerryPointPointCloud );
            grpIn_grpOctree->addItemDrawable(querryPointScene);

            // Recupere le noeud feuille contenant le querry point
            int level = -2;
            NORM_NewOctreeNodeV2* nodeContainingPoint;
            itemIn_itmOctree->getNodeContainingPoint( querryPoint,
                                                      nodeContainingPoint,
                                                      level );

            // Si le noeud existe
            if( nodeContainingPoint != NULL )
            {
                // Cree une scene contenant les points du noeud
                CT_AbstractUndefinedSizePointCloud* nodePointCloud = nodeContainingPoint->getPoints();
                CT_NMPCIR depositNodePointCloud = PS_REPOSITORY->registerUndefinedSizePointCloud(nodePointCloud);
                CT_Scene* nodePointScene = new CT_Scene( _autoRenameModelPointsOfNodeScene.completeName(),
                                                         res_RsltOctree,
                                                         depositNodePointCloud );
                grpIn_grpOctree->addItemDrawable(nodePointScene);

                // Cree une scene contenant les points samples
                CT_AbstractUndefinedSizePointCloud* sampledPointCloud = nodeContainingPoint->getSamplePointsOnFace( _face,
                                                                                                                    _sup > 0,
                                                                                                                    _nPointsOnEdge );
                CT_NMPCIR depositSampledPointCloud = PS_REPOSITORY->registerUndefinedSizePointCloud(sampledPointCloud);
                CT_Scene* sampledPointScene = new CT_Scene( _autoRenameModelSampledPointsOnFace.completeName(),
                                                           res_RsltOctree,
                                                           depositSampledPointCloud );
                grpIn_grpOctree->addItemDrawable(sampledPointScene);
            }

        }
    }
}
