/************************************************************************************
* Filename :  norm_stepfilteronsigmaoctreeandestimatenormals.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "norm_stepfilteronsigmaoctreeandestimatenormals.h"

#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithoutpointcloud.h"
#include "ct_itemdrawable/ct_pointsattributesnormal.h"

#include "tools/norm_leastsquarefitting.h"
#include <QDebug>

// Alias for indNORMing models
#define DEFin_inResultOctree "inResultOctree"
#define DEFin_inGroupOctree "inGroupOctree"
#define DEFin_inItemOctree "inItemItemWithOctree"

#define DEFout_outResultFilteredCloud "outResultFilteredCloud"
#define DEFout_outGroupFilteredCloud "outGroupFilteredCloud"
#define DEFout_outItemFilteredCloud "outItemFilteredCloud"

#define DEFout_ResultNormals "ResultNormals"
#define DEFout_GroupNormals "GroupNormals"
#define DEFout_ItemNormals "ItemNormals"

#include "ct_attributes/ct_attributesnormal.h"

typedef CT_PointsAttributesScalarTemplated<float> CT_PointAttributeSigma;
typedef CT_PointsAttributesNormal AttributeNormals;

// Constructor : initialization of parameters
NORM_StepFilterOnSigmaOctreeAndEstimateNormals::NORM_StepFilterOnSigmaOctreeAndEstimateNormals(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _seuilSigma = 0.009;
}

// Step description (tooltip of contNORMtual menu)
QString NORM_StepFilterOnSigmaOctreeAndEstimateNormals::getStepDescription() const
{
    return tr("Filtre les cellules de l'octree et calcule les normales en meme temps");
}

// Step detailled description
QString NORM_StepFilterOnSigmaOctreeAndEstimateNormals::getStepDetailledDescription() const
{
    return tr("L'indice de planeite se calcule par ACP :<br>"
"lambda3 / somme_des_labdas");
}

// Step URL
QString NORM_StepFilterOnSigmaOctreeAndEstimateNormals::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_StepFilterOnSigmaOctreeAndEstimateNormals::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_StepFilterOnSigmaOctreeAndEstimateNormals(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_StepFilterOnSigmaOctreeAndEstimateNormals::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_inResultOctree = createNewInResultModel(DEFin_inResultOctree, tr("inResultOctree"));
    resIn_inResultOctree->setRootGroup(DEFin_inGroupOctree, CT_AbstractItemGroup::staticGetType(), tr("inGroupOctree"));
    resIn_inResultOctree->addItemModel(DEFin_inGroupOctree, DEFin_inItemOctree, NORM_OctreePCA::staticGetType(), tr("inItemItemWithOctree"));
}

// Creation and affiliation of OUT models
void NORM_StepFilterOnSigmaOctreeAndEstimateNormals::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_outResultFilteredCloud = createNewOutResultModel(DEFout_outResultFilteredCloud, tr("outResultScene"));
    res_outResultFilteredCloud->setRootGroup(DEFout_outGroupFilteredCloud, new CT_StandardItemGroup(), tr("outGroupFilteredCloud"));
    res_outResultFilteredCloud->addItemModel(DEFout_outGroupFilteredCloud, DEFout_outItemFilteredCloud, new CT_Scene(), tr("outItemFilteredCloud"));

    CT_OutResultModelGroup *res_ResultNormals = createNewOutResultModel(DEFout_ResultNormals, tr("Estimated Normals"));
    res_ResultNormals->setRootGroup(DEFout_GroupNormals, new CT_StandardItemGroup(), tr("Group"));
    res_ResultNormals->addItemModel(DEFout_GroupNormals, DEFout_ItemNormals, new AttributeNormals(), tr("Normals"));
}

// Semi-automatic creation of step parameters DialogBox
void NORM_StepFilterOnSigmaOctreeAndEstimateNormals::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Seuil sigma", "(%)", 0.0001, 100, 4, _seuilSigma, 1);
}

void NORM_StepFilterOnSigmaOctreeAndEstimateNormals::compute()
{
    // DONT'T FORGET TO ADD THIS STEP TO THE PLUGINMANAGER !!!!!
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_inResultOctree = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_outResultFilteredScene = outResultList.at(0);
    CT_ResultGroup* res_outNormals = outResultList.at(1);

    // IN results browsing
    CT_ResultGroupIterator itIn_inGroupOctree(resIn_inResultOctree, this, DEFin_inGroupOctree);
    const NORM_OctreePCA* itemIn_inItemOctree;
    const CT_AbstractItemGroup* grpIn_inGroupOctree;
    while (itIn_inGroupOctree.hasNext() && !isStopped())
    {
        // Access the octree
        grpIn_inGroupOctree = (CT_AbstractItemGroup*) itIn_inGroupOctree.next();
        itemIn_inItemOctree = (NORM_OctreePCA*)grpIn_inGroupOctree->firstItemByINModelName(this, DEFin_inItemOctree);
        if ( itemIn_inItemOctree != NULL)
        {
            // Cree un nuage pour la scene de sortie
            CT_AbstractUndefinedSizePointCloud* curOutScene = PS_REPOSITORY->createNewUndefinedSizePointCloud();
            CT_NormalCloudStdVector* curNormalCloud = new CT_NormalCloudStdVector();


            // Log temporaire
            QFile file ( "/home/joris/data-disk/donnees/test-snakes-vegetation/fusion-tree3-normales-computree.xyz" );
            if( !file.open( QIODevice::WriteOnly ) )
            {
                qDebug() << "Impossible d'ouvrir \"/home/joris/data-disk/donnees/test-snakes-vegetation/fusion-tree3-normales-computree.xyz\"";
                return;
            }
            QTextStream stream( &file );

            // Lance le filtrage
            int nPoints = 0;
            bool stop = false;
            float errorMin = std::numeric_limits<float>::max();
            float errorMax = -std::numeric_limits<float>::max();
            filterOnSigmaRecursive( itemIn_inItemOctree->root(), (NORM_OctreePCA*)itemIn_inItemOctree, curOutScene, curNormalCloud, _seuilSigma, nPoints, stream, stop, errorMin, errorMax );
//            qDebug() << "Nb points output : " << nPoints << "Erreur min et max " << errorMin << errorMax    ;

            // Creation du resultat
            CT_NMPCIR registeredCurSubCloud = PS_REPOSITORY->registerUndefinedSizePointCloud( curOutScene );
            CT_Scene* curScene = new CT_Scene( DEFout_outItemFilteredCloud, res_outResultFilteredScene, registeredCurSubCloud );

            CT_StandardItemGroup* grpCurExtractedCloud = new CT_StandardItemGroup( DEFout_outGroupFilteredCloud, res_outResultFilteredScene );
            grpCurExtractedCloud->addItemDrawable( curScene );
            res_outResultFilteredScene->addGroup(grpCurExtractedCloud);

            file.close();
        }
    }
}

void NORM_StepFilterOnSigmaOctreeAndEstimateNormals::filterOnSigmaRecursive(NORM_OctreeNodePCA *node,
                                                                            NORM_OctreePCA* octree,
                                                                            CT_AbstractUndefinedSizePointCloud* outPointCloud,
                                                                            CT_NormalCloudStdVector* outNormalCloud,
                                                                            double seuilSigmaMax,
                                                                            int& nOutputPoints,
                                                                            QTextStream &stream,
                                                                            bool& stop,
                                                                            float& errorMin,
                                                                            float& errorMax )
{
    if( stop )
    {
        return;
    }

    // Si on est pas une feuille on continue a traverser l'octreee pour atteindre les feuilles
    if( !node->isLeaf() )
    {
        for( int i = 0 ; i < 8 ; i++ )
        {
            filterOnSigmaRecursive( node->getChild(i), octree, outPointCloud, outNormalCloud, seuilSigmaMax, nOutputPoints, stream, stop, errorMin, errorMax );
        }
    }

    // Si on est une feuille on regarde la valeur de sigma
    if( node->sigma() < seuilSigmaMax )
    {
        CT_Point v1Normalized = node->v1().normalized();
        CT_Point v2Normalized = node->v2().normalized();
        CT_Point v3Normalized = node->v3().normalized();
        Eigen::Matrix3d retourBaseCartesienne;
        retourBaseCartesienne << v1Normalized(0) , v2Normalized(0) , v3Normalized(0),
                                 v1Normalized(1) , v2Normalized(1) , v3Normalized(1),
                                 v1Normalized(2) , v2Normalized(2) , v3Normalized(2);

        // On recupere le nuage d'indice du noeud
        CT_PointCloudIndexVector* indexCloud = node->getIndexCloud();
        CT_PointAccessor pAccessor;

        // On recupere un vecteur des points dans le noeud
        QVector<CT_Point>* pointCloud = node->getPointCloudInAcpBasis();

        // On fitte une quadrique dessus
        float a, b, c, d, e, f;
        NORM_Tools::NORM_LeastSquareFitting::quadraticSurfaceFitting( pointCloud,
                                                                      a, b, c, d, e, f );

        node->getQuadraticSurface();
        QVector<float>* errors = node->getErrorOnQuadraticSurface();

        // Pour chaque point du noeud dans la base locale on calcule sa normale comme la normale au point x,y sur la quadrique
        int nPts = pointCloud->size();
        CT_Normal currentNormal;
        CT_Point currentNormalCT_Point;
        CT_Point currentNormalCT_PointTmp;
        CT_Point curPoint;
        for( int i = 0 ; i < nPts ; i++ )
        {
            // ----------------------------------------------------------------------
            // Partie NORMALES
            curPoint = pointCloud->at(i);

            // Nouveaute : on cherche le projette orthogonal du point sur la surface
            Eigen::Matrix3f A;
            A(0,0) = a;         A(0,1) = b/2.0;     A(0,2) = 0;
            A(1,0) = b/2.0;     A(1,1) = c;         A(1,2) = 0;
            A(2,0) = 0;         A(2,1) = 0;         A(2,2) = 0;

            Eigen::Vector3f B; B(0) = d; B(1) = e; B(2) = -1;
            Eigen::Vector4f p0;
            for(int j = 0 ; j < 3 ; j++ ) p0(j) = curPoint(j); p0(3) = 0;
            Eigen::Vector4f projette = NORM_Tools::NORM_LeastSquareFitting::newton( A, B, f, p0, 1e-6, 50 );

            currentNormalCT_Point(0) = (2*a*projette.x()) + (b*projette.y()) + d;
            currentNormalCT_Point(1) = (2*c*projette.y()) + (b*projette.x()) + e;
            currentNormalCT_Point(2) = -1;
            currentNormalCT_Point.normalize();
            currentNormalCT_PointTmp = currentNormalCT_Point;

            // On repasse la normale dans la base canonique cartesienne
            currentNormalCT_Point = retourBaseCartesienne * currentNormalCT_PointTmp;

            currentNormal(0) = currentNormalCT_Point(0);
            currentNormal(1) = currentNormalCT_Point(1);
            currentNormal(2) = currentNormalCT_Point(2);

            // Et on l'ajoute au tableau des normales
            outNormalCloud->addNormal( currentNormal );

            // ----------------------------------------------------------------------
            // Partie POINTS
            CT_Point initialPoint;
            initialPoint = pAccessor.pointAt( indexCloud->indexAt(i) );

            // On l'ajoute au nuage de sortie
            outPointCloud->addPoint( initialPoint );
            if( errors->at(i) < errorMin )
            {
                errorMin = errors->at(i);
            }
            if( errors->at(i) > errorMax )
            {
                errorMax = errors->at(i);
            }

//            stream << initialPoint(0) << " " << initialPoint(1) << " " << initialPoint(2) << " " << errors->at(i) << "\n";
            stream << initialPoint(0) << " " << initialPoint(1) << " " << initialPoint(2) << " " << currentNormalCT_Point(0) << " " << currentNormalCT_Point(1) << " " << currentNormalCT_Point(2) << " " << errors->at(i) << "\n";

            nOutputPoints++;
        }

            // Libere la memoire
            delete pointCloud;
//            stop = true;
//        }
    }
}
