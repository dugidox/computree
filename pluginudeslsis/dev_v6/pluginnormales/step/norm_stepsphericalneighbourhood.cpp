#include "norm_stepsphericalneighbourhood.h"

#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_scanner.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_accessor/ct_pointaccessor.h"
#include "tools/norm_boundingbox.h"

#include "octree/norm_octreestd.h"

// Alias for indexing models
#define DEFin_ResultWithPointCloud "ResultWithPointCloud"
#define DEFin_GroupSinglePointCloud "GroupSinglePointCloud"
#define DEFin_ItemWithPointCloud "ItemWithPointCloud"

#define DEFout_ResultSphericalNeighbourhood "ResultSphericalNeighbourhood"
#define DEFout_GroupSphericalNeighbourhood "GroupSphericalNeighbourhood"
#define DEFout_ItemSphere "ItemSphere"
#define DEFout_ItemScene "ItemScene"
#define DEFout_ItemAttributeCloudIdNode "ItemAttributeCloudIdNode"

typedef CT_PointsAttributesScalarTemplated<int> AttributEntier;

// Constructor : initialization of parameters
NORM_StepSphericalNeighbourhood::NORM_StepSphericalNeighbourhood(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _querryx = 0;
    _querryy = 0;
    _querryz = 0;

    _minNodeSize = 0.0001;
    _nbPointsMinInNode = 1;

    _radius = 0.0001;
}

// Step description (tooltip of contextual menu)
QString NORM_StepSphericalNeighbourhood::getStepDescription() const
{
    return tr("Get spherical Neighbourhood of a point");
}

// Step detailled description
QString NORM_StepSphericalNeighbourhood::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_StepSphericalNeighbourhood::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_StepSphericalNeighbourhood::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_StepSphericalNeighbourhood(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_StepSphericalNeighbourhood::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_ResultWithPointCloud = createNewInResultModel(DEFin_ResultWithPointCloud, tr("Nuage de points"));
    resIn_ResultWithPointCloud->setRootGroup(DEFin_GroupSinglePointCloud, CT_AbstractItemGroup::staticGetType(), tr("Groupe contenant une nuage de points"));
    resIn_ResultWithPointCloud->addItemModel(DEFin_GroupSinglePointCloud, DEFin_ItemWithPointCloud, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Nuage de points"));

}

// Creation and affiliation of OUT models
void NORM_StepSphericalNeighbourhood::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_ResultSphericalNeighbourhood = createNewOutResultModel(DEFout_ResultSphericalNeighbourhood, tr("Voisinnage spherique"));
    res_ResultSphericalNeighbourhood->setRootGroup(DEFout_GroupSphericalNeighbourhood, new CT_StandardItemGroup(), tr("Groupe"));
    res_ResultSphericalNeighbourhood->addItemModel(DEFout_GroupSphericalNeighbourhood, DEFout_ItemSphere, new CT_Scanner(), tr("Sphere de voisinage"));
    res_ResultSphericalNeighbourhood->addItemModel(DEFout_GroupSphericalNeighbourhood, DEFout_ItemScene, new CT_Scene(), tr("Points dans le voisinage"));
    res_ResultSphericalNeighbourhood->addItemModel(DEFout_GroupSphericalNeighbourhood, DEFout_ItemAttributeCloudIdNode, new AttributEntier(), tr("Coloration de l'octree"));
}

// Semi-automatic creation of step parameters DialogBox
void NORM_StepSphericalNeighbourhood::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("x", "", -9999, 9999, 4, _querryx, 1);
    configDialog->addDouble("y", "", -9999, 9999, 4, _querryy, 1);
    configDialog->addDouble("z", "", -9999, 9999, 4, _querryz, 1);
    configDialog->addDouble("radius", "", 0.0001, 9999, 4, _radius, 1);
    configDialog->addDouble("Taille de cellule minimale", "", 0.0001, 100, 4, _minNodeSize, 1);
    configDialog->addInt("Nombre de points minimum par cellule", "", 1, 10000, _nbPointsMinInNode);
}

void NORM_StepSphericalNeighbourhood::compute()
{
    // DONT'T FORGET TO ADD THIS STEP TO THE PLUGINMANAGER !!!!!

    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_ResultWithPointCloud = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_ResultSphericalNeighbourhood = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_GroupSinglePointCloud(resIn_ResultWithPointCloud, this, DEFin_GroupSinglePointCloud);
    while (itIn_GroupSinglePointCloud.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_GroupSinglePointCloud = (CT_AbstractItemGroup*) itIn_GroupSinglePointCloud.next();
        
        const CT_AbstractItemDrawableWithPointCloud* itemIn_ItemWithPointCloud = (CT_AbstractItemDrawableWithPointCloud*)grpIn_GroupSinglePointCloud->firstItemByINModelName(this, DEFin_ItemWithPointCloud);
        CT_PCIR inputIndexCloud = itemIn_ItemWithPointCloud->getPointCloudIndexRegistered();

        if (itemIn_ItemWithPointCloud != NULL)
        {
            // On cree l'octree
            CT_Point bot = createCtPoint( itemIn_ItemWithPointCloud->minX(), itemIn_ItemWithPointCloud->minY(), itemIn_ItemWithPointCloud->minZ() );
            CT_Point top = createCtPoint( itemIn_ItemWithPointCloud->maxX(), itemIn_ItemWithPointCloud->maxY(), itemIn_ItemWithPointCloud->maxZ() );
            NORM_OctreeStd octree( _minNodeSize, _nbPointsMinInNode, bot, top, inputIndexCloud );
            octree.createOctree();

            // On fait la recherche du noeud contenant le point
            CT_Point querryPoint = createCtPoint(_querryx, _querryy, _querryz );
            CT_PointCloudIndexVector* outIndexVector = octree.sphericalNeighbourhood( querryPoint, _radius );

            // Calcul de la bounding box
            CT_PointAccessor pAccessor;
            CT_Point curPoint;
            float xmin = std::numeric_limits<float>::max();
            float xmax = -std::numeric_limits<float>::max();
            float ymin = std::numeric_limits<float>::max();
            float ymax = -std::numeric_limits<float>::max();
            float zmin = std::numeric_limits<float>::max();
            float zmax = -std::numeric_limits<float>::max();
            size_t nbPoints = outIndexVector->size();
            for( size_t i = 0 ; i < nbPoints ; i++ )
            {
                pAccessor.pointAt( outIndexVector->indexAt(i), curPoint );
                NORM_Tools::NORM_BBox::updateBoundingBox( xmin, ymin, zmin, xmax, ymax, zmax, curPoint );
            }

            // On cree une scene a partir des points de ce noeud
            CT_PCIR outIndexVectorRegistered = PS_REPOSITORY->registerPointCloudIndex( outIndexVector );
            CT_Scene* item_outItemScene = new CT_Scene(DEFout_ItemScene, res_ResultSphericalNeighbourhood);
            item_outItemScene->setPointCloudIndexRegistered( outIndexVectorRegistered );
            item_outItemScene->setBoundingBox( xmin, ymin, zmin, xmax, ymax, zmax );

            // On ajoute cette scene aux resultats
            CT_StandardItemGroup* grp_GroupSphericalNeighbourhood = new CT_StandardItemGroup(DEFout_GroupSphericalNeighbourhood, res_ResultSphericalNeighbourhood);
            res_ResultSphericalNeighbourhood->addGroup(grp_GroupSphericalNeighbourhood);
            grp_GroupSphericalNeighbourhood->addItemDrawable(item_outItemScene);

            // On fait pareil pour la couleur des points dans l'octree
            AttributEntier* item_AttributeCloudIdNode = octree.getIdAttribute( DEFout_ItemAttributeCloudIdNode, res_ResultSphericalNeighbourhood );
            grp_GroupSphericalNeighbourhood->addItemDrawable(item_AttributeCloudIdNode);

            CT_Point vertical = createCtPoint( 0,0,1 );
            CT_Scanner* item_outItemSphere = new CT_Scanner( DEFout_ItemSphere, res_ResultSphericalNeighbourhood,
                                                             0, querryPoint, vertical,
                                                             360, 360,
                                                             1, 1,
                                                             0, 0,
                                                             true,
                                                             true );
            grp_GroupSphericalNeighbourhood->addItemDrawable( item_outItemSphere );
        }
    }
}
