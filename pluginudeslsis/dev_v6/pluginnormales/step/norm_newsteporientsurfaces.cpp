#include "norm_newsteporientsurfaces.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"

#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_scene.h"
#include "new/norm_newoctreev2.h"

#include <QElapsedTimer>
#include <QDebug>

// Alias for indexing models
#define DEFin_rsltOctree "RsltOctree"
#define DEFin_grpOctree "grpOctree"
#define DEFin_itmOctree "itmOctree"

// Constructor : initialization of parameters
NORM_NewStepOrientSurfaces::NORM_NewStepOrientSurfaces(CT_StepInitializeData &dataInit) :
    CT_AbstractStep(dataInit)
{
    _nSamplePoints = 10;
}

// Step description (tooltip of contextual menu)
QString NORM_NewStepOrientSurfaces::getStepDescription() const
{
    return tr("2bis - Oriente les surfaces de l'octree dans la meme direction");
}

// Step detailled description
QString NORM_NewStepOrientSurfaces::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_NewStepOrientSurfaces::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_NewStepOrientSurfaces::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_NewStepOrientSurfaces(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_NewStepOrientSurfaces::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_rsltOctree = createNewInResultModelForCopy(DEFin_rsltOctree, "Octree Result");
    resIn_rsltOctree->setZeroOrMoreRootGroup();

    resIn_rsltOctree->addGroupModel("", DEFin_grpOctree, CT_AbstractItemGroup::staticGetType(), tr("Input octree Group"));
    resIn_rsltOctree->addItemModel( DEFin_grpOctree, DEFin_itmOctree, NORM_NewOctreeV2::staticGetType(), tr("Input Octree"));
}

// Creation and affiliation of OUT models
void NORM_NewStepOrientSurfaces::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res_RsltOctree = createNewOutResultModelToCopy( DEFin_rsltOctree );

    if(res_RsltOctree != NULL)
    {
        res_RsltOctree->addItemModel( DEFin_grpOctree, _autoRenameModelComposantesBfs, new CT_PointsAttributesScalarTemplated<int>(), tr("Composantes du BFS"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void NORM_NewStepOrientSurfaces::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addInt( "Nb Sampling points", "", 2, std::numeric_limits<int>::max(), _nSamplePoints );
}

void NORM_NewStepOrientSurfaces::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_RsltOctree = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_grpOctree( res_RsltOctree, this, DEFin_grpOctree );
    while ( itIn_grpOctree.hasNext()
            &&
            !isStopped())
    {
        CT_StandardItemGroup* grpIn_grpOctree = (CT_StandardItemGroup*) itIn_grpOctree.next();

        NORM_NewOctreeV2* itemIn_itmOctree = (NORM_NewOctreeV2*)grpIn_grpOctree->firstItemByINModelName( this, DEFin_itmOctree );
        if ( itemIn_itmOctree != NULL )
        {
            CT_PointsAttributesScalarTemplated<int>* composantes = new CT_PointsAttributesScalarTemplated<int>( _autoRenameModelComposantesBfs.completeName(),
                                                                                                                res_RsltOctree,
                                                                                                                itemIn_itmOctree->getScene()->getPointCloudIndexRegistered() );

            // Oriente les surfaces
            QElapsedTimer timer;
            timer.start();
            itemIn_itmOctree->orientSurfaces( _nSamplePoints, composantes, 0 );
            qDebug() << "Temps d'orientation des surfaces " << timer.elapsed();

            grpIn_grpOctree->addItemDrawable( composantes );
        }
    }
}
