#include "norm_stepapproximatesphericalneighbourhood.h"

#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "../itemdrawable/norm_sphere.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_accessor/ct_pointaccessor.h"
#include "tools/norm_boundingbox.h"

#include "octree/norm_octree.h"
#include "octree/norm_octreenode.h"

// Alias for indexing models
#define DEFin_ResultOctree "ResultOctree"
#define DEFin_GroupOctree "GroupOctree"
#define DEFin_ItemOctree "ItemOctree"

#define DEFout_ResultSphericalNeighbourhood "ResultSphericalNeighbourhood"
#define DEFout_GroupSphericalNeighbourhood "GroupSphericalNeighbourhood"
#define DEFout_ItemSphere "ItemSphere"
#define DEFout_ItemScene "ItemScene"
#define DEFout_ItemAttributeCloudIdNode "ItemAttributeCloudIdNode"

typedef CT_PointsAttributesScalarTemplated<int> AttributEntier;

// Constructor : initialization of parameters
NORM_StepApproximateSphericalNeighbourhood::NORM_StepApproximateSphericalNeighbourhood(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _querryx = 0;
    _querryy = 0;
    _querryz = 0;
    _radius = 0.0001;
}

// Step description (tooltip of contextual menu)
QString NORM_StepApproximateSphericalNeighbourhood::getStepDescription() const
{
    return tr("Get approximated spherical Neighbourhood of a point");
}

// Step detailled description
QString NORM_StepApproximateSphericalNeighbourhood::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_StepApproximateSphericalNeighbourhood::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_StepApproximateSphericalNeighbourhood::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_StepApproximateSphericalNeighbourhood(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_StepApproximateSphericalNeighbourhood::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_ResultOctree = createNewInResultModel(DEFin_ResultOctree, tr("Resultat d'une' creation d'octree"));
    resIn_ResultOctree->setRootGroup(DEFin_GroupOctree, CT_AbstractItemGroup::staticGetType(), tr("Groupe contenant un octree"));
    resIn_ResultOctree->addItemModel(DEFin_GroupOctree, DEFin_ItemOctree, NORM_Octree::staticGetType(), tr("Un octree cree"));
}

// Creation and affiliation of OUT models
void NORM_StepApproximateSphericalNeighbourhood::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_ResultSphericalNeighbourhood = createNewOutResultModel(DEFout_ResultSphericalNeighbourhood, tr("Voisinnage spherique"));
    res_ResultSphericalNeighbourhood->setRootGroup(DEFout_GroupSphericalNeighbourhood, new CT_StandardItemGroup(), tr("Groupe"));
    res_ResultSphericalNeighbourhood->addItemModel(DEFout_GroupSphericalNeighbourhood, DEFout_ItemSphere, new NORM_Sphere(), tr("Sphere de voisinage"));
    res_ResultSphericalNeighbourhood->addItemModel(DEFout_GroupSphericalNeighbourhood, DEFout_ItemScene, new CT_Scene(), tr("Points dans le voisinage"));
}

// Semi-automatic creation of step parameters DialogBox
void NORM_StepApproximateSphericalNeighbourhood::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("x", "", -9999, 9999, 4, _querryx, 1);
    configDialog->addDouble("y", "", -9999, 9999, 4, _querryy, 1);
    configDialog->addDouble("z", "", -9999, 9999, 4, _querryz, 1);
    configDialog->addDouble("radius", "", 0.0001, 9999, 4, _radius, 1);
}

void NORM_StepApproximateSphericalNeighbourhood::compute()
{
    // DONT'T FORGET TO ADD THIS STEP TO THE PLUGINMANAGER !!!!!

    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_ResultOctree = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_ResultSphericalNeighbourhood = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_GroupOctree(resIn_ResultOctree, this, DEFin_GroupOctree);
    while (itIn_GroupOctree.hasNext() && !isStopped())
    {
        // Acces a l'octree
        const CT_AbstractItemGroup* grpIn_GroupOctree = (CT_AbstractItemGroup*) itIn_GroupOctree.next();
        const NORM_Octree* itemIn_ItemOctree = (NORM_Octree*)grpIn_GroupOctree->firstItemByINModelName(this, DEFin_ItemOctree);

        if (itemIn_ItemOctree != NULL)
        {
            // On fait la recherche du noeud contenant le point
            CT_Point querryPoint = createCtPoint(_querryx, _querryy, _querryz );
            CT_PointCloudIndexVector* outIndexVector = itemIn_ItemOctree->approximateSphericalNeighbourhood( querryPoint, _radius );

            // Calcul de la bounding box
            CT_PointAccessor pAccessor;
            CT_Point curPoint;
            float xmin = std::numeric_limits<float>::max();
            float xmax = -std::numeric_limits<float>::max();
            float ymin = std::numeric_limits<float>::max();
            float ymax = -std::numeric_limits<float>::max();
            float zmin = std::numeric_limits<float>::max();
            float zmax = -std::numeric_limits<float>::max();
            size_t nbPoints = outIndexVector->size();
            for( size_t i = 0 ; i < nbPoints ; i++ )
            {
                pAccessor.pointAt( outIndexVector->indexAt(i), curPoint );
                NORM_Tools::NORM_BBox::updateBoundingBox( xmin, ymin, zmin, xmax, ymax, zmax, curPoint );
            }

            // On cree une scene a partir des points de ce noeud
            CT_PCIR outIndexVectorRegistered = PS_REPOSITORY->registerPointCloudIndex( outIndexVector );
            CT_Scene* item_outItemScene = new CT_Scene(DEFout_ItemScene, res_ResultSphericalNeighbourhood);
            item_outItemScene->setPointCloudIndexRegistered( outIndexVectorRegistered );
            item_outItemScene->setBoundingBox( xmin, ymin, zmin, xmax, ymax, zmax );

            // On ajoute cette scene aux resultats
            CT_StandardItemGroup* grp_GroupSphericalNeighbourhood = new CT_StandardItemGroup(DEFout_GroupSphericalNeighbourhood, res_ResultSphericalNeighbourhood);
            res_ResultSphericalNeighbourhood->addGroup(grp_GroupSphericalNeighbourhood);
            grp_GroupSphericalNeighbourhood->addItemDrawable(item_outItemScene);

            // On ajoute une sphere au resultat representant la pshere de voisinage demande
            CT_Point fooVertical = createCtPoint( 0,0,1 );
            CT_CircleData* outSphereData = new CT_CircleData( querryPoint, fooVertical, _radius );
            NORM_Sphere* item_outItemSphere = new NORM_Sphere( DEFout_ItemSphere, res_ResultSphericalNeighbourhood, outSphereData );
            grp_GroupSphericalNeighbourhood->addItemDrawable( item_outItemSphere );

            PS_LOG->addInfoMessage( PS_LOG->info, "Il y a " + QString::number( outIndexVector->size() ) + " points dans ce voisinage" );
        }
    }
}
