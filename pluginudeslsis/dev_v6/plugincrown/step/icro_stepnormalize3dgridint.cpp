#include "icro_stepnormalize3dgridint.h"

#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

// Alias for indexing models
#define DEFin_rsltWith3Rasterd "rsltWith3Rasterd"
#define DEFin_grpWith3dRaster "grpWith3dRaster"
#define DEFin_itmRaster3D "itmRaster3D"

#define DEFout_rsltNormalisedImage "rsltResultNormalisedImage"

// Resultat contient un groupe d'image
#define DEFout_grpNormalisedImage "grpImage"

// le groupe d'images contient une grille3d d'int
#define DEFout_itmNormalisedImage "itmNormalisedImage"

// Constructor : initialization of parameters
ICRO_StepNormalize3dGridInt::ICRO_StepNormalize3dGridInt(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _w = 0.5;
}

// Step description (tooltip of contextual menu)
QString ICRO_StepNormalize3dGridInt::getStepDescription() const
{
    return tr("Normalise une image avec une combinaison de normalisation locale et globale");
}

// Step detailled description
QString ICRO_StepNormalize3dGridInt::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString ICRO_StepNormalize3dGridInt::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ICRO_StepNormalize3dGridInt::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ICRO_StepNormalize3dGridInt(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ICRO_StepNormalize3dGridInt::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_rsltWith3RasterAndScene = createNewInResultModel(DEFin_rsltWith3Rasterd, tr("Result with 3d raster"));
    resIn_rsltWith3RasterAndScene->setRootGroup(DEFin_grpWith3dRaster, CT_AbstractItemGroup::staticGetType(), tr("Group with 3D raster"));
    resIn_rsltWith3RasterAndScene->addItemModel(DEFin_grpWith3dRaster, DEFin_itmRaster3D, CT_Grid3D<int>::staticGetType(), tr("3d Raster (int)"));
}

// Creation and affiliation of OUT models
void ICRO_StepNormalize3dGridInt::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_rsltNormalisedImage = createNewOutResultModel(DEFout_rsltNormalisedImage, tr("Normalised image"));

    // Resultat contient un groupe d'image
    res_rsltNormalisedImage->setRootGroup(DEFout_grpNormalisedImage, new CT_StandardItemGroup(), tr("Image Group"));

    // Groupe d'image contient
    // - une grille 3D qui contient l'image normalisee
    res_rsltNormalisedImage->addItemModel(DEFout_grpNormalisedImage, DEFout_itmNormalisedImage, new CT_Grid3D<int>(), tr("Normalised image"));
}

// Semi-automatic creation of step parameters DialogBox
void ICRO_StepNormalize3dGridInt::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Poids de la normalisation globale", "", 0, 1, 4, _w );
}

void ICRO_StepNormalize3dGridInt::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_rsltWith3Rasterd = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resOut_rsltNormalisedImage = outResultList.at(0);

    CT_ResultGroupIterator itIn_grpWith3dRaster(resIn_rsltWith3Rasterd, this, DEFin_grpWith3dRaster);
    while (itIn_grpWith3dRaster.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_grpWith3dRaster = (CT_AbstractItemGroup*) itIn_grpWith3dRaster.next();

        CT_Grid3D<int>* itemIn_itmIntensityGrid = (CT_Grid3D<int>*)grpIn_grpWith3dRaster->firstItemByINModelName(this, DEFin_itmRaster3D);
        if (itemIn_itmIntensityGrid != NULL )
        {
            // Resultat contient un groupe d'image
            CT_StandardItemGroup* grpNormalisedImage = new CT_StandardItemGroup(DEFout_grpNormalisedImage, resOut_rsltNormalisedImage );
            resOut_rsltNormalisedImage->addGroup( grpNormalisedImage );

            // Creation de la grille normalisee
            CT_Grid3D<int>* itemOut_itmNormalisedImage = new CT_Grid3D<int>( DEFout_itmNormalisedImage, resOut_rsltNormalisedImage,
                                                                             itemIn_itmIntensityGrid->minX(), itemIn_itmIntensityGrid->minY(), itemIn_itmIntensityGrid->minZ(),
                                                                             itemIn_itmIntensityGrid->xArraySize(), itemIn_itmIntensityGrid->yArraySize(), itemIn_itmIntensityGrid->zArraySize(),
                                                                             itemIn_itmIntensityGrid->resolution(),
                                                                             std::numeric_limits<int>::max(), std::numeric_limits<int>::max() );

            // On s'assure que les min max de l'input soient bons
            itemIn_itmIntensityGrid->computeMinMax();

            // Normalise l'image
            normalizeImage( itemIn_itmIntensityGrid, _w, itemOut_itmNormalisedImage );

            // Mise a jour des min max de la grille pour affichage
            itemOut_itmNormalisedImage->computeMinMax();

            // Ajout de la grille au resultat
            grpNormalisedImage->addItemDrawable( itemOut_itmNormalisedImage );
        }
    }
}

void ICRO_StepNormalize3dGridInt::normalizeImage(CT_Grid3D<int> *inputImage,
                                                 double globalWeight,
                                                 CT_Grid3D<int> *outputNormalizedImage)
{
    int xdim = inputImage->xArraySize();
    int ydim = inputImage->yArraySize();
    int zdim = inputImage->zArraySize();

    // Pour chaque pixel
    for( int x = 0 ; x < xdim ; x++ )
    {
        for( int y = 0 ; y < ydim ; y++ )
        {
            for( int z = 0 ; z < zdim ; z++ )
            {
                // On normalise la valeur
                int normalisedValue = (globalWeight * globalNormalisationOfPixel( inputImage, x, y, z ) ) + ( (1-globalWeight) * localNormalisationOfPixel( inputImage, x, y, z ) );
                outputNormalizedImage->setValue( x, y, z, normalisedValue );
            }
        }
    }
}

int ICRO_StepNormalize3dGridInt::localNormalisationOfPixel(CT_Grid3D<int> *inputImage,
                                                           int x,
                                                           int y,
                                                           int z)
{
    int xdim = inputImage->xArraySize();
    int ydim = inputImage->yArraySize();
    int zdim = inputImage->zArraySize();

    // BBox de la fenetre d'analyse
    int botx = std::max(0,x-1);
    int boty = std::max(0,y-1);
    int botz = std::max(0,z-1);
    int topx = std::min(xdim-1,x+1);
    int topy = std::min(ydim-1,y+1);
    int topz = std::min(zdim-1,z+1);

    // On recupere le min max du voisinage
    int min = std::numeric_limits<int>::max();
    int max = -std::numeric_limits<int>::max();

    // Pour chaque pixel dans la fenetre d'analyse
    for( int xx = botx ; xx <= topx ; xx++ )
    {
        for( int yy = boty ; yy <= topy ; yy++ )
        {
            for( int zz = botz ; zz <= topz ; zz++ )
            {
                if( inputImage->value(xx,yy,zz) != 0 )
                {
                    if( inputImage->value(xx,yy,zz) < min )
                    {
                        min = inputImage->value(xx,yy,zz);
                    }

                    if( inputImage->value(xx,yy,zz) > max )
                    {
                        max = inputImage->value(xx,yy,zz);
                    }
                }
            }
        }
    }

    // On normalise la valeur centrale entre 0 et 255
    int normalised = 255 * ( (float)( inputImage->value(x,y,z) - min ) / (float)( max - min ) );

    return normalised;
}

int ICRO_StepNormalize3dGridInt::globalNormalisationOfPixel(CT_Grid3D<int> *inputImage,
                                                            int x,
                                                            int y,
                                                            int z)
{
    // On normalise la valeur centrale entre 0 et 255
    int normalised = 255 * ( (float)( inputImage->value(x,y,z) - inputImage->dataMin() ) / (float)( inputImage->dataMax() - inputImage->dataMin() ) );

    return normalised;
}
