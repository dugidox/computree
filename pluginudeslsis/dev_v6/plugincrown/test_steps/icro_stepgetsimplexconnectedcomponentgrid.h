#ifndef ICRO_STEPGETSIMPLEXCONNECTEDCOMPONENTGRID_H
#define ICRO_STEPGETSIMPLEXCONNECTEDCOMPONENTGRID_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "../tools/connectedcomponent.h"

#include "ct_itemdrawable/ct_grid3d.h"
#include "ct_itemdrawable/ct_circle.h"

/*!
 * \class ICRO_StepGetSimplexConnectedComponentGrid
 * \ingroup Steps_ICRO
 * \brief <b>Segmente une grille 3D en composantes connexes simplexes.</b>
 *
 * No detailled description for this step
 *
 * \param _minValue
 *
 */

class ICRO_StepGetSimplexConnectedComponentGrid: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    ICRO_StepGetSimplexConnectedComponentGrid(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     *
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

    void computeConnectedComponentsUntilSimplexes(CT_Grid3D<int>* inputGrid,
                                                  CT_Grid3D<int>* outConnectedComponentsGrid,
                                                  double minIntensity,
                                                  int superPixelResolution,
                                                  int minComponentSize,
                                                  QVector<CT_CircleData *>& circles,
                                                  QVector<ConnectedComponent *>& outConnectedComponents);

    void labelSuperConnectedComponents(CT_Grid3D<int>* inputGrid,
                                       CT_Grid3D<int>* outConnectedComponentsGrid,
                                       double minIntensity,
                                       int superPixelResolution,
                                       QVector<ConnectedComponent *>& outConnectedComponents);

    void labelSuperConnectedComponentFromSeed(CT_Grid3D<int> *inputGrid,
                                              CT_Grid3D<int> *outLabeledGrid,
                                              double minValue,
                                              int startx,
                                              int starty,
                                              int startz,
                                              int label,
                                              int resolution,
                                              ConnectedComponent* outSuperComponent);

    void markSuperComponent( ConnectedComponent* superComponent,
                             CT_Grid3D<int>* grid,
                             int label );

    void updateComponentsType(QVector< ConnectedComponent* >& components,
                              int minComponentSize);

    void addPixelsToSuperConnectedComponent(SuperPixel &superPixel,
                                            ConnectedComponent* outSuperComponent);


    ConnectedComponent* getComponentFromLabel(QVector< ConnectedComponent* >& components,
                                              int label);

    void getComponentsType(CT_Grid3D<int> *labeledGrid,
                           QVector< CT_CircleData* >& circles,
                           QVector< ConnectedComponent* >& components,
                           int minLabelSize);

    void splitComplexComponent(ConnectedComponent* componentToBeSplitted,
                               CT_Grid3D<int>* valueGrid,
                               CT_Grid3D<int>* labelGrid,
                               int initialResolution,
                               int minValue,
                               QVector<ConnectedComponent *> &outComponents);

    void splitComponentUntilSimplexes(ConnectedComponent* componentToBeSplitted,
                                      QVector<ConnectedComponent *>& outSplittedComponents,
                                      int initialResolution,
                                      int minValue,
                                      int minLabelSize,
                                      CT_Grid3D<int>* intesityGrid,
                                      CT_Grid3D<int>* labelGrid );

    QVector<CT_CircleData*> getCirclesFromFile(QString dir, QString fileName);

    bool circleIntersect2D( QVector< CT_CircleData* >& circles, CT_CircleData* c );

    bool circleIntersect2D( CT_CircleData* c1, CT_CircleData* c2 );

private:

    // Step parameters
    double      _intensityThreshold;    /*!< Intensite minimale pour prendre en compte un pixel dans les composantes connexes (un super pixel doit contenir au moins un pixel de cette intensite pour etre pris en compte) */
    int         _minPixelsInComponent;  /*!< Nombre de pixels minimum que doit contenir une composante connexe pour etre prise en compte */
    int         _resolution;            /*!< Resolution de départ pour l'approche multi échelle (taille initiale d'un super pixel) */

    QVector< CT_CircleData* >   _circles;
};

#endif // ICRO_STEPGETSIMPLEXCONNECTEDCOMPONENTGRID_H
