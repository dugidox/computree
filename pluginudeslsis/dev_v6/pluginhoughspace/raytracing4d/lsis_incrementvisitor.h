/************************************************************************************
* Filename :  lsis_incrementvisitor.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_INCREMENTVISITOR_H
#define LSIS_INCREMENTVISITOR_H

// Inherits from abstract visitor
#include "./lsis_abstractvisitorgrid4d.h"

template < typename DataImage >
class CT_Grid4D;

template < typename DataT >
class LSIS_IncrementVisitor : public LSIS_AbstractVisitorGrid4D<DataT>
{
public:
    /*!
     * \brief LSIS_IncrementVisitor
     *
     * Constructeur
     *
     * \param grid : grille que le visiteur viste
     */
    LSIS_IncrementVisitor(CT_Grid4D<DataT>* grid);

    /*!
      * Destructeur (rien a faire, il ne doit pas liberer l'image qu'il visite!!)
      */
    virtual ~LSIS_IncrementVisitor();

    /*!
     * \brief visit
     *
     * \param levw : coordonnee du pixel a visiter
     * \param levx : coordonnee du pixel a visiter
     * \param levy : coordonnee du pixel a visiter
     * \param levz : coordonnee du pixel a visiter
     * \param beam : rayon qui traverse la grille
     */
    virtual void visit(size_t levw, size_t levx, size_t levy, size_t levz, LSIS_Beam4D const * beam);
};

// Inclusion des implementations template
#include "lsis_incrementvisitor.hpp"

#endif // LSIS_INCREMENTVISITOR_H
