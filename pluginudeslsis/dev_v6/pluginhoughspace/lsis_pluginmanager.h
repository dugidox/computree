#ifndef LSIS_PLUGINMANAGER_H
#define LSIS_PLUGINMANAGER_H

#include "ct_abstractstepplugin.h"

class LSIS_PluginManager : public CT_AbstractStepPlugin
{
public:
    LSIS_PluginManager();
    ~LSIS_PluginManager();

    QString getPluginURL() const {return QString("http://rdinnovation.onf.fr/projects/plugin-udes-lsis/wiki");}

    QString getPluginRISCitation() const;

protected:

    bool loadGenericsStep();
    bool loadOpenFileStep();
    bool loadCanBeAddedFirstStep();
    bool loadActions();
    bool loadExporters();
    bool loadReaders();

};

#endif // LSIS_PLUGINMANAGER_H
