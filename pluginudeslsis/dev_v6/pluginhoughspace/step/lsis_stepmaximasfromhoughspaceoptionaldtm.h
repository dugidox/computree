/************************************************************************************
* Filename :  lsis_stepmaximasfromhoughspaceoptionaldtm.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_STEPMAXIMASFROMHOUGHSPACEOPTIONALDTM_H
#define LSIS_STEPMAXIMASFROMHOUGHSPACEOPTIONALDTM_H

#include "ct_step/abstract/ct_abstractstep.h"

template < typename DataT >
class CT_Grid2DXY;
class LSIS_HoughSpace4D;

/*!
 * \class LSIS_StepMaximasFromHoughSpaceOptionalDtm
 * \ingroup Steps_LSIS
 * \brief <b>Extraction des maximas locaux d'un espace de Hough avec un mnt optionnel.</b>
 *
 * No detailled description for this step
 *
 *
 */
class LSIS_StepMaximasFromHoughSpaceOptionalDtm: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    LSIS_StepMaximasFromHoughSpaceOptionalDtm(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     *
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

    void compute(const LSIS_HoughSpace4D* houghSpace, const CT_Grid2DXY<float>* mnt );

private:

    // Step parameters
    double      _minz;
    double      _maxz;

    CT_ResultGroup* _outResultHoughSpaceMaximas;
};

#endif // LSIS_STEPMAXIMASFROMHOUGHSPACEOPTIONALDTM_H
