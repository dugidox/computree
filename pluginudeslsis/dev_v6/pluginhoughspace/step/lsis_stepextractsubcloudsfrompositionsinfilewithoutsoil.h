/************************************************************************************
* Filename :  lsis_stepextractsubcloudsfrompositionsinfilewithoutsoil.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_STEPEXTRACTSUBCLOUDSFROMPOSITIONSINFILEWITHOUTSOIL_H
#define LSIS_STEPEXTRACTSUBCLOUDSFROMPOSITIONSINFILEWITHOUTSOIL_H

#include "ct_step/abstract/ct_abstractstep.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_grid2dxy.h"
#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_circle.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"

//In/Out dependencies
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

//Qt dependencies
#include <QFile>
#include <QDir>

class LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil : public CT_AbstractStep
{
    Q_OBJECT
public:
    LSIS_StepExtractSubCloudsFromPositionsInFileWithoutSoil(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     *
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

    void compute(QString& fileCloudFullName,
                 QVector< CT_Circle* >& circles,
                 CT_Grid2DXY<float>* mnt);

    void computeAndSave( QString& fileCloudFullName,
                         QVector< CT_Circle* >& circles,
                         CT_Grid2DXY<float> *mnt,
                         QString outDirPath );

    bool isPointIn2dBox ( const CT_Point& point,
                          float xMin, float ymin,
                          float xmax, float ymax );

    void updateBBox( CT_Point& bot, CT_Point& top, const CT_Point& point );

    CT_Grid2DXY<float>* loadMnt(QString filename, const QString& modelName, CT_ResultGroup* result );

private:

    // Step parameters
    double                  _size;    /*!<  */
    double                  _height;
    bool                    _saveMode;
    QString                 _inputCloudFileName;
    QString                 _inputMntFileName;
    QString                 _outputDirPath;
    CT_ResultGroup*         _rsltSubClouds;
    CT_StandardItemGroup*   _grpGrpPointClouds;
};

#endif // LSIS_STEPEXTRACTSUBCLOUDSFROMPOSITIONSINFILEWITHOUTSOIL_H
