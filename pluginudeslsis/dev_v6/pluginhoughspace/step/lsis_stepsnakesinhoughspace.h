#ifndef LSIS_STEPSNAKESINHOUGHSPACE_H
#define LSIS_STEPSNAKESINHOUGHSPACE_H

#include "ct_step/abstract/ct_abstractstep.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_image2d.h"
#include "ct_itemdrawable/ct_circle.h"
#include "ct_itemdrawable/ct_line.h"
#include "ct_itemdrawable/ct_standarditemgroup.h"

//Tools dependencies
#include "ct_tools/model/ct_autorenamemodels.h"
#include "../houghspace/lsis_houghspace4d.h"

template< typename DataT >
class LSIS_ActiveContour4DContinuous;

/*!
 * \class LSIS_StepSnakesInHoughSpace
 * \ingroup Steps_LSIS
 * \brief <b>Lignes de cretes de l'espace.</b>
 *
 * No detailled description for this step
 *
 * \param _nIterMax 
 * \param _growCoeff 
 * \param _timeStep 
 * \param _threshGradMove 
 * \param _threshGradLength 
 *
 */
class LSIS_StepSnakesInHoughSpace: public CT_AbstractStep
{
    Q_OBJECT

    template< typename DataImage >
    struct DebugStructure
    {
        CT_Grid4D<DataImage>*   debugHoughSpace;        // L'espace de Hough a afficher lors d'un stop pendant le debug
        DocumentInterface*      documentHoughSpace;     // Le document sur lequel cette grille sera affichee
        QVector<CT_Line*>*      debugSkeleton;          // Squelette du contours sous forme de lignes consecutives
        QVector<CT_Circle*>*    debugCircles;           // Liste de cercles d'un contour a afficher
        DocumentInterface*      documentCircle;         // Le document sur lequel cette grille sera affichee
    };

public:

    /*! \brief Step constructor
     * 
     * Create a new instance of the step
     * 
     * \param dataInit Step parameters object
     */
    LSIS_StepSnakesInHoughSpace(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     * 
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     * 
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     * 
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     * 
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

    // CT_AbstractStep non obligatoire :
    // Que faire avant de s'arreter pendant le mode debug ?
    void preWaitForAckIfInDebugMode();

    // CT_AbstractStep non obligatoire :
    // Que faire apres s'etre arete pendant le mode debug ?
    void postWaitForAckIfInDebugMode();

    // Methode qui en mode debug permet de selectionner le document dans lequel afficher les elements de debug.
    // Voir aussi la variable _debugStructure
    void setDocumentForDebugDisplay(QList<DocumentInterface*> docList);

protected:

    /*! \brief Input results specification
     * 
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     * 
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     * 
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     * 
     * Step computation, using input results, and creating output results
     */
    void compute();

    void compute(const LSIS_HoughSpace4D *houghSpace,
                 CT_StandardItemGroup *rootGrp);
private:

    // Step parameters
    int       _nIterMaxOptim;    /*!<  */
    double    _treeHeightMaximum;
    double    _growCoeff;    /*!<  */
    double    _timeStep;    /*!<  */
    double    _alpha;       /*! */
    double    _beta;       /*! */
    double    _gama;       /*! */
    int       _minValueForHoughSpaceMaxima;
    int       _houghSpaceMaximaNeighbourhoodSize;
    double    _angleConeRecherche;
    int       _tailleConeRecherche;
    double    _tailleConeRechercheCm;
    double    _seuilSigmaL1;
    double    _seuilSigmaL4;
    double    _threshGradMove;    /*!<  */
    double    _threshGradLength;    /*!<  */
    double    _longueurMin;
    double    _minHeightForMaximumSearch;
    double    _maxHeightForMaximumSearch;
    int       _minValueForMaximumSearch;
    double    _movementThresh;
    bool      _forkSearchActive;
    int       _nForkLevels;
    int                     _nSnakesMax;
    CT_Image2D<float>*     _optionalMnt;

    CT_ResultGroup*  _snakesResult;

    CT_AutoRenameModels     _grpSnakes_AutoRenameModel;
    CT_AutoRenameModels     _grpForks_AutoRenameModel;
    CT_AutoRenameModels     _itmMesh_AutoRenameModel;
    CT_AutoRenameModels     _itmMeshForks_AutoRenameModel[3];
    CT_AutoRenameModels     _grpCircles_AutoRenameModel;
    CT_AutoRenameModels     _itmCircle_AutoRenameModel;
    CT_AutoRenameModels     _itmCircleForks_AutoRenameModel[3];
    CT_AutoRenameModels     _itmName_AutoRenameModel;
    CT_AutoRenameModels     _itmNameField_AutoRenameModel;
    CT_AutoRenameModels     _activeContour_AutoRenameModel;



// Toute la partir du dessous est copiee/colle simplement d'une etape d'un autre plugin (plugin houghspace initial)
private :
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    // Parametres relatifs au debug mode                                                                                                            //
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    /**/    DebugStructure<int>                     _debugStructure;  /*!< Structure de debug, contient tout pour l'affichage lors du debug mode */ //
    /**/    bool                                    _verbose; /*!< Indique si l'etape est "verbeuse" ou non (sortie console lors du debug) */       //
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //

public :
    /*!
     * \brief setDebugHoughSpace
     *
     * Permet de selectionner une grille a visualiser en mode debug
     *
     * \param hs : grille a visualiser en mode debug
     */
    template< typename DataImage >
    inline void setDebugHoughSpace( CT_Grid4D<DataImage>* hs )
    {
        _debugStructure.debugHoughSpace = hs;
    }

    /*!
     * \brief setDebugCirclesList
     *
     * Permet de selectionner une liste de cercles a visualiser en mode debug
     *
     * \param circlesList : le vecteur de cercles a visualiser en mod debug
     */
    inline void setDebugCirclesList( QVector<CT_Circle*>* circlesList )
    {
        _debugStructure.debugCircles = circlesList;
    }

    /*!
     * \brief setDebugSkeleton
     *
     * Permet de selectionner un squelette sous forme de vecteur de lignes a visualiser en mode debug
     *
     * \param lines : squelette du contours sous forme de suite de lignes
     */
    inline void setDebugSkeleton( QVector<CT_Line*>* lines )
    {
        _debugStructure.debugSkeleton = lines;
    }

    /*!
     * \brief callDebugVisualization
     *
     * Place l'etape en mode debug afin de visualiser les cercles d'un contours actif ainsi que l'espace de hough associe
     *
     * \param hs : espace de hough a visualiser
     * \param contour : contour actif dont on veut visualiser les cercles
     */
    template< typename DataImage >
    void callDebugVisualization( CT_Grid4D<DataImage> * hs, LSIS_ActiveContour4DContinuous<DataImage> * contour )
    {
        if ( isDebugModeOn() )
        {
            // Transforme le snake en vecteur de cercles pour pouvoir les afficher
            QVector<CT_Circle*>         vecCircles;
            QVector<CT_Line*>           vecLines;
            int nbCercles = 0;
            int nbLignes = 0;

            if ( contour != NULL )
            {
                vecLines = contour->toLines();
                vecCircles = contour->toCircles(5, LSIS_ActiveContour4DContinuous<DataImage>::NO_COLOR, "", NULL );

                setDebugCirclesList( &vecCircles );
                setDebugSkeleton( &vecLines );
            }

            else
            {
                setDebugCirclesList( NULL );
                setDebugSkeleton( NULL );
            }

            setDebugHoughSpace( hs );

            waitForAckIfInDebugMode();

            if ( contour != NULL )
            {
                // On vide le vecteur de cercles et libere la memoire
                nbCercles = vecCircles.size();
                for ( int i = 0 ; i < nbCercles ; i++ )
                {
                    // Le circleData est en autodelete quand on supprime le circle
                    delete vecCircles[i];
                }

                // On vide le vecteur de lignes et libere la memoire
                nbLignes = vecLines.size();
                for ( int i = 0 ; i < nbLignes ; i++ )
                {
                    // Le lineData est en autodelete quand on supprime la ligne
                    delete vecLines[i];
                }
            }
        }
    }

    template< typename DataImage >
    void callDebugVisualizationHoughSpaceOnSecondWindow( CT_Grid4D<DataImage>* hs )
    {
        if ( isDebugModeOn() )
        {
            setDebugHoughSpace( hs );
            waitForAckIfInDebugMode();
        }
    }

    /*!
     * \brief isVerbose
     *
     * Indique si l'etape est en mode verbose
     *
     * \return true si l'etape est en mode verbose, false sinon
     */
    inline bool isVerbose()
    {
        return _verbose;
    }

    void recursiveForkBuilding(const LSIS_HoughSpace4D *houghSpace,
                               CT_StandardItemGroup* rootGrp,
                               int currentRecursionLevel,
                               int maxRecursionLevel,
                               QVector< LSIS_ActiveContour4DContinuous<int>* >* contours,
                               CT_Grid4D_Sparse<bool> *repulseImage,
                               const CT_AbstractResult *result);
};

#endif // LSIS_STEPSNAKESINHOUGHSPACE_H
