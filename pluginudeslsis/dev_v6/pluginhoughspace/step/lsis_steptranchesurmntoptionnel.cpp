#include "lsis_steptranchesurmntoptionnel.h"

//In/Out dependencies
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_scene.h"

//Tools dependencies
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "../assert/assertwithmessage.h"
#include "ct_iterator/ct_pointiterator.h"

//Qt dependencies
#include <QDebug>

// Alias for indexing models
#define DEFin_rsltPointCloud "rsltPointCloud"
#define DEFin_grpPointCloud "grpPointCloud"
#define DEFin_itmPointCloud "itmPointCloud"

#define DEFin_rsltMntOptionnel "rsltMntOptionnel"
#define DEFin_grpMntOptionnel "grpMntOptionnel"
#define DEFin_itmMntOptionnel "itmMntOptionnel"

#define DEFout_rsltPointCloud "rsltPointCloud"
#define DEFout_grp "grp"
#define DEFout_itmTranche "itmTranche"

// Constructor : initialization of parameters
LSIS_StepTrancheSurMntOptionnel::LSIS_StepTrancheSurMntOptionnel(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _minHeight = 1.5;
    _maxHeight = 1.8;
    _dirPath = ".";
    _fileName = "output.xyz";
    _saveMode = false;
}

// Step description (tooltip of contextual menu)
QString LSIS_StepTrancheSurMntOptionnel::getStepDescription() const
{
    return tr("Fait une tranche au dessus du mnt ou a hauteur fixe si pas de mnt");
}

// Step detailled description
QString LSIS_StepTrancheSurMntOptionnel::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString LSIS_StepTrancheSurMntOptionnel::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepTrancheSurMntOptionnel::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepTrancheSurMntOptionnel(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepTrancheSurMntOptionnel::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_rsltPointCloud = createNewInResultModel(DEFin_rsltPointCloud, tr("Resultt PointCloud"), tr(""), true );
    resIn_rsltPointCloud->setRootGroup(DEFin_grpPointCloud, tr("Group"), tr(""), CT_InAbstractGroupModel::CG_ChooseOneIfMultiple );
    resIn_rsltPointCloud->addItemModel(DEFin_grpPointCloud, DEFin_itmPointCloud, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Point Cloud"));

    CT_InResultModelGroup *resIn_rsltMntOptionnel = createNewInResultModel(DEFin_rsltMntOptionnel, tr("Result MNT Optionnel"), tr(""), true);
    resIn_rsltMntOptionnel->setZeroOrMoreRootGroup();
    resIn_rsltMntOptionnel->setMinimumNumberOfPossibilityThatMustBeSelectedForOneTurn(0);
    resIn_rsltMntOptionnel->setRootGroup(DEFin_grpMntOptionnel, tr("Group"), tr(""), CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    resIn_rsltMntOptionnel->addItemModel(DEFin_grpMntOptionnel, DEFin_itmMntOptionnel, CT_Grid2DXY<float>::staticGetType(), tr("Mnt Optionnel"), tr(""), CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);
}

// Creation and affiliation of OUT models
void LSIS_StepTrancheSurMntOptionnel::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_rsltPointCloud = createNewOutResultModel(DEFout_rsltPointCloud, tr("Result Point Cloud"));
    res_rsltPointCloud->setRootGroup(DEFout_grp, new CT_StandardItemGroup(), tr("Group"));
    res_rsltPointCloud->addItemModel(DEFout_grp, DEFout_itmTranche, new CT_Scene(), tr("Tranche au dessus du Mnt"));

}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepTrancheSurMntOptionnel::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("MinHeight", "", 0, 999999, 4, _minHeight, 1);
    configDialog->addDouble("MaxHeight", "", 0, 99999, 4, _maxHeight, 1);
    configDialog->addString("SaveDir", "", _dirPath );
    configDialog->addString("FileName", "", _fileName );
    configDialog->addBool("SaveMode","","", _saveMode );
}

void LSIS_StepTrancheSurMntOptionnel::compute()
{
    // Recupere le resultat de sortie que l'on va remplir avec la(es) tranche(s)
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    _resultatTranche = outResultList.at(0);

    // Recupere les resultats d'entree
    QList<CT_ResultGroup*> inResultListPointCloud = getInputResultsForModel( DEFin_rsltPointCloud );
    QList<CT_ResultGroup*> inResultListMntOptionnel = getInputResultsForModel( DEFin_rsltMntOptionnel );

    // Recupere le(s) nuage de points
    QVector< CT_AbstractItemDrawableWithPointCloud* > inPointClouds;
    CT_ResultGroup* resIn_rsltPointCloud = inResultListPointCloud.at(0);
    CT_ResultGroupIterator itIn_grpPointCloud(resIn_rsltPointCloud, this, DEFin_grpPointCloud);
    while (itIn_grpPointCloud.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_grpPointCloud = (CT_AbstractItemGroup*) itIn_grpPointCloud.next();

        CT_AbstractItemDrawableWithPointCloud* curPointCloud = (CT_AbstractItemDrawableWithPointCloud*)grpIn_grpPointCloud->firstItemByINModelName(this, DEFin_itmPointCloud);
        if ( curPointCloud != NULL)
        {
            inPointClouds.push_back( curPointCloud );
        }
    }

    // Recupere le(s) nuage(s) de points
    QVector< CT_Grid2DXY<float>* > inMnts;
    if( !inResultListMntOptionnel.empty() )
    {
        // Recupere le(s) mnt(s)
        CT_ResultGroup* resIn_rsltMnt = inResultListMntOptionnel.at(0);
        CT_ResultGroupIterator itIn_grpMnt(resIn_rsltMnt, this, DEFin_grpMntOptionnel);
        while (itIn_grpMnt.hasNext() && !isStopped())
        {
            const CT_AbstractItemGroup* grpIn_grpMnt = (CT_AbstractItemGroup*) itIn_grpMnt.next();

            CT_Grid2DXY<float>* curMnt = (CT_Grid2DXY<float>*)grpIn_grpMnt->firstItemByINModelName(this, DEFin_itmMntOptionnel);

            if ( curMnt != NULL)
            {
                inMnts.push_back( curMnt );
            }
        }

        assertWithMessage( inPointClouds.size() == inMnts.size(), "Pas autant de mnt que de nuages de points" );
    }

    if( _saveMode == false )
    {
        // Effectue les traitements differement selon la presence ou non de mnt
        int nPointClouds = inPointClouds.size();
        if( !inMnts.empty() )
        {
            for( int i = 0 ; i < nPointClouds ; i++ )
            {
                compute( inMnts.at(i), inPointClouds.at(i) );
            }
        }

        else
        {
            for( int i = 0 ; i < nPointClouds ; i++ )
            {
                compute( inPointClouds.at(i) );
            }
        }
    }

    else
    {
        // Effectue les traitements differement selon la presence ou non de mnt
        int nPointClouds = inPointClouds.size();
        if( !inMnts.empty() )
        {
            for( int i = 0 ; i < nPointClouds ; i++ )
            {
                computeAndSave( inMnts.at(i), inPointClouds.at(i), _dirPath, _fileName );
            }
        }

        else
        {
            for( int i = 0 ; i < nPointClouds ; i++ )
            {
                computeAndSave( inPointClouds.at(i), _dirPath, _fileName );
            }
        }
    }
}

void LSIS_StepTrancheSurMntOptionnel::compute(CT_Grid2DXY<float> *mnt, CT_AbstractItemDrawableWithPointCloud *pointCloud)
{
    // On regarde le z du bot de labounding box
    CT_Point bot, top;
    pointCloud->getBoundingBox(bot, top);
    float zBot = bot.z();

    // On Cree un nouveau nuage pour la tranche
    CT_AbstractUndefinedSizePointCloud* tranchePointCloud = PS_REPOSITORY->createNewUndefinedSizePointCloud();
    CT_Point bboxBot = createCtPoint( std::numeric_limits<float>::max(),
                                      std::numeric_limits<float>::max(),
                                      std::numeric_limits<float>::max());
    CT_Point bboxTop = createCtPoint( -std::numeric_limits<float>::max(),
                                      -std::numeric_limits<float>::max(),
                                      -std::numeric_limits<float>::max());

    // Un iterateur pour parcourir le nuage
    CT_PointIterator itPoint( pointCloud->getPointCloudIndexRegistered() );
    float mntHeightAtPoint;
    while (itPoint.hasNext()&& !isStopped())
    {
        // Avance l'iterateur et recupere le point sous l'iterateur
        const CT_Point &currentPoint = itPoint.next().currentPoint();
        mntHeightAtPoint = mnt->valueAtXY( currentPoint.x(), currentPoint.y() );
        if( mntHeightAtPoint == mnt->NA() )
        {
            mntHeightAtPoint = zBot;
        }

        // Si le point est dans la zone de recherche
        if( ( currentPoint.z() - mntHeightAtPoint ) >= _minHeight && ( currentPoint.z() - mntHeightAtPoint ) <= _maxHeight )
        {
            // Alors on l'ajoute au nuage de points de sortie
            tranchePointCloud->addPoint( currentPoint );
            updateBBox( bboxBot, bboxTop, currentPoint );
        }
    }

    CT_NMPCIR registeredTranchePointCloud = PS_REPOSITORY->registerUndefinedSizePointCloud( tranchePointCloud );
    CT_Scene* trancheScene = new CT_Scene( DEFout_itmTranche, _resultatTranche, registeredTranchePointCloud );
    trancheScene->setBoundingBox( bboxBot.x(), bboxBot.y(), bboxBot.z(),
                                  bboxTop.x(), bboxTop.y(), bboxTop.z() );

    CT_StandardItemGroup* grpTranchePointCloud = new CT_StandardItemGroup( DEFout_grp, _resultatTranche );
    grpTranchePointCloud->addItemDrawable( trancheScene );
    _resultatTranche->addGroup(grpTranchePointCloud);
}

void LSIS_StepTrancheSurMntOptionnel::compute(CT_AbstractItemDrawableWithPointCloud *pointCloud)
{
    // On regarde le z du bot de labounding box
    CT_Point bot, top;
    pointCloud->getBoundingBox(bot, top);
    float zBot = bot.z();

    // On Cree un nouveau nuage pour la tranche
    CT_AbstractUndefinedSizePointCloud* tranchePointCloud = PS_REPOSITORY->createNewUndefinedSizePointCloud();
    CT_Point bboxBot = createCtPoint( std::numeric_limits<float>::max(),
                                      std::numeric_limits<float>::max(),
                                      std::numeric_limits<float>::max());
    CT_Point bboxTop = createCtPoint( -std::numeric_limits<float>::max(),
                                      -std::numeric_limits<float>::max(),
                                      -std::numeric_limits<float>::max());

    // Un iterateur pour parcourir le nuage
    CT_PointIterator itPoint( pointCloud->getPointCloudIndexRegistered() );
    while (itPoint.hasNext()&& !isStopped())
    {
        // Avance l'iterateur et recupere le point sous l'iterateur
        const CT_Point &currentPoint = itPoint.next().currentPoint();

        // Si le point est dans la zone de recherche
        if( ( currentPoint.z() - zBot ) >= _minHeight && ( currentPoint.z() - zBot ) <= _maxHeight )
        {
            // Alors on l'ajoute au nuage de points de sortie
            tranchePointCloud->addPoint( currentPoint );
            updateBBox( bboxBot, bboxTop, currentPoint );
        }
    }

    CT_NMPCIR registeredTranchePointCloud = PS_REPOSITORY->registerUndefinedSizePointCloud( tranchePointCloud );
    CT_Scene* trancheScene = new CT_Scene( DEFout_itmTranche, _resultatTranche, registeredTranchePointCloud );
    trancheScene->setBoundingBox( bboxBot.x(), bboxBot.y(), bboxBot.z(),
                                  bboxTop.x(), bboxTop.y(), bboxTop.z() );

    CT_StandardItemGroup* grpTranchePointCloud = new CT_StandardItemGroup( DEFout_grp, _resultatTranche );
    grpTranchePointCloud->addItemDrawable( trancheScene );
    _resultatTranche->addGroup(grpTranchePointCloud);
}

void LSIS_StepTrancheSurMntOptionnel::computeAndSave(CT_Grid2DXY<float> *mnt, CT_AbstractItemDrawableWithPointCloud *pointCloud, QString dirPath, QString fileName)
{
    // Cree le dossier au besoin
    QDir dir( dirPath );
    if( !dir.exists() )
    {
        dir.mkdir(".");
    }

    QFile file( dirPath + "/" + fileName );
    if( !file.open( QIODevice::WriteOnly ) )
    {
        qDebug() << "Impossible d'ecrire le fichier " << dirPath + "/" + fileName;
        exit(0);
    }
    QTextStream stream( &file );

    // On regarde le z du bot de labounding box
    CT_Point bot, top;
    pointCloud->getBoundingBox(bot, top);
    float zBot = bot.z();

    // Un iterateur pour parcourir le nuage
    CT_PointIterator itPoint( pointCloud->getPointCloudIndexRegistered() );
    float mntHeightAtPoint;
    while (itPoint.hasNext()&& !isStopped())
    {
        // Avance l'iterateur et recupere le point sous l'iterateur
        const CT_Point &currentPoint = itPoint.next().currentPoint();
        mntHeightAtPoint = mnt->valueAtXY( currentPoint.x(), currentPoint.y() );
        if( mntHeightAtPoint == mnt->NA() )
        {
            mntHeightAtPoint = zBot;
        }

        // Si le point est dans la zone de recherche
        if( ( currentPoint.z() - mntHeightAtPoint ) >= _minHeight && ( currentPoint.z() - mntHeightAtPoint ) <= _maxHeight )
        {
            // Alors on l'ajoute au fichier de sortie
            stream << currentPoint.x() << " " << currentPoint.y() << " " << currentPoint.z();
            if( itPoint.hasNext() )
            {
                stream << "\n";
            }
        }
    }

    file.close();
}

void LSIS_StepTrancheSurMntOptionnel::computeAndSave(CT_AbstractItemDrawableWithPointCloud *pointCloud, QString dirPath, QString fileName)
{
    // Cree le dossier au besoin
    QDir dir( dirPath );
    if( !dir.exists() )
    {
        dir.mkdir(".");
    }

    QFile file( dirPath + "/" + fileName );
    if( !file.open( QIODevice::WriteOnly ) )
    {
        qDebug() << "Impossible d'ecrire le fichier " << dirPath + "/" + fileName;
        exit(0);
    }
    QTextStream stream( &file );

    // On regarde le z du bot de labounding box
    CT_Point bot, top;
    pointCloud->getBoundingBox(bot, top);
    float zBot = bot.z();

    // Un iterateur pour parcourir le nuage
    CT_PointIterator itPoint( pointCloud->getPointCloudIndexRegistered() );
    while (itPoint.hasNext()&& !isStopped())
    {
        // Avance l'iterateur et recupere le point sous l'iterateur
        const CT_Point &currentPoint = itPoint.next().currentPoint();

        // Si le point est dans la zone de recherche
        if( ( currentPoint.z() - zBot ) >= _minHeight && ( currentPoint.z() - zBot ) <= _maxHeight )
        {
            // Alors on l'ajoute au fichier de sortie
            stream << currentPoint.x() << " " << currentPoint.y() << " " << currentPoint.z();
            if( itPoint.hasNext() )
            {
                stream << "\n";
            }
        }
    }

    file.close();
}

void LSIS_StepTrancheSurMntOptionnel::updateBBox(CT_Point &bot, CT_Point &top, const CT_Point &point)
{
    for( int i = 0 ; i < 3 ; i++ )
    {
        if( point(i) < bot(i) )
        {
            bot(i) = point(i);
        }

        if( point(i) > top(i) )
        {
            top(i) = point(i);
        }
    }
}
