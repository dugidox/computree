#include "lsis_stepfoostep.h"

#include "raytracing4d/lsis_beam4d.h"
#include "pixel/lsis_pixel4d.h"
#include "streamoverload/streamoverload.h"
#include "openactivecontour/lsis_activecontour4dcontinuous.h"

#include "ct_result/ct_resultgroup.h"

// Constructor : initialization of parameters
LSIS_StepFooStep::LSIS_StepFooStep(CT_StepInitializeData &dataInit) : CT_AbstractStepCanBeAddedFirst(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString LSIS_StepFooStep::getStepDescription() const
{
    return tr("Etape de test");
}

// Step detailled description
QString LSIS_StepFooStep::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString LSIS_StepFooStep::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStepCanBeAddedFirst::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepFooStep::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepFooStep(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepFooStep::createInResultModelListProtected()
{
    // No in result is needed
    setNotNeedInputResult();
}

// Creation and affiliation of OUT models
void LSIS_StepFooStep::createOutResultModelListProtected()
{
    // No OUT model definition => create an empty result
    createNewOutResultModel("emptyResult");
}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepFooStep::createPostConfigurationDialog()
{
    // No parameter dialog for this step
}

void LSIS_StepFooStep::compute()
{
    CT_Grid4D<int>* image = new CT_Grid4D<int>( NULL, NULL, 0,0,0,0, 10,10,10,10, 1,1,1,1, 0,0 );
    LSIS_Pixel4D head(0,0,0,0);
    LSIS_Pixel4D back(1,1,1,1);
    LSIS_Pixel4D back2(1,1,0,0);
    LSIS_ActiveContour4DContinuous<int> contours( image, head, back2 );
    qDebug() << "Contours = \n" << contours;
    qDebug() << head;
    qDebug() << LSIS_Point4DInt(0,1,2,3);
    qDebug() << LSIS_Point4DDouble(0,1,2,3);
    qDebug() << LSIS_Point4DFloat(0,1,2,3);

    Eigen::Matrix4f m;
    m.setRandom();
    LSIS_Point4DFloat rowFromMatrix = m.row(0);

    std::cout << m << std::endl << std::flush;
    qDebug() << rowFromMatrix;
}
