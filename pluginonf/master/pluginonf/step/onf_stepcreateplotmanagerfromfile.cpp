/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcreateplotmanagerfromfile.h"

#include "ct_itemdrawable/ct_plotgridmanager.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "ct_view/ct_asciifilechoicebutton.h"
#include "ct_view/ct_combobox.h"

#include "ct_itemdrawable/ct_circle2d.h"

// Alias for indexing models
#define DEFout_result "result"
#define DEFout_grp "grp"
#define DEFout_plotmanageritem "plotmanageritem"



// Alias for indexing models
#define DEFin_result "resultDataSource"
#define DEFin_grpShape2D "grpshape2D"
#define DEFin_shape2D "shape2D"



// Constructor : initialization of parameters
ONF_StepCreatePlotManagerFromFile::ONF_StepCreatePlotManagerFromFile(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _neededFields.append(CT_TextFileConfigurationFields("ID_Plot", QRegExp("([pP][lL][oO][tT]|[pP][lL][aA][cC][eE][tT][tT][eE]|[pP][lL][aA][cC]|[iI][dD][_][pP][lL][oO][tT]|[iI][dD][_][pP][lL][aA][cC][eE][tT][tT][eE]|[iI][dD][_][pP][lL][aA][cC])"), false));
    _neededFields.append(CT_TextFileConfigurationFields("X", QRegExp("[xX]"), false));
    _neededFields.append(CT_TextFileConfigurationFields("Y", QRegExp("[yY]"), false));

    _refFileName = "";
    _refHeader = true;
    _refSeparator = "\t";
    _refDecimal = ".";
    _refLocale = QLocale(QLocale::English, QLocale::UnitedKingdom).name();
    _refSkip = 0;

    _plotRadius = 45.0;
}

// Step description (tooltip of contextual menu)
QString ONF_StepCreatePlotManagerFromFile::getStepDescription() const
{
    return tr("1- Créée une liste de placettes à partir d'un fichier ASCII");
}

// Step detailled description
QString ONF_StepCreatePlotManagerFromFile::getStepDetailledDescription() const
{
    return tr("Charge des placettes depuis un fichier ASCII. <br>"
              "L'import est configurable, le fichier devant contenir les champs suivants :<br>"
              "- IDplot : Identifiant placette<br>"
              "- X      : Coordonnée X de l'arbre<br>"
              "- Y      : Coordonnée Y de l'arbre<br>");
}

// Step URL
QString ONF_StepCreatePlotManagerFromFile::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ONF_StepCreatePlotManagerFromFile::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ONF_StepCreatePlotManagerFromFile(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ONF_StepCreatePlotManagerFromFile::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn = createNewInResultModelForCopy(DEFin_result, tr("Emprise"), tr(""), true);
    resIn->setZeroOrMoreRootGroup();
    resIn->addGroupModel("", DEFin_grpShape2D, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    resIn->addItemModel(DEFin_grpShape2D, DEFin_shape2D, CT_AbstractAreaShape2D::staticGetType(), tr("Emprise (sans buffer)"));
}

// Creation and affiliation of OUT models
void ONF_StepCreatePlotManagerFromFile::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEFin_result);

    if(res != NULL) {
        res->addGroupModel(DEFin_grpShape2D, _outPlotGroup_ModelName, new CT_StandardItemGroup(), tr("Placettes"));
        res->addItemModel(_outPlotGroup_ModelName, _outPlot_ModelName, new CT_Circle2D(), tr("Placette"));
        res->addItemAttributeModel(_outPlot_ModelName, _outPlotAtt_ModelName, new CT_StdItemAttributeT<QString>(CT_AbstractCategory::DATA_VALUE), tr("PlotID"));
    }

}

// Semi-automatic creation of step parameters DialogBox
void ONF_StepCreatePlotManagerFromFile::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addAsciiFileChoice("Fichier des placettes", "Fichier ASCII (*.txt ; *.asc)", true, _neededFields, _refFileName, _refHeader, _refSeparator, _refDecimal, _refLocale, _refSkip, _refColumns);
    configDialog->addDouble(tr("Rayon de placette"), "m", 0, 1e+10, 2, _plotRadius);
}

void ONF_StepCreatePlotManagerFromFile::compute()
{
    // read plot file
    QList<QString> ids;
    QList<double> xcoord;
    QList<double> ycoord;


    int colIDplot_ref  = _refColumns.value("ID_Plot", -1);
    if (colIDplot_ref < 0) {return;}

    QFile fRef(_refFileName);
    if (fRef.exists() && fRef.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&fRef);
        stream.setLocale(_refLocale);

        int colX   = _refColumns.value("X", -1);
        int colY   = _refColumns.value("Y", -1);

        if (colX < 0) {PS_LOG->addMessage(LogInterface::error, LogInterface::step, QString(tr("Champ X non défini")));}
        if (colY < 0) {PS_LOG->addMessage(LogInterface::error, LogInterface::step, QString(tr("Champ Y non défini")));}

        if (colX >= 0 && colY >= 0)
        {

            int colMax = colX;
            if (colY   > colMax) {colMax = colY;}
            if (colIDplot_ref > colMax) {colMax = colIDplot_ref;}

            for (int i = 0 ; i < _refSkip ; i++) {stream.readLine();}
            if (_refHeader) {stream.readLine();}

            size_t cpt = 1;
            while (!stream.atEnd())
            {
                QString line = stream.readLine();
                cpt++;
                if (!line.isEmpty())
                {
                    QStringList values = line.split(_refSeparator);
                    if (values.size() >= colMax)
                    {
                        QString plot = "";
                        if (colIDplot_ref >= 0)
                        {
                            plot =  values.at(colIDplot_ref);
                        }

                        bool okX, okY;
                        double x = _refLocale.toDouble(values.at(colX), &okX);
                        double y = _refLocale.toDouble(values.at(colY), &okY);

                        if (okX && okY)
                        {
                            ids.append(plot);
                            xcoord.append(x);
                            ycoord.append(y);
                        } else {
                            PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("Ligne %1 du fichier REF non valide")).arg(cpt));
                        }
                    }
                }
            }
        }
        fRef.close();
    }

    if (xcoord.size() > 0)
    {
        int cpt = 0;
        QList<CT_ResultGroup*> outResultList = getOutResultList();
        CT_ResultGroup* resOut = outResultList.at(0);

        CT_ResultGroupIterator it(resOut, this, DEFin_grpShape2D);
        while(!isStopped() && it.hasNext())
        {
            CT_StandardItemGroup *group = (CT_StandardItemGroup*) it.next();

            if (group != NULL)
            {
                const CT_AbstractAreaShape2D *shape2D = (CT_AbstractAreaShape2D*) group->firstItemByINModelName(this, DEFin_shape2D);

                if (shape2D != NULL)
                {
                    for (int i = 0 ; i < xcoord.size() ; i++)
                    {
                        double x = xcoord.value(i);
                        double y = ycoord.value(i);
                        QString plot = ids.value(i);

                        if (shape2D->contains(x, y))
                        {
                            ++cpt;
                            CT_StandardItemGroup* plotGroup = new CT_StandardItemGroup(_outPlotGroup_ModelName.completeName(), resOut);

                            CT_Circle2D* circle = new CT_Circle2D(_outPlot_ModelName.completeName(), resOut, new CT_Circle2DData(Eigen::Vector2d(x, y), _plotRadius));
                            plotGroup->addItemDrawable(circle);
                            circle->addItemAttribute(new CT_StdItemAttributeT<QString>(_outPlotAtt_ModelName.completeName(), CT_AbstractCategory::DATA_ID, resOut, plot));

                            group->addGroup(plotGroup);
                        }
                    }
                }
            }
        }
        PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("%1 placettes sur l'emprise")).arg(cpt));
    }
}
