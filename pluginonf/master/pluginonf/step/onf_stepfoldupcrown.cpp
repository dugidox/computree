#include "onf_stepfoldupcrown.h"

// Utilise le depot
#include "ct_global/ct_context.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_iterator/ct_mutablepointiterator.h"

// Alias for indexing in models
#define DEF_resultIn_inputResult "inputResult"
#define DEF_groupIn_inputScene "inputGroup"
#define DEF_itemIn_scene "inputScene"

#include <QDebug>

// Constructor : initialization of parameters
ONF_StepFoldUpCrown::ONF_StepFoldUpCrown(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString ONF_StepFoldUpCrown::getStepDescription() const
{
    return tr("Fold Up Crown");
}

// Step copy method
CT_VirtualAbstractStep* ONF_StepFoldUpCrown::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ONF_StepFoldUpCrown(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ONF_StepFoldUpCrown::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_resultIn_inputResult, tr("Scene(s)"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_groupIn_inputScene, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultModel->addItemModel(DEF_groupIn_inputScene, DEF_itemIn_scene, CT_Scene::staticGetType(), tr("Scene(s)"));
}

// Creation and affiliation of OUT models
void ONF_StepFoldUpCrown::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_resultIn_inputResult);

    if (resultModel != NULL)
    {
        resultModel->addItemModel(DEF_groupIn_inputScene, _outSceneModelName, new CT_Scene(), tr("FoldUp Scene"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void ONF_StepFoldUpCrown::createPostConfigurationDialog()
{
}

void ONF_StepFoldUpCrown::compute()
{
    CT_ResultGroup *outResult = getOutResultList().first();

    CT_ResultGroupIterator it(outResult, this, DEF_groupIn_inputScene);
    while(!isStopped() && it.hasNext())
    {
        CT_StandardItemGroup *group = (CT_StandardItemGroup*) it.next();

        if (group != NULL)
        {
            const CT_Scene* itemIn_scene = (CT_Scene*) group->firstItemByINModelName(this, DEF_itemIn_scene);
            const CT_AbstractPointCloudIndex *cloudIndex = itemIn_scene->getPointCloudIndex();
            CT_PointIterator itP(cloudIndex);

            // On Cree un nouveau nuage qui sera le translate
            CT_NMPCIR outputCloud = PS_REPOSITORY->createNewPointCloud(cloudIndex->size());
            CT_MutablePointIterator itPM(outputCloud);

            Eigen::Vector3d apex(0, 0, -std::numeric_limits<double>::max());

            while (itP.hasNext())
            {
                itP.next();
                const CT_Point &point = itP.currentPoint();

                if (point(2) > apex(2))
                {
                    apex = point;
                }
            }


            size_t i = 0;
            itP.toFront();
            // On applique la translation a tous les points du nuage
            while (itP.hasNext() && itPM.hasNext())
            {
                itP.next();
                const CT_Point &point = itP.currentPoint();
                Eigen::Vector3d xyPoint = point;
                xyPoint(0) = apex(0);
                xyPoint(1) = apex(1) + sqrt(pow(apex(0) - point(0), 2) + pow(apex(1) - point(1), 2));
                xyPoint(2) = point(2);

                itPM.next().replaceCurrentPoint(xyPoint);

                // Barre de progression
                setProgress(100.0*i++ /cloudIndex->size());

                // On regarde si on est en debug mode
                waitForAckIfInDebugMode();
            }

            CT_Scene* itemOut_scene = new CT_Scene(_outSceneModelName.completeName(), outResult, outputCloud);
            itemOut_scene->updateBoundingBox();

            group->addItemDrawable(itemOut_scene);
        }
    }
}
