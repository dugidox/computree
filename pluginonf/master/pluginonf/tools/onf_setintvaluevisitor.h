#ifndef ONF_SETINTVALUEVISITOR_H
#define ONF_SETINTVALUEVISITOR_H

#include "ct_itemdrawable/tools/gridtools/ct_abstractgrid3dbeamvisitor.h"
#include "ct_itemdrawable/ct_grid3d.h"

class ONF_SetIntValueVisitor : public CT_AbstractGrid3DBeamVisitor
{
public:

    ONF_SetIntValueVisitor(CT_Grid3D<int> *grid, int value);

    virtual void visit(const size_t &index, const CT_Beam *beam);

private:
    CT_Grid3D<int>*     _grid;
    int                 _value;
};

#endif // ONF_SETINTVALUEVISITOR_H
