/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepconvertscenetocluster.h"

ONF_StepConvertSceneToCluster::ONF_StepConvertSceneToCluster() : SuperClass()
{
}

QString ONF_StepConvertSceneToCluster::description() const
{
    return tr("Convertir CT_Scene en CT_PointCluster");
}

QString ONF_StepConvertSceneToCluster::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString ONF_StepConvertSceneToCluster::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepConvertSceneToCluster::createNewInstance() const
{
    return new ONF_StepConvertSceneToCluster();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepConvertSceneToCluster::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroup *res_inRes = createNewInResultModel(_inRes);
    res_inRes->setRootGroup(_inG);
    res_inRes->addItemModel(_inG, _inScene, CT_Scene::staticGetType());

}

void ONF_StepConvertSceneToCluster::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroup *res_res = createNewOutResultModel(_outRes, tr(""));
    res_res->setRootGroup(_outGr, new CT_StandardItemGroup());
    res_res->addItemModel(_outGr, _outCluster, new CT_PointCluster());

}

void ONF_StepConvertSceneToCluster::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    // No parameter dialog for this step
}

void ONF_StepConvertSceneToCluster::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* res_inRes = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_res = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator it_inG(res_inRes, this, _inG);
    while (it_inG.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grp_inG = (CT_AbstractItemGroup*) it_inG.next();

        const CT_Scene* item_inScene = (CT_Scene*)grp_inG->firstItemByINModelName(this, _inScene);
        if (item_inScene != nullptr)
        {
            const CT_AbstractPointCloudIndex *PCI_item_inScene = item_inScene->pointCloudIndex();

            CT_StandardItemGroup* grp_gr= new CT_StandardItemGroup(_outGr, res_res);
            res_res->addGroup(grp_gr);

            CT_PointCluster* item_cluster = new CT_PointCluster(_outCluster, res_res);
            grp_gr->addSingularItem(item_cluster);

            CT_PointIterator itP(PCI_item_inScene);
            while (itP.hasNext() && !isStopped())
            {
                size_t index = itP.next().currentGlobalIndex();
                item_cluster->addPoint(index);
            }
        }
    }

}
