/****************************************************************************
 Copyright (C) 2010-2019 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepsmoothdem.h"

#include "ct_log/ct_logmanager.h"

#define EPSILON 0.000001

ONF_StepSmoothDEM::ONF_StepSmoothDEM() : SuperClass()
{
    _smoothDist = 2;
    _name = tr("MNS");
}

QString ONF_StepSmoothDEM::description() const
{
    return tr("Lisser un MNE (MNS, MNT ou autre)");
}

QString ONF_StepSmoothDEM::detailledDescription() const
{
    return tr("Cette étape permet d'appliquer un lissage moyen à un raster<float>.");
}

CT_VirtualAbstractStep* ONF_StepSmoothDEM::createNewInstance() const
{
    // cree une copie de cette etape
    return new ONF_StepSmoothDEM();
}

/////////////////////// PROTECTED ///////////////////////

void ONF_StepSmoothDEM::declareInputModels(CT_StepInModelStructureManager& manager)
{

    manager.addResult(_inResult, tr("MNE à interpoler"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inDEM, tr("MNE à interpoler"));
}

void ONF_StepSmoothDEM::fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog)
{
    preInputConfigDialog->addString(tr("Type/nom de raster sélectionné (MNS/MNT/MNC...)"), "", _name);
}


void ONF_StepSmoothDEM::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addInt(tr("Voisinage de lissage :"), "Cases", 1, 99999999, _smoothDist);
}

void ONF_StepSmoothDEM::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outDEM, tr("%1 lissé").arg(_name));
}

void ONF_StepSmoothDEM::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_Image2D<float>* inDEM : group->singularItems(_inDEM))
        {
            if (isStopped()) {return;}

            setProgress(20);

            // interpolation du raster
            CT_Image2D<float>* outDEM = new CT_Image2D<float>(inDEM->minX(), inDEM->minY(), inDEM->xdim(), inDEM->ydim(), inDEM->resolution(), inDEM->minZ(), inDEM->NA(), -9999);

            int cpt = 0;
            int ncells = inDEM->xdim() * inDEM->ydim();
            for (int xx = 0 ; xx < inDEM->xdim() ; ++xx)
            {
                for (int yy = 0 ; yy < inDEM->ydim() ; ++yy)
                {

                    QList<float> neighbours = inDEM->neighboursValues(xx, yy, int(_smoothDist), false, CT_Image2D<float>::CM_DropCenter);
                    int size_neigh = neighbours.size();
                    double somme = 0;
                    for (int i = 0 ; i < size_neigh ; i++)
                    {
                        somme += double(neighbours.at(i));
                    }

                    if (size_neigh >0)
                    {
                        somme = somme / double(size_neigh);
                    }

                    outDEM->setValue(xx, yy, float(somme));

                    // Progression Etape 5
                    setProgress(float((cpt++/ncells)*60.0 + 20.0));
                }
            }

            PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Lissage du DEM terminé"));

            setProgress(80.0f);

            // ajout du raster lissé
            outDEM->computeMinMax();
            group->addSingularItem(_outDEM, outDEM);
        }
    }
    setProgress(100.0f);
}

