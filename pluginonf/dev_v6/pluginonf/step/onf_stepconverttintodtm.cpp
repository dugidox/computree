/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepconverttintodtm.h"

#include "ct_itemdrawable/tools/image2dtools/ct_image2dnaturalneighboursinterpolator.h"

#define EPSILON 0.000001

ONF_StepConvertTINtoDTM::ONF_StepConvertTINtoDTM() : SuperClass()
{
    _gridsize   = 0.5;

}

QString ONF_StepConvertTINtoDTM::description() const
{
    return tr("Convertir un TIN en MNT");
}

QString ONF_StepConvertTINtoDTM::detailledDescription() const
{
    return tr("TO DO");
}

CT_VirtualAbstractStep* ONF_StepConvertTINtoDTM::createNewInstance() const
{
    // cree une copie de cette etape
    return new ONF_StepConvertTINtoDTM();
}

/////////////////////// PROTECTED ///////////////////////

void ONF_StepConvertTINtoDTM::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("TIN"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInTIN, CT_Triangulation2D::staticGetType(), tr("TIN"));
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInArea, CT_AbstractAreaShape2D::staticGetType(),
                              tr("Emprise"), "", CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);
}

void ONF_StepConvertTINtoDTM::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Résolution de la grille :"), "cm", 1, 1000, 0, _gridsize, 100);
}

void ONF_StepConvertTINtoDTM::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(resultModel != nullptr)
        resultModel->addItemModel(DEF_SearchInGroup, _outDTMModelName, new CT_Image2D<float>(), tr("MNT"));
}

void ONF_StepConvertTINtoDTM::compute()
{
    // recupere les resultats de sortie
    const QList<CT_ResultGroup*> &outResList = getOutResultList();
    // récupération des modéles out
    CT_ResultGroup *outResult = outResList.at(0);

    CT_ResultGroupIterator it(outResult, this, DEF_SearchInGroup);
    while (!isStopped() && it.hasNext())
    {
        CT_StandardItemGroup* group = (CT_StandardItemGroup*) it.next();

        if (group != nullptr)
        {
            const CT_Triangulation2D *tin = (const CT_Triangulation2D*)group->firstItemByINModelName(this, DEF_SearchInTIN);
            const CT_AbstractAreaShape2D *emprise = (const CT_AbstractAreaShape2D*)group->firstItemByINModelName(this, DEF_SearchInArea);

            if (tin != nullptr)
            {
                Eigen::Vector3d min, max;
                if (emprise != nullptr)
                {
                    emprise->boundingBox(min, max);
                } else {
                    tin->boundingBox(min, max);
                }

                double minX = min(0);
                double minY = min(1);
                double maxX = max(0) - EPSILON;
                double maxY = max(1) - EPSILON;

                CT_Image2D<float>* mnt = CT_Image2D<float>::createImage2DFromXYCoords(_outDTMModelName.completeName(), outResult, minX, minY, maxX, maxY, _gridsize, 0, -9999, -9999);

                CT_DelaunayTriangulation *triangulation = tin->getDelaunayT();

                CT_DelaunayTriangle* refTriangle = (CT_DelaunayTriangle*) triangulation->getRefTriangle();

                for (size_t xx = 0 ; xx < mnt->colDim() ; xx++)
                {
                    for (size_t yy = 0 ; yy < mnt->linDim() ; yy++)
                    {
                        Eigen::Vector3d coord;
                        mnt->getCellCenterCoordinates(xx, yy, coord);

                        double z = -9999;
                        refTriangle = (CT_DelaunayTriangle*) triangulation->getZCoordForXY(coord(0), coord(1), z, refTriangle);

                        if (refTriangle != nullptr)
                        {
                            mnt->setValue(xx, yy, (float)z);
                        }
                    }
                }

                setProgress(90);
                PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Convertion terminée"));

                // ajout du raster MNT
                group->addSingularItem(mnt);
                mnt->computeMinMax();
            }

        }
    }
    setProgress(100);
}

