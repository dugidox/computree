#ifndef ONF_STEPFILTERWIRES_H
#define ONF_STEPFILTERWIRES_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_grid3d_points.h"
#include "ct_itemdrawable/ct_scene.h"

class ONF_StepFilterWires: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepFilterWires();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
    int                     _nbNeigbours;
    double                  _distMax;
    double                  _resGrid;
    double                  _slopeThreshold;
    double                  _rmseThreshold;
    bool                    _slopeMax;
    bool                    _rmseMax;


    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarTemplated<float>>                 _outSlope;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarTemplated<float>>                 _outRMSE;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outSceneKept;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outSceneWires;



};

#endif // ONF_STEPFILTERWIRES_H
