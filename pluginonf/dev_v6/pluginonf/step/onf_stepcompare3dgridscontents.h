/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPCOMPARE3DGRIDSCONTENTS_H
#define ONF_STEPCOMPARE3DGRIDSCONTENTS_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_grid3d.h"

class ONF_StepCompare3DGridsContents: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepCompare3DGridsContents();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
    double    _threshold;


    CT_HandleInResultGroup<>                                                            _inResult1;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup1;
    CT_HandleInStdGroup<>                                                               _inGroup1;
    CT_HandleInSingularItem<CT_AbstractGrid3D>                                          _inGrid1;

    CT_HandleInResultGroup<>                                                            _inResult2;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup2;
    CT_HandleInStdGroup<>                                                               _inGroup2;
    CT_HandleInSingularItem<CT_AbstractGrid3D>                                          _inGrid2;

    CT_HandleOutResultGroup                             _outResult;
    CT_HandleOutStdGroup                                _outRootGroup;
    CT_HandleOutSingularItem<CT_Grid3D<short> >         _outGrid;

};

#endif // ONF_STEPCOMPARE3DGRIDSCONTENTS_H
