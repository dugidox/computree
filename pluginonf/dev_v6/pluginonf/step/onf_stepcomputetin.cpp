/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcomputetin.h"

#include "ct_log/ct_logmanager.h"

ONF_StepComputeTIN::ONF_StepComputeTIN() : SuperClass()
{

}

QString ONF_StepComputeTIN::description() const
{
    return tr("Créer TIN à partir de points");
}

QString ONF_StepComputeTIN::detailledDescription() const
{
    return tr("Créée un Triangulated Irregular Network à partir des points.");
}

CT_VirtualAbstractStep* ONF_StepComputeTIN::createNewInstance() const
{
    // cree une copie de cette etape
    return new ONF_StepComputeTIN();
}

/////////////////////// PROTECTED ///////////////////////

void ONF_StepComputeTIN::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Points sol"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Points sol"));
}

void ONF_StepComputeTIN::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outTIN, tr("TIN"));
}

void ONF_StepComputeTIN::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* scene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            if (scene != nullptr)
            {
                const CT_AbstractPointCloudIndex *pointCloudIndex = scene->pointCloudIndex();

                CT_DelaunayTriangulation *delaunay = new CT_DelaunayTriangulation();
                delaunay->init(scene->minX() - 1.0, scene->minY() - 1.0, scene->maxX() + 1.0, scene->maxY() + 1.0);

                CT_PointIterator itP(pointCloudIndex);
                while(itP.hasNext() && !isStopped())
                {
                    const CT_Point &point =itP.next().currentPoint();

                    Eigen::Vector3d* pt = new Eigen::Vector3d(point);
                    delaunay->addVertex(pt, true);
                }
                setProgress(50);

                delaunay->doInsertion();
                delaunay->updateCornersZValues();

                CT_Triangulation2D* triangulation = new CT_Triangulation2D(delaunay);
                group->addSingularItem(_outTIN, triangulation);
            } else {
                PS_LOG->addErrorMessage(LogInterface::step, tr("Aucun point sol !"));
            }
        }
        setProgress(100);
    }
}
