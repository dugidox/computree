/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcomputechm.h"

ONF_StepComputeCHM::ONF_StepComputeCHM() : SuperClass()
{
}

QString ONF_StepComputeCHM::description() const
{
    return tr("Créer MNH");
}

QString ONF_StepComputeCHM::detailledDescription() const
{
    return tr("Cette étape permet de générer un MNH à partir d'un MNS et d'un MNT. L'emprise et la résolution du CHM seront calés sur le MNS. ");
}

CT_VirtualAbstractStep* ONF_StepComputeCHM::createNewInstance() const
{
    // cree une copie de cette etape
    return new ONF_StepComputeCHM();
}

/////////////////////// PROTECTED ///////////////////////

void ONF_StepComputeCHM::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResultDTM, tr("DTM"));
    manager.setZeroOrMoreRootGroup(_inResultDTM, _inZeroOrMoreRootGroupDTM);
    manager.addGroup(_inZeroOrMoreRootGroupDTM, _inGroupDTM);
    manager.addItem(_inGroupDTM, _inDTM, tr("DTM"));

    manager.addResult(_inResultDSM, tr("DSM"));
    manager.setZeroOrMoreRootGroup(_inResultDSM, _inZeroOrMoreRootGroupDSM);
    manager.addGroup(_inZeroOrMoreRootGroupDSM, _inGroupDSM);
    manager.addItem(_inGroupDSM, _inDSM, tr("DSM"));
}

void ONF_StepComputeCHM::declareOutputModels(CT_StepOutModelStructureManager& manager)
{    
    manager.addResultCopy(_inResultDSM);
    manager.addItem(_inGroupDSM, _outCHM, tr("CHM"));
}

void ONF_StepComputeCHM::compute()
{        
    const CT_Image2D<float>* mnt = nullptr;
    for (const CT_Image2D<float>* constMnt : _inDTM.iterateInputs(_inResultDTM))
    {
        mnt = constMnt;
    }
    if (mnt == nullptr) {return;}

    for (CT_StandardItemGroup* group : _inGroupDSM.iterateOutputs(_inResultDSM))
    {
        for (const CT_Image2D<float>* mns : group->singularItems(_inDSM))
        {
            if (isStopped()) {return;}

            CT_Image2D<float>* chm = new CT_Image2D<float>(mns->minX(), mns->minY(), mns->xdim(), mns->ydim(), mns->resolution(), mns->minZ(), mns->NA(), 0);

            size_t ncells = mns->nCells();
            for (size_t index = 0 ; index < ncells ; index++)
            {
                Eigen::Vector3d center;
                mns->getCellCenterCoordinates(index, center);

                float mnsVal = mns->valueAtIndex(index);
                float mntVal = mnt->valueAtCoords(center(0), center(1));

                if (!qFuzzyCompare(mnsVal, mns->NA()) && !qFuzzyCompare(mntVal, mnt->NA()))
                {
                    chm->setValueAtIndex(index, mnsVal - mntVal);
                } else {
                    chm->setValueAtIndex(index, chm->NA());
                }

                setProgress(float(90.0*index/ncells));
            }

            // ajout du raster MNS
            group->addSingularItem(_outCHM,chm);
            chm->computeMinMax();
        }
        setProgress(100.0f);
    }
}
