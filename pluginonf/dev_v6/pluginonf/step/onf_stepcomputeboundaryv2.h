/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPCOMPUTEBOUNDARYV2_H
#define ONF_STEPCOMPUTEBOUNDARYV2_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_image2d.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_standarditemgroup.h"
#include "ct_itemdrawable/ct_loopcounter.h"
#include "ct_itemdrawable/ct_polygon2d.h"

class ONF_StepComputeBoundaryV2: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepComputeBoundaryV2();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:
    double  _res;
    CT_Image2D<quint8>* _outRaster;

    CT_HandleInResultGroupCopy<>                                                        _inRfootprint;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroupFootprint;
    CT_HandleInStdGroup<>                                                               _inGrpfootprint;
    CT_HandleInSingularItem<CT_AbstractGeometricalItem>                                 _inFootprint;

    CT_HandleInResultGroup<>                                                            _inRscene;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroupScene;
    CT_HandleInStdGroup<>                                                               _inGrpsc;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;

    CT_HandleInResultGroup<0,1>                                                         _inResultCounter;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroupCounter;
    CT_HandleInSingularItem<CT_LoopCounter>                                             _inCounter;

    CT_HandleOutResultGroup                                                             _outResult;
    CT_HandleOutStdGroup                                                                _outRootGroup;
    CT_HandleOutSingularItem<CT_Image2D<quint8>>                                        _outFootprintRaster;



};

#endif // ONF_STEPCOMPUTEBOUNDARYV2_H

