/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepfiltergroupsbygroupsnumber.h"

ONF_StepFilterGroupsByGroupsNumber::ONF_StepFilterGroupsByGroupsNumber() : SuperClass()
{
    _minSize = 4;
}

QString ONF_StepFilterGroupsByGroupsNumber::description() const
{
    return tr("2- Filter les Billons par nombre de Clusters");
}

QString ONF_StepFilterGroupsByGroupsNumber::detailledDescription() const
{
    return tr("Cette étape très générique travaille sur deux niveau de groupes.<br>"
              "Tout groupe du niveau 1 contenant <b>nombre de groupes</b> de niveau 2 insuffisant est éliminé.<br>"
              "Un usage de cette étape est d'éliminer des groupes de niveau 1 ne contenant pas assez de groupes de niveau 2.<br>"
              "Comme par exemple après une étape ONF_StepDetectSection.");
}

CT_VirtualAbstractStep* ONF_StepFilterGroupsByGroupsNumber::createNewInstance() const
{
    // crée une copie de cette étape
    return new ONF_StepFilterGroupsByGroupsNumber();
}

//////////////////// PROTECTED //////////////////

void ONF_StepFilterGroupsByGroupsNumber::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addGroup(_inGroup, _inGroupToCount);
}

void ONF_StepFilterGroupsByGroupsNumber::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addInt(tr("Nombre de groupes minimum de niveau 2 dans un groupe de niveau 1"), "groupes", 0, 1000, _minSize);
}

void ONF_StepFilterGroupsByGroupsNumber::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
}

void ONF_StepFilterGroupsByGroupsNumber::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        if (isStopped()) {return;}

        if (group->groups(_inGroupToCount).count() < _minSize) {group->removeFromParent(true);}
    }

    setProgress( 100 );
}
