/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepapplydtmtocircle2d.h"

#include "ct_shapedata/ct_linedata.h"


ONF_StepApplyDTMToCircle2D::ONF_StepApplyDTMToCircle2D() : SuperClass()
{
    _offset = 1.3;
}

QString ONF_StepApplyDTMToCircle2D::description() const
{
    return tr("Translater des cercles 2D sur un MNT");
}

QString ONF_StepApplyDTMToCircle2D::detailledDescription() const
{
    return tr("Permet d'ajouter une coordonnées Z, issue d'un MNT, à des cercles 2D, pour produire des cercles 3D (horizontaux).");
}

CT_VirtualAbstractStep* ONF_StepApplyDTMToCircle2D::createNewInstance() const
{
    // cree une copie de cette etape
    return new ONF_StepApplyDTMToCircle2D();
}

//////////////////// PROTECTED //////////////////

void ONF_StepApplyDTMToCircle2D::declareInputModels(CT_StepInModelStructureManager& manager)
{   
    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inCircle2D, tr("Scene(s)"));
    manager.addItemAttribute(_inCircle2D, _inCircle2DAttID, CT_AbstractCategory::DATA_VALUE, tr("ID"));
    manager.addItemAttribute(_inCircle2D, _inCircle2DAttVal, CT_AbstractCategory::DATA_VALUE, tr("Value"));

    manager.addResult(_inResultDTM, tr("MNT"), "", true);
    manager.setZeroOrMoreRootGroup(_inResultDTM, _inZeroOrMoreRootGroupDTM);
    manager.addGroup(_inZeroOrMoreRootGroupDTM, _inGroupDTM);
    manager.addItem(_inGroupDTM, _inDTM, tr("MNT"));
}

void ONF_StepApplyDTMToCircle2D::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    postInputConfigDialog->addDouble(tr("Offset"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 2, _offset);
}

void ONF_StepApplyDTMToCircle2D::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outcircle3D, tr("Cercle 3D"));
    manager.addItemAttribute(_outcircle3D, _outcircle3DID, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("ID"));
    manager.addItemAttribute(_outcircle3D, _outcircle3DVal, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_NUMBER), tr("Value"));
}

void ONF_StepApplyDTMToCircle2D::compute()
{
    const CT_Image2D<float>* mnt = nullptr;
    for (const CT_Image2D<float>* imageIn : _inDTM.iterateInputs(_inResultDTM))
    {
            mnt = imageIn;
    }

    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_Circle2D* circle2d : group->singularItems(_inCircle2D))
        {
            if (isStopped()) {return;}

            QString circleID;
            const CT_AbstractItemAttribute* attID = circle2d->itemAttribute(_inCircle2DAttID);
            if (attID != nullptr) {circleID = attID->toString(circle2d, nullptr);}

            double circleVal;
            const CT_AbstractItemAttribute* attVal = circle2d->itemAttribute(_inCircle2DAttVal);
            if (attVal != nullptr) {circleVal = attVal->toDouble(circle2d, nullptr);}

            double zval = double(mnt->valueAtCoords(circle2d->centerX(), circle2d->centerY())) + _offset;

            CT_Circle* circle = new CT_Circle(new CT_CircleData(Eigen::Vector3d(circle2d->centerX(), circle2d->centerY(), zval), Eigen::Vector3d(0, 0, 1), circle2d->getRadius(), 0));
            group->addSingularItem(_outcircle3D, circle);

            circle->addItemAttribute(_outcircle3DID, new CT_StdItemAttributeT<QString>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), circleID));
            circle->addItemAttribute(_outcircle3DVal, new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_NUMBER), circleVal));
        }
    }
}

