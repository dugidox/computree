/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPEXTRACTPOINTSFORPLOTS_H
#define ONF_STEPEXTRACTPOINTSFORPLOTS_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/abstract/ct_abstractareashape2d.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_image2d.h"

class ONF_StepExtractPointsForPlots: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepExtractPointsForPlots();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    struct PlotPointsIndices {

        PlotPointsIndices(const PlotPointsIndices* other)
        {
            _indices = new CT_PointCloudIndexVector();
            _indices->setSortType(CT_PointCloudIndexVector::NotSorted);
            _group = other->_group;

            for (size_t i = 0 ; i < other->_indices->size() ; i++)
            {
                _indices->addIndex(other->_indices->indexAt(i));
            }
        }

        PlotPointsIndices(CT_StandardItemGroup* group)
        {
            _indices = new CT_PointCloudIndexVector();
            _indices->setSortType(CT_PointCloudIndexVector::NotSorted);
            _group = group;
        }

        CT_StandardItemGroup*            _group;
        CT_PointCloudIndexVector*        _indices;
    };

    double                  _cellSize;

    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGrpScene;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInStdGroup<>                                                               _inGrpPlot;
    CT_HandleInSingularItem<CT_AbstractAreaShape2D>                                     _inPlot;

    CT_HandleOutSingularItem<CT_Scene>                                                  _outPoints;


};

#endif // ONF_STEPEXTRACTPOINTSFORPLOTS_H
