/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPCOMPUTECROWNPROJECTION_H
#define ONF_STEPCOMPUTECROWNPROJECTION_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_standarditemgroup.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_beam.h"
#include "ct_itemdrawable/ct_polygon2d.h"

class ONF_StepComputeCrownProjection: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

    struct level
    {
    public:
        level(double zmin, double zmax, double zlevel)
        {
            _zmin = zmin;
            _zmax = zmax;
            _zlevel = zlevel;
        }

        double  _zmin;
        double  _zmax;
        double  _zlevel;
        QList<Eigen::Vector2d*> _pointList;
    };

public:

    ONF_StepComputeCrownProjection();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Declaration of autoRenames Variables (groups or items added to In models copies)

    CT_ResultGroup*        _rscene;

    // Step parameters
    bool        _computeSlices;
    double       _spacing;
    double      _thickness;
    bool        _computeDirs;
    int         _nbDir;

    double    _zmin;
    double    _zmax;

    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;

    CT_HandleOutSingularItem<CT_Polygon2D>                                              _outconvexHull;
    CT_HandleOutStdGroup                                                                _outgrpSlice;
    CT_HandleOutSingularItem<CT_Polygon2D>                                              _outscliceCvx;


    void computeConvexHullForOneSceneGroup(CT_StandardItemGroup *group) const;
};

#endif // ONF_STEPCOMPUTECROWNPROJECTION_H
