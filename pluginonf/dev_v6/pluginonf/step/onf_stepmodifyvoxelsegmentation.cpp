/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepmodifyvoxelsegmentation.h"

#include "ct_math/ct_mathpoint.h"

#include <QtConcurrent/QtConcurrentMap>
#include <QMessageBox>

ONF_StepModifyVoxelSegmentation::ONF_StepModifyVoxelSegmentation() : SuperClass()
{
    _keepValidatedOnly = false;
    m_doc = nullptr;

    setManual(true);
}

QString ONF_StepModifyVoxelSegmentation::description() const
{
    return tr("Modify voxel grid segmentation");
}

QString ONF_StepModifyVoxelSegmentation::detailledDescription() const
{
    return tr("");
}

QString ONF_StepModifyVoxelSegmentation::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepModifyVoxelSegmentation::createNewInstance() const
{
    return new ONF_StepModifyVoxelSegmentation();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepModifyVoxelSegmentation::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("Grilles"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInScene, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Scene à segmenter"));
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInGridPoints, CT_Grid3D_Points::staticGetType(), tr("Grille de points"));
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInGridSeeds, CT_Grid3D_Sparse<int>::staticGetType(), tr("Grille segmentée"));
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInGridReverseTopology, CT_Grid3D_Points::staticGetType(), tr("Grille topologique"));
}

void ONF_StepModifyVoxelSegmentation::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(res != nullptr) {
        res->addItemModel(DEF_SearchInGroup, _outGridModelName, new CT_Grid3D_Sparse<int>(), tr("Grille segmentée corrigée"));
        res->addGroupModel(DEF_SearchInGroup, _outGrpIdModelName, new CT_StandardItemGroup(), tr("IDs"));
        res->addItemModel(_outGrpIdModelName, _outIdItemModelName, new CT_ItemAttributeList(), tr("Labels"));
        res->addItemAttributeModel(_outIdItemModelName, _outattClusterModelName, new CT_StdItemAttributeT<qint32>(CT_AbstractCategory::DATA_ID), tr("VoxelCluster"));
        res->addItemAttributeModel(_outIdItemModelName, _outattLabelModelName, new CT_StdItemAttributeT<QString>(CT_AbstractCategory::DATA_VALUE), tr("Label"));
    }
}

void ONF_StepModifyVoxelSegmentation::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    postInputConfigDialog->addBool(tr("Ne conserver que les arbres validés ?"), "", "", _keepValidatedOnly);
}

void ONF_StepModifyVoxelSegmentation::compute()
{
    _positionLabels.clear();
    CT_ResultGroup* res_out = getOutResultList().first();
    CT_StandardItemGroup* grp = nullptr;
    // Création de la liste des positions 2D
    CT_ResultGroupIterator grpPosIt(res_out, this, DEF_SearchInGroup);
    if (grpPosIt.hasNext())
    {
        grp = (CT_StandardItemGroup*) grpPosIt.next();

        _scene = (CT_AbstractItemDrawableWithPointCloud*)grp->firstItemByINModelName(this, DEF_SearchInScene);
        _pointGrid = (CT_Grid3D_Points*)grp->firstItemByINModelName(this, DEF_SearchInGridPoints);
        CT_Grid3D_Sparse<int>* seedGrid = (CT_Grid3D_Sparse<int>*)grp->firstItemByINModelName(this, DEF_SearchInGridSeeds);
        _topologyGrid = (CT_Grid3D_Points*)grp->firstItemByINModelName(this, DEF_SearchInGridReverseTopology);

        if (_scene != nullptr && _pointGrid != nullptr && seedGrid != nullptr && _topologyGrid != nullptr)
        {
            _outSegmentationGrid = new CT_Grid3D_Sparse<int>(_outGridModelName.completeName(), res_out, seedGrid->minX(), seedGrid->minY(), seedGrid->minZ(), seedGrid->xdim(), seedGrid->ydim(), seedGrid->zdim(), seedGrid->resolution(), -1, -1);

            QList<size_t> list;
            seedGrid->getIndicesWithData(list);

            for (int i = 0 ; i < list.size() ; i++)
            {
                size_t index = list.at(i);
                _outSegmentationGrid->setValueAtIndex(index, seedGrid->valueAtIndex(index));
            }
        }
        _outSegmentationGrid->computeMinMax();
    }

    if (grp != nullptr)
    {

        // Début de la partie interactive
        m_doc = nullptr;

        m_status = 0;
        requestManualMode();

        m_status = 1;
        requestManualMode();

        if (_keepValidatedOnly)
        {
            QList<size_t> list;
            _pointGrid->getIndicesWithPoints(list);

            for (int i = 0 ; i < list.size() ; i++)
            {
                size_t cellIndex = list.at(i);
                int clusterIndex = _outSegmentationGrid->valueAtIndex(cellIndex);
                if (!_validated[clusterIndex])
                {
                    _outSegmentationGrid->setValueAtIndex(cellIndex, -1);
                }
            }
        }

        QMapIterator<int, QString>itLab(_positionLabels);
        while (itLab.hasNext())
        {
            itLab.next();
            int cluster = itLab.key();
            QString label = itLab.value();

            if (!_keepValidatedOnly || _validated[cluster])
            {
                CT_StandardItemGroup* itemGrp = new CT_StandardItemGroup(_outGrpIdModelName.completeName(), res_out);
                CT_ItemAttributeList* attList = new CT_ItemAttributeList(_outIdItemModelName.completeName(), res_out);
                attList->addItemAttribute(new CT_StdItemAttributeT<qint32>(_outattClusterModelName.completeName(), CT_AbstractCategory::DATA_ID, res_out, cluster));
                attList->addItemAttribute(new CT_StdItemAttributeT<QString>(_outattLabelModelName.completeName(), CT_AbstractCategory::DATA_VALUE, res_out, label));

                itemGrp->addSingularItem(attList);
                grp->addGroup(itemGrp);
            }
        }

        // Fin de la partie interactive

        // create output segmented scenes
        _outSegmentationGrid->computeMinMax();
        grp->addSingularItem(_outSegmentationGrid);
    }

}

void ONF_StepModifyVoxelSegmentation::initManualMode()
{
    // create a new 3D document
    if(m_doc == nullptr)
        m_doc = getGuiContext()->documentManager()->new3DDocument();

    m_doc->removeAllItemDrawable();

    // set the action (a copy of the action is added at all graphics view, and the action passed in parameter is deleted)
    m_doc->setCurrentAction(new ONF_ActionModifyVoxelSegmentation(_scene, _pointGrid, _topologyGrid, _outSegmentationGrid, &_validated, &_positionLabels));

    QMessageBox::information(nullptr, tr("Mode manuel"), tr("Bienvenue dans le mode manuel de cette "
                                                         "étape de correction de segmentation."), QMessageBox::Ok);
}

void ONF_StepModifyVoxelSegmentation::useManualMode(bool quit)
{
    if(m_status == 0)
    {
        if(quit)
        {
        }
    }
    else if(m_status == 1)
    {
        if(!quit)
        {
            m_doc = nullptr;

            quitManualMode();
        }
    }
}
