/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepaddfakecounter.h"

ONF_StepAddFakeCounter::ONF_StepAddFakeCounter() : SuperClass()
{
}

QString ONF_StepAddFakeCounter::description() const
{
    return tr("Ajoute un compteur unitaire");
}

QString ONF_StepAddFakeCounter::detailledDescription() const
{
    return tr("Cette étape permet de créer une fausse boucle, avec un unique tour. Cela permet d'utiliser toutes les étapes dédiées à des boucles, même si on a qu'un unique fichier à traiter.");
}

QString ONF_StepAddFakeCounter::URL() const
{
    return SuperClass::URL();
}

CT_VirtualAbstractStep* ONF_StepAddFakeCounter::createNewInstance() const
{
    return new ONF_StepAddFakeCounter();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepAddFakeCounter::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Nom"), "", true);
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGrp, tr("Groupe"));
    manager.addItem(_inGrp, _inItem, tr("Item"));
    manager.addItemAttribute(_inItem, _inNameAtt, CT_AbstractCategory::DATA_VALUE, tr("Nom"));
}

void ONF_StepAddFakeCounter::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGrp, _outCounter, tr("Counter"));
}

void ONF_StepAddFakeCounter::compute()
{
    mCounterIndex = 1;

    for (CT_StandardItemGroup* group : _inGrp.iterateOutputs(_inResult))
    {
        for (const CT_AbstractSingularItemDrawable* item : group->singularItems(_inItem))
        {
            if (isStopped()) {return;}

            const CT_AbstractItemAttribute* attribute = item->itemAttribute(_inNameAtt);

            if (attribute != nullptr)
            {
                bool ok;
                QString filename = attribute->toString(item, &ok);

                if (ok && !filename.isEmpty())
                {
                    QSharedPointer<CT_Counter> counter = QSharedPointer<CT_Counter>(new CT_Counter(mCounterIndex));

                    CT_LoopCounter* loopCounter = new CT_LoopCounter(counter, nullptr);
                    loopCounter->setTurnName(filename);
                    group->addSingularItem(_outCounter, loopCounter);
                }
            }
        }
    }
}
