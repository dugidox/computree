#include "onf_stepselectclustersinlogs.h"

#include "actions/onf_actionselectclustersinlogs.h"

#include <QMessageBox>

ONF_StepSelectClustersInLogs::ONF_StepSelectClustersInLogs() : SuperClass()
{
    m_doc = nullptr;
    setManual(true);
}

QString ONF_StepSelectClustersInLogs::description() const
{
    return tr("Sélection de clusters dans des billons");
}

CT_VirtualAbstractStep* ONF_StepSelectClustersInLogs::createNewInstance() const
{
    return new ONF_StepSelectClustersInLogs();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepSelectClustersInLogs::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroupToCopy *resultInModel_R = createNewInResultModelForCopy(DEF_result_inR, tr("Result"), tr(""), false);
    resultInModel_R->setZeroOrMoreRootGroup();
    resultInModel_R->addGroupModel("", DEF_group_inLog);
    resultInModel_R->addGroupModel(DEF_group_inLog, DEF_group_inCluster);
    resultInModel_R->addItemModel(DEF_group_inCluster, DEF_item_inI, CT_AbstractSingularItemDrawable::staticGetType(), tr("Item"));
}

void ONF_StepSelectClustersInLogs::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    createNewOutResultModelToCopy(DEF_result_inR);
}

void ONF_StepSelectClustersInLogs::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    // No parameter dialog for this step
    //CT_StepConfigurableDialog *dialog = newStandardPostConfigurationDialog();

}

void ONF_StepSelectClustersInLogs::compute()
{
    m_doc = nullptr;
    m_status = 0;

    // ----------------------------------------------------------------------------
    // Get the group model corresponding to DEF_group_inG
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup *resultOut = outResultList.first();

    // ---------------------------
    // Gets OUT results and models
    m_itemDrawableToAdd.clear();

    QList<CT_AbstractItemGroup*> groupsToRemove;

    CT_ResultGroupIterator itG_log(resultOut, this, DEF_group_inLog);

    // create a list of itemdrawable to add in the document
    while(itG_log.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup *groupLog = itG_log.next();

        quint64 logID = groupLog->id();

        CT_GroupIterator itG_cluster(groupLog, this, DEF_group_inCluster);

        while(itG_cluster.hasNext() && !isStopped())
        {
            const CT_AbstractItemGroup *group_cluster = itG_cluster.next();

            CT_AbstractSingularItemDrawable *item = group_cluster->firstItemByINModelName(this, DEF_item_inI);

            if (item != nullptr)
            {
                m_itemDrawableToAdd.insert(item, (CT_AbstractItemGroup*)group_cluster);

                m_clusterToLog.insert(item, logID);
                m_logToCluster.insert(logID, item);
            }
        }
    }

    // request the manual mode
    requestManualMode();

    // remove item selected from the list (to not remove them from the out result)
    QListIterator<CT_AbstractItemDrawable*> it(m_validatedItems);

    while(it.hasNext() && !isStopped())
    {
        m_itemDrawableToAdd.remove(it.next());
    }

    groupsToRemove.append(m_itemDrawableToAdd.values());

    // we remove the parent group of all ItemDrawable that must be deleted from the out result
    // and all groups that don't contains a ItemDrawable researched
    QListIterator<CT_AbstractItemGroup*> itE(groupsToRemove);

    while(itE.hasNext())
    {
        CT_AbstractItemGroup *group = itE.next();
        recursiveRemoveGroup(group->parentGroup(), group);
    }

    m_status = 1;
    requestManualMode();
}

void ONF_StepSelectClustersInLogs::recursiveRemoveGroup(CT_AbstractItemGroup *parent, CT_AbstractItemGroup *group) const
{
    if(parent != nullptr)
    {
        parent->removeGroup(group);

        if(parent->isEmptyOfGroups())
        {
            recursiveRemoveGroup(parent->parentGroup(), parent);
        }
    }
    else
    {
        ((CT_ResultGroup*)group->result())->removeGroupSomethingInStructure(group);
    }
}

void ONF_StepSelectClustersInLogs::initManualMode()
{
    // create a new 3D document
    if(m_doc == nullptr)
        m_doc = getGuiContext()->documentManager()->new3DDocument();

    m_validatedItems.clear();
    m_doc->removeAllItemDrawable();

    // TODO add async with GuiManagerInterface
    QHashIterator<CT_AbstractItemDrawable*, CT_AbstractItemGroup*> it(m_itemDrawableToAdd);

    while(it.hasNext())
    {
        m_doc->addSingularItem(*it.next().key());
    }

    // set the action (a copy of the action is added at all graphics view, and the action passed in parameter is deleted)
    m_doc->setCurrentAction(new ONF_ActionSelectClustersInLogs(&m_clusterToLog, &m_logToCluster, &m_validatedItems));

    QMessageBox::information(nullptr, tr("Mode manuel"), tr("Bienvenue dans le mode manuel de cette "
                                                         "étape de filtrage. Veuillez sélectionner les "
                                                         "éléments dans la vue graphique puis valider en cliquant "
                                                         "sur le pouce en haut de la fenêtre principale. Les éléments "
                                                         "sélectionnés seront gardés dans le résultat de sortie."), QMessageBox::Ok);
}

void ONF_StepSelectClustersInLogs::useManualMode(bool quit)
{
    if(m_status == 0)
    {
        if(quit)
        {
        }
    }
    else if(m_status == 1)
    {
        if(!quit)
        {
            m_doc = nullptr;

            quitManualMode();
        }
    }
}
