/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepmergescenesbymodality.h"

//Inclusion of actions
#include "actions/onf_actionaggregateitems.h"

#include <QtConcurrent/QtConcurrentMap>

#include <QMessageBox>

ONF_StepMergeScenesByModality::ONF_StepMergeScenesByModality() : SuperClass()
{
    m_doc = nullptr;

    _modalitiesString = "Data,Intermediate,Noise";
    setManual(true);
}

QString ONF_StepMergeScenesByModality::description() const
{
    return tr("Merge scenes interactively");
}

QString ONF_StepMergeScenesByModality::detailledDescription() const
{
    return tr("TO DO");
}

QString ONF_StepMergeScenesByModality::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepMergeScenesByModality::createNewInstance() const
{
    return new ONF_StepMergeScenesByModality();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepMergeScenesByModality::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroupToCopy *resIn = createNewInResultModelForCopy(_inRPos, tr("Scenes"), tr(""), true);
    resIn->setZeroOrMoreRootGroup();
    resIn->addGroupModel("", _inRootgrp);
    resIn->addGroupModel(_inRootgrp, _inGrp);
    resIn->addItemModel(_inGrp, _inScene, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Scene"));
}

void ONF_StepMergeScenesByModality::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    _modalities = _modalitiesString.split(",", QString::SkipEmptyParts);

    _outSceneModelName.resize(_modalities.size());

    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(_inRPos);

    if(res != nullptr) {
        for (int i = 0 ; i < _modalities.size() ; i++)
        {
            res->addItemModel(_inRootgrp, _outSceneModelName[i], new CT_Scene(), _modalities.at(i));
        }
    }
}

void ONF_StepMergeScenesByModality::fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog)
{

    postInputConfigDialog->addString(tr("Modalities (comma separated)"), "", _modalitiesString, tr("List all wanted modalities, separated by commas)"));
}

void ONF_StepMergeScenesByModality::compute()
{

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resOut = outResultList.at(0);

    _inputScenes.clear();

    // Création de la liste des positions 2D
    CT_ResultGroupIterator rootGrpIt(resOut, this, _inRootgrp);
    while (rootGrpIt.hasNext() && !isStopped())
    {
        CT_StandardItemGroup* rootGroup = (CT_StandardItemGroup*) rootGrpIt.next();

        CT_GroupIterator grpIt(rootGroup, this, _inGrp);
        while (grpIt.hasNext())
        {
            CT_StandardItemGroup* group = (CT_StandardItemGroup*) grpIt.next();
            CT_AbstractItemDrawableWithPointCloud* sceneIn = (CT_AbstractItemDrawableWithPointCloud*)group->firstItemByINModelName(this, _inScene);
            if (sceneIn != nullptr)
            {
                _inputScenes.append(sceneIn);
            }
        }

        // Début de la partie interactive
        m_doc = nullptr;
        m_status = 0;
        requestManualMode();
        // Fin de la partie interactive

        for (int i = 0 ; i < _modalities.size() ; i++)
        {
            CT_PointCloudIndexVector *outCloudIndex = new CT_PointCloudIndexVector();
            outCloudIndex->setSortType(CT_PointCloudIndexVector::NotSorted);

            for (int j = 0 ; j < _inputScenes.size() ; j++)
            {
                if (_inputScenesModalities.size() > j && _inputScenesModalities.at(j) == _modalities.at(i))
                {
                    CT_AbstractItemDrawableWithPointCloud* sceneIn  = (CT_AbstractItemDrawableWithPointCloud*) _inputScenes.at(j);
                    const CT_AbstractPointCloudIndex *pointCloudIndex = sceneIn->pointCloudIndex();

                    CT_PointIterator itP(pointCloudIndex);
                    while(itP.hasNext())
                    {
                        size_t index = itP.next().currentGlobalIndex();
                        outCloudIndex->addIndex(index);
                    }
                }
            }

            if (outCloudIndex->size() > 0)
            {
                outCloudIndex->setSortType(CT_PointCloudIndexVector::SortedInAscendingOrder);
                CT_Scene* scene = new CT_Scene(_outSceneModelName[i].completeName(), resOut, PS_REPOSITORY->registerPointCloudIndex(outCloudIndex));
                scene->updateBoundingBox();
                rootGroup->addSingularItem(scene);
            } else {
                delete outCloudIndex;
            }
        }

        m_status = 1;
        requestManualMode();

    }

    setProgress(100);
}

void ONF_StepMergeScenesByModality::initManualMode()
{
    // create a new 3D document
    if(m_doc == nullptr)
        m_doc = getGuiContext()->documentManager()->new3DDocument();

    m_doc->removeAllItemDrawable();

    // set the action (a copy of the action is added at all graphics view, and the action passed in parameter is deleted)
    m_doc->setCurrentAction(new ONF_ActionAggregateItems(_modalities, _inputScenes, _inputScenesModalities));

    QMessageBox::information(nullptr, tr("Mode manuel"), tr("Bienvenue dans le mode manuel de cette "
                                                         "étape de filtrage."), QMessageBox::Ok);
}

void ONF_StepMergeScenesByModality::useManualMode(bool quit)
{
    if(m_status == 0)
    {
        if(quit)
        {
        }
    }
    else if(m_status == 1)
    {
        if(!quit)
        {
            m_doc = nullptr;

            quitManualMode();
        }
    }
}

