/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepselectbboxbyfilename.h"

#include "ct_view/ct_asciifilechoicebutton.h"
#include "ct_view/ct_combobox.h"

#include <QFile>
#include <QTextStream>

ONF_StepSelectBBoxByFileName::ONF_StepSelectBBoxByFileName() : SuperClass()
{
}

QString ONF_StepSelectBBoxByFileName::description() const
{
    return tr("Charger l'emprise correspondant à un nom de fichier");
}

QString ONF_StepSelectBBoxByFileName::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString ONF_StepSelectBBoxByFileName::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepSelectBBoxByFileName::createNewInstance() const
{
    return new ONF_StepSelectBBoxByFileName();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepSelectBBoxByFileName::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroup *res_inRes = createNewInResultModelForCopy(_inResBBox, tr("Emprises disponibles"),
                                                                     tr("Résultat contenant toutes les emprises disponibles.\n"
                                                                        "Chaque groupe contient :\n"
                                                                        "- Une Emprise (item ayant une boite englobante : en général Forme 2D)\n"
                                                                        "- Un item avec un attribut conteant le nom du fichier correspondant (Header)\n"));
    res_inRes->setZeroOrMoreRootGroup();
    res_inRes->addGroupModel("", _inGrpBBox, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    res_inRes->addItemModel(_inGrpBBox, _inHeader, CT_AbstractSingularItemDrawable::staticGetType(), tr("Item Nom de fichier"));
    res_inRes->addItemAttributeModel(_inHeader, _inATT, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::STRING, tr("Nom de fichier"));
    res_inRes->addItemModel(_inGrpBBox, _inBbox, CT_AbstractAreaShape2D::staticGetType(), tr("Emprise correspondante"));

    CT_InResultModelGroupToCopy *res_inResCpy = createNewInResultModelForCopy(_inResHeader, tr("Fichier dont l'emprise doit être chargée"),
                                                                              tr("Résultat contenant le nom du fichier pour lequel il faut charger l'emprise (Header)"),
                                                                              true);
    res_inResCpy->setZeroOrMoreRootGroup();
    res_inResCpy->addGroupModel("", _inGrpHeader, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    res_inResCpy->addItemModel(_inGrpHeader, _inHeader, CT_FileHeader::staticGetType(), tr("Entête de fichier"));
}

void ONF_StepSelectBBoxByFileName::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroupToCopyPossibilities *resCpy_res = createNewOutResultModelToCopy(_inResHeader);

    if(resCpy_res != nullptr)
        resCpy_res->addItemModel(_inGrpHeader, _outBBox, new CT_Box2D(), tr("Emprise"));
}

void ONF_StepSelectBBoxByFileName::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    //
}

void ONF_StepSelectBBoxByFileName::compute()
{

    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* inRes = inResultList.at(0);

    QMap<QString, CT_AbstractAreaShape2D*> corresp;

    // Création de la liste des id recherchés
    CT_ResultGroupIterator itGrpIn(inRes, this, _inGrpBBox);
    while (itGrpIn.hasNext() && !isStopped())
    {
        CT_StandardItemGroup* group = (CT_StandardItemGroup*) itGrpIn.next();

        if (group != nullptr)
        {
            CT_AbstractSingularItemDrawable* fileHeader  = (CT_AbstractSingularItemDrawable*) group->firstItemByINModelName(this, _inHeader);
            CT_AbstractAreaShape2D* bbox  = (CT_AbstractAreaShape2D*) group->firstItemByINModelName(this, _inBbox);

            if (fileHeader != nullptr && bbox != nullptr)
            {
                CT_AbstractItemAttribute *attribute = fileHeader->firstItemAttributeByINModelName(inRes, this, _inATT);
                if (attribute != nullptr)
                {
                    bool ok;
                    QString filename = attribute->toString(fileHeader, &ok);

                    if (ok && !filename.isEmpty())
                    {
                        corresp.insert(filename.toLower(), bbox);
                    }
                }
            }
        }
    }

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* outRes = outResultList.at(0);

    // Création de la liste des id recherchés
    CT_ResultGroupIterator itGrpOut(outRes, this, _inGrpHeader);
    while (itGrpOut.hasNext() && !isStopped())
    {
        CT_StandardItemGroup* group = (CT_StandardItemGroup*) itGrpOut.next();

        if (group != nullptr)
        {
            CT_FileHeader* fileHeader  = (CT_FileHeader*) group->firstItemByINModelName(this, _inHeader);

            if (fileHeader != nullptr)
            {
                QString filename = fileHeader->getFileInfo().fileName();
                CT_AbstractAreaShape2D* bbox = corresp.value(filename.toLower(), nullptr);

                if (!filename.isEmpty() && bbox != nullptr)
                {
                    Eigen::Vector3d min, max;
                    bbox->boundingBox(min, max);
                    Eigen::Vector2d min2d, max2d;

                    min2d(0) = min(0);
                    min2d(1) = min(1);
                    max2d(0) = max(0);
                    max2d(1) = max(1);

                    CT_Box2DData *data = new CT_Box2DData(min2d, max2d);
                    CT_Box2D *box = new CT_Box2D(_outBBox.completeName(), outRes, data);
                    group->addSingularItem(box);
                }
            }
        }
    }

}
