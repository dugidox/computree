/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepfilterclustersbysize.h"

#include "ct_log/ct_logmanager.h"

ONF_StepFilterClustersBySize::ONF_StepFilterClustersBySize() : SuperClass()
{
    _minSize = 4;
}

QString ONF_StepFilterClustersBySize::description() const
{
    return tr("Filtrer les Clusters par nombre de points");
}

QString ONF_StepFilterClustersBySize::detailledDescription() const
{
    return tr("Cette étape filtre des clusters (tout item contenant des points).<br>"
              "Tout cluster ayant un nombre de points strictement inférieur au <b>nombre de points minimum</b> spécifié est éliminé.");
}

CT_VirtualAbstractStep* ONF_StepFilterClustersBySize::createNewInstance() const
{
    // crée une copie de cette étape
    return new ONF_StepFilterClustersBySize();
}

//////////////////// PROTECTED //////////////////

void ONF_StepFilterClustersBySize::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Clusters"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inCluster, tr("Points"));
}

void ONF_StepFilterClustersBySize::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addInt(tr("Nombre de points minimum dans un cluster"), "pts",1 , 1000000, _minSize);
}

void ONF_StepFilterClustersBySize::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
}

void ONF_StepFilterClustersBySize::compute()
{
    size_t totalNumberOfClusters = 0;
    size_t totalNumberOfClustersRemoved = 0;

    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        if (isStopped()) {return;}

        const CT_AbstractItemDrawableWithPointCloud* item = group->singularItem(_inCluster);

        ++totalNumberOfClusters;

        if (item->pointCloudIndexSize() < size_t(_minSize))
        {
            group->removeFromParent(true);
            ++totalNumberOfClustersRemoved;
        }
    }

    PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("Nombre de clusters avant filtrage : %1")).arg(totalNumberOfClusters));
    PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("Nombre de clusters éliminés : %1")).arg(totalNumberOfClustersRemoved));
    PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("Nombre de clusters restants : %1")).arg(totalNumberOfClusters-totalNumberOfClustersRemoved));

    setProgress(100);
}
