#ifndef ONF_STEPCREATECOLORCOMPOSITE_H
#define ONF_STEPCREATECOLORCOMPOSITE_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_image2d.h"
#include "ct_itemdrawable/ct_colorcomposite.h"

class ONF_StepCreateColorComposite: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepCreateColorComposite();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:


    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractImage2D>                                         _inRed;
    CT_HandleInSingularItem<CT_AbstractImage2D>                                         _inGreen;
    CT_HandleInSingularItem<CT_AbstractImage2D>                                         _inBlue;

    CT_HandleInResultGroup<0,1>                                        _inResultDEM;
    CT_HandleInStdZeroOrMoreGroup                                      _inZeroOrMoreRootGroupDEM;
    CT_HandleInStdGroup<>                                           _inGroupDEM;
    CT_HandleInSingularItem<CT_Image2D<float> >                     _inDEM;


    CT_HandleOutSingularItem<CT_ColorComposite>                                         _outColorcomposite;

};

#endif // ONF_STEPCREATECOLORCOMPOSITE_H
