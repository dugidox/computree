/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_actionmodifydemoptions.h"
#include "ui_onf_actionmodifydemoptions.h"
#include "ct_global/ct_context.h"

#include <QColorDialog>

ONF_ActionModifyDEMOptions::ONF_ActionModifyDEMOptions(const ONF_ActionModifyDEM *action) :
    CT_GAbstractActionOptions(action),
    ui(new Ui::ONF_ActionModifyDEMOptions)
{
    ui->setupUi(this);

    ui->tb_up->setToolTip(tr("Augmenter la hauteur des points sélectionnés [FLECHE DROITE]"));
    ui->tb_up_ceil->setToolTip(tr("Augmenter la hauteur des points sélectionnés, sans dépasser la hauteur du plus haut [FLECHE HAUT]"));
    ui->tb_down->setToolTip(tr("Diminuer la hauteur des points sélectionnés [FLECHE GAUCHE]"));
    ui->tb_down_floor->setToolTip(tr("Diminuer la hauteur des points sélectionnés, sans descendre sous la hauteur du plus bas [FLECHE BAS]"));
    ui->tb_raz->setToolTip(tr("Réinitialiser la hauteur des points sélectionnés [RETOUR ARRIERE]"));
    ui->dsb_step->setToolTip(tr("Incrément: [+] pour augmenter, [-] pour diminuer"));
    ui->tb_smooth->setToolTip(tr("Interpoler les points sélectionnés, à partir des points les entourant"));
}

ONF_ActionModifyDEMOptions::~ONF_ActionModifyDEMOptions()
{
    delete ui;
}

void ONF_ActionModifyDEMOptions::setMultiSelect(bool multi)
{
    if (multi)
    {
        ui->toolButtonSelectMulti->setChecked(true);
    } else {
        ui->toolButtonSelectOne->setChecked(true);
    }
}

void ONF_ActionModifyDEMOptions::changeStepValue(bool increase)
{
    if (increase)
    {
        ui->dsb_step->setValue(ui->dsb_step->value() + ui->dsb_step->singleStep());
    } else {
        ui->dsb_step->setValue(ui->dsb_step->value() - ui->dsb_step->singleStep());
    }
}

float ONF_ActionModifyDEMOptions::getStepValue()
{
    return ui->dsb_step->value();
}

int ONF_ActionModifyDEMOptions::getBrushSize()
{
    return ui->sb_brushSize->value();
}

bool ONF_ActionModifyDEMOptions::isImageSelected()
{
    if (!ui->cb_image->isVisible()) {return false;}
    return ui->cb_image->isChecked();
}

void ONF_ActionModifyDEMOptions::setImageAvailable(bool available)
{
    ui->cb_image->setVisible(available);
}

void ONF_ActionModifyDEMOptions::on_buttonGroupMode_buttonReleased(int id)
{
    Q_UNUSED(id)

    (dynamic_cast<ONF_ActionModifyDEM*>(action()))->setSelectionMode(selectionMode());
}

void ONF_ActionModifyDEMOptions::on_buttonGroupSelection_buttonReleased(int id)
{
    Q_UNUSED(id)

    (dynamic_cast<ONF_ActionModifyDEM*>(action()))->setSelectionMode(selectionMode());
}



GraphicsViewInterface::SelectionMode ONF_ActionModifyDEMOptions::selectionMode() const
{   
    int mode = GraphicsViewInterface::NONE;

    if (ui->toolButtonSelectOne->isChecked())
    {
        if (ui->toolButtonReplaceMode->isChecked()) {
            mode = GraphicsViewInterface::SELECT_ONE_POINT;
        } else if (ui->toolButtonAddMode->isChecked()) {
            mode = GraphicsViewInterface::ADD_ONE_POINT;
        } else {
            mode = GraphicsViewInterface::REMOVE_ONE_POINT;
        }
    } else if (ui->toolButtonSelectMulti->isChecked()) {
        if (ui->toolButtonReplaceMode->isChecked()) {
            mode = GraphicsViewInterface::SELECT_POINTS;
        } else if (ui->toolButtonAddMode->isChecked()) {
            mode = GraphicsViewInterface::ADD_POINTS;
        } else {
            mode = GraphicsViewInterface::REMOVE_POINTS;
        }
    } else {
        return (GraphicsViewInterface::SelectionMode)mode;
    }

    return (GraphicsViewInterface::SelectionMode)mode;
}

void ONF_ActionModifyDEMOptions::setSelectionMode(GraphicsViewInterface::SelectionMode mode)
{
    if(mode != GraphicsViewInterface::NONE)
    {
        int m = mode;

        while(m > GraphicsViewInterface::REMOVE_ONE_POINT)
            m -= GraphicsViewInterface::REMOVE_ONE;       

        if (mode == GraphicsViewInterface::SELECT_POINTS) {
            ui->toolButtonSelectMulti->setChecked(true);
            ui->toolButtonReplaceMode->setChecked(true);
        } else if (mode == GraphicsViewInterface::ADD_POINTS) {
            ui->toolButtonSelectMulti->setChecked(true);
            ui->toolButtonAddMode->setChecked(true);
        } else if (mode == GraphicsViewInterface::REMOVE_POINTS) {
            ui->toolButtonSelectMulti->setChecked(true);
            ui->toolButtonRemoveMode->setChecked(true);
        } else if (mode == GraphicsViewInterface::SELECT_ONE_POINT) {
            ui->toolButtonSelectOne->setChecked(true);
            ui->toolButtonReplaceMode->setChecked(true);
        } else if (mode == GraphicsViewInterface::ADD_ONE_POINT) {
            ui->toolButtonSelectOne->setChecked(true);
            ui->toolButtonAddMode->setChecked(true);
        } else if (mode == GraphicsViewInterface::REMOVE_ONE_POINT) {
            ui->toolButtonSelectOne->setChecked(true);
            ui->toolButtonRemoveMode->setChecked(true);
        }
    }
}

void ONF_ActionModifyDEMOptions::on_tb_up_clicked()
{
    emit levelPoints(true, false);
}

void ONF_ActionModifyDEMOptions::on_tb_down_clicked()
{
    emit levelPoints(false, false);
}

void ONF_ActionModifyDEMOptions::on_tb_up_ceil_clicked()
{
    emit levelPoints(true, true);
}

void ONF_ActionModifyDEMOptions::on_tb_down_floor_clicked()
{
    emit levelPoints(false, true);
}

void ONF_ActionModifyDEMOptions::on_tb_raz_clicked()
{
    emit razPoints();
}


void ONF_ActionModifyDEMOptions::on_tb_smooth_clicked()
{
    emit smooth();
}

void ONF_ActionModifyDEMOptions::on_cb_image_toggled(bool checked)
{
    emit imageStateChanged();
}
