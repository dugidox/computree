#include "onf_nullifytraversedcellvisitor.h"
#include "Eigen/Core"

ONF_NullifyTraveserdCellVisitor::ONF_NullifyTraveserdCellVisitor(CT_Grid3D_Sparse<int> *grid)
{
  _grid = grid;
  _NA = _grid->NA();
}

void ONF_NullifyTraveserdCellVisitor::visit(const size_t &index, const CT_Beam *beam)
{
    Q_UNUSED(beam);

    if (_grid->valueAtIndex(index) > 0)
    {
        _grid->setValueAtIndex(index, _NA);
    }
}
