#include "onf_actionselectclustersinlogsoptions.h"
#include "ui_onf_actionselectclustersinlogsoptions.h"
#include "ct_global/ct_context.h"

ONF_ActionSelectClustersInLogsOptions::ONF_ActionSelectClustersInLogsOptions(const ONF_ActionSelectClustersInLogs *action) :
    CT_GAbstractActionOptions(action),
    ui(new Ui::ONF_ActionSelectClustersInLogsOptions)
{
    ui->setupUi(this);

    ui->rb_cluster->setToolTip(tr("Sélectionner des clusters [E]"));
    ui->rb_cluster->setToolTip(tr("Sélectionner des billlons [E]"));
    ui->pb_validate->setToolTip(tr("Ajouter la sélection [A]"));
    ui->pb_unvalidate->setToolTip(tr("Retirer la sélection [Z]"));

    connect(action, SIGNAL(selectionModeChanged(GraphicsViewInterface::SelectionMode)), this, SLOT(setSelectionMode(GraphicsViewInterface::SelectionMode)));
}

ONF_ActionSelectClustersInLogsOptions::~ONF_ActionSelectClustersInLogsOptions()
{
    delete ui;
}

GraphicsViewInterface::SelectionMode ONF_ActionSelectClustersInLogsOptions::selectionMode() const
{   
    int mode = GraphicsViewInterface::NONE;

    if (ui->toolButtonSelectOne->isChecked())
    {
        if (ui->toolButtonReplaceMode->isChecked()) {
            mode = GraphicsViewInterface::SELECT_ONE;
        } else if (ui->toolButtonAddMode->isChecked()) {
            mode = GraphicsViewInterface::ADD_ONE;
        } else {
            mode = GraphicsViewInterface::REMOVE_ONE;
        }
    } else if (ui->toolButtonSelectMulti->isChecked() || ui->toolButtonSelectMultiByPolygon->isChecked()) {
        if (ui->toolButtonReplaceMode->isChecked()) {
            mode = GraphicsViewInterface::SELECT;
        } else if (ui->toolButtonAddMode->isChecked()) {
            mode = GraphicsViewInterface::ADD;
        } else {
            mode = GraphicsViewInterface::REMOVE;
        }
    } else {
        return (GraphicsViewInterface::SelectionMode)mode;
    }

    return (GraphicsViewInterface::SelectionMode)mode;
}

ONF_ActionSelectClustersInLogs::SelectionTool ONF_ActionSelectClustersInLogsOptions::selectionTool() const
{
    if(ui->toolButtonSelectMulti->isChecked())
        return ONF_ActionSelectClustersInLogs::Rectangle;
    else if(ui->toolButtonSelectMultiByPolygon->isChecked())
        return ONF_ActionSelectClustersInLogs::Polygon;

    return ONF_ActionSelectClustersInLogs::Point;
}

bool ONF_ActionSelectClustersInLogsOptions::clusterSelection() const
{
    return ui->rb_cluster->isChecked();
}

void ONF_ActionSelectClustersInLogsOptions::toggleClusterSelection() const
{
    if (ui->rb_cluster->isChecked()) {ui->rb_log->setChecked(true);}
    else {ui->rb_cluster->setChecked(true);}
}

//ONF_ActionSelectClustersInLogs::SelectionDrawMode ONF_ActionSelectClustersInLogsOptions::drawMode() const
//{
//    return (ONF_ActionSelectClustersInLogs::SelectionDrawMode)ui->buttonGroupOptimization->checkedId();
//}

void ONF_ActionSelectClustersInLogsOptions::on_buttonGroupType_buttonReleased(int id)
{
    Q_UNUSED(id)

    (dynamic_cast<ONF_ActionSelectClustersInLogs*>(action()))->setSelectionMode(selectionMode());
    (dynamic_cast<ONF_ActionSelectClustersInLogs*>(action()))->setSelectionTool(selectionTool());
}

void ONF_ActionSelectClustersInLogsOptions::on_buttonGroupMode_buttonReleased(int id)
{
    Q_UNUSED(id)

    (dynamic_cast<ONF_ActionSelectClustersInLogs*>(action()))->setSelectionMode(selectionMode());
    (dynamic_cast<ONF_ActionSelectClustersInLogs*>(action()))->setSelectionTool(selectionTool());
}

void ONF_ActionSelectClustersInLogsOptions::on_toolButtonToggleSelection_clicked()
{
    (dynamic_cast<ONF_ActionSelectClustersInLogs*>(action()))->toggleSelection();
}

//void ONF_ActionSelectClustersInLogsOptions::on_buttonGroupOptimization_buttonReleased(int id)
//{
//    Q_UNUSED(id)

//    (dynamic_cast<ONF_ActionSelectClustersInLogs*>(action()))->setDrawMode(drawMode());
//}

void ONF_ActionSelectClustersInLogsOptions::on_buttonGroupSelection_buttonReleased(int id)
{
    Q_UNUSED(id)

    (dynamic_cast<ONF_ActionSelectClustersInLogs*>(action()))->setSelectionMode(selectionMode());
    (dynamic_cast<ONF_ActionSelectClustersInLogs*>(action()))->setSelectionTool(selectionTool());
}

void ONF_ActionSelectClustersInLogsOptions::setSelectionMode(GraphicsViewInterface::SelectionMode mode)
{
    if(mode != GraphicsViewInterface::NONE)
    {
        GraphicsViewInterface::SelectionMode m = (dynamic_cast<ONF_ActionSelectClustersInLogs*>(action()))->selectionModeToBasic(mode);

        if ((m == GraphicsViewInterface::SELECT) || (m == GraphicsViewInterface::SELECT_ONE))
            ui->toolButtonReplaceMode->setChecked(true);
        else if ((m == GraphicsViewInterface::ADD) || (m == GraphicsViewInterface::ADD_ONE))
            ui->toolButtonAddMode->setChecked(true);
        else if ((m == GraphicsViewInterface::REMOVE) || (m == GraphicsViewInterface::REMOVE_ONE))
            ui->toolButtonRemoveMode->setChecked(true);
    }
}

void ONF_ActionSelectClustersInLogsOptions::setSelectionTool(ONF_ActionSelectClustersInLogs::SelectionTool tool)
{
    if(tool == ONF_ActionSelectClustersInLogs::Rectangle)
        ui->toolButtonSelectMulti->setChecked(true);
    else if(tool == ONF_ActionSelectClustersInLogs::Polygon)
        ui->toolButtonSelectMultiByPolygon->setChecked(true);
    else
        ui->toolButtonSelectOne->setChecked(true);
}

void ONF_ActionSelectClustersInLogsOptions::on_pb_validate_clicked()
{
    emit validate();
}

void ONF_ActionSelectClustersInLogsOptions::on_pb_unvalidate_clicked()
{
    emit unvalidate();
}

