#include "onf_stepkeepintersectingitems.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/inModel/tools/ct_instdresultmodelpossibility.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_global/ct_context.h"

#include "ct_model/tools/ct_modelsearchhelper.h"

#include <QDebug>
#include <Eigen/Core>

#define DEF_inResult "inResult"
#define DEF_inGroup  "inGroup"
#define DEF_inItem   "inItem"

#define DEF_inResult_ref "refinResult"
#define DEF_inItem_ref   "refinItem"

ONF_StepKeepIntersectingItems::ONF_StepKeepIntersectingItems(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

ONF_StepKeepIntersectingItems::~ONF_StepKeepIntersectingItems()
{
}

QString ONF_StepKeepIntersectingItems::getStepDescription() const
{
    return tr("Conserve les items intersectant les surfaces de référence");
}

QString ONF_StepKeepIntersectingItems::getStepDetailledDescription() const
{
    return tr("");
}

CT_VirtualAbstractStep* ONF_StepKeepIntersectingItems::createNewInstance(CT_StepInitializeData &dataInit)
{
    // cree une copie de cette etape
    return new ONF_StepKeepIntersectingItems(dataInit);
}

//////////////////// PROTECTED //////////////////

void ONF_StepKeepIntersectingItems::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_inResult, tr("Items to filter"), "", true);
    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_inGroup, CT_AbstractItemGroup::staticGetType());
    resultModel->addItemModel(DEF_inGroup, DEF_inItem, CT_AbstractSingularItemDrawable::staticGetType(), tr("Item to filter"));

    CT_InResultModelGroup *resultModelREF = createNewInResultModel(DEF_inResult_ref, tr("Filtering items"), "", true);
    resultModelREF->setZeroOrMoreRootGroup();
    resultModelREF->addItemModel("", DEF_inItem_ref, CT_AbstractSingularItemDrawable::staticGetType(), tr("Filtering items"));

}

// Redefine in children steps to complete ConfigurationDialog
void ONF_StepKeepIntersectingItems::createPostConfigurationDialog()
{
}

// Redefine in children steps to complete out Models
void ONF_StepKeepIntersectingItems::createOutResultModelListProtected()
{
    createNewOutResultModelToCopy(DEF_inResult);
}

// Redefine in children steps to complete compute method
void ONF_StepKeepIntersectingItems::compute()
{
    CT_ResultGroup *inResult = getInputResults().at(1);

    QList<CT_AbstractSingularItemDrawable*> refList;
    CT_ResultItemIterator it(inResult, this, DEF_inItem_ref);
    while (it.hasNext() && (!isStopped()))
    {
        CT_AbstractSingularItemDrawable *item = (CT_AbstractSingularItemDrawable*) it.next();

        if (item != NULL)
        {
            refList.append(item);
        }
    }


    CT_ResultGroup *outResult = getOutResultList().at(0);

    QList<CT_AbstractItemGroup*> groupsToBeRemoved;

    CT_ResultGroupIterator it2(outResult, this, DEF_inGroup);
    while (it2.hasNext() && (!isStopped()))
    {
        CT_AbstractItemGroup *group = (CT_AbstractItemGroup*) it2.next();

        CT_AbstractSingularItemDrawable* item = (CT_AbstractSingularItemDrawable*) group->firstItemByINModelName(this, DEF_inItem);
        if (item != NULL)
        {
            Eigen::Vector3d min, max;
            item->getBoundingBox(min, max);

            bool intersected = false;

            for (int i = 0 ; i < refList.size() && !intersected; i++)
            {
                CT_AbstractSingularItemDrawable* refItem = refList.at(i);
                Eigen::Vector3d minRef, maxRef;
                refItem->getBoundingBox(minRef, maxRef);

                if (!(maxRef(0) < min(0) || minRef(0) > max(0) ||
                      maxRef(1) < min(1) || minRef(1) > max(1)))
                {
                    intersected = true;
                }
            }

            if (!intersected) {groupsToBeRemoved.append(group);}
        }
    }

    while (!groupsToBeRemoved.isEmpty())
    {
        CT_AbstractItemGroup *group = groupsToBeRemoved.takeLast();
        recursiveRemoveGroupIfEmpty(group->parentGroup(), group);
    }

    setProgress( 100 );
}

void ONF_StepKeepIntersectingItems::recursiveRemoveGroupIfEmpty(CT_AbstractItemGroup *parent, CT_AbstractItemGroup *group) const
{
    if(parent != NULL)
    {
        parent->removeGroup(group);

        if(parent->isEmpty())
            recursiveRemoveGroupIfEmpty(parent->parentGroup(), parent);
    }
    else
    {
        ((CT_ResultGroup*)group->result())->removeGroupSomethingInStructure(group);
    }
}
