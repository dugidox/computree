#include "onf_stepcomputerelativeintensityattribute.h"

// Utilise le depot
#include "ct_global/ct_context.h"

// Utilise les attributs optionnels
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_iterator/ct_groupiterator.h"
#include "ctliblas/itemdrawable/las/ct_stdlaspointsattributescontainer.h"

#include <QDebug>

// Alias for indexing in models
#define DEFin_resSc "resSc"
#define DEFin_scGrp "scGrp"
#define DEFin_sc "sc"
#define DEFin_attLAS "attLAS"

// Constructor : initialization of parameters
ONF_StepComputeRelativeIntensityAttribute::ONF_StepComputeRelativeIntensityAttribute(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString ONF_StepComputeRelativeIntensityAttribute::getStepDescription() const
{
    return tr("Calculer l'intensité relative");
}

// Step copy method
CT_VirtualAbstractStep* ONF_StepComputeRelativeIntensityAttribute::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ONF_StepComputeRelativeIntensityAttribute(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ONF_StepComputeRelativeIntensityAttribute::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy * resultSc = createNewInResultModelForCopy(DEFin_resSc, tr("Scene(s)"));
    resultSc->setZeroOrMoreRootGroup();
    resultSc->addGroupModel("", DEFin_scGrp, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultSc->addItemModel(DEFin_scGrp, DEFin_sc, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Scene(s)"));
    resultSc->addItemModel(DEFin_scGrp, DEFin_attLAS, CT_StdLASPointsAttributesContainer::staticGetType(), tr("Attributs LAS"), tr("Attribut LAS"));
}

// Creation and affiliation of OUT models
void ONF_StepComputeRelativeIntensityAttribute::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEFin_resSc);

    if(res != NULL)
    {
        res->addItemModel(DEFin_scGrp, _outRelIntensityAttributeModelName, new CT_PointsAttributesScalarTemplated<double>(), tr("Relative intensity"));
    }

}

// Semi-automatic creation of step parameters DialogBox
void ONF_StepComputeRelativeIntensityAttribute::createPreConfigurationDialog()
{
    //CT_StepConfigurableDialog *configDialog = newStandardPreConfigurationDialog();
}

void ONF_StepComputeRelativeIntensityAttribute::compute()
{   
    CT_ResultGroup* resOut = getOutResultList().first();

    CT_ResultGroupIterator itSc(resOut, this, DEFin_scGrp);
    while (itSc.hasNext())
    {
        CT_StandardItemGroup* grp = (CT_StandardItemGroup*) itSc.next();

        if (grp != NULL)
        {
            CT_AbstractItemDrawableWithPointCloud* scene = (CT_AbstractItemDrawableWithPointCloud*) grp->firstItemByINModelName(this, DEFin_sc);
            const CT_StdLASPointsAttributesContainer* attributeLAS = (CT_StdLASPointsAttributesContainer*)grp->firstItemByINModelName(this, DEFin_attLAS);

            if (scene != NULL && attributeLAS != NULL && !isStopped())
            {
                const CT_AbstractPointCloudIndex *pointCloudIndex = scene->getPointCloudIndex();

                // Retrieve attributes
                QHashIterator<CT_LasDefine::LASPointAttributesType, CT_AbstractPointAttributesScalar *> it(attributeLAS->lasPointsAttributes());
                if (!it.hasNext()) {return;}

                CT_AbstractPointAttributesScalar *firstAttribute = it.next().value();
                if (firstAttribute == NULL) {return;}

                const CT_AbstractPointCloudIndex* pointCloudIndexLAS = firstAttribute->getPointCloudIndex();
                if (pointCloudIndexLAS == NULL) {return;}

                CT_AbstractPointAttributesScalar* attributeGPS          = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::GPS_Time);
                CT_AbstractPointAttributesScalar* attributeIntensity     = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Intensity);

                if (attributeIntensity == NULL || attributeGPS == NULL) {return;}


                QList<CandidatePoint*> candidatePoints;

                size_t pointIndex = 0;
                CT_PointIterator itP(pointCloudIndex);
                while (itP.hasNext() && !isStopped())
                {
                    size_t globalIndex = itP.next().currentGlobalIndex();
                    size_t localIndex = pointCloudIndexLAS->indexOf(globalIndex);

                    double gpsTime = 0;   // Récupération du temps GPS pour le point
                    double intensity = 0; // Récupération de l'intensité pour le point
                    if (localIndex < pointCloudIndexLAS->size())
                    {
                        gpsTime = attributeGPS->dValueAt(localIndex);
                        intensity = attributeIntensity->dValueAt(localIndex);
                    }

                    candidatePoints.append(new CandidatePoint(pointIndex++, gpsTime, intensity));
                }

                std::sort(candidatePoints.begin(), candidatePoints.end(), sortByGPSTime);

                double intensitySum = 0;
                double ibegin = 0;

                if (candidatePoints.size() > 0) {intensitySum = candidatePoints.first()->_intensity;}
                for (int i = 1 ; i < candidatePoints.size() ; i++)
                {
                    CandidatePoint* cp0 = candidatePoints.at(i-1);
                    CandidatePoint* cp1 = candidatePoints.at(i);

                    if (cp1->_gpsTime != cp0->_gpsTime)
                    {
                        for (int j = ibegin ; j < i ; j++)
                        {
                            candidatePoints.at(j)->_sumIntensity = intensitySum;
                        }
                        intensitySum = cp1->_intensity;
                        ibegin = i;
                    } else {
                        intensitySum += cp1->_intensity;
                    }
                }

                // On declare un tableau d'attributs double que l'on va remplir avec les coordonnées correspondant a l'axe demandé
                CT_StandardCloudStdVectorT<double> *attribute = NULL;
                attribute = new CT_StandardCloudStdVectorT<double>(pointCloudIndex->size());

                double minAttribute = std::numeric_limits<double>::max();
                double maxAttribute = -std::numeric_limits<double>::max();

                // On applique la translation a tous les points du nuage
                for (int i = 0 ; i < candidatePoints.size() ; i++)
                {
                    CandidatePoint* cp = candidatePoints.at(i);
                    double intensityRel = 0;
                    if (cp->_sumIntensity > 0) {intensityRel = cp->_intensity / cp->_sumIntensity;}

                    attribute->replaceT(cp->_pointIndex, intensityRel);

                    if (intensityRel < minAttribute) {minAttribute = intensityRel;}
                    if (intensityRel > maxAttribute) {maxAttribute = intensityRel;}
                }

                if (candidatePoints.size() > 0)
                {
                    CT_PointsAttributesScalarTemplated<double>*  itemOut_attribute  = new CT_PointsAttributesScalarTemplated<double>(_outRelIntensityAttributeModelName.completeName(),
                                                                                                                                     resOut,
                                                                                                                                     scene->getPointCloudIndexRegistered(),
                                                                                                                                     attribute,
                                                                                                                                     minAttribute,
                                                                                                                                     maxAttribute);
                    grp->addItemDrawable(itemOut_attribute);
                }

                qDeleteAll(candidatePoints);
            }
        }
    }

}
