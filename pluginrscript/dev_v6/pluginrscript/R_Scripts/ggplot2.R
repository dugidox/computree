# COMPUTREE_HEADER_BEGIN

# name = Plot a ggplot2 graph
# description = This script allow to plot a ggplot graphic
# description = col and label variables are optionnal
# author = Alexandre Piboule
# organization = Office National des Forets
# version = 1.0

# param = gtype ; type = list      ; beforelab = Plot type ; afterlab = ; default = Points ; Points ; Lines ; Labels ; Histogram ;

# intable = inData ; desc = data to plot
# field = x ; type = numeric ; desc = x axis ;
# field = y ; type = numeric ; desc = y axis ; 
# field = col ; type = factor ; desc = coulor ; opt = true
# field = label ; type = character ; desc = labels ; opt = true

# COMPUTREE_HEADER_END

#setwd("../R_Data")

result <- tryCatch({
  
  ######################
  # Script begins here #
  ######################
  
  param  = read.table("../R_Data/param.txt", sep="\t", dec=".", header=T, colClasses=c("gtype"="character"))
  
  # logical, integer, numeric, complex, character, factor
  inData  = read.table("../R_Data/inData.txt", sep="\t", dec=".", header=T, colClasses=c("x"="numeric", "y"="numeric"))
          
  if (!is.null(inData$col)) {inData$col = as.factor(inData$col)}
  if (!is.null(inData$label)) {inData$label = as.character(inData$label)}
  
  
  require(ggplot2)

  p = ggplot(inData) + aes(x=x)
  
  if (param[1,1] == "Histogram") 
  {
    p = p + aes(x=y)
  } else 
  {
    p = p + aes(y=y)
  }
  
  if (!is.null(inData$col)) {p = p + aes(col=col)}
  if (!is.null(inData$label)) {p = p + aes(label=label)}
  
  if (param[1,1] == "Points") {p = p + geom_point()}
  if (param[1,1] == "Lines") {p = p + geom_line()}
  if (param[1,1] == "Labels") {p = p + geom_text()}
  if (param[1,1] == "Histogram") {p = p + geom_histogram()}
  
  print(p)

  ######################
  # Script ends here   #
  ######################
  
}, error = function(err) {
  write(paste(err), "error.err")
})
