#ifndef OUTTABLE_H
#define OUTTABLE_H

#include <QObject>
#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_itemattributelist.h"
#include "outfield.h"

class OutTable : public QObject
{
    Q_OBJECT

public:

	OutTable ()
	{

	}

    QString                 _name;
    QString                 _desc;
    bool                    _cpy;


    CT_HandleOutResultGroup                         _outResult;
    CT_HandleOutStdGroup                            _outGrp;
    CT_HandleOutSingularItem<CT_ItemAttributeList>  _outItem;

};

#endif // OUTTABLE_H
