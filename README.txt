Prerequisites:
- Have installed the subversion package and qt-sdk

The directory containing the files in this kit will be the root directory of Computree.

1) Start a terminal (CTRL + ALT + T).

2) Go to the root directory of Computree.

3) Type the following command: sh recuperer_depots.sh

Enter your user login and then your svn password.

In the case of a svn error related to a PROXY:
You must set the following file: etc/subversion/servers in your user folder (section [global] at the end of the file).
The .subversion folder is a hidden file.
Then restart command 3).


4) Start Qt-creator.

5) Open the file all.pro (keep the checked boxes by default).

6) Go to the Project tab, disable the shadow build for release and debug versions.


NB: If you are working with other plugins, you can create directories and make proper checkout in the Computree root directory
Then, modify the file all.pro, to add the paths to the .pro files of the plugins after the others (with a "\" at the end of each line except the last one).
